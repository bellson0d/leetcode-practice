/*
 * @lc app=leetcode.cn id=1018 lang=javascript
 *
 * [1018] 可被 5 整除的二进制前缀
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {boolean[]}
 */
var prefixesDivBy5 = function (nums) {
  let res = [],
    pre = 0;
  for (let i = 0; i < nums.length; i++) {
    pre = ((pre << 1) + nums[i]) % 5;
    res.push(pre === 0);
  }
  return res;
};
// @lc code=end
// Math 余数处理 https://leetcode-cn.com/problems/binary-prefix-divisible-by-5/solution/tong-yu-ding-li-ge-wei-bigint3jie-fa-by-ii0yd/
// console.log(prefixesDivBy5([0, 1, 1, 1, 1, 1]));
// console.log(
//   prefixesDivBy5([
//     1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0,
//     0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1,
//     0, 0, 1, 1, 0, 0, 1, 1, 1,
//   ])
// );

// [false,false,true,true,true,true,true,true,true,true,true,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,true,true,false,false,false,true,true,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,true,true,true,false,false,false]
