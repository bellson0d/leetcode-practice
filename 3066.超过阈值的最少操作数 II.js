class MinHeap {
    constructor() {
        this.heap = [];
    }
    
    push(val) {
        this.heap.push(val);
        this.bubbleUp(this.heap.length - 1);
    }
    
    pop() {
        if (this.heap.length < 2) return this.heap.pop() || null;
        const result = this.heap[0];
        this.heap[0] = this.heap.pop();
        this.bubbleDown(0);
        return result;
    }
    
    peek() {
        return this.heap[0];
    }
    
    size() {
        return this.heap.length;
    }
    
    bubbleUp(index) {
        while (index > 0) {
            const parentIndex = (index - 1) >> 1;
            if (this.heap[parentIndex] > this.heap[index]) {
                [this.heap[parentIndex], this.heap[index]] = [this.heap[index], this.heap[parentIndex]];
                index = parentIndex;
            } else break;
        }
    }
    
    bubbleDown(index) {
        const size = this.heap.length;
        while (true) {
            let smallest = index;
            const left = (index << 1) + 1;
            const right = left + 1;
            
            if (left < size && this.heap[left] < this.heap[smallest]) smallest = left;
            if (right < size && this.heap[right] < this.heap[smallest]) smallest = right;
            
            if (smallest === index) break;
            
            [this.heap[index], this.heap[smallest]] = [this.heap[smallest], this.heap[index]];
            index = smallest;
        }
    }
}

/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number}
 */
function minOperations(nums, k) {
    const heap = new MinHeap();
    let operations = 0;
    let hasSmallNum = false;
    
    // 添加所有数字到堆中，同时检查是否有小于k的数
    for (const num of nums) {
        heap.push(num);
        if (num < k) hasSmallNum = true;
    }
    
    // 如果没有小于k的数，直接返回0
    if (!hasSmallNum) return 0;
    
    while (heap.size() >= 2) {
        const x = heap.pop();
        const y = heap.pop();
        operations++;
        
        // 直接计算新值并放回堆中，除非新值已经大于等于k且堆中剩余的所有数都大于等于k
        const newVal = Math.min(x, y) * 2 + Math.max(x, y);
        if (newVal < k || heap.peek() < k) {
            heap.push(newVal);
        }
        
        // 如果堆为空或堆顶元素大于等于k且新值大于等于k，则可以停止
        if ((!heap.size() || heap.peek() >= k) && newVal >= k) {
            break;
        }
    }
    
    return operations;
}

const log = console.log;

log(minOperations([999999999,999999999,999999999], 1000000000)); // 2
log(minOperations([42,46], 42)); // 0
log(minOperations([1000000000,999999999,1000000000,999999999,1000000000,999999999], 1000000000)); // 2
log(minOperations([2,11,10,1,3], 10)); // 2
log(minOperations([1,1,2,4,9], 20)); // 4
// @lc code=end
