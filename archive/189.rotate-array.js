/*
 * @lc app=leetcode id=189 lang=javascript
 *
 * [189] Rotate Array
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @param {number} k
 * @return {void} Do not return anything, modify nums in-place instead.
 */
var rotate = function (nums, k) {
  const len = nums.length;
  const n = k % len;
  if (len < 2 || n === 0) return nums;
  nums.unshift(...nums.splice(len - n));
  //   return nums;
};
// @lc code=end
// module.exports = rotate;
