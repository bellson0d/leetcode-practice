const targetFun = require('./98.validate-binary-search-tree');
const { getTree } = require('../utils/treeNode');
const IS_ARRAY_RESULT = false; // 输出是否为数组
const MULTI_ARGS = false; // 输入参数是否为多个

const testStr = [
  [getTree([]), true],
  [getTree([1]), true],
  [getTree([2, 1, 3]), true],
  [getTree([5, 1, 4, null, null, 3, 6]), false],
  [getTree([5, null, 6, null, null, 3, 7]), false],
  [getTree([5, null, 7, null, null, 6, 8]), true],
  [getTree([10, 5, 15, null, null, 6, 20]), false],
  [getTree([1, 1]), false],
];

const time1 = new Date().getTime();
console.log(time1);

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];

  let result;
  if (MULTI_ARGS) {
    result = targetFun(...input);
  } else {
    result = targetFun(input);
  }

  if (
    IS_ARRAY_RESULT ? result.join('|') !== output.join('|') : result !== output
  ) {
    console.log('Number ' + count + ' Case Wrong !!!');
    console.log('Expected output: ', output);
    console.log('Output', result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
