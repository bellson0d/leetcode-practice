const targetFun = require('./41.first-missing-positive');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  [[1, 2, 0], 3],
  [[3, 4, -1, 1], 2],
  [[7, 8, 9, 11, 12], 1],
];

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input);
  if (result !== output) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
