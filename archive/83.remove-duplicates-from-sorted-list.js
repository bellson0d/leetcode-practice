/*
 * @lc app=leetcode id=83 lang=javascript
 *
 * [83] Remove Duplicates from Sorted List
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
var deleteDuplicates = function (head) {
  if (!head) return head;
  let currentNode = head,
    nextNode = head.next;

  while (nextNode) {
    if (currentNode.val === nextNode.val) {
      nextNode = nextNode.next;
      currentNode.next = null;
    } else {
      currentNode.next = nextNode;
      currentNode = nextNode;
      nextNode = nextNode.next;
    }
  }

  return head;
};
// @lc code=end
