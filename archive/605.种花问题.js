/*
 * @lc app=leetcode.cn id=605 lang=javascript
 *
 * [605] 种花问题
 */

// @lc code=start
/**
 * @param {number[]} flowerbed
 * @param {number} n
 * @return {boolean}
 */
var helper = (arr) => {
  let count = 0,
    i = 0,
    temp = [];
  while (i < arr.length) {
    if (arr[i] === 0 && arr[i - 1] !== 1 && arr[i + 1] !== 1) {
      count++;
    } else {
      if (count) temp.push(count);
      count = 0;
    }
    i++;
  }
  if (count) temp.push(count);

  let sum = 0;
  for (const range of temp) {
    sum += Math.ceil(range / 2);
  }

  return sum;
};
var canPlaceFlowers = function (flowerbed, n) {
  if (!n) return true;
  return helper(flowerbed) >= n;
};
// @lc code=end

// console.log(canPlaceFlowers([1, 0, 0, 0, 1], 1));
// console.log(canPlaceFlowers([1, 0, 0, 0, 1], 2));
// console.log(canPlaceFlowers([0, 0, 0, 0, 0], 1));
// console.log(canPlaceFlowers([0, 0, 0, 0, 0], 2));
// console.log(canPlaceFlowers([0, 0, 0, 0, 0], 3));
// console.log(canPlaceFlowers([], 1));
// console.log(canPlaceFlowers([0], 1));
// console.log(canPlaceFlowers([0], 0));
// console.log(canPlaceFlowers([1, 0, 1, 0, 0], 1));
// console.log(canPlaceFlowers([1, 0, 1, 0, 0], 2));
