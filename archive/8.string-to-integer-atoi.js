/*
 * @lc app=leetcode id=8 lang=javascript
 *
 * [8] String to Integer (atoi)
 */

// @lc code=start
/**
 * @param {string} str
 * @return {number}
 */
const INT_MAX = 2147483647;
const INT_MIN = -2147483648;

var myAtoi = function (str) {
  const tmpArr = str.trim().replace(/\s+/g, ' ').split(' ');
  let firstItem = tmpArr[0];
  const preSign =
    firstItem[0] === '-' || firstItem[0] === '+' ? firstItem[0] : '';
  if (preSign) {
    firstItem = firstItem.slice(1);
  }
  const hasWord = firstItem.match(/\D/);
  if (hasWord) {
    firstItem = firstItem.slice(0, hasWord.index);
  }

  const tmpNum = Number(preSign + firstItem);
  if (isNaN(tmpNum)) return 0;

  if (tmpNum > INT_MAX) return INT_MAX;
  if (tmpNum < INT_MIN) return INT_MIN;

  return tmpNum;
};

// myAtoi('  -0012a42');

module.exports = myAtoi;
// @lc code=end
