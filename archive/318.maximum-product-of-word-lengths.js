/*
 * @lc app=leetcode id=318 lang=javascript
 *
 * [318] Maximum Product of Word Lengths
 */

// @lc code=start
/**
 * @param {string[]} words
 * @return {number}
 */
var diffLen = (s1, s2) => {
  for (const s of s1) {
    if (s2.includes(s)) return 0;
  }
  return s1.length * s2.length;
};

var maxProduct = function (words) {
  const len = words.length;
  words.sort((a, b) => a.length - b.length);
  let p1 = len - 2,
    p2 = len - 1,
    max = 0;

  while (p2 > 0) {
    let res = diffLen(words[p1], words[p2]);
    if (res > max) max = res;
    if (words[p1].length <= max / words[p2].length || p1 === 0) {
      p2--;
      p1 = p2 - 1;
    } else {
      p1--;
    }
  }
  return max;
};

// console.log(maxProduct(['abcw', 'baz', 'foo', 'bar', 'xtfn', 'abcdef']));
// console.log(maxProduct(['a', 'ab', 'abc', 'd', 'cd', 'bcd', 'abcd']));
// console.log(maxProduct(['a', 'aa', 'aaa', 'aaaa']));
// console.log(maxProduct(['a', 'ab', 'abc', 'd', 'cd', 'bcd', 'abcd']));
