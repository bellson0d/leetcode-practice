/*
 * @lc app=leetcode id=114 lang=javascript
 *
 * [114] Flatten Binary Tree to Linked List
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {void} Do not return anything, modify root in-place instead.
 */
var flatten = function (root) {
  if (!root || (!root.left && !root.right)) return root;

  let tmpArr = [root],
    valArr = [],
    unSort = [];

  while (tmpArr.length > 0) {
    const currentNode = tmpArr[0];
    if (!currentNode.visited) {
      valArr.unshift(currentNode.val);
      unSort.push(currentNode);
      currentNode.visited = true;
    }
    const { left, right } = currentNode;

    if (left && !left.visited) {
      tmpArr.unshift(left);
    } else if (right && !right.visited) {
      tmpArr.unshift(right);
    } else {
      tmpArr.shift();
    }
  }
  //   console.log(valArr);
  for (let i = 0; i < unSort.length; i++) {
    unSort[i].left = null;
    unSort[i].right = unSort[i + 1] ? unSort[i + 1] : null;
  }

  return unSort[0];
};
// @lc code=end

// const { getTree } = require('./utils/treeNode');
// console.log(flatten(getTree([])));
// console.log(flatten(getTree([1])));
// console.log(flatten(getTree([1, 2, 3])));
// console.log(flatten(getTree([1, 2, 5, 3, 4, null, 6])));
// console.log(flatten(getTree([1, 2, null, 3, 4, null, null, 5])));
// console.log(flatten(getTree([1, null, 2, null, null, 4, 3])));
