/*
 * @lc app=leetcode.cn id=942 lang=javascript
 *
 * [942] 增减字符串匹配
 */

// @lc code=start
/**
 * @param {string} s
 * @return {number[]}
 */
var diStringMatch = function (str) {
  const arr = [];
  let max = str.length;
  let min = 0;

  for (let i = 0; i < str.length; i++) {
    if (str[i] === 'I') {
      arr.push(min);
      min++;
    } else {
      arr.push(max);
      max--;
    }
  }
  arr.push(min);

  return arr;
};
// @lc code=end
// console.log(diStringMatch('IDID'));
// console.log(diStringMatch('III'));
// console.log(diStringMatch('DDI'));
