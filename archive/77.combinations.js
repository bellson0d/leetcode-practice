/*
 * @lc app=leetcode id=77 lang=javascript
 *
 * [77] Combinations
 */

// @lc code=start
/**
 * @param {number} n
 * @param {number} k
 * @return {number[][]}
 */
const range = (n) =>
  Array(n)
    .fill(1)
    .map((v, i) => v + i);

var combine = function (n, k) {
  const helpFn = (arr1, arr2) => {
    let tmpArr = [];
    for (let i = 0; i < arr2.length; i++) {
      const ele = arr2[i];
      const newArr = arr2.slice(i + 1);
      if (arr1.length < k - 1) {
        if (newArr.length > 0) {
          tmpArr = tmpArr.concat(helpFn(arr1.concat(ele), newArr));
        }
      } else {
        tmpArr.push(arr1.concat(ele));
      }
    }
    return tmpArr;
  };

  return helpFn([], range(n));
};

// @lc code=end
console.log(combine(4, 2));
// console.log(combine(5, 3));
