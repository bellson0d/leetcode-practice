/*
 * @lc app=leetcode id=383 lang=javascript
 *
 * [383] Ransom Note
 */

// @lc code=start
/**
 * @param {string} ransomNote
 * @param {string} magazine
 * @return {boolean}
 */
var canConstruct = function (ransomNote, magazine) {
  let obj2 = {};
  for (const r of magazine) {
    if (!obj2[r]) {
      obj2[r] = 1;
    } else {
      obj2[r]++;
    }
  }

  for (const r of ransomNote) {
    if (obj2[r]) {
      obj2[r]--;
    } else {
      return false;
    }
  }
  return true;
};
// @lc code=end

// console.log(canConstruct('a', 'b'));
// console.log(canConstruct('aa', 'ab'));
// console.log(canConstruct('aa', 'aab'));
