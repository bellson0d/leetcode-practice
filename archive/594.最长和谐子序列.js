/*
 * @lc app=leetcode.cn id=594 lang=javascript
 *
 * [594] 最长和谐子序列
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var findLHS = function (nums) {
  nums.sort((a, b) => a - b);
  const len = nums.length;
  let max = 0;

  for (let i = 0; i < len - 1; i++) {
    let step = 0,
      count = 1,
      flag = false;
    for (let j = i + 1; j < len; j++) {
      const diff = Math.abs(nums[i] - nums[j]);
      if (diff === 0) {
        step++;
        count++;
      } else if (diff === 1) {
        flag = true;
        count++;
      } else {
        break;
      }
    }
    max = Math.max(max, flag ? count : 0);
    i += step;
  }
  return max;
};
// @lc code=end
// console.log(findLHS([1, 3, 2, 2, 5, 2, 3, 7]));
// console.log(findLHS([1, 2, 3, 4]));
// console.log(findLHS([1, 1, 1, 1]));
// console.log(
//   findLHS([
//     1, 1, 1, 1, 5, 4, 2, 1, 2, 3, 4, 5, 6, 7, 1, 1, 2, 3, 4, 5, 8, 3, 2, 123, 3,
//   ])
// );
