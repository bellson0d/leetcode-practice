/*
 * @lc app=leetcode.cn id=498 lang=javascript
 *
 * [498] 对角线遍历
 */

// @lc code=start
/**
 * @param {number[][]} mat
 * @return {number[]}
 */
var findDiagonalOrder = function (mat) {
  const x = mat[0].length;
  const y = mat.length;

  const res = [];
  let tmpArr = [],
    flag = true;
  for (let i = 0; i < y; i++) {
    let x1 = 0,
      y1 = i;
    while (x1 < x && y1 >= 0) {
      tmpArr.push(mat[y1][x1]);
      x1++;
      y1--;
    }
    if (flag) {
      res.push(...tmpArr);
    } else {
      res.push(...tmpArr.reverse());
    }
    flag = !flag;
    tmpArr = [];
  }

  for (let i = 1; i < x; i++) {
    let x1 = y - 1,
      y1 = i;
    while (x1 >= 0 && y1 < x) {
      tmpArr.push(mat[x1][y1]);
      x1--;
      y1++;
    }

    if (flag) {
      res.push(...tmpArr);
    } else {
      res.push(...tmpArr.reverse());
    }
    flag = !flag;
    tmpArr = [];
  }

  return res;
};
// @lc code=end

// console.log(
//   findDiagonalOrder([
//     [1, 2, 3],
//     [4, 5, 6],
//     [7, 8, 9],
//   ])
// );
// console.log(
//   findDiagonalOrder([
//     [1, 2],
//     [3, 4],
//   ])
// );
// console.log(findDiagonalOrder([[1, 2]]));
// console.log(findDiagonalOrder([[1], [2]]));
// console.log(
//   findDiagonalOrder([
//     [1, 2],
//     [1, 2],
//     [1, 2],
//     [1, 2],
//   ])
// );
