/*
 * @lc app=leetcode id=303 lang=javascript
 *
 * [303] Range Sum Query - Immutable
 */

// @lc code=start
/**
 * @param {number[]} nums
 */
var NumArray = function (nums) {
  this.arr = nums;
  return this;
};

/**
 * @param {number} left
 * @param {number} right
 * @return {number}
 */
NumArray.prototype.sumRange = function (left, right) {
  return this.arr.slice(left, right + 1).reduce((a, b) => a + b, 0);
};

/**
 * Your NumArray object will be instantiated and called as such:
 * var obj = new NumArray(nums)
 * var param_1 = obj.sumRange(left,right)
 */
// @lc code=end

// const numArray = new NumArray([-2, 0, 3, -5, 2, -1]);
// console.log(numArray.sumRange(0, 2)); // return (-2) + 0 + 3 = 1
// console.log(numArray.sumRange(2, 5)); // return 3 + (-5) + 2 + (-1) = -1
// console.log(numArray.sumRange(0, 5)); // return (-2) + 0 + 3 + (-5) + 2 + (-1) = -3
