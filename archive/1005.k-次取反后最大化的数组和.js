/*
 * @lc app=leetcode.cn id=1005 lang=javascript
 *
 * [1005] K 次取反后最大化的数组和
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number}
 */
var largestSumAfterKNegations = function (nums, k) {
  nums.sort((a, b) => a - b);

  for (let i = 0; i < nums.length; i++) {
    const ele = nums[i];
    if (ele < 0) {
      if (k > 0) {
        k--;
        nums[i] = -ele;
      } else {
        return nums.reduce((a, b) => a + b);
      }
    }
  }

  const sum = nums.reduce((a, b) => a + b);
  if (k % 2 === 0) return sum;

  return sum - Math.min(...nums) * 2;
};
// @lc code=end
