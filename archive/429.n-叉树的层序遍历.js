/*
 * @lc app=leetcode.cn id=429 lang=javascript
 *
 * [429] N 叉树的层序遍历
 */

// @lc code=start
/**
 * // Definition for a Node.
 * function Node(val,children) {
 *    this.val = val;
 *    this.children = children;
 * };
 */

/**
 * @param {Node|null} root
 * @return {number[][]}
 */
var levelOrder = function (root) {
  if (!root) return [];
  let arr = [[root]],
    res = [];
  while (arr.length) {
    const nodes = arr.pop();
    let children = [],
      child = [];

    for (const node of nodes) {
      child.push(node.val);
      children.push(...node.children);
    }
    res.push(child);
    if (children.length) arr.push(children);
  }
  return res;
};
// @lc code=end
