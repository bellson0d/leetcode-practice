/*
 * @lc app=leetcode.cn id=997 lang=javascript
 *
 * [997] 找到小镇的法官
 */

// @lc code=start
/**
 * @param {number} n
 * @param {number[][]} trust
 * @return {number}
 */
var findJudge = function (n, trust) {
  if (n === 1 && !trust.length) return 1;
  let users = new Set(),
    obj = {};

  for (let i = 0; i < trust.length; i++) {
    const [user, v2] = trust[i];
    users.add(user);

    if (obj[v2]) {
      obj[v2][0]++;
    } else {
      obj[v2] = [1, v2];
    }
  }

  if (users.size !== n - 1) return -1;
  const arr = Object.values(obj).filter((v) => v[0] === n - 1);

  for (const [count, user] of arr) {
    if (!users.has(user)) return user;
  }

  return -1;
};
// @lc code=end

// console.log(
//   findJudge(4, [
//     [1, 3],
//     [1, 4],
//     [2, 3],
//     [2, 4],
//     [4, 3],
//   ])
// ); // 3
