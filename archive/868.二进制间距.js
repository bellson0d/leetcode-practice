/*
 * @lc app=leetcode.cn id=868 lang=javascript
 *
 * [868] 二进制间距
 */

// @lc code=start
/**
 * @param {number} n
 * @return {number}
 */
var binaryGap = function (n) {
  const str = n.toString(2);
  let pre = null,
    min = 0;

  for (let i = 0; i < str.length; i++) {
    if (str[i] === '1') {
      if (pre !== null) {
        min = Math.max(i - pre, min);
      }
      pre = i;
    }
  }

  return min;
};
// @lc code=end

// console.log(binaryGap(22));
// console.log(binaryGap(5));
// console.log(binaryGap(6));
// console.log(binaryGap(8));
// console.log(binaryGap(1));
