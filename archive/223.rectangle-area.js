/*
 * @lc app=leetcode id=223 lang=javascript
 *
 * [223] Rectangle Area
 */

// @lc code=start
/**
 * @param {number} ax1
 * @param {number} ay1
 * @param {number} ax2
 * @param {number} ay2
 * @param {number} bx1
 * @param {number} by1
 * @param {number} bx2
 * @param {number} by2
 * @return {number}
 */
var computeArea = function (ax1, ay1, ax2, ay2, bx1, by1, bx2, by2) {
    if (ay1 >= by2 || ay2 <= by1 || ax2 <= bx1 || ax1 >= bx2)
        return (ax2 - ax1) * (ay2 - ay1) + (bx2 - bx1) * (by2 - by1);
    if (ax1 <= bx1 && ay1 <= by1 && ax2 >= bx2 && ay2 >= by2)
        return (ax2 - ax1) * (ay2 - ay1);
    if (ax1 >= bx1 && ay1 >= by1 && ax2 <= bx2 && ay2 <= by2)
        return (bx2 - bx1) * (by2 - by1);
    const [, cx1, cx2] = [ax1, ax2, bx1, bx2].sort((a, b) => a - b);
    const [, cy1, cy2] = [ay1, ay2, by1, by2].sort((a, b) => a - b);
    return (
        (ax2 - ax1) * (ay2 - ay1) +
        (bx2 - bx1) * (by2 - by1) -
        (cx2 - cx1) * (cy2 - cy1)
    );
};
// @lc code=end

// console.log(computeArea(-3, 0, 3, 4, 0, -1, 9, 2)); // 45
// console.log(computeArea(-2, -2, 2, 2, -2, -2, 2, 2)); // 16
// console.log(computeArea(-2, 0, -1, 2, 0, 0, 2, 2)); // 6
// console.log(computeArea(-2, 0, 2, 2, -1, 0, 1, 1)); // 8
// console.log(computeArea(-2, 0, 2, 2, -3, 0, 3, 3)); // 18
