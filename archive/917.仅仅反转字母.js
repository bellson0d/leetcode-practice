/*
 * @lc app=leetcode.cn id=917 lang=javascript
 *
 * [917] 仅仅反转字母
 */

// @lc code=start
/**
 * @param {string} s
 * @return {string}
 */
var reverseOnlyLetters = function (s) {
  let res = [],
    nonChar = [];
  for (let i = 0; i < s.length; i++) {
    const ele = s[i];
    const charCode = ele.charCodeAt();
    if (
      (charCode <= 90 && charCode >= 65) ||
      (charCode <= 122 && charCode >= 97)
    ) {
      res.unshift(ele);
    } else {
      nonChar.push(i);
    }
  }

  let idx = 0,
    str = '';
  while (idx < s.length) {
    if (nonChar[0] === idx) {
      str += s[nonChar.shift()];
    } else {
      str += res.shift();
    }
    idx++;
  }
  return str;
};
// @lc code=end

// console.log(reverseOnlyLetters('ab-cd'));
// console.log(reverseOnlyLetters('a-bC-dEf-ghIj'));
// console.log(reverseOnlyLetters('Test1ng-Leet=code-Q!'));
