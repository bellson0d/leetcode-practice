/*
 * @lc app=leetcode id=168 lang=javascript
 *
 * [168] Excel Sheet Column Title
 */

// @lc code=start
/**
 * @param {number} n
 * @return {string}
 */

var convertToTitle = function (n) {
  let pre = '',
    times,
    left;
  while (n > 0) {
    if (n < 27) return String.fromCodePoint(64 + n) + pre;

    times = ~~(n / 26);
    left = n % 26;
    if (left === 0) {
      n = times - 1;
      pre = 'Z' + pre;
    } else {
      n = times;
      pre = String.fromCodePoint(64 + left) + pre;
    }
  }
  return pre;
};
// @lc code=end
// module.exports = convertToTitle;
