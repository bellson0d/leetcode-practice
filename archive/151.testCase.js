const targetFun = require('./151.reverse-words-in-a-string');
const IS_ARRAY_RESULT = false; // 输出是否为数组
const MULTI_ARGS = false; // 输入参数是否为多个

const testStr = [
  ['the sky is blue', 'blue is sky the'],
  ['  hello world  ', 'world hello'],
  ['a good   example', 'example good a'],
  ['  Bob    Loves  Alice   ', 'Alice Loves Bob'],
  ['Alice does not even like bob', 'bob like even not does Alice'],
];

const time1 = new Date().getTime();
console.log(time1);

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  let result;
  if (MULTI_ARGS) {
    result = targetFun(...input);
  } else {
    result = targetFun(input);
  }

  if (
    IS_ARRAY_RESULT ? result.join('|') !== output.join('|') : result !== output
  ) {
    console.log('Number ' + count + ' Case Wrong !!!');
    console.log('Input: ', input);
    console.log('Expected output: ', output);
    console.log('Output', result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
