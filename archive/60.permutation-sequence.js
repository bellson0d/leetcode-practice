/*
 * @lc app=leetcode id=60 lang=javascript
 *
 * [60] Permutation Sequence
 */

// @lc code=start
/**
 * @param {number} n
 * @param {number} k
 * @return {string}
 */

const factorial = {
  2: 2,
  3: 6,
  4: 24,
  5: 120,
  6: 720,
  7: 5040,
  8: 40320,
  9: 362880,
};

var getPermutation = function (n, k) {
  if (n === 1) return '1';
  if (n === 2) {
    if (k == 1) return '12';
    if (k == 2) return '21';
  }

  const nums = Array(n)
    .fill(1)
    .map((v, i) => 1 + i + '');
  let count = 0;

  const helpFn = (arr, num, str) => {
    if (count === k) return str;
    const len1 = arr.length;
    if (len1 === 2) {
      count++;
      if (num === 0) return str + arr[0] + arr[1];
      return str + arr[1] + arr[0];
    }

    const divide = factorial[len1 - 1];
    const idx = ~~(num / divide);

    num = num % divide;
    str += arr[idx];
    const newArr = arr.slice(0, idx).concat(arr.slice(idx + 1));

    return helpFn(newArr, num, str);
  };

  return helpFn(nums, k - 1, '');
};

// @lc code=end
// module.exports = getPermutation;
// console.log(getPermutation(4, 6));
