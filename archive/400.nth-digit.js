/*
 * @lc app=leetcode id=400 lang=javascript
 *
 * [400] Nth Digit
 */

// @lc code=start
/**
 * @param {number} n
 * @return {number}
 */
var baseMap = [
  [9, 9, 0],
  [99, 189, 1],
  [999, 2889, 2],
  [9999, 38889, 3],
  [99999, 488889, 4],
  [999999, 5888889, 5],
  [9999999, 68888889, 6],
  [99999999, 788888889, 7],
  [999999999, 8888888889, 8],
  [9999999999, 98888888889, 9],
];

var findNthDigit = function (n) {
  if (n < 10) return n;

  const idx = baseMap.find((v) => v[1] > n)[2] - 1;
  const [num, count] = baseMap[idx];
  const len = String(num + 1).length;
  let tmp = n - count;
  const remainder = tmp % len;
  const times = Math.floor(tmp / len);
  if (remainder) {
    return ~~String(num + times + 1).split('')[remainder - 1];
  } else {
    return ~~String(num + times)
      .split('')
      .reverse()[0];
  }
};
// // @lc code=end
// console.log(findNthDigit(1)); // 1
// console.log(findNthDigit(3)); // 3
// console.log(findNthDigit(11)); // 0
// console.log(findNthDigit(12)); // 1
// console.log(findNthDigit(13)); // 1
// console.log(findNthDigit(15)); // 2
// console.log(findNthDigit(10000000)); // 7
// console.log(findNthDigit(1000000000)); // 1
