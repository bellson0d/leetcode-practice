/*
 * @lc app=leetcode id=27 lang=javascript
 *
 * [27] Remove Element
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @param {number} val
 * @return {number}
 */
var removeElement = function (nums, val) {
  const len = nums.length;

  let i = 0,
    count = 0;

  while (count + i < len) {
    if (nums[count] === val) {
      while (nums[count + i] === val && count + i < len) i++;
      const tmp = nums[count];
      nums[count] = nums[count + i];
      nums[count + i] = tmp;
    }
    count++;
  }

  return len - i;
  // return nums;
};

// module.exports = removeElement;
// console.log(removeElement([3, 2, 2, 3], 3));
// console.log(removeElement([0, 1, 2, 2, 3, 0, 4, 2], 2));
// console.log(removeElement([0, 4, 4, 0, 4, 4, 4, 0, 2], 4));
// @lc code=end
