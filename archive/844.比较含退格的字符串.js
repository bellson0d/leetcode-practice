/*
 * @lc app=leetcode.cn id=844 lang=javascript
 *
 * [844] 比较含退格的字符串
 */

// @lc code=start
/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */
var helper = (str) => {
  const pre = [];
  for (const s of str) {
    if (s === '#') {
      pre.pop();
    } else {
      pre.push(s);
    }
  }
  return pre.join('');
};
var backspaceCompare = function (s, t) {
  return helper(s) === helper(t);
};
// @lc code=end

// console.log(backspaceCompare('ab#c', 'ad#c'));
// console.log(backspaceCompare('ab##', 'c#d#'));
// console.log(backspaceCompare('a##c', '#a#c'));
// console.log(backspaceCompare('a#c', 'b'));
