/*
 * @lc app=leetcode id=342 lang=javascript
 *
 * [342] Power of Four
 */

// @lc code=start
/**
 * @param {number} n
 * @return {boolean}
 */
var isPowerOfFour = function (n) {
  let res = 1,
    count = 1;
  while (res < n) {
    res = 4 ** count;
    count++;
  }
  return res === n;
};
// @lc code=end
