/*
 * @lc app=leetcode id=476 lang=javascript
 *
 * [476] Number Complement
 */

// @lc code=start
/**
 * @param {number} num
 * @return {number}
 */
var findComplement = function (num) {
  let res = '';
  for (const s of num.toString(2)) {
    res += s === '0' ? '1' : '0';
  }
  return parseInt(res, 2);
};
// @lc code=end

// console.log(findComplement(5));
