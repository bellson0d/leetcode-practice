/*
 * @lc app=leetcode id=349 lang=javascript
 *
 * [349] Intersection of Two Arrays
 */

// @lc code=start
/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @return {number[]}
 */
var intersection = function (nums1, nums2) {
  let longArr = nums1,
    shortArr = nums2;
  if (nums2.length > nums1.length) {
    longArr = nums2;
    shortArr = nums1;
  }
  let res = new Set();
  const set = new Set(shortArr);
  for (const n of longArr) {
    if (set.has(n)) res.add(n);
  }
  return Array.from(res);
};
// @lc code=end
// console.log(intersection([1, 2, 2, 1], [2, 2]));
