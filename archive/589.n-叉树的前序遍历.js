/*
 * @lc app=leetcode.cn id=589 lang=javascript
 *
 * [589] N 叉树的前序遍历
 */

// @lc code=start
/**
 * // Definition for a Node.
 * function Node(val, children) {
 *    this.val = val;
 *    this.children = children;
 * };
 */

/**
 * @param {Node|null} root
 * @return {number[]}
 */
var preorder = function (root) {
  if (!root) return [];
  let arr = [root],
    res = [];
  while (arr.length) {
    const node = arr.shift();
    res.push(node.val);

    if (node.children) arr.unshift(...node.children);
  }
  return res;
};
// @lc code=end
