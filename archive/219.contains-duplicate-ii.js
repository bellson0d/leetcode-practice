/*
 * @lc app=leetcode id=219 lang=javascript
 *
 * [219] Contains Duplicate II
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @param {number} k
 * @return {boolean}
 */
var containsNearbyDuplicate = function (nums, k) {
    let obj = {};
    for (let i = 0; i < nums.length; i++) {
        const ele = nums[i];
        if (obj.hasOwnProperty(ele) && i - obj[ele] <= k) {
            return true;
        }
        obj[ele] = i;
    }
    return false;
};
// @lc code=end
