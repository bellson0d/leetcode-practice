/*
 * @lc app=leetcode id=33 lang=javascript
 *
 * [33] Search in Rotated Sorted Array
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
function findIdx(arr, val, addN) {
  const len = arr.length;
  if (len < 2) {
    return arr[0] !== val ? -1 : addN;
  }

  const middleIdx = ~~(len / 2);
  const middle = arr[middleIdx];

  if (middle === val) return addN + middleIdx;
  if (middle > val) {
    return findIdx(arr.slice(0, middleIdx), val, addN);
  } else {
    return findIdx(arr.slice(middleIdx + 1), val, addN + middleIdx + 1);
  }
}

var search = function (nums, target) {
  let idx = -1;
  const len = nums.length;

  for (let i = 0; i < len - 1; i++) {
    const ele = nums[i];
    const ele2 = nums[i + 1];

    if (ele > ele2) {
      idx = i + 1;
      break;
    }
  }

  nums = idx === -1 ? nums : nums.slice(idx).concat(nums.slice(0, idx));

  let result = findIdx(nums, target, 0);

  if (idx === -1 || result === -1) return result;

  const sum = result + idx;
  if (sum > len - 1) {
    return sum - len;
  } else {
    return sum;
  }
};

// search([4, 5, 6, 7, 0, 1, 2], 3);
// module.exports = search;

// @lc code=end
