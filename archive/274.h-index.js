/*
 * @lc app=leetcode id=274 lang=javascript
 *
 * [274] H-Index
 */

// @lc code=start
/**
 * @param {number[]} citations
 * @return {number}
 */
var hIndex = function (citations) {
    let h = 0;
    citations.sort((a, b) => b - a);

    for (let i = 0; i < citations.length; i++) {
        const ele = citations[i];
        if (ele > i) {
            h = i + 1;
        } else {
            break;
        }
    }

    return h;
};
// @lc code=end

// console.log(hIndex([3, 0, 6, 1, 5])); //3
// console.log(hIndex([1, 3, 1])); //1
// console.log(hIndex([0])); //0
// console.log(hIndex([1, 100, 0])); // 1
// console.log(hIndex([1, 2, 3, 4, 5, 6])); // 3
// console.log(hIndex([1, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5])); // 5
