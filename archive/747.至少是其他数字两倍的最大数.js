/*
 * @lc app=leetcode.cn id=747 lang=javascript
 *
 * [747] 至少是其他数字两倍的最大数
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var dominantIndex = function (nums) {
  const len = nums.length;
  if (len === 1) return 0;

  nums = nums.map((v, i) => [v, i]).sort((a, b) => b[0] - a[0]);
  for (let i = 1; i < len; i++) {
    if (nums[0][0] < nums[i][0] * 2) return -1;
  }
  return nums[0][1];
};
// @lc code=end
// console.log(dominantIndex([3, 6, 1, 0]));
// console.log(dominantIndex([1, 2, 3]));
// console.log(dominantIndex([1]));
