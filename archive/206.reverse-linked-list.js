/*
 * @lc app=leetcode id=206 lang=javascript
 *
 * [206] Reverse Linked List
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
var reverseList = function(head) {
    if (!head || !head.next) return head;
    let pre = head, current = head.next, next = current ? current.next : null;
    current.next = pre;
    pre.next = null;
    while (next) {
        pre = current;
        current = next;
        next = next.next ? next.next : null;
        current.next = pre;
    }
    return current;
};
// @lc code=end
// module.exports = reverseList;
