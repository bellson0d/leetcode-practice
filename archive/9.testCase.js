const targetFun = require('./9.palindrome-number');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  ['121', true],
  ['-121', false],
  ['10', false],
];
let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input);
  if (result !== output) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
