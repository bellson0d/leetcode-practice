/*
 * @lc app=leetcode.cn id=944 lang=javascript
 *
 * [944] 删列造序
 */

// @lc code=start
/**
 * @param {string[]} strs
 * @return {number}
 */
var minDeletionSize = function (strs) {
  let count = 0;
  for (let i = 0; i < strs[0].length; i++) {
    for (let j = 1; j < strs.length; j++) {
      if (strs[j][i] < strs[j - 1][i]) {
        count++;
        break;
      }
    }
  }
  return count;
};
// @lc code=end

// console.log(minDeletionSize(['cba', 'daf', 'ghi']));
// console.log(minDeletionSize(['a', 'b']));
// console.log(minDeletionSize(['zyx', 'wvu', 'tsr']));
