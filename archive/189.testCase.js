const targetFun = require('./189.rotate-array');
const IS_ARRAY_RESULT = true; // 输出是否为数组
const MULTI_ARGS = true; // 输入参数是否为多个

const testStr = [
  [[[1], 0], [1]],
  [[[1], 10000], [1]],
  [
    [[1, 2, 3, 4, 5, 6, 7], 3],
    [5, 6, 7, 1, 2, 3, 4],
  ],
  [
    [[-1, -100, 3, 99], 2],
    [3, 99, -1, -100],
  ],
];

const time1 = new Date().getTime();
console.log(time1);

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  let result;
  if (MULTI_ARGS) {
    result = targetFun(...input);
  } else {
    result = targetFun(input);
  }

  if (
    IS_ARRAY_RESULT ? result.join('|') !== output.join('|') : result !== output
  ) {
    console.log('Number ' + count + ' Case Wrong !!!');
    console.log('Input: ', input);
    console.log('Expected output: ', output);
    console.log('Output', result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
