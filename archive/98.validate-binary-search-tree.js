/*
 * @lc app=leetcode id=98 lang=javascript
 *
 * [98] Validate Binary Search Tree
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {boolean}
 */
var isValidBST = function (root) {
  let arr1 = [[[], [], root]];

  while (arr1.length > 0) {
    const [roots, roots2, currentNode] = arr1.shift();
    if (!currentNode) continue;
    const leftNode = currentNode.left;
    const rightNode = currentNode.right;

    if (leftNode) {
      if (
        currentNode.val <= leftNode.val ||
        roots.find((v) => v <= leftNode.val) ||
        roots2.find((v) => v >= leftNode.val)
      )
        return false;
      arr1.push([roots.concat([currentNode.val]), roots2.slice(), leftNode]);
    }

    if (rightNode) {
      if (
        currentNode.val >= rightNode.val ||
        roots.find((v) => v <= rightNode.val) ||
        roots2.find((v) => v >= rightNode.val)
      )
        return false;
      arr1.push([roots.slice(), roots2.concat([currentNode.val]), rightNode]);
    }
  }

  return true;
};
// @lc code=end
// module.exports = isValidBST;
