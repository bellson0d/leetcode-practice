/*
 * @lc app=leetcode id=130 lang=javascript
 *
 * [130] Surrounded Regions
 */

// @lc code=start
/**
 * @param {character[][]} board
 * @return {void} Do not return anything, modify board in-place instead.
 */
var solve = function (b) {
  const m = b.length;
  const n = b[0].length;
  if (m < 3 || n < 3) return b;

  let oArr = [],
    obj = {};
  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      if (b[i][j] === 'O' && (i === 0 || j === 0 || i === m - 1 || j === n - 1))
        oArr.push([i, j]);
    }
  }

  var expand = (x, y, b) => {
    let position = [],
      xx = x - 1,
      yy = y;
    if (xx > -1 && b[xx][yy] === 'O' && !obj.hasOwnProperty(xx + '-' + yy))
      position.push([xx, yy]);
    xx = x;
    yy = y + 1;
    if (yy < n && b[xx][yy] === 'O' && !obj.hasOwnProperty(xx + '-' + yy))
      position.push([xx, yy]);
    xx = x + 1;
    yy = y;
    if (xx < m && b[xx][yy] === 'O' && !obj.hasOwnProperty(xx + '-' + yy))
      position.push([xx, yy]);
    xx = x;
    yy = y - 1;
    if (yy > -1 && b[xx][yy] === 'O' && !obj.hasOwnProperty(xx + '-' + yy))
      position.push([xx, yy]);
    return position;
  };

  var helper = (x, y, b, visited, isFree) => {
    visited.push([x, y]);
    if (isFree || x === m - 1 || y === n - 1 || x === 0 || y === 0) {
      isFree = true;
      for (const v of visited) {
        obj[v[0] + '-' + v[1]] = true;
      }
      visited = [];
    } else {
      obj[x + '-' + y] = false;
    }
    const positions = expand(x, y, b);

    for (const p of positions) {
      isFree = helper(p[0], p[1], b, visited, isFree);
    }
    return isFree;
  };

  while (oArr.length) {
    const [x, y] = oArr.shift();
    if (!obj.hasOwnProperty(x + '-' + y)) {
      helper(x, y, b, [], false);
    }
  }

  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      if (!obj[i + '-' + j]) b[i][j] = 'X';
    }
  }
  return b;
};
// @lc code=end

// console.log(
//   solve([
//     ['X', 'X', 'X', 'X', 'O', 'O', 'X', 'X', 'O'],
//     ['O', 'O', 'O', 'O', 'X', 'X', 'O', 'O', 'X'],
//     ['X', 'O', 'X', 'O', 'O', 'X', 'X', 'O', 'X'],
//     ['O', 'O', 'X', 'X', 'X', 'O', 'O', 'O', 'O'],
//     ['X', 'O', 'O', 'X', 'X', 'X', 'X', 'X', 'O'],
//     ['O', 'O', 'X', 'O', 'X', 'O', 'X', 'O', 'X'],
//     ['O', 'O', 'O', 'X', 'X', 'O', 'X', 'O', 'X'],
//     ['O', 'O', 'O', 'X', 'O', 'O', 'O', 'X', 'O'],
//     ['O', 'X', 'O', 'O', 'O', 'X', 'O', 'X', 'O'],
//   ])
// );

// [
//   ['X', 'X', 'X', 'X', 'O', 'O', 'X', 'X', 'O'],
//   ['O', 'O', 'O', 'O', 'X', 'X', 'O', 'O', 'X'],
//   ['X', 'O', 'X', 'O', 'O', 'X', 'X', 'O', 'X'],
//   ['O', 'O', 'X', 'X', 'X', 'O', 'O', 'O', 'O'],
//   ['X', 'O', 'O', 'X', 'X', 'X', 'X', 'X', 'O'],
//   ['O', 'O', 'X', 'X', 'X', 'O', 'X', 'X', 'X'],
//   ['O', 'O', 'O', 'X', 'X', 'O', 'X', 'X', 'X'],
//   ['O', 'O', 'O', 'X', 'O', 'O', 'O', 'X', 'O'],
//   ['O', 'X', 'O', 'O', 'O', 'X', 'O', 'X', 'O'],
// ];

// [
//   ['X', 'X', 'X', 'X', 'O', 'O', 'X', 'X', 'O'],
//   ['O', 'O', 'O', 'O', 'X', 'X', 'O', 'O', 'X'],
//   ['X', 'O', 'X', 'O', 'O', 'X', 'X', 'O', 'X'],
//   ['O', 'O', 'X', 'X', 'X', 'X', 'XO', 'O', 'O'], // 3\6
//   ['X', 'O', 'O', 'X', 'X', 'X', 'X', 'X', 'O'],
//   ['O', 'O', 'X', 'X', 'X', 'O', 'X', 'X', 'X'],
//   ['O', 'O', 'O', 'X', 'X', 'O', 'X', 'X', 'X'],
//   ['O', 'O', 'O', 'X', 'O', 'O', 'O', 'X', 'O'],
//   ['O', 'X', 'O', 'O', 'O', 'X', 'O', 'X', 'O'],
// ];

// console.log(
//   solve([
//     ['X', 'X', 'X', 'X'],
//     ['X', 'O', 'O', 'X'],
//     ['X', 'X', 'O', 'X'],
//     ['X', 'O', 'X', 'X'],
//   ])
// );
// [
//   ['X', 'X', 'X', 'X'],
//   ['X', 'X', 'X', 'X'],
//   ['X', 'X', 'X', 'X'],
//   ['X', 'O', 'X', 'X'],
// ];

// console.log(solve([['X']])); // ['X']
// console.log(solve([['O']])); // ['O']
// console.log(solve([['X', 'O']])); // ['X','O']
// console.log(
//   solve([
//     ['X', 'X', 'X'],
//     ['X', 'O', 'X'],
//     ['X', 'X', 'X'],
//   ])
// );
// [
//   ['X', 'X', 'X'],
//   ['X', 'X', 'X'],
//   ['X', 'X', 'X'],
// ];
