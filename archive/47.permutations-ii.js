/*
 * @lc app=leetcode id=47 lang=javascript
 *
 * [47] Permutations II
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number[][]}
 */

const filterDuplicate = (arr1, arr2) => {
  const len = arr1.length;
  if (len !== arr2.length) return false;

  let count = 0;
  while (count < len) {
    if (arr1[count] !== arr2[count]) return false;
    count++;
  }
  return true;
};

const helpFn = (nums, arr) => {
  const len = nums.length;
  let tmpArr = [],
    tmpRepeat = [];

  if (len === 0) return arr;

  if (arr.length === 0) {
    for (let i = 0; i < len; i++) {
      const ele = nums[i];
      if (!tmpRepeat.includes(ele)) {
        tmpRepeat.push(ele);
        const newNums = nums.slice(0, i).concat(nums.slice(i + 1));
        tmpArr = tmpArr.concat(helpFn(newNums, [[ele]]));
      }
    }
    return tmpArr;
  }

  if (len === 1) {
    const ele = nums[0];
    return arr.map((v) => v.concat([ele]));
  } else {
    for (let i = 0; i < len; i++) {
      const ele = nums[i];
      if (!tmpRepeat.includes(ele)) {
        tmpRepeat.push(ele);
        const newNums = nums.slice(0, i).concat(nums.slice(i + 1));
        tmpArr = tmpArr.concat(
          helpFn(
            newNums,
            arr.map((v) => v.concat([ele]))
          )
        );
      }
    }
  }

  return tmpArr;
};

var permuteUnique = function (nums) {
  return Array.from(new Set(helpFn(nums, [])));
};
// @lc code=end

// console.log(permuteUnique([1, 1, 2]));
// console.log(permuteUnique([]));
// console.log(permuteUnique([1, 1]));
// console.log(permuteUnique([1]));
// @lc code=end
