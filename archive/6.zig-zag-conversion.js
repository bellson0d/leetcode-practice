/*
 * @lc app=leetcode id=6 lang=javascript
 *
 * [6] ZigZag Conversion
 */

// @lc code=start
/**
 * @param {string} s
 * @param {number} numRows
 * @return {string}
 */

const formatData = (arr) =>
  arr
    .sort((a, b) => (a[0] === b[0] ? a[1] - b[1] : a[0] - b[0]))
    .reduce((a, b) => a + b[2], '');

var convert = function (s, numRows) {
  if (numRows === 1) return s;

  let isVertical = true;
  let count = 0;
  let x = 0; // 横坐标指针
  let y = 0; // 纵坐标指针
  const tmpArr = [];

  while (count < s.length) {
    tmpArr.push([y, count, s[count]]);
    if (isVertical) {
      y++;
      if (y === numRows - 1) {
        isVertical = false;
      }
    } else {
      y--;
      x++;
      if (y === 0) {
        isVertical = true;
      }
    }
    count++;
  }

  return formatData(tmpArr);
};

module.exports = convert;
// @lc code=end
