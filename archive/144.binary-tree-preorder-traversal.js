/*
 * @lc app=leetcode id=144 lang=javascript
 *
 * [144] Binary Tree Preorder Traversal
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
var preorderTraversal = function (root) {
  if (!root) return [];
  let res = [],
    tmpArr = [root];
  while (tmpArr.length > 0) {
    const currentNode = tmpArr.pop();
    const left = currentNode.left;
    const right = currentNode.right;
    if (!currentNode.visited) {
      res.push(currentNode.val);
      currentNode.visited = true;
      if (left) {
        tmpArr.push(currentNode);
        tmpArr.push(left);
      } else if (right) {
        tmpArr.push(currentNode);
        tmpArr.push(right);
      }
    } else if (left && !left.visited) {
      res.push(left.val);
      left.visited = true;
      tmpArr.push(currentNode);
      tmpArr.push(left);
    } else if (right && !right.visited) {
      res.push(right.val);
      right.visited = true;
      tmpArr.push(currentNode);
      tmpArr.push(right);
    }
  }

  return res;
};
// @lc code=end
// module.exports = preorderTraversal;
// const { getTree } = require('./utils/treeNode');
// preorderTraversal(getTree([4, 1, 2, null, null, 5, 3]));
