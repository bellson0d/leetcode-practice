/*
 * @lc app=leetcode id=448 lang=javascript
 *
 * [448] Find All Numbers Disappeared in an Array
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number[]}
 */
var findDisappearedNumbers = function (nums) {
  const len = nums.length;
  const set = new Set(nums);
  let arr = [];

  for (let i = 1; i <= len; i++) {
    if (!set.has(i)) arr.push(i);
  }
  return arr;
};
// @lc code=end
