/*
 * @lc app=leetcode id=66 lang=javascript
 *
 * [66] Plus One
 */

// @lc code=start
/**
 * @param {number[]} digits
 * @return {number[]}
 */
var plusOne = function (digits) {
  const len = digits.length;
  if (len === 0) return [1];
  if (digits[len - 1] < 9) {
    digits[len - 1] = digits[len - 1] + 1;
    return digits;
  }

  let count = len - 1,
    addOne = 1;

  while (count >= 0) {
    const res = digits[count] + addOne;
    if (res === 10) {
      digits[count] = 0;
      addOne = 1;
    } else {
      digits[count] = res;
      addOne = 0;
      break;
    }

    count--;
  }

  if (addOne) {
    digits.unshift(1);
  }

  return digits;
};
// @lc code=end
