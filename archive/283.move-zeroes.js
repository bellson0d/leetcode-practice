/*
 * @lc app=leetcode id=283 lang=javascript
 *
 * [283] Move Zeroes
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {void} Do not return anything, modify nums in-place instead.
 */
var moveZeroes = function (nums) {
  for (let i = 0; i < nums.length - 1; i++) {
    const ele = nums[i];
    if (!ele) {
      let j = i + 1;
      while (j < nums.length) {
        if (nums[j]) {
          let tmp = nums[j];
          nums[j] = 0;
          nums[i] = tmp;
          break;
        }
        j++;
      }
    }
  }
  return nums;
};
// @lc code=end

// console.log(moveZeroes([0, 1, 0, 3, 12]));
