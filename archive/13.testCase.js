const targetFun = require('./13.roman-to-integer');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  ['III', 3],
  ['IV', 4],
  ['IX', 9],
  ['LVIII', 58],
  ['MCMXCIV', 1994],
  ['IV', 4],
];
let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input);
  if (result !== output) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
