/*
 * @lc app=leetcode.cn id=976 lang=javascript
 *
 * [976] 三角形的最大周长
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var largestPerimeter = function (nums) {
  nums.sort((a, b) => b - a);

  for (let i = 0; i < nums.length - 2; i++) {
    if (nums[i] < nums[i + 1] + nums[i + 2])
      return nums[i] + nums[i + 1] + nums[i + 2];
  }

  return 0;
};
// @lc code=end

// console.log(largestPerimeter([2, 1, 2])); // 5
// console.log(largestPerimeter([1, 2, 1])); // 0
// console.log(largestPerimeter([3, 2, 3, 4])); // 10
// console.log(largestPerimeter([3, 6, 2, 3])); // 8
// console.log(largestPerimeter([10, 8, 8, 7, 6, 5, 4])); // 26
