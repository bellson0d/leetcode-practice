/*
 * @lc app=leetcode id=48 lang=javascript
 *
 * [48] Rotate Image
 */

// @lc code=start
/**
 * @param {number[][]} matrix
 * @return {void} Do not return anything, modify matrix in-place instead.
 */
var rotate = function (matrix) {
  if (matrix.length === 0) return [];
  const n = matrix[0].length;
  if (n === 1) return matrix;

  let pointerY = n - 1,
    pointerX = 0,
    obj = {};

  for (let i = 0; i < n; i++) {
    for (let j = 0; j < n; j++) {
      obj[i + '|' + j] = matrix[i][j];
    }
  }
  let count = 0;
  while (count < n ** 2) {
    matrix[pointerX][n - 1 - pointerY] = obj[pointerY + '|' + pointerX];
    if (pointerY === 0) {
      pointerY = n - 1;
      pointerX++;
    } else {
      pointerY--;
    }
    count++;
  }

  return matrix;
};
// @lc code=end

// console.log(rotate([]));
// console.log(rotate([[1]]));
// console.log(
//   rotate([
//     [1, 2],
//     [3, 4],
//   ])
// );
// console.log(
//   rotate([
//     [1, 2, 3],
//     [4, 5, 6],
//     [7, 8, 9],
//   ])
// );
// console.log(
//   rotate([
//     [5, 1, 9, 11],
//     [2, 4, 8, 10],
//     [13, 3, 6, 7],
//     [15, 14, 12, 16],
//   ])
// );
// console.log(
//   rotate([
//     [126, 96, 126, 0, 42, 128, 3, 0, 111, -1, 66, 37],
//     [123, 57, 130, 22, -1, 12, 53, 40, 106, 57, 51, 20],
//     [112, 104, 80, 12, 137, 116, 88, 136, 85, 71, 137, 129],
//     [56, 13, 2, 41, 13, 70, 79, 137, 0, 83, 32, 16],
//     [96, 103, 74, 75, 17, 142, 112, 2, 104, 49, 15, 98],
//     [22, 105, 91, 109, 49, 102, 111, 123, 116, 114, 21, 131],
//     [57, 117, 141, 59, 57, 31, 92, 26, 135, 23, 102, 25],
//     [39, 72, 44, 0, 122, 61, 115, 18, 39, 79, 128, 89],
//     [38, 112, 69, 12, 83, 91, 16, 14, 82, 14, 90, 140],
//     [62, 39, 40, 70, 80, -1, 112, 120, 88, 14, 137, 84],
//     [76, 109, 103, 116, 45, 104, 78, 101, 73, 21, 130, 29],
//     [129, 3, 44, 68, 18, 135, 66, 98, 32, 107, 25, 129],
//   ])
// );
