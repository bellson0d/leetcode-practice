/*
 * @lc app=leetcode id=138 lang=javascript
 *
 * [138] Copy List with Random Pointer
 */

// @lc code=start
/**
 * // Definition for a Node.
 * function Node(val, next, random) {
 *    this.val = val;
 *    this.next = next;
 *    this.random = random;
 * };
 */

/**
 * @param {Node} head
 * @return {Node}
 */
var copyRandomList = function (head) {
  if (!head) return null;
  let current = head,
    preNode = null,
    newH = null,
    idx = 0,
    arr1 = [],
    arr2 = [];
  while (current) {
    current.idx = idx;
    let node = new Node(current.val, null, null);
    if (!newH) newH = node;
    if (preNode) preNode.next = node;
    preNode = node;
    arr1.push(current);
    arr2.push(node);
    current = current.next;
    idx++;
  }

  for (let i = 0; i < arr1.length; i++) {
    const { random } = arr1[i];
    if (random) {
      arr2[i].random = arr2[random.idx];
    }
  }

  return newH;
};
// @lc code=end

// function Node(val, next, random) {
//   this.val = val;
//   this.next = next;
//   this.random = random;
// }
