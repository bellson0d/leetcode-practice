/*
 * @lc app=leetcode id=29 lang=javascript
 *
 * [29] Divide Two Integers
 */

// @lc code=start
/**
 * @param {number} dividend
 * @param {number} divisor
 * @return {number}
 */
const MAX = 2 ** 31 - 1;
const MIN = (-2) ** 31;
var divide = function (dividend, divisor) {
  let result = dividend / divisor;
  if (result < 0) {
    result = Math.ceil(result);
  } else {
    result = Math.floor(result);
  }

  if (result > MAX) return MAX;
  if (result < MIN) return MIN;
  return result;
};
// @lc code=end
