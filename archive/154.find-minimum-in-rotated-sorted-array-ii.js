/*
 * @lc app=leetcode id=154 lang=javascript
 *
 * [154] Find Minimum in Rotated Sorted Array II
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var findMin = function (nums) {
  nums = Array.from(new Set(nums));
  const len = nums.length;
  if (len === 1) return nums[0];
  for (let i = 0; i < len; i++) {
    const ele = nums[i];
    const pre = nums[i === 0 ? len - 1 : i - 1];
    const next = nums[i === len - 1 ? 0 : i + 1];
    if (ele < next && ele < pre) return ele;
  }
};
// @lc code=end
