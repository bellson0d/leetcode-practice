/*
 * @lc app=leetcode id=409 lang=javascript
 *
 * [409] Longest Palindrome
 */

// @lc code=start
/**
 * @param {string} s
 * @return {number}
 */
var longestPalindrome = function (str) {
  let obj = {};
  for (const s of str) {
    if (!obj[s]) {
      obj[s] = 1;
    } else {
      obj[s]++;
    }
  }

  let len = 0,
    tmp = [];
  const arr = Object.values(obj);
  for (let i = 0; i < arr.length; i++) {
    const ele = arr[i];
    if (ele % 2 === 0) {
      len += ele;
    } else {
      tmp.push(ele);
    }
  }
  tmp.sort((a, b) => a - b);
  const len2 = tmp.length;
  if (len2 > 0) len += tmp.map((v) => v - 1).reduce((a, b) => a + b, 0) + 1;

  return len;
};
// @lc code=end

// console.log(longestPalindrome('abccccdd')); // 7
// console.log(longestPalindrome('a')); // 1
// console.log(longestPalindrome('bb')); // 2
// console.log(longestPalindrome('abc')); // 1
// console.log(longestPalindrome('abcc')); // 3
// console.log(longestPalindrome('ccc')); // 3
// console.log(longestPalindrome('ccccc')); // 5
// console.log(longestPalindrome('cccbbb')); // 5
// console.log(longestPalindrome('cccbbbaaa')); // 7
