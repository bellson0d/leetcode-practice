/*
 * @lc app=leetcode id=374 lang=javascript
 *
 * [374] Guess Number Higher or Lower
 */

// @lc code=start
/**
 * Forward declaration of guess API.
 * @param {number} num   your guess
 * @return 	            -1 if num is lower than the guess number
 *			             1 if num is higher than the guess number
 *                       otherwise return 0
 * var guess = function(num) {}
 */

/**
 * @param {number} n
 * @return {number}
 */
var guessNumber = function (n) {
  let left = 1,
    right = n;
  while (left < right) {
    const half = ~~((right - left) / 2) + left;
    const res = guess(half);
    if (res === 0) return half;
    if (res === 1) left = half + 1;
    if (res === -1) right = half - 1;
  }
  return left;
};
// @lc code=end
