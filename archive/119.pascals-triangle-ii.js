/*
 * @lc app=leetcode id=119 lang=javascript
 *
 * [119] Pascal's Triangle II
 */

// @lc code=start
/**
 * @param {number} rowIndex
 * @return {number[]}
 */
var getRow = function (rowIndex) {
  const numRows = rowIndex + 1;
  if (numRows === 1) return [1];
  if (numRows === 2) return [1, 1];

  let result = [[1], [1, 1]];

  let count = 2;
  while (count < numRows) {
    const tmpArr = result[count - 1];
    let newArr = [];

    for (let i = 0; i < tmpArr.length - 1; i++) {
      newArr[i] = tmpArr[i] + tmpArr[i + 1];
    }
    newArr.unshift(1);
    newArr.push(1);

    result.push(newArr);
    count++;
  }

  return result[numRows - 1];
};
// @lc code=end

// console.log(getRow(3));
// console.log(getRow(5));
