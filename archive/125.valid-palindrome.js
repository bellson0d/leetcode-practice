/*
 * @lc app=leetcode id=125 lang=javascript
 *
 * [125] Valid Palindrome
 */

// @lc code=start
/**
 * @param {string} s
 * @return {boolean}
 */
const reg = /[a-z0-9]/;
var isPalindrome = function (s) {
  if (!s) return true;

  let pointerLeft = 0,
    pointerRight = s.length - 1,
    left = null,
    right = null;

  while (pointerLeft < pointerRight) {
    left = s[pointerLeft].toLowerCase();
    right = s[pointerRight].toLowerCase();
    if (!reg.test(left)) {
      pointerLeft++;
    } else if (!reg.test(right)) {
      pointerRight--;
    } else if (left !== right) {
      return false;
    } else {
      pointerLeft++;
      pointerRight--;
    }
    if (pointerLeft === pointerRight) return true;
  }

  return left === right;
};
// @lc code=end

// console.log(isPalindrome('A man, a plan, a canal: Panama'));
// console.log(isPalindrome('race a car'));
// console.log(isPalindrome('ab_a'));
// console.log(isPalindrome('0P'));
