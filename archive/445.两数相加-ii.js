/*
 * @lc app=leetcode.cn id=445 lang=javascript
 *
 * [445] 两数相加 II
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */

const addTwo = (a, b, c) => {
  const sum = Number(a) + Number(b) + c;
  return sum > 9 ? [sum % 10, 1] : [sum, 0];
};

var addTwoNumbers = function (a, b) {
  let str1 = '';
  let str2 = '';
  let node1 = a;
  let node2 = b;

  while (node1) {
    str1 += node1.val;
    node1 = node1.next;
  }
  while (node2) {
    str2 += node2.val;
    node2 = node2.next;
  }

  const len1 = str1.length;
  const len2 = str2.length;
  const maxLen = Math.max(len1, len2);

  let i = 0,
    carry = 0,
    sum = 0,
    res = [];
  while (i < maxLen) {
    n1 = str1[len1 - 1 - i] || 0;
    n2 = str2[len2 - 1 - i] || 0;
    [sum, carry] = addTwo(n1, n2, carry);
    res.unshift(sum);
    i++;
  }
  if (carry) res.unshift(carry);

  let currentNode = new ListNode(),
    head = currentNode;
  res.forEach((v, i) => {
    currentNode.val = v;
    if (i < res.length - 1) {
      const tmp = new ListNode();
      currentNode.next = tmp;
      currentNode = tmp;
    }
  });
  return head;
};
// @lc code=end
