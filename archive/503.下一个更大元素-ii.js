/*
 * @lc app=leetcode.cn id=503 lang=javascript
 *
 * [503] 下一个更大元素 II
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number[]}
 */
var nextGreaterElements = function (nums) {
  const len = nums.length;
  const arr = [...nums, ...nums];
  const res = [];

  for (let i = 0; i < len; i++) {
    const ele1 = nums[i];
    let tmp = -1;
    for (let j = i + 1; j < len + i; j++) {
      if (arr[j] > ele1) {
        tmp = arr[j];
        break;
      }
    }
    res.push(tmp);
  }

  return res;
};
// @lc code=end

// console.log(nextGreaterElements([1, 2, 1]));
// console.log(nextGreaterElements([1, 2, 2]));
// console.log(nextGreaterElements([1, 2, 3, 4, 5, 6, 7]));
