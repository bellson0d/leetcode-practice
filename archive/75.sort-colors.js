/*
 * @lc app=leetcode id=75 lang=javascript
 *
 * [75] Sort Colors
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {void} Do not return anything, modify nums in-place instead.
 */
var sortColors = function (nums) {
  let count = 0,
    idx = 0;
  while (count < nums.length) {
    const ele = nums[idx];

    if (ele === 0) {
      nums.splice(idx, 1);
      nums.unshift(0);
    }

    if (ele === 2) {
      nums.splice(idx, 1);
      nums.push(2);
      idx--;
    }

    count++;
    idx++;
  }

  return nums;
};
// @lc code=end

// console.log(sortColors([1, 2, 0]));
