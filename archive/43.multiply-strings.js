/*
 * @lc app=leetcode id=43 lang=javascript
 *
 * [43] Multiply Strings
 */

// @lc code=start
/**
 * @param {string} num1
 * @param {string} num2
 * @return {string}
 */

const splitStr = (str) => {
  const len = str.length;
  let count = 0,
    tmpArr = [];

  while (count < len) {
    const idx = ~~(count / 7);
    if (tmpArr[idx]) {
      tmpArr[idx].push(str[count]);
    } else {
      tmpArr[idx] = [str[count]];
    }
    count++;
  }

  const tmpLen = tmpArr.length;
  const lastLen = tmpArr[tmpLen - 1].length;
  return tmpArr.map((v, i) => {
    const num = Number(v.join(''));
    let z = '0'.repeat(lastLen);
    if (i < tmpLen - 1) {
      z += '0000000'.repeat(tmpLen - i - 2);
    } else {
      z = '';
    }
    return num + z;
  });
};

const countZero = (str) => {
  const len = str.length;
  let count = 0;

  while (count < len && str[len - count - 1] === '0') {
    count++;
  }

  return [Number(str.slice(0, len - count)), count];
};

const multiplyStr = (str1, str2) => {
  const [num1, zeros1] = countZero(str1);
  const [num2, zeros2] = countZero(str2);

  return num1 * num2 + '0'.repeat(zeros1 + zeros2);
};

const trimZero = (str) => {
  let count = 0;
  while (count < str.length - 1 && str[count] === '0') {
    count++;
  }
  return str.slice(count);
};

const addStr = (str1, str2) => {
  const len1 = str1.length;
  const len2 = str2.length;
  const len = len1 > len2 ? len1 : len2;

  let count = 0,
    carry = 0,
    result = [];
  while (count < len) {
    const ele1 = ~~str1[len1 - count - 1];
    const ele2 = ~~str2[len2 - count - 1];

    const ele = ele1 + ele2 + carry;

    if (ele > 9) {
      result.push(ele % 10);
      carry = 1;
    } else {
      result.push(ele);
      carry = 0;
    }

    count++;
  }

  return trimZero(result.reverse().join(''));
};

var multiply = function (num1, num2) {
  const len1 = num1.length;
  const len2 = num2.length;

  if (len1 < 8 && len2 < 8) return ~~num1 * ~~num2 + '';

  const arr1 = splitStr(num1);
  const arr2 = splitStr(num2);
  let tmpArr = [];

  for (let i = 0; i < arr1.length; i++) {
    const ele1 = arr1[i];
    for (let j = 0; j < arr2.length; j++) {
      const ele2 = arr2[j];
      const tmpStr = multiplyStr(ele1, ele2);

      if (tmpArr[i]) {
        tmpArr[i].push(tmpStr);
      } else {
        tmpArr[i] = [tmpStr];
      }
    }
  }

  return tmpArr
    .map((v) => v.reduce((a, b) => addStr(a, b), '0'))
    .reduce((a, b) => addStr(a, b), '0');
};
// @lc code=end

// module.exports = multiply;

// console.log(multiply('0', '332620029'));
