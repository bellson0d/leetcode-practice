/*
 * @lc app=leetcode id=151 lang=javascript
 *
 * [151] Reverse Words in a String
 */

// @lc code=start
/**
 * @param {string} s
 * @return {string}
 */
var reverseWords = function (s) {
  return s.trim().split(' ').filter(Boolean).reverse().join(' ');
};
// @lc code=end

// module.exports = reverseWords;
