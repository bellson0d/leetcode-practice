/*
 * @lc app=leetcode id=21 lang=javascript
 *
 * [21] Merge Two Sorted Lists
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */
var mergeTwoLists = function (l1, l2) {
  if (!l1) return l2;
  if (!l2) return l1;

  let head, currentNode, node1, node2;
  if (l1.val < l2.val) {
    head = currentNode = l1;
    node1 = l1.next;
    node2 = l2;
  } else {
    head = currentNode = l2;
    node2 = l2.next;
    node1 = l1;
  }

  while (node1 || node2) {
    if (!node1) {
      currentNode.next = node2;
      node2 = null;
      continue;
    }
    if (!node2) {
      currentNode.next = node1;
      node1 = null;
      continue;
    }
    if (node1.val < node2.val) {
      currentNode.next = node1;
      currentNode = node1;
      node1 = node1.next;
    } else {
      currentNode.next = node2;
      currentNode = node2;
      node2 = node2.next;
    }
  }

  return head;
};

// function ListNode(val, next) {
//   this.val = val === undefined ? 0 : val;
//   this.next = next === undefined ? null : next;
// }

// let arr = [new ListNode(0)];
// let arr2 = [new ListNode(0)];
// let count = 1;
// while (count < 3) {
//   const node = new ListNode(count * 2);
//   const node2 = new ListNode(count * 2);
//   arr[count - 1].next = node;
//   arr2[count - 1].next = node2;
//   arr.push(node);
//   arr2.push(node2);
//   count++;
// }

// // let result = arr[0];
// // let result = arr2[0];
// let result = mergeTwoLists(arr[0], arr2[0]);
// while (result) {
//   console.log(result.val);
//   result = result.next;
// }

// @lc code=end
