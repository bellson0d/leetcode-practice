/*
 * @lc app=leetcode id=88 lang=javascript
 *
 * [88] Merge Sorted Array
 */

// @lc code=start
/**
 * @param {number[]} nums1
 * @param {number} m
 * @param {number[]} nums2
 * @param {number} n
 * @return {void} Do not return anything, modify nums1 in-place instead.
 */
var merge = function (nums1, m, nums2, n) {
  let pointer1 = 0;

  while (pointer1 < m) {
    const ele = nums1[pointer1];

    if (ele > nums2[0]) {
      nums1[pointer1] = nums2[0];
      nums2[0] = ele;
      nums2.sort((a, b) => a - b);
    }
    pointer1++;
  }

  let count = 0;
  while (count < n) {
    nums1[pointer1 + count] = nums2[count];
    count++;
  }
};

// @lc code=end
// module.exports = merge;
// console.log(merge([1, 2, 3, 0, 0, 0], 3, [2, 5, 6], 3));
