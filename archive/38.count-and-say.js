/*
 * @lc app=leetcode id=38 lang=javascript
 *
 * [38] Count and Say
 */

// @lc code=start
/**
 * @param {number} n
 * @return {string}
 */
var countAndSay = function (n) {
  if (n === 1) return '1';
  if (n === 2) return '11';
  if (n === 3) return '21';
  if (n === 4) return '1211';
  if (n === 5) return '111221';

  let current = '111221',
    count = 5;
  while (count < n) {
    let preVal = current[0],
      count1 = 1,
      result = '';
    for (let i = 1; i < current.length; i++) {
      const ele = current[i];
      if (ele === preVal) {
        count1++;
      } else {
        result += count1 + preVal;
        count1 = 1;
        preVal = ele;
      }
    }
    result += count1 + preVal;
    current = result;
    count++;
  }

  return current;
};

// @lc code=end

// module.exports = countAndSay;
