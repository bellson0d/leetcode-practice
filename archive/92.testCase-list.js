const targetFun = require('./92.reverse-linked-list-ii');
const { getList, isSameList, logList } = require('./utils/listNode');

const testStr = [
  [[getList([1, 2, 3, 4, 5]), 2, 4], getList([1, 4, 3, 2, 5])],
  [[getList([1]), 1, 1], getList([1])],
  [[getList([3, 5]), 1, 2], getList([5, 3])],
  [[getList([3, 5]), 1, 1], getList([3, 5])],
];

const time1 = new Date().getTime();
console.log(time1);

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(...input);

  if (!isSameList(result, output, true)) {
    console.log('Wrong !!!');
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
