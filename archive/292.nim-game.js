/*
 * @lc app=leetcode id=292 lang=javascript
 *
 * [292] Nim Game
 */

// @lc code=start
/**
 * @param {number} n
 * @return {boolean}
 */
var winMap = [1, 2, 3, 5, 6, 7, 9, 10, 11];
var canWinNim = function (n) {
  if (n < 12) return winMap.indexOf(n) !== -1;
  return n % 4 !== 0;
};
// @lc code=end
