const targetFun = require('./167.two-sum-ii-input-array-is-sorted');
const IS_ARRAY_RESULT = true; // 输出是否为数组
const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  [
    [[1, 2], 3],
    [1, 2],
  ],
  [
    [[1, 4, 5, 6], 7],
    [1, 4],
  ],
  [
    [[2, 7, 11, 15], 9],
    [1, 2],
  ],
  [
    [[2, 3, 4], 6],
    [1, 3],
  ],
  [
    [[-1, 0], -1],
    [1, 2],
  ],
];

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input[0], input[1]);
  if (
    IS_ARRAY_RESULT ? result.join('|') !== output.join('|') : result !== output
  ) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
