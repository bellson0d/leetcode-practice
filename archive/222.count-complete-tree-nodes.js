/*
 * @lc app=leetcode id=222 lang=javascript
 *
 * [222] Count Complete Tree Nodes
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
var countNodes = function(root) {
    if (!root) return root;
    let arr = [root], count = 0;

    while (arr.length) {
        const node = arr.pop();
        count++;
        if (node.left) arr.unshift(node.left);
        if (node.right) arr.unshift(node.right);
    }
    return count;
};
// @lc code=end

