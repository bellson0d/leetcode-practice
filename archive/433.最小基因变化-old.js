/*
 * @lc app=leetcode.cn id=433 lang=javascript
 *
 * [433] 最小基因变化
 */

// @lc code=start
/**
 * @param {string} start
 * @param {string} end
 * @param {string[]} bank
 * @return {number}
 */
var helper = (str1, str2) => {
  let res = [];
  for (let i = 0; i < 8; i++) {
    if (str1[i] !== str2[i]) {
      res.push([i, str2[i], str1]);
    }
  }
  return res;
};
var minMutation = function (start, end, bank) {
  if (!bank.includes(end)) return -1;
  let temp = [helper(start, end)];
  const count = temp[0].length;

  while (temp.length) {
    const diff = temp.shift();
    let tmp = [];
    for (let i = 0; i < diff.length; i++) {
      const [idx, str, preStr] = diff[i];
      const newStr = preStr.slice(0, idx) + str + preStr.slice(idx + 1);
      if (newStr === end) return count;
      if (bank.includes(newStr))
        tmp.push(
          diff
            .slice(0, i)
            .concat(diff.slice(i + 1))
            .map((v) => {
              v[2] = newStr;
              return v;
            })
        );
    }
    if (tmp.length) temp = tmp;
  }

  return -1;
};
// @lc code=end

// console.log(minMutation('AACCGGTT', 'AACCGGTA', ['AACCGGTA']));
// console.log(
//   minMutation('AACCGGTT', 'AAACGGTA', ['AACCGGTA', 'AACCGCTA', 'AAACGGTA'])
// );
// console.log(
//   minMutation('AAAAACCC', 'AACCCCCC', ['AAAACCCC', 'AAACCCCC', 'AACCCCCC'])
// );
console.log(
  minMutation('AACCGGTT', 'AAACGGTA', [
    'AACCGATT',
    'AACCGATA',
    'AAACGATA',
    'AAACGGTA',
  ])
);
