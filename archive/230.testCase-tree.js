const targetFun = require("./230.kth-smallest-element-in-a-bst");
const { getTree } = require("./utils/treeNode");
const IS_ARRAY_RESULT = false; // 输出是否为数组
const MULTI_ARGS = true; // 输入参数是否为多个

const testStr = [
    [[getTree([3, 1, 4, null, 2]), 1], 1],
    [[getTree([5, 3, 6, 2, 4, null, null, 1]), 3], 3],
    [[getTree([1, null, 2]), 2], 2],
    [[getTree([1, null, 2, null, null, null, 3]), 2], 2],
    [[getTree([1, null, 2, null, null, null, 3]), 3], 3],
];

const time1 = new Date().getTime();
console.log(time1);

let count = 0;
while (count < testStr.length) {
    const [input, output] = testStr[count];

    let result;
    if (MULTI_ARGS) {
        result = targetFun(...input);
    } else {
        result = targetFun(input);
    }

    if (
        IS_ARRAY_RESULT
            ? result.join("|") !== output.join("|")
            : result !== output
    ) {
        console.log("Number " + count + " Case Wrong !!!");
        return;
    }
    count++;
}
console.log("All pass!");

const time2 = new Date().getTime();
console.log("Duration: ", time2 - time1);
