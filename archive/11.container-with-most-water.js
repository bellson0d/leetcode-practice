/*
 * @lc app=leetcode id=11 lang=javascript
 *
 * [11] Container With Most Water
 */

// @lc code=start
/**
 * @param {number[]} height
 * @return {number}
 */
// var maxArea = function (height) {
//   let area = 0;
//   let l1 = 0;
//   let l2 = 0;
//   for (let i = 0; i < height.length; i++) {
//     const ele = height[i];
//     for (let j = height.length - 1; j > i; j--) {
//       const line2 = j - i;
//       const ele2 = height[j];

//       if (l1 > ele && l1 > ele2 && l2 > line2) continue;
//       const line1 = ele < ele2 ? ele : ele2;

//       const tmpArea = line1 * line2;
//       if (tmpArea > area) {
//         l1 = line1;
//         l2 = line2;
//         area = tmpArea;
//       }
//     }
//   }

//   return area;
// };

var maxArea = function (height) {
  let area = 0;
  let i = 0;
  let j = height.length - 1;

  while (i < j) {
    const ele = height[i];
    const ele2 = height[j];
    const line2 = j - i;
    let line1;
    if (ele < ele2) {
      i++;
      line1 = ele;
    } else {
      j--;
      line1 = ele2;
    }
    const tmpArea = line1 * line2;

    if (tmpArea > area) {
      area = tmpArea;
    }
  }

  return area;
};

// console.log(maxArea([1, 8, 6, 2, 5, 4, 8, 3, 7]));

module.exports = maxArea;
// @lc code=end
