/*
 * @lc app=leetcode.cn id=508 lang=javascript
 *
 * [508] 出现次数最多的子树元素和
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
var findFrequentTreeSum = function (root) {
  let obj = {};
  const helper = (node) => {
    let sum = node.val;
    if (node.left) sum += helper(node.left);
    if (node.right) sum += helper(node.right);
    if (obj[sum]) {
      obj[sum][0]++;
    } else {
      obj[sum] = [1, sum];
    }
    return sum;
  };
  helper(root);

  const res = Object.values(obj).sort((a, b) => b[0] - a[0]);
  const count = res[0][0];
  return res.filter((v) => v[0] === count).map((v) => v[1]);
};
// @lc code=end

// const { getTree, logTree } = require('./utils/treeNode');

// const tree = getTree([3, 1, 5, 0, 2, 4, 6, null, null, null, 3]);
// logTree(tree);
// console.log(findFrequentTreeSum(tree));
