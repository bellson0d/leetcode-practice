/*
 * @lc app=leetcode id=39 lang=javascript
 *
 * [39] Combination Sum
 */

// @lc code=start
/**
 * @param {number[]} candidates
 * @param {number} target
 * @return {number[][]}
 */
function helpFn(arr, val, num, preVal) {
  if (num === 1) {
    const ele = arr.find((v) => v === val);
    if (ele) {
      return [preVal.concat([ele])];
    } else {
      return null;
    }
  }

  let result = [];
  for (let i = 0; i < arr.length; i++) {
    const ele = arr[i];

    if (ele < val) {
      const ele2 = helpFn(
        arr.slice(i),
        val - ele,
        num - 1,
        preVal.concat([ele])
      );

      if (ele2 && ele2.length > 0) {
        result = result.concat(ele2);
      }
    }
  }

  return result;
}

var combinationSum = function (c, target) {
  const candidates = c.sort((a, b) => a - b);
  if (target < candidates[0]) return [];
  let maxNums = 1;

  while (candidates[0] * maxNums <= target) {
    maxNums++;
  }
  maxNums--;

  let count = 1,
    result = [];
  while (count <= maxNums) {
    const ele2 = helpFn(candidates, target, count, []);
    if (ele2 && ele2.length > 0) {
      result = result.concat(ele2);
    }
    count++;
  }

  return result;
};

// console.log(combinationSum([2, 3, 6, 7], 7));
// console.log(combinationSum([2, 3, 5], 8));
// console.log(combinationSum([3, 5, 7], 10));
// console.log(combinationSum([8, 7, 4, 3], 11));

// @lc code=end
