const log = console.log;

// const getNum = (n) => {
//   if (typeof n === 'number') {
//     return n;
//   }

//   return n.reduce((a, b, i) => a + b * 10 ** i, 0);
// };

const getNum = (n) => {
  let count = 0;
  let sum = 0;
  let node = n[0];
  while (node) {
    sum += node * 10 ** count;
    count++;
    node = n[count];
  }
  return sum;
};

var addTwoNumbers = function (nums, target) {
  return String(getNum(nums) + getNum(target))
    .split('')
    .reverse()
    .map(Number);
};

log(addTwoNumbers([2, 4, 3], [5, 6, 4]));
