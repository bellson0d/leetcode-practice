/*
 * @lc app=leetcode.cn id=442 lang=javascript
 *
 * [442] 数组中重复的数据
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number[]}
 */
var findDuplicates = function (nums) {
  return Array.from(
    new Set(
      nums
        .sort((a, b) => a - b)
        .filter((v, i, arr) => {
          if (i === 0) {
            return v === arr[i + 1];
          }
          if (i === arr.length - 1) {
            return v === arr[i - 1];
          }
          return v === arr[i - 1] || v === arr[i + 1];
        })
    )
  );
};
// @lc code=end
// console.log(findDuplicates([4, 3, 2, 7, 8, 2, 3, 1]));
