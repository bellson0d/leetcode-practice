/*
 * @lc app=leetcode.cn id=590 lang=javascript
 *
 * [590] N 叉树的后序遍历
 */

// @lc code=start
/**
 * // Definition for a Node.
 * function Node(val,children) {
 *    this.val = val;
 *    this.children = children;
 * };
 */

/**
 * @param {Node|null} root
 * @return {number[]}
 */
var postorder = function (root) {
  if (!root) return [];
  let arr = [root],
    res = [];
  while (arr.length) {
    const node = arr[arr.length - 1];

    if (node.children && node.children.filter((v) => !v.visited).length) {
      arr.push(...node.children.reverse());
    } else {
      res.push(node.val);
      node.visited = true;
      arr.pop();
    }
  }
  return res;
};
// @lc code=end
