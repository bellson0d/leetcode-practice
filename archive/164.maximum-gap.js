/*
 * @lc app=leetcode id=164 lang=javascript
 *
 * [164] Maximum Gap
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var maximumGap = function (nums) {
  const len = nums.length;
  if (len < 2) return 0;
  nums.sort((a, b) => a - b);
  let gap = nums[1] - nums[0],
    count = 1;

  while (count < len - 1) {
    const ele1 = nums[count];
    const ele2 = nums[count + 1];
    const g = ele2 - ele1;
    if (g > gap) gap = g;

    count++;
  }
  return gap;
};
// @lc code=end
// console.log(maximumGap([3, 6, 9, 1]));
// console.log(maximumGap([100, 3, 2, 1]));
// Bucket Sort Method
