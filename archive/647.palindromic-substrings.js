/*
 * @lc app=leetcode id=647 lang=javascript
 *
 * [647] Palindromic Substrings
 */

// @lc code=start
/**
 * @param {string} s
 * @return {number}
 */
function validatePalindromic(s) {
  const len = s.length;
  for (let i = 0; i < len / 2; i++) {
    const head = s[i];
    const tail = s[len - i - 1];
    if (head !== tail) return false;
  }
  return true;
}

var countSubstrings = function (s) {
  const len = s.length;
  let idx = 1,
    res = 0;
  while (idx < len + 1) {
    const str = s.slice(0, idx);
    let idx2 = 0;
    while (idx2 < idx) {
      const subStr = str.slice(idx - idx2 - 1);
      if (validatePalindromic(subStr)) res++;
      idx2++;
    }
    idx++;
  }
  return res;
};
// @lc code=end

// console.log(countSubstrings('aaa'));
// console.log(countSubstrings('abc'));
