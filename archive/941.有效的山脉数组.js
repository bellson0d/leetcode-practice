/*
 * @lc app=leetcode.cn id=941 lang=javascript
 *
 * [941] 有效的山脉数组
 */

// @lc code=start
/**
 * @param {number[]} arr
 * @return {boolean}
 */
var validMountainArray = function (arr) {
  const len = arr.length;
  if (len === 1) return false;
  let ascent = true;
  for (let i = 0; i < len - 1; i++) {
    const ele = arr[i];
    if (i === 0) {
      if (ele >= arr[i + 1]) return false;
    } else if (i === len - 1) {
      if (ele <= arr[i + 1]) return false;
    } else {
      if (ele === arr[i + 1]) return false;
      if (ele > arr[i + 1]) {
        ascent = false;
      } else if (!ascent) {
        return false;
      }
    }
  }
  return !ascent;
};
// @lc code=end

// console.log(validMountainArray([1])); //F
// console.log(validMountainArray([2, 1])); //F
// console.log(validMountainArray([3, 5, 5])); //F
// console.log(validMountainArray([0, 3, 2, 1])); //T
// console.log(validMountainArray([0, 1, 2])); //F
// console.log(validMountainArray([2, 1, 0])); //F
// console.log(validMountainArray([1, 2, 3, 4, 3, 2, 3])); //F
