/*
 * @lc app=leetcode id=394 lang=javascript
 *
 * [394] Decode String
 */

// @lc code=start
/**
 * @param {string} s
 * @return {string}
 */
var reg = /\d/;
var decodeString = function (s) {
  let left = [];
  for (let i = 0; i < s.length; i++) {
    let ele = s[i];
    if (reg.test(ele)) {
      let tmp = ele,
        idx = i;
      i++;
      while (s[i] !== '[') {
        tmp += s[i];
        i++;
      }
      left.push([Number(tmp), idx, i]);
    } else if (ele === ']') {
      const [n, idx1, idx2] = left.pop();
      const str = s.slice(idx2 + 1, i).repeat(n);
      s = s.slice(0, idx1) + str + s.slice(i + 1);
      i = idx1 + str.length - 1;
    }
  }
  return s;
};
// @lc code=end

// console.log(decodeString('3[a]2[bc]'));
// console.log(decodeString('3[a2[c]]'));
// console.log(decodeString('2[abc]3[cd]ef'));
// console.log(decodeString('abc3[cd]xyz'));
// console.log(decodeString('1[a2[b3[c4[dz]e]f]g]'));
