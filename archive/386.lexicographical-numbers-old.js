/*
 * @lc app=leetcode id=386 lang=javascript
 *
 * [386] Lexicographical Numbers
 */

// @lc code=start
/**
 * @param {number} n
 * @return {number[]}
 */
var obj2arr = (obj, pre = '') => {
  let keys = Object.keys(obj),
    res = [];

  for (const key of keys) {
    const tmpObj = obj[key];
    res.push(Number(pre + key));
    if (Object.keys(tmpObj).length !== 0) {
      res = res.concat(obj2arr(tmpObj, pre + key));
    }
  }

  return res;
};

var lexicalOrder = function (n) {
  let obj = {};
  for (let i = 1; i <= n; i++) {
    let sArr = String(i).split(''),
      tmpObj = obj;
    while (sArr.length > 1) {
      const key = sArr.shift();
      tmpObj = tmpObj[key];
    }
    tmpObj[sArr[0]] = {};
  }
  return obj2arr(obj);
};
// @lc code=end

// console.log(lexicalOrder(13));
// console.log(lexicalOrder(2));
// console.log(lexicalOrder(1));
// console.log(lexicalOrder(100));
