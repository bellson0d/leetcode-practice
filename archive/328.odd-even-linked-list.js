/*
 * @lc app=leetcode id=328 lang=javascript
 *
 * [328] Odd Even Linked List
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
var oddEvenList = function (head) {
  let head2 = null,
    node1 = head,
    node2 = null,
    flag = true;

  while ((flag && node1) || (!flag && node2)) {
    if (flag) {
      if (node2) {
        node2.next = node1.next;
        node2 = node1.next;
      } else {
        head2 = node1.next;
        node2 = node1.next;
      }
    } else {
      if (!node2.next) {
        node1.next = head2;
        return head;
      }
      node1.next = node2.next;
      node1 = node2.next;
    }

    flag = !flag;
  }

  node1 && (node1.next = head2);
  return head;
};
// @lc code=end

// const { getList, logList } = require('./utils/listNode');

// logList(oddEvenList(getList([])));
// logList(oddEvenList(getList([1])));
// logList(oddEvenList(getList([1, 2])));
// logList(oddEvenList(getList([1, 2, 3, 4, 5])));
// logList(oddEvenList(getList([2, 1, 3, 5, 6, 4, 7])));
