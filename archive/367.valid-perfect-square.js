/*
 * @lc app=leetcode id=367 lang=javascript
 *
 * [367] Valid Perfect Square
 */

// @lc code=start
/**
 * @param {number} num
 * @return {boolean}
 */

var isPerfectSquare = function (num) {
  for (let i = 1; i < 46342; i++) {
    const res = i ** 2;
    if (res === num) return true;
    if (res > num) return false;
  }
};

// @lc code=end
