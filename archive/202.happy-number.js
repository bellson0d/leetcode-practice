/*
 * @lc app=leetcode id=202 lang=javascript
 *
 * [202] Happy Number
 */

// @lc code=start
/**
 * @param {number} n
 * @return {boolean}
 */

const helper = (n) => {
    let res = 0;
    String(n)
        .split("")
        .forEach((v) => (res += v * v));
    return res;
};

const isHappy = function (n) {
    let arr = {},
        tmp = n;
    while (true) {
        tmp = helper(tmp);
        if (tmp === 1) return true;
        if (arr[tmp]) return false;
        arr[tmp] = true;
    }
};
// @lc code=end
// console.log(isHappy(19)); // true
// console.log(isHappy(2)); // false
// console.log(isHappy(78)); // false
