/*
 * @lc app=leetcode id=59 lang=javascript
 *
 * [59] Spiral Matrix II
 */

// @lc code=start
/**
 * @param {number} n
 * @return {number[][]}
 */
var generateMatrix = function (n) {
  if (n === 0) return [[]];
  if (n === 1) return [[1]];
  if (n === 2)
    return [
      [1, 2],
      [4, 3],
    ];

  const matrix = Array(n)
    .fill(1)
    .map((v) => []);

  const totalEle = n ** 2;

  let count = 0,
    i = 0,
    j = 0,
    currentDirection = 0,
    addCount = 0;

  while (count < totalEle) {
    if (currentDirection === 0) {
      matrix[i][j] = count + 1;
      j++;
      if (j === n - 1 - addCount) currentDirection = (currentDirection + 1) % 4;
    } else if (currentDirection === 1) {
      matrix[i][j] = count + 1;
      i++;
      if (i === n - 1 - addCount) currentDirection = (currentDirection + 1) % 4;
    } else if (currentDirection === 2) {
      matrix[i][j] = count + 1;
      j--;
      if (j === addCount) {
        currentDirection = (currentDirection + 1) % 4;
        addCount++;
      }
    } else if (currentDirection === 3) {
      matrix[i][j] = count + 1;
      i--;
      if (i === addCount) currentDirection = (currentDirection + 1) % 4;
    }

    count++;
  }

  return matrix;
};

// @lc code=end
// console.log(generateMatrix(3));
// console.log(generateMatrix(4));
// console.log(generateMatrix(5));
