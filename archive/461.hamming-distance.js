/*
 * @lc app=leetcode id=461 lang=javascript
 *
 * [461] Hamming Distance
 */

// @lc code=start
/**
 * @param {number} x
 * @param {number} y
 * @return {number}
 */
var hammingDistance = function (x, y) {
  let arr1 = Array(32).fill('0');
  let arr2 = Array(32).fill('0');

  const a1 = x.toString(2).split('');
  const a2 = y.toString(2).split('');

  arr1 = arr1.slice(0, 32 - a1.length).concat(a1);
  arr2 = arr2.slice(0, 32 - a2.length).concat(a2);

  let count = 0;
  for (let i = 0; i < 32; i++) {
    if (arr1[i] !== arr2[i]) count++;
  }
  return count;
};
// @lc code=end
// console.log(hammingDistance(1, 4));
// console.log(hammingDistance(3, 1));
// console.log(hammingDistance(1023, 0));
