/*
 * @lc app=leetcode id=117 lang=javascript
 *
 * [117] Populating Next Right Pointers in Each Node II
 */

// @lc code=start
/**
 * // Definition for a Node.
 * function Node(val, left, right, next) {
 *    this.val = val === undefined ? null : val;
 *    this.left = left === undefined ? null : left;
 *    this.right = right === undefined ? null : right;
 *    this.next = next === undefined ? null : next;
 * };
 */

/**
 * @param {Node} root
 * @return {Node}
 */
var connect = function (root) {
  if (!root) return null;

  let depth = 0,
    tmpArr = [[depth, root]];

  while (tmpArr.length > 0) {
    const [currentDepth, currentNode] = tmpArr.shift();

    if (!tmpArr[0] || currentDepth !== tmpArr[0][0]) {
      currentNode.next = null;
    } else {
      currentNode.next = tmpArr[0][1];
    }

    if (currentNode.left) tmpArr.push([currentDepth + 1, currentNode.left]);
    if (currentNode.right) tmpArr.push([currentDepth + 1, currentNode.right]);
  }

  return root;
};
// @lc code=end

// const { getTree } = require('./utils/treeNode');
// console.log(connect(getTree([])));
// console.log(connect(getTree([1])));
// console.log(connect(getTree([1, 2, 3])));
// console.log(connect(getTree([1, 2, 3, 4, 5, 6, 7])));
// console.log(connect(getTree([1, 2])));
// console.log(connect(getTree([1, 2, 3, 4, 5, null, 7])));
