/*
 * @lc app=leetcode.cn id=744 lang=javascript
 *
 * [744] 寻找比目标字母大的最小字母
 */

// @lc code=start
/**
 * @param {character[]} letters
 * @param {character} target
 * @return {character}
 */
var nextGreatestLetter = function (letters, target) {
  for (let i = 0; i < letters.length; i++) {
    if (target < letters[i]) return letters[i];
  }
  return letters[0];
};
// @lc code=end
// console.log(nextGreatestLetter(['c', 'f', 'j'], 'a')); // "c"
// console.log(nextGreatestLetter(['c', 'f', 'j'], 'c')); // "f"
// console.log(nextGreatestLetter(['c', 'f', 'j'], 'd')); // "f"
// console.log(nextGreatestLetter(['c', 'f', 'j'], 'g')); // "j"
// console.log(nextGreatestLetter(['c', 'f', 'j'], 'j')); // "c"
// console.log(nextGreatestLetter(['c', 'f', 'j'], 'k')); // "c"
