const targetFun = require('./38.count-and-say');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  [1, '1'],
  [2, '11'],
  [3, '21'],
  [4, '1211'],
  [5, '111221'],
  [6, '312211'],
  [7, '13112221'],
  [8, '1113213211'],
  [9, '31131211131221'],
  [10, '13211311123113112211'],
];

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input);
  if (result !== output) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
