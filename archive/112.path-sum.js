/*
 * @lc app=leetcode id=112 lang=javascript
 *
 * [112] Path Sum
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} sum
 * @return {boolean}
 */
var hasPathSum = function (root, sum) {
  if (!root) return false;
  if (!root.left && !root.right) return root.val === sum;

  let tmpArr = [[0, root]];

  while (tmpArr.length > 0) {
    const [pre, currentNode] = tmpArr.shift();

    if (!currentNode.left && !currentNode.right) {
      if (pre + currentNode.val === sum) return true;
    } else {
      if (currentNode.left)
        tmpArr.unshift([pre + currentNode.val, currentNode.left]);
      if (currentNode.right)
        tmpArr.unshift([pre + currentNode.val, currentNode.right]);
    }
  }

  return false;
};
// @lc code=end

// const { getTree } = require('./utils/treeNode');
// console.log(hasPathSum(getTree([]), 1));
// console.log(hasPathSum(getTree([1]), 1));
// console.log(hasPathSum(getTree([1, 2, 3]), 5));
// console.log(hasPathSum(getTree([3, 9, 20, null, null, 15, 7]), 30));
// console.log(hasPathSum(getTree([1, 2, 3, 4, 5, 6, 7]), 8));
