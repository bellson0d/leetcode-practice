/*
 * @lc app=leetcode id=485 lang=javascript
 *
 * [485] Max Consecutive Ones
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var findMaxConsecutiveOnes = function (nums) {
  const arr = nums.join('').match(/(1+)/g);
  return arr ? arr.sort((a, b) => b.length - a.length)[0].length : 0;
};
// @lc code=end

// console.log(findMaxConsecutiveOnes([1, 1, 0, 1, 1, 1]));
// console.log(findMaxConsecutiveOnes([1, 0, 1, 1, 0, 1]));
