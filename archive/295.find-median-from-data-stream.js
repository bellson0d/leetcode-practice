/*
 * @lc app=leetcode id=295 lang=javascript
 *
 * [295] Find Median from Data Stream
 */

// @lc code=start
/**
 * initialize your data structure here.
 */
var MedianFinder = function () {
  this.arr = [];
  return this;
};

/**
 * @param {number} num
 * @return {void}
 */
MedianFinder.prototype.addNum = function (num) {
  if (this.arr.length === 0 || num <= this.arr[0]) {
    this.arr.unshift(num);
  } else if (num >= this.arr[this.arr.length - 1]) {
    this.arr.push(num);
  } else {
    let start = 0,
      end = this.arr.length - 1;
    while (start < end) {
      const mid = ~~((end - start) / 2) + start;
      if (this.arr[mid] === num) {
        this.arr = this.arr
          .slice(0, mid)
          .concat([num])
          .concat(this.arr.slice(mid));
        break;
      } else if (this.arr[mid] > num) {
        end = mid;
      } else {
        start = mid;
      }

      if (start === end - 1) {
        this.arr = this.arr
          .slice(0, end)
          .concat([num])
          .concat(this.arr.slice(end));
        break;
      }
    }
  }
};

/**
 * @return {number}
 */
MedianFinder.prototype.findMedian = function () {
  const len = this.arr.length;
  const mid = ~~(len / 2);
  if (len % 2 === 0) return (this.arr[mid] + this.arr[mid - 1]) / 2;
  return this.arr[mid];
};

/**
 * Your MedianFinder object will be instantiated and called as such:
 * var obj = new MedianFinder()
 * obj.addNum(num)
 * var param_2 = obj.findMedian()
 */
// @lc code=end

// const medianFinder = new MedianFinder();
// [6, 10, 2, 6, 5, 0, 6, 3, 1, 0, 0].forEach((v) => {
//   medianFinder.addNum(v);
//   console.log(medianFinder.findMedian());
// });

// var arr1 = [6.0, 8.0, 6.0, 6.0, 6.0, 4.83333, 6.0, 4.75, 5.0, 3.9, 3.0];
// var arr2 = [6.0, 8.0, 6.0, 6.0, 6.0, 5.5, 6.0, 5.5, 5.0, 4.0, 3.0];
