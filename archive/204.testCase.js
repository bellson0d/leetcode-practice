const targetFun = require("./204.count-primes");
const IS_ARRAY_RESULT = false; // 输出是否为数组
const MULTI_ARGS = false; // 输入参数是否为多个

const testStr = [
    [10, 4],
    [2, 0],
    [5, 2],
    [0, 0],
    [1, 0],
    [100, 25],
    [101, 25],
    [102, 26],
    [106, 27],
    [1000, 168],
    [10000, 1229],
    [499979, 41537],
];

const time1 = new Date().getTime();
console.log(time1);

let count = 0;
while (count < testStr.length) {
    const [input, output] = testStr[count];
    let result;
    if (MULTI_ARGS) {
        result = targetFun(...input);
    } else {
        result = targetFun(input);
    }

    if (
        IS_ARRAY_RESULT
            ? result.join("|") !== output.join("|")
            : result !== output
    ) {
        console.log("Number " + count + " Case Wrong !!!");
        console.log("Input: ", input);
        console.log("Expected output: ", output);
        console.log("Output", result);
        return;
    }
    count++;
}
console.log("All pass!");

const time2 = new Date().getTime();
console.log("Duration: ", time2 - time1);
