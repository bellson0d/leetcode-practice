const targetFun = require('./563.二叉树的坡度');
const { getTree } = require('./utils/treeNode');
const IS_ARRAY_RESULT = false; // 输出是否为数组
const MULTI_ARGS = false; // 输入参数是否为多个

const testStr = [
  [getTree([1, 2, 3]), 1],
  [getTree([4, 2, 9, 3, 5, null, 7]), 15],
  [getTree([21, 7, 14, 1, 1, 2, 2, 3, 3]), 9],
  [getTree([-8, 3, 0, -8, null, null, null, null, -1, null, 8]), 18],
];

const time1 = new Date().getTime();
console.log(time1);

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];

  let result;
  if (MULTI_ARGS) {
    result = targetFun(...input);
  } else {
    result = targetFun(input);
  }

  if (
    IS_ARRAY_RESULT ? result.join('|') !== output.join('|') : result !== output
  ) {
    console.log('Number ' + count + ' Case Wrong !!!');
    console.log('Input: ', input);
    console.log('Expected output: ', output);
    console.log('Output', result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
