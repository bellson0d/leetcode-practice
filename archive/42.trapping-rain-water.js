/*
 * @lc app=leetcode id=42 lang=javascript
 *
 * [42] Trapping Rain Water
 */

// @lc code=start
/**
 * @param {number[]} height
 * @return {number}
 */
var trap = function (height) {
  const len = height.length;

  if (len < 3) return 0;

  let pointerLeft = 0,
    pointerRight = len - 1;

  while (height[pointerLeft] === 0) {
    pointerLeft++;
    if (pointerLeft + 1 === pointerRight) return 0;
  }
  while (height[pointerRight] === 0) {
    pointerRight--;
    if (pointerRight - 1 === pointerLeft) return 0;
  }

  let water = 0;
  while (pointerLeft < pointerRight) {
    const leftHeight = height[pointerLeft];
    const rightHeight = height[pointerRight];

    let tmpPointer, step, fillPart;
    if (leftHeight < rightHeight) {
      tmpPointer = pointerLeft + 1;
      step = 0;
      fillPart = 0;
      while (
        height[tmpPointer] < height[pointerLeft] &&
        tmpPointer <= pointerRight
      ) {
        fillPart += height[tmpPointer];
        step++;
        tmpPointer++;
      }

      water += height[pointerLeft] * step - fillPart;
      pointerLeft = tmpPointer;
    } else {
      tmpPointer = pointerRight - 1;
      step = 0;
      fillPart = 0;
      while (
        height[tmpPointer] < height[pointerRight] &&
        tmpPointer >= pointerLeft
      ) {
        fillPart += height[tmpPointer];
        step++;
        tmpPointer--;
      }

      water += height[pointerRight] * step - fillPart;
      pointerRight = tmpPointer;
    }
  }

  return water;
};
// @lc code=end

module.exports = trap;
