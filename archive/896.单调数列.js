/*
 * @lc app=leetcode.cn id=896 lang=javascript
 *
 * [896] 单调数列
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {boolean}
 */
var isMonotonic = function (nums) {
  let pre = nums[1] - nums[0];
  for (let i = 2; i < nums.length; i++) {
    const res = nums[i] - nums[i - 1];
    if (pre === 0) {
      pre = res;
    } else if (pre > 0) {
      if (res < 0) return false;
    } else {
      if (res > 0) return false;
    }
  }
  return true;
};
// @lc code=end

// console.log(isMonotonic([1]));
// console.log(isMonotonic([1, 2, 2, 3]));
// console.log(isMonotonic([1, 1]));
// console.log(isMonotonic([6, 5, 4, 4]));
// console.log(isMonotonic([1, 3, 2]));
// console.log(isMonotonic([1, 2, 4, 5]));
// console.log(isMonotonic([2, 2, 2, 1, 4, 5]));
