/*
 * @lc app=leetcode id=171 lang=javascript
 *
 * [171] Excel Sheet Column Number
 */

// @lc code=start
/**
 * @param {string} s
 * @return {number}
 */
var letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
var titleToNumber = function (s) {
  let r = 0,
    e = 0,
    count = s.length;
  while (--count > -1) {
    r += (letters.indexOf(s[count]) + 1) * 26 ** e++;
  }
  return r;
};
// @lc code=end
// console.log(titleToNumber('A'));
// console.log(titleToNumber('AB'));
// console.log(titleToNumber('ZY'));
