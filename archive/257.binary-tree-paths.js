/*
 * @lc app=leetcode id=257 lang=javascript
 *
 * [257] Binary Tree Paths
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {string[]}
 */
var binaryTreePaths = function (root) {
    let res = [],
        tmp = [[root, '']];
    while (tmp.length) {
        const [node, pre] = tmp.pop();
        const newPre = (pre ? pre + "->" : "") + node.val;

        if (!node.left && !node.right) {
            res.push(newPre);
        } else {
            if (node.left) tmp.unshift([node.left, newPre]);
            if (node.right) tmp.unshift([node.right, newPre]);
        }
    }

    return res;
};
// @lc code=end
