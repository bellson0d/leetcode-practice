/*
 * @lc app=leetcode id=233 lang=javascript
 *
 * [233] Number of Digit One
 */

// @lc code=start
/**
 * @param {number} n
 * @return {number}
 */

var countDigitOne = function (n) {
  if (n === 0) return 0;

  let countr = 0;
  for (let i = 1; i <= n; i *= 10) {
    let divider = i * 10;
    countr +=
      ~~(n / divider) * i + Math.min(Math.max((n % divider) - i + 1, 0), i);
  }
  return countr;
};
// @lc code=end
// 归纳法无能，魔改 C++
// console.log(countDigitOne(10)); // 2
// console.log(countDigitOne(11)); // 4
// console.log(countDigitOne(13)); // 6
// console.log(countDigitOne(20)); // 12
// console.log(countDigitOne(0)); // 0
// console.log(countDigitOne(99));
// console.log(countDigitOne(999));
console.log(countDigitOne(123));
// console.log('------------------------');
// console.log(countDigitOne(1));
// console.log(countDigitOne(10));
// console.log(countDigitOne(100));
// console.log(countDigitOne(1000));
// console.log(countDigitOne(10000));
// console.log(countDigitOne(100000));
// console.log(countDigitOne(1000000));
// console.log(countDigitOne(2 * 10 ** 5));
// console.log(countDigitOne(2 * 10 ** 6));
// console.log(countDigitOne(2 * 10 ** 7));
// console.log(countDigitOne(2 * 10 ** 8));
// console.log(countDigitOne(2 * 10 ** 9));
