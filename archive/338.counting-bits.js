/*
 * @lc app=leetcode id=338 lang=javascript
 *
 * [338] Counting Bits
 */

// @lc code=start
/**
 * @param {number} n
 * @return {number[]}
 */
var countBits = function (n) {
  let res = [],
    count = 0;
  while (count <= n) {
    let tmp = 0;
    for (const s of count.toString(2)) {
      if (s === '1') tmp++;
    }
    res.push(tmp);
    count++;
  }
  return res;
};
// @lc code=end
