/*
 * @lc app=leetcode.cn id=953 lang=javascript
 *
 * [953] 验证外星语词典
 */

// @lc code=start
/**
 * @param {string[]} words
 * @param {string} order
 * @return {boolean}
 */
var isAlienSorted = function (words, order) {
  let obj = {};
  for (let i = 0; i < order.length; i++) {
    const ele = order[i];
    obj[ele] = i;
  }

  for (let i = 0; i < words.length - 1; i++) {
    const ele1 = words[i];
    const ele2 = words[i + 1];

    let idx = 0;
    while (ele1[idx] || ele2[idx]) {
      if (ele1[idx] && ele2[idx]) {
        if (obj[ele1[idx]] < obj[ele2[idx]]) {
          break;
        } else if (obj[ele1[idx]] > obj[ele2[idx]]) {
          return false;
        }
      } else if (ele1[idx] && !ele2[idx]) {
        return false;
      } else {
        break;
      }

      idx++;
    }
  }

  return true;
};
// @lc code=end

// console.log(isAlienSorted(['hello', 'leetcode'], 'hlabcdefgijkmnopqrstuvwxyz'));
// console.log(
//   isAlienSorted(['word', 'world', 'row'], 'worldabcefghijkmnpqstuvxyz')
// );
// console.log(isAlienSorted(['apple', 'app'], 'abcdefghijklmnopqrstuvwxyz'));
