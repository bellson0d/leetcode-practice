/*
 * @lc app=leetcode.cn id=884 lang=javascript
 *
 * [884] 两句话中的不常见单词
 */

// @lc code=start
/**
 * @param {string} s1
 * @param {string} s2
 * @return {string[]}
 */
var helper = (s) => {
  let obj = {};
  s.split(' ').forEach((v) => {
    if (obj[v]) {
      obj[v][0]++;
    } else {
      obj[v] = [1, v];
    }
  });
  return obj;
};
var uncommonFromSentences = function (s1, s2) {
  const obj1 = helper(s1);
  const obj2 = helper(s2);

  const res = [];
  Object.values(obj1).forEach((v) => {
    if (v[0] === 1 && !obj2[v[1]]) res.push(v[1]);
  });
  Object.values(obj2).forEach((v) => {
    if (v[0] === 1 && !obj1[v[1]]) res.push(v[1]);
  });
  return res;
};
// @lc code=end

// console.log(uncommonFromSentences('this apple is sweet', 'this apple is sour'));
// console.log(uncommonFromSentences('apple apple', 'banana'));
