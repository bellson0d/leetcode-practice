/*
 * @lc app=leetcode.cn id=961 lang=javascript
 *
 * [961] 在长度 2N 的数组中找出重复 N 次的元素
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var repeatedNTimes = function (nums) {
  let set = new Set();
  for (const n of nums) {
    if (set.has(n)) return n;
    set.add(n);
  }
};
// @lc code=end
