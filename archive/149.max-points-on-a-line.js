/*
 * @lc app=leetcode id=149 lang=javascript
 *
 * [149] Max Points on a Line
 */

// @lc code=start
/**
 * @param {number[][]} points
 * @return {number}
 */
var getSlope = (p1, p2) => {
  const deno = p2[0] - p1[0];
  if (deno === 0) return 'deno';
  return (p2[1] - p1[1]) / deno;
};

var maxPoints = function (points) {
  const len = points.length;
  if (len < 3) return len;

  points.sort((a, b) => a[0] - b[0]);
  let obj = {},
    max = 2;

  for (let i = 0; i < len - 1; i++) {
    let p1 = points[i],
      slopArr = {};
    for (let j = i + 1; j < len; j++) {
      const p2 = points[j];
      const slope = getSlope(p1, p2);
      const k1 = p1.join('-') + slope;
      const k2 = p2.join('-') + slope;
      if (obj[k2]) {
        continue;
      } else {
        obj[k1] = true;
        obj[k2] = true;
      }

      if (slopArr[slope]) {
        slopArr[slope].push(k2);
      } else {
        slopArr[slope] = [k1, k2];
      }
    }
    const tmp = Math.max(...Object.values(slopArr).map((v) => v.length));
    if (tmp > len / 2) return tmp;
    if (tmp > max) max = tmp;
  }

  return max;
};
// @lc code=end

// console.log(
//   maxPoints([
//     [1, 1],
//     [2, 2],
//     [3, 3],
//   ])
// ); // 3
// console.log(
//   maxPoints([
//     [1, 1],
//     [3, 2],
//     [5, 3],
//     [4, 1],
//     [2, 3],
//     [1, 4],
//   ])
// ); // 4
// console.log(
//   maxPoints([
//     [0, 1],
//     [0, 0],
//     [0, 4],
//     [0, -2],
//     [0, -1],
//     [0, 3],
//     [0, -4],
//   ])
// ); // 7
