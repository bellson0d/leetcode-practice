/*
 * @lc app=leetcode id=73 lang=javascript
 *
 * [73] Set Matrix Zeroes
 */

// @lc code=start
/**
 * @param {number[][]} matrix
 * @return {void} Do not return anything, modify matrix in-place instead.
 */
var setZeroes = function (matrix) {
  const len = matrix.length;
  const len2 = matrix[0].length;
  if (len === 1 && len2 === 1) return matrix;

  let xArr = [],
    yArr = [];

  for (let i = 0; i < len; i++) {
    for (let j = 0; j < len2; j++) {
      const ele = matrix[i][j];
      if (ele === 0) {
        if (!xArr.includes(i)) xArr.push(i);
        if (!yArr.includes(j)) yArr.push(j);
      }
    }
  }

  for (let i = 0; i < len; i++) {
    for (let j = 0; j < len2; j++) {
      if (xArr.includes(i) || yArr.includes(j)) matrix[i][j] = 0;
    }
  }
};
// @lc code=end

// console.log(
//   setZeroes([
//     [1, 1, 1],
//     [1, 0, 1],
//     [1, 1, 1],
//   ])
// );
// console.log(
//   setZeroes([
//     [0, 1, 2, 0],
//     [1, 1, 1, 1],
//     [1, 1, 1, 1],
//   ])
// );
