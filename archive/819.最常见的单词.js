/*
 * @lc app=leetcode.cn id=819 lang=javascript
 *
 * [819] 最常见的单词
 */

// @lc code=start
/**
 * @param {string} paragraph
 * @param {string[]} banned
 * @return {string}
 */
var mostCommonWord = function (paragraph, banned) {
  let obj1 = {},
    obj2 = {};

  for (const b of banned) {
    obj2[b] = true;
  }

  const arr = paragraph
    .toLowerCase()
    .replace(/,/g, ' ')
    .split(/ +/)
    .map((v) => {
      let str = '';
      for (const s of v) {
        const code = s.charCodeAt();
        if (code >= 97 && code <= 122) str += s;
      }
      return str;
    });

  for (const a of arr) {
    if (!obj1[a]) obj1[a] = [0, a];
    obj1[a][0]++;
  }

  const arr2 = Object.values(obj1).sort((a, b) => b[0] - a[0]);

  for (const a of arr2) {
    if (!obj2[a[1]]) return a[1];
  }
};
// @lc code=end

// console.log(
//   mostCommonWord('Bob hit a ball, the hit BALL flew far after it was hit.', [
//     'hit',
//   ])
// );
// console.log(mostCommonWord('a, a, a, a, b,b,b,c, c', ['b']));
