/*
 * @lc app=leetcode id=62 lang=javascript
 *
 * [62] Unique Paths
 */

// @lc code=start
/**
 * @param {number} m
 * @param {number} n
 * @return {number}
 */
var uniquePaths = function (m, n) {
  if (m <= 0 || n <= 0) return 0;
  if (m === 1 || n === 1) return 1;
  if (m === 2 && n === 2) return 2;

  let tmpArr = Array(m).fill(Array(n).fill(1));
  for (let i = 1; i < m; i++) {
    for (let j = 1; j < n; j++) {
      tmpArr[i][j] = tmpArr[i - 1][j] + tmpArr[i][j - 1];
    }
  }

  return tmpArr[m - 1][n - 1];
};
// @lc code=end
// module.exports = uniquePaths;

// console.log(uniquePaths(17, 13));
// console.log(uniquePaths(51, 9));
