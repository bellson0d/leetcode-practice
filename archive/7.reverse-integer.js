/*
 * @lc app=leetcode id=7 lang=javascript
 *
 * [7] Reverse Integer
 */

// @lc code=start
/**
 * @param {number} x
 * @return {number}
 */
var reverse = function (x) {
  const edge = 2147483647;
  let isNegative = x < 0;
  let str = String(x);
  if (isNegative) {
    str = str.slice(1);
  }
  let tmpStr = '';

  for (let i = str.length - 1; i >= 0; i--) {
    tmpStr += str[i];
  }

  const num = Number(tmpStr);
  if (num > edge) return 0;
  return isNegative ? -num : num;
};

// console.log(reverse(1534236469));
// @lc code=end
