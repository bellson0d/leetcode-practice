function ListNode(val) {
  this.val = val;
  this.next = null;
}

const getArr = (d) => (Array.isArray(d) ? d : [d]);
const alignmentArr = (arr1, arr2) => {
  const len1 = arr1.length;
  const len2 = arr2.length;
  const diff = Math.abs(len1 - len2);

  if (diff === 0) return [arr1, arr2];

  if (len1 > len2) return [arr1, arr2.concat(Array(diff).fill(0))];

  return [arr1.concat(Array(diff).fill(0), arr2)];
};

var addTwoNumbers = function (a, b) {
  let arr1 = getArr(a);
  let arr2 = getArr(b);
  [arr1, arr2] = alignmentArr(arr1, arr2);

  let carry = 0;
  let nodes = arr1.map((v, i) => {
    const sum = v + arr2[i] + carry;
    carry = Math.floor(sum / 10);
    return new ListNode(sum % 10);
  });

  if (carry !== 0) {
    nodes = nodes.concat([new ListNode(carry)]);
  }

  nodes.map((v, i) => {
    v.next = nodes[i + 1] || null;
    return v;
  });

  return nodes[0];
};

let node = addTwoNumbers([2, 4, 3], [5, 6, 4]);
while (node.next) {
  console.log(node.val);
  node = node.next;
}
console.log(node.val);
