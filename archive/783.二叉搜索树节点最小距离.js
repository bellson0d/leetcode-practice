/*
 * @lc app=leetcode.cn id=783 lang=javascript
 *
 * [783] 二叉搜索树节点最小距离
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */

var minDiffInBST = function (root) {
  let min = Infinity;

  var helper = (node) => {
    const res = [node.val, node.val];
    if (node.left) {
      const [_, max] = helper(node.left);
      if (max) {
        const tmp = Math.abs(node.val - max);
        min = Math.min(min, tmp);
        if (min === 1) return [];
        res[0] = _;
      }
    }

    if (node.right) {
      const [min2, _] = helper(node.right);
      if (min2) {
        const tmp = Math.abs(node.val - min2);
        min = Math.min(min, tmp);
        if (min === 1) return [];
        res[1] = _;
      }
    }
    return res;
  };
  helper(root);

  return min;
};
// @lc code=end
