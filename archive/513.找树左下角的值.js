/*
 * @lc app=leetcode.cn id=513 lang=javascript
 *
 * [513] 找树左下角的值
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
var findBottomLeftValue = function (tree) {
  let arr = [tree],
    tmp = [];

  while (arr.length) {
    for (const n of arr) {
      if (n.left) tmp.push(n.left);
      if (n.right) tmp.push(n.right);
    }
    if (tmp.length) {
      arr = tmp.slice();
      tmp = [];
    } else {
      break;
    }
  }

  return arr[0].val;
};
// @lc code=end
