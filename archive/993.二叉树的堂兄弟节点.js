/*
 * @lc app=leetcode.cn id=993 lang=javascript
 *
 * [993] 二叉树的堂兄弟节点
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} x
 * @param {number} y
 * @return {boolean}
 */
var isCousins = function (root, x, y) {
  const nodes = [[root, 1, null]];
  let xL = null,
    yL = null;

  while (nodes.length) {
    const [node, level, parent] = nodes.pop();

    if (node.val === x) xL = [level, parent];
    if (node.val === y) yL = [level, parent];
    if (xL && yL) return xL[0] === yL[0] && xL[1] !== yL[1];

    if (node.left) nodes.unshift([node.left, level + 1, node.val]);
    if (node.right) nodes.unshift([node.right, level + 1, node.val]);
  }
};
// @lc code=end
