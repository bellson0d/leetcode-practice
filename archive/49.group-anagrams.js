/*
 * @lc app=leetcode id=49 lang=javascript
 *
 * [49] Group Anagrams
 */

// @lc code=start
/**
 * @param {string[]} strs
 * @return {string[][]}
 */
var groupAnagrams = function (strs) {
  const len = strs.length;
  if (len === 0) return [];
  if (len === 1) return [strs];

  let obj = {};
  let count = 0;
  while (count < len) {
    const ele = strs[count];
    const tmpStr = strs[count]
      .split('')
      .sort((a, b) => a.charCodeAt() - b.charCodeAt());

    if (obj[tmpStr]) {
      obj[tmpStr].push(ele);
    } else {
      obj[tmpStr] = [ele];
    }

    count++;
  }

  return Object.values(obj);
};
// @lc code=end

// console.log(groupAnagrams(['eat', 'tea', 'tan', 'ate', 'nat', 'bat']));
