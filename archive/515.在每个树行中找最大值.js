/*
 * @lc app=leetcode.cn id=515 lang=javascript
 *
 * [515] 在每个树行中找最大值
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
var largestValues = function (tree) {
  if (!tree) return [];
  let arr = [tree],
    tmp = [];
  const res = [];

  while (arr.length) {
    res.push(Math.max(...arr.map((v) => v.val)));
    for (const n of arr) {
      if (n.left) tmp.push(n.left);
      if (n.right) tmp.push(n.right);
    }

    if (tmp.length) {
      arr = tmp.slice();
      tmp = [];
    } else {
      break;
    }
  }

  return res;
};
// @lc code=end
