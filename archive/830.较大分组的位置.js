/*
 * @lc app=leetcode.cn id=830 lang=javascript
 *
 * [830] 较大分组的位置
 */

// @lc code=start
/**
 * @param {string} s
 * @return {number[][]}
 */
var largeGroupPositions = function (s) {
  const res = [];
  let pre = [s[0], 0, 1];
  for (let i = 1; i < s.length; i++) {
    const ele = s[i];
    if (ele === pre[0]) {
      pre[2]++;
    } else {
      if (pre[2] > 2) res.push([pre[1], pre[1] + pre[2] - 1]);
      pre = [ele, i, 1];
    }
  }
  if (pre[2] > 2) res.push([pre[1], pre[1] + pre[2] - 1]);

  return res;
};
// @lc code=end

// console.log(largeGroupPositions('abcdddeeeeaabbbcd'));
// console.log(largeGroupPositions('aba'));
// console.log(largeGroupPositions('abc'));
// console.log(largeGroupPositions('abbxxxxzzy'));
// console.log(largeGroupPositions('aaa'));
