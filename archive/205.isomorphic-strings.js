/*
 * @lc app=leetcode id=205 lang=javascript
 *
 * [205] Isomorphic Strings
 */

// @lc code=start
/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */
var isIsomorphic = function (s, t) {
    let obj1 = {}, obj2 = {};
    const len = s.length;
    if (len !== t.length) return false;
    if (len < 2) return true;

    for (let i = 0; i < len; i++) {
        const ele1 = s[i];
        const ele2 = t[i];
        if (!obj1[ele1] && !obj2[ele2]) {
            obj1[ele1] = ele2;
            obj2[ele2] = ele1;
        } else if(obj1[ele1] !== ele2 || obj2[ele2] !== ele1){
            return false;
        }
    }
    return true;
};
// @lc code=end
// console.log(isIsomorphic("egg", "add"));
// console.log(isIsomorphic("foo", "bar"));
// console.log(isIsomorphic("paper", "title"));
// console.log(isIsomorphic("badc", "baba"));
