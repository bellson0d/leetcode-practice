/*
 * @lc app=leetcode id=234 lang=javascript
 *
 * [234] Palindrome Linked List
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {boolean}
 */
var isPalindrome = function (head) {
    let arr = [],
        node = head;
    while (node) {
        arr.push(node.val);
        node = node.next;
    }
    const len = arr.length;
    if (len < 2) return true;
    let idx = 0;
    while (idx < len / 2) {
        if (arr[idx] !== arr[len - idx - 1]) return false;
        idx++;
    }
    return true;
};
// @lc code=end
