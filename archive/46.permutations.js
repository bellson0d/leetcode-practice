/*
 * @lc app=leetcode id=46 lang=javascript
 *
 * [46] Permutations
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number[][]}
 */
const helpFn = (nums, arr) => {
  const len = nums.length;
  let tmpArr = [];

  if (len === 0) return arr;

  if (arr.length === 0) {
    for (let i = 0; i < len; i++) {
      const ele = nums[i];
      const newNums = nums.slice(0, i).concat(nums.slice(i + 1));
      tmpArr = tmpArr.concat(helpFn(newNums, [[ele]]));
    }
    return tmpArr;
  }

  if (len === 1) {
    const ele = nums[0];
    return arr.map((v) => v.concat([ele]));
  } else {
    for (let i = 0; i < len; i++) {
      const ele = nums[i];
      const newNums = nums.slice(0, i).concat(nums.slice(i + 1));
      tmpArr = tmpArr.concat(
        helpFn(
          newNums,
          arr.map((v) => v.concat([ele]))
        )
      );
    }
  }

  return tmpArr;
};

var permute = function (nums) {
  return helpFn(nums, []);
};
// @lc code=end

// console.log(permute([1, 2, 3]));
