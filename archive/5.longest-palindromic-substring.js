/*
 * @lc app=leetcode id=5 lang=javascript
 *
 * [5] Longest Palindromic Substring
 */

// @lc code=start
/**
 * @param {string} s
 * @return {string}
 */

function validatePalindromic(s) {
  const len = s.length;
  const halfLen = len / 2;

  for (let i = 0; i < halfLen; i++) {
    const head = s[i];
    const tail = s[len - i - 1];
    // console.log(head, tail, s, i, len);
    if (head !== tail) {
      return false;
    }
  }

  return true;
}

var longestPalindrome = function (s) {
  if (s.length < 2) return s;
  let len = s.length;

  while (len > 0) {
    for (let i = 0; i <= s.length - len; i++) {
      const ele = s.slice(i, len + i);
      if (validatePalindromic(ele)) {
        return ele;
      }
    }
    len--;
  }

  return null;
};

module.exports = longestPalindrome;

// @lc code=end
