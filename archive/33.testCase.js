const targetFun = require('./33.search-in-rotated-sorted-array');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  [[[4, 5, 6, 7, 0, 1, 2], 0], 4],
  [[[4, 5, 6, 7, 0, 1, 2], 3], -1],
  [[[1, 2, 3, 4, 0], 4], 3],
  [[[6, 7, 8, 1, 2, 3], 3], 5],
  [[[6, 7, 8, 1, 2, 3], 7], 1],
  [[[], 7], -1],
];
let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input[0], input[1]);
  if (result !== output) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
