/*
 * @lc app=leetcode.cn id=693 lang=javascript
 *
 * [693] 交替位二进制数
 */

// @lc code=start
/**
 * @param {number} n
 * @return {boolean}
 */
var hasAlternatingBits = function (n) {
  let pre = null;
  for (const num of n.toString(2)) {
    if (pre && pre === num) return false;
    pre = num;
  }
  return true;
};
// @lc code=end
// console.log(hasAlternatingBits(5));
// console.log(hasAlternatingBits(7));
// console.log(hasAlternatingBits(11));
// console.log(hasAlternatingBits(10));
// console.log(hasAlternatingBits(3));
