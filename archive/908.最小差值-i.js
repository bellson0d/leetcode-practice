/*
 * @lc app=leetcode.cn id=908 lang=javascript
 *
 * [908] 最小差值 I
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number}
 */
var smallestRangeI = function (nums, k) {
  const len = nums.length;
  if (len === 1) return 0;

  nums.sort((a, b) => a - b);

  const min = nums[0];
  const max = nums[len - 1];
  const range = max - min;

  const k2 = 2 * k;
  if (range <= k2) return 0;
  return range - k2;
};
// @lc code=end

// console.log(smallestRangeI([2, 7, 2], 1));
// console.log(smallestRangeI([1, 9, 17, 30], 3));
