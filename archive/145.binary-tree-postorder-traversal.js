/*
 * @lc app=leetcode id=145 lang=javascript
 *
 * [145] Binary Tree Postorder Traversal
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
var postorderTraversal = function (root) {
  if (!root) return [];
  let res = [],
    tmpArr = [root];

  while (tmpArr.length > 0) {
    const currentNode = tmpArr[0];
    const left = currentNode.left || { visited: true };
    const right = currentNode.right || { visited: true };

    if (right.visited && left.visited) {
      res.push(currentNode.val);
      currentNode.visited = true;
      tmpArr.shift();
    } else {
      if (!right.visited) tmpArr.unshift(right);
      if (!left.visited) tmpArr.unshift(left);
    }
  }

  return res;
};
// @lc code=end
// module.exports = postorderTraversal;
// const { getTree } = require('./utils/treeNode');
// postorderTraversal(getTree([4, 1, 2, null, null, 5, 3]));
