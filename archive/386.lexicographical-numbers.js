/*
 * @lc app=leetcode id=386 lang=javascript
 *
 * [386] Lexicographical Numbers
 */

// @lc code=start
/**
 * @param {number} n
 * @return {number[]}
 */
var lexicalOrder = function (n) {
  return Array(n)
    .fill(0)
    .map((v, i) => String(i + 1))
    .sort()
    .map(Number);
};
// @lc code=end

// console.log(lexicalOrder(13));
// console.log(lexicalOrder(2));
// console.log(lexicalOrder(1));
// console.log(lexicalOrder(100));
