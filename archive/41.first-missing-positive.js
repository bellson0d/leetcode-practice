/*
 * @lc app=leetcode id=41 lang=javascript
 *
 * [41] First Missing Positive
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var firstMissingPositive = function (nums) {
  let idx = 1,
    tmpArr = [],
    count = 0;
  const len = nums.length;

  while (count < len) {
    const ele = nums[count];
    if (!tmpArr[ele]) {
      tmpArr[ele] = ele;
    }

    let ele2 = ele;
    while (ele2 === idx) {
      idx++;
      ele2 = tmpArr[idx];
    }
    count++;
  }

  return idx;
};
// @lc code=end

module.exports = firstMissingPositive;
