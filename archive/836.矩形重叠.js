/*
 * @lc app=leetcode.cn id=836 lang=javascript
 *
 * [836] 矩形重叠
 */

// @lc code=start
/**
 * @param {number[]} rec1
 * @param {number[]} rec2
 * @return {boolean}
 */
var helper = (rec1, rec2) => {
  const x1 = rec1[0],
    x2 = rec1[2],
    y1 = rec1[1],
    y2 = rec1[3];
  const x3 = rec2[0],
    x4 = rec2[2],
    y3 = rec2[1],
    y4 = rec2[3];

  if (x2 > x3 && y1 < y4 && x2 <= x4 && y1 >= y3) return true;
  if (x1 < x4 && y1 < y4 && x1 >= x3 && y1 >= y3) return true;

  if (x1 < x4 && y2 > y3 && x1 >= x3 && y2 <= y4) return true;
  if (x2 > x3 && y2 > y3 && x2 <= x4 && y2 <= y4) return true;

  if (x1 > x3 && x1 < x4 && y1 < y3 && y2 > y4) return true;
  if (y1 > y3 && y1 < y4 && x1 < x3 && x2 > x4) return true;
  return false;
};
var isRectangleOverlap = function (rec1, rec2) {
  return helper(rec1, rec2) || helper(rec2, rec1);
};
// @lc code=end

// console.log(isRectangleOverlap([0, 0, 2, 2], [1, 1, 3, 3])); // tree
// console.log(isRectangleOverlap([0, 0, 1, 1], [1, 0, 2, 1])); // f
// console.log(isRectangleOverlap([0, 0, 1, 1], [2, 2, 3, 3])); // f
// console.log(isRectangleOverlap([7, 8, 13, 15], [10, 8, 12, 20])); // t
// console.log(isRectangleOverlap([0, 0, 6, 7], [3, 0, 5, 12])); // t
// console.log(isRectangleOverlap([-7, -3, 10, 5], [-6, -5, 5, 10])); // t
