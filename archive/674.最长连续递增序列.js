/*
 * @lc app=leetcode.cn id=674 lang=javascript
 *
 * [674] 最长连续递增序列
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var findLengthOfLCIS = function (nums) {
  const len = nums.length;

  let max = 1,
    tmp = 1;
  for (let i = 1; i < len; i++) {
    if (nums[i] > nums[i - 1]) {
      tmp++;
    } else {
      max = Math.max(max, tmp);
      tmp = 1;
    }
  }
  max = Math.max(max, tmp);
  return max;
};
// @lc code=end

// console.log(findLengthOfLCIS([1, 3, 5, 4, 7]));
// console.log(findLengthOfLCIS([2, 2, 2, 2, 2]));
