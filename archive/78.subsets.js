/*
 * @lc app=leetcode id=78 lang=javascript
 *
 * [78] Subsets
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number[][]}
 */
var subsets = function (nums) {
  if (nums.length === 0) return [];
  let result = [];
  let result2 = [];

  const helpFn = (str) => {
    let tmpArr = [];
    for (let i = 0; i < str.length; i++) {
      const newStr = str.slice(0, i).concat(str.slice(i + 1));
      const newArr = newStr.join('|');
      if (newStr.length > 0) {
        if (!result2.includes(newArr)) {
          result.push(newStr);
          result2.push(newArr);
          tmpArr = tmpArr.concat(helpFn(newStr));
        }
      }
    }
    return tmpArr;
  };
  return [nums, []].concat(result.concat(helpFn(nums)));
};
// @lc code=end

// console.log(subsets([1]));
// console.log(subsets([1, 2, 3]));
// console.log(subsets([1, 2, 3, 4]));
// console.log(subsets([-1, 1, 2]));
