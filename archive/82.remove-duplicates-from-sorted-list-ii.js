/*
 * @lc app=leetcode id=82 lang=javascript
 *
 * [82] Remove Duplicates from Sorted List II
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
var deleteDuplicates = function (head) {
  if (!head || !head.next) return head;

  let current = head,
    tmpArr = [];

  while (current) {
    if (!current.next || current.val !== current.next.val) {
      tmpArr.push(current);
      current = current.next;
    } else {
      current = current.next;
      while (current.next && current.val === current.next.val) {
        current = current.next;
      }
      current = current.next;
    }
  }

  let count = 0,
    len = tmpArr.length;
  if (len === 0) return null;
  while (count < len) {
    tmpArr[count].next = tmpArr[count + 1] || null;
    count++;
  }

  return tmpArr[0];
};
// @lc code=end

// function ListNode(val, next) {
//   this.val = val === undefined ? 0 : val;
//   this.next = next === undefined ? null : next;
// }

// let arr = [];
// let count = 1;
// while (count < 3) {
//   const node = new ListNode(count);
//   const node2 = new ListNode(count);
//   arr.push(node);
//   arr[count - 1].next = node2;
//   if (count < 2) {
//     arr.push(node2);
//   }
//   count++;
// }

// // let result = arr[0];
// let result = deleteDuplicates(arr[0]);
// while (result) {
//   console.log(result.val);
//   result = result.next;
// }
