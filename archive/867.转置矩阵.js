/*
 * @lc app=leetcode.cn id=867 lang=javascript
 *
 * [867] 转置矩阵
 */

// @lc code=start
/**
 * @param {number[][]} matrix
 * @return {number[][]}
 */
var transpose = function (matrix) {
  const y = matrix.length;
  const x = matrix[0].length;
  const arr = Array(x)
    .fill()
    .map((v) => []);

  for (let i = 0; i < y; i++) {
    for (let j = 0; j < x; j++) {
      arr[j][i] = matrix[i][j];
    }
  }
  return arr;
};
// @lc code=end

// console.log(
//   transpose([
//     [1, 2, 3],
//     [4, 5, 6],
//     [7, 8, 9],
//   ])
// );
// console.log(
//   transpose([
//     [1, 2, 3],
//     [4, 5, 6],
//   ])
// );
