/*
 * @lc app=leetcode id=111 lang=javascript
 *
 * [111] Minimum Depth of Binary Tree
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
var minDepth = function (root) {
  if (!root) return 0;

  let tmpArr = [[1, root]],
    dep = 1;

  while (tmpArr.length > 0) {
    const [count, currentNode] = tmpArr.shift();

    if (!currentNode.left && !currentNode.right) {
      dep = count;
      break;
    }
    if (currentNode.left) {
      dep = count + 1;
      tmpArr.push([count + 1, currentNode.left]);
    }
    if (currentNode.right) {
      dep = count + 1;
      tmpArr.push([count + 1, currentNode.right]);
    }
  }

  return dep;
};
// @lc code=end

// const { getTree } = require('./utils/treeNode');
// console.log(minDepth(getTree([])));
// console.log(minDepth(getTree([1])));
// console.log(minDepth(getTree([1, 2, 3])));
// console.log(minDepth(getTree([3, 9, 20, null, null, 15, 7])));
// console.log(minDepth(getTree([1, 2, 3, 4, 5, 6, 7])));
// console.log(minDepth(getTree([1, 2, 3, 4, 5, 6, 7, 8, 9])));
// console.log(minDepth(getTree([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])));
