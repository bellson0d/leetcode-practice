/*
 * @lc app=leetcode id=347 lang=javascript
 *
 * [347] Top K Frequent Elements
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */
var topKFrequent = function (nums, k) {
  let obj = {};
  for (const n of nums) {
    if (!obj[n]) {
      obj[n] = [1, n];
    } else {
      obj[n][0]++;
    }
  }
  return Object.values(obj)
    .sort((a, b) => b[0] - a[0])
    .slice(0, k)
    .map((v) => v[1]);
};
// @lc code=end

// console.log(topKFrequent([1, 1, 1, 2, 2, 3], 2));
// console.log(topKFrequent([1, 1, 1, 2, 2, 3], 3));
// console.log(topKFrequent([1], 1));
// console.log(topKFrequent([1, 2, 2, 3], 1));
