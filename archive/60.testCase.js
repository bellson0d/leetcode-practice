const targetFun = require('./60.permutation-sequence');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  [[3, 2], '132'],
  [[3, 3], '213'],
  [[4, 9], '2314'],
  [[8, 27891], '64731528'],
  [[9, 353955], '972561438'],
  [[9, 2], '123456798'],
  [[4, 6], '1432'],
];

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input[0], input[1]);
  if (result !== output) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
