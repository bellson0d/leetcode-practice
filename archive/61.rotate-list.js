/*
 * @lc app=leetcode id=61 lang=javascript
 *
 * [61] Rotate List
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} k
 * @return {ListNode}
 */

var rotateRight = function (head, k) {
  if (!head) return null;
  let pointer = head,
    tmpArr = [];

  while (pointer) {
    tmpArr.push(pointer);
    pointer = pointer.next;
  }
  const len = tmpArr.length;
  if (len === 1 || k === 0 || k % len === 0) return head;

  const sliceIdx = len - (k % len);
  tmpArr = tmpArr.slice(sliceIdx).concat(tmpArr.slice(0, sliceIdx));

  let first = tmpArr[0],
    current = tmpArr[0];
  for (let i = 1; i < len; i++) {
    const ele = tmpArr[i];
    current.next = ele;
    current = ele;
  }
  current.next = null;
  return first;
};

// function ListNode(val, next) {
//   this.val = val === undefined ? 0 : val;
//   this.next = next === undefined ? null : next;
// }

// let arr = [new ListNode(1)];
// let arr2 = [new ListNode(4)];
// let count = 1;
// while (count < 5) {
//   const node = new ListNode(count + 1);
//   const val = count + 4 < 6 ? count + 4 : count - 1;
//   const node2 = new ListNode(val);
//   arr[count - 1].next = node;
//   arr2[count - 1].next = node2;
//   arr.push(node);
//   arr2.push(node2);
//   count++;
// }

// let result = arr2[0];
// let res = rotateRight(arr[0], 6);
// while (res) {
//   if (res.val !== result.val) {
//     console.log('Wrong!', res.val, result.val);
//     break;
//   }
//   console.log(res.val);
//   res = res.next;
//   result = result.next;
// }
// @lc code=end
