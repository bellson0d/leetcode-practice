/*
 * @lc app=leetcode id=287 lang=javascript
 *
 * [287] Find the Duplicate Number
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var findDuplicate = function (nums) {
  const obj = new Set();
  for (const n of nums) {
    if (obj.has(n)) return n;
    obj.add(n);
  }
};
// @lc code=end
