/*
 * @lc app=leetcode.cn id=563 lang=javascript
 *
 * [563] 二叉树的坡度
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
var helper = (node) => {
  if (!node) return [0, 0];
  if (!node.left && !node.right) return [node.val, 0];

  if (node.left && node.right) {
    const [sum1, po1] = helper(node.left);
    const [sum2, po2] = helper(node.right);
    return [sum1 + sum2 + node.val, Math.abs(sum1 - sum2) + po1 + po2];
  }
  if (node.left) {
    const [sum, po] = helper(node.left);
    return [sum + node.val, Math.abs(sum) + po];
  } else {
    const [sum, po] = helper(node.right);
    return [sum + node.val, Math.abs(sum) + po];
  }
};
var findTilt = function (root) {
  return helper(root)[1];
};
// @lc code=end
module.exports = findTilt;
