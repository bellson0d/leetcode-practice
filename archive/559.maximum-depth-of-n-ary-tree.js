/*
 * @lc app=leetcode id=559 lang=javascript
 *
 * [559] Maximum Depth of N-ary Tree
 */

// @lc code=start
/**
 * // Definition for a Node.
 * function Node(val,children) {
 *    this.val = val;
 *    this.children = children;
 * };
 */

/**
 * @param {Node|null} root
 * @return {number}
 */
var maxDepth = function (root) {
  if (!root) return 0;

  let max = 0,
    nodes = [[root, 0]];
  while (nodes.length) {
    let [node, pre] = nodes.pop();
    pre++;
    max = Math.max(max, pre);

    const { children } = node;
    children.forEach((n) => {
      nodes.push([n, pre]);
    });
  }
  return max;
};
// @lc code=end
// module.exports = maxDepth;
