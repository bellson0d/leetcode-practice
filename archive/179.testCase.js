const targetFun = require('./179.largest-number');
const IS_ARRAY_RESULT = false; // 输出是否为数组
const MULTI_ARGS = false; // 输入参数是否为多个

const testStr = [
  [[10, 2], '210'],
  [[3, 30, 34, 5, 9], '9534330'],
  [[1], '1'],
  [[10], '10'],
  [[22, 2], '222'],
  [[24, 2], '242'],
  [[24, 24], '2424'],
  [[8999, 9], '98999'],
  [[1, 2, 3, 4, 5, 6, 7], '7654321'],
  [[0, 1], '10'],
  [[0, 1, 2], '210'],
  [[0, 1, 20], '2010'],
  [[0, 2, 23], '2320'],
  [[0, 0, 0, 1, 1], '11000'],
  [[111311, 1113], '1113111311'],
  [[111311, 111300], '111311111300'],
  [[0, 0], '0'],
];

const time1 = new Date().getTime();
console.log(time1);

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  let result;
  if (MULTI_ARGS) {
    result = targetFun(...input);
  } else {
    result = targetFun(input);
  }

  if (
    IS_ARRAY_RESULT ? result.join('|') !== output.join('|') : result !== output
  ) {
    console.log('Number ' + count + ' Case Wrong !!!');
    console.log('Input: ', input);
    console.log('Expected output: ', output);
    console.log('Output', result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
