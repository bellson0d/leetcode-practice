/*
 * @lc app=leetcode id=200 lang=javascript
 *
 * [200] Number of Islands
 */

// @lc code=start
/**
 * @param {character[][]} grid
 * @return {number}
 */
var numIslands = function (grid) {
  const m = grid.length,
    n = grid[0].length;
  let count = 0,
    arr = [];
  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      const ele = grid[i][j];
      if (ele === -1) continue;
      if (ele === '1') {
        count++;
        arr.push([i, j]);
        while (arr.length > 0) {
          const [x, y] = arr.pop();
          grid[x][y] = -1;
          if (x + 1 < m && grid[x + 1][y] === '1') arr.push([x + 1, y]);
          if (y + 1 < n && grid[x][y + 1] === '1') arr.push([x, y + 1]);
          if (x - 1 > -1 && grid[x - 1][y] === '1') arr.push([x - 1, y]);
          if (y - 1 > -1 && grid[x][y - 1] === '1') arr.push([x, y - 1]);
        }
      }
    }
  }
  return count;
};
// @lc code=end
// module.exports = numIslands;
