/*
 * @lc app=leetcode id=148 lang=javascript
 *
 * [148] Sort List
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
// Operator O(1) just use linked property feature
// Time nlog(n) need use extra array structure and binary divide sort method
// TLTW just copy 147 solved submit :(
var sortList = function (head) {
  if (!head || !head.next) return head;
  let idx = 1,
    preNode = head,
    tailNode = head.next.next,
    currentNode = head.next;

  do {
    let count = 0,
      h = head,
      p = null;
    while (count < idx) {
      if (currentNode.val < h.val) {
        if (p) {
          p.next = currentNode;
        } else {
          head = currentNode;
        }
        currentNode.next = h;
        preNode.next = tailNode;
        currentNode = tailNode;
        tailNode = tailNode ? tailNode.next : null;
        idx++;
        break;
      } else if (count === idx - 1) {
        preNode = currentNode;
        currentNode = currentNode.next;
        tailNode = currentNode ? currentNode.next : null;
        idx++;
        break;
      } else {
        p = h;
        h = h.next;
      }
      count++;
    }
  } while (currentNode);

  return head;
};
// @lc code=end
