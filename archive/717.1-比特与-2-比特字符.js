/*
 * @lc app=leetcode.cn id=717 lang=javascript
 *
 * [717] 1比特与2比特字符
 */

// @lc code=start
/**
 * @param {number[]} bits
 * @return {boolean}
 */
var isOneBitCharacter = function (bits) {
  if (bits[bits.length - 1] !== 0) return false;
  let pre = -1;
  for (let i = 0; i < bits.length - 1; i++) {
    if (pre === -1) {
      if (bits[i] === 1) pre = 1;
    } else {
      pre = -1;
    }
  }
  return pre === -1;
};
// @lc code=end
// console.log(isOneBitCharacter([1, 1, 1, 0]));
