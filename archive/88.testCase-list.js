const targetFun = require('./88.merge-sorted-array');
const { getList, isSameList, logList } = require('./utils/listNode');

const testStr = [
  [
    [getList([1, 2, 3, 0, 0, 0]), 3, getList([2, 5, 6]), 3],
    getList([1, 2, 2, 3, 5, 6]),
  ],
  [[getList([0, 0, 0]), 0, getList([1, 2, 3]), 3], getList([1, 2, 3])],
  [[getList([5, 0, 0, 0]), 1, getList([2, 5, 6]), 3], getList([2, 5, 5, 6])],
];

const time1 = new Date().getTime();
console.log(time1);

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(...input);

  logList(result);
  if (!isSameList(result, output, true)) {
    console.log('Wrong !');
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
