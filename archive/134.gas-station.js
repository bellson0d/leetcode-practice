/*
 * @lc app=leetcode id=134 lang=javascript
 *
 * [134] Gas Station
 */

// @lc code=start
/**
 * @param {number[]} gas
 * @param {number[]} cost
 * @return {number}
 */
var canCompleteCircuit = function (gas, cost) {
  let idx = 0,
    len = gas.length;
  while (idx < len) {
    let base = gas[idx],
      count = 0,
      flag = false;
    while (count < len) {
      let i = idx + count;
      if (i >= len) {
        i = i - len;
      }
      base -= cost[i];
      if (base < 0) {
        flag = true;
        break;
      }
      base += i === len - 1 ? gas[0] : gas[i + 1];
      count++;
    }
    if (!flag) return idx;

    idx++;
  }

  return -1;
};
// @lc code=end

// console.log(canCompleteCircuit([1, 2, 3, 4, 5], [3, 4, 5, 1, 2])); // 3
// console.log(canCompleteCircuit([2, 3, 4], [3, 4, 3])); // -1
