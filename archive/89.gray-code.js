/*
 * @lc app=leetcode id=89 lang=javascript
 *
 * [89] Gray Code
 */

// @lc code=start
/**
 * @param {number} n
 * @return {number[]}
 */

var grayCode = function (n) {
  if (n === 0) return [0];
  if (n === 1) return [0, 1];
  if (n === 2) return [0, 1, 3, 2];

  let count = 2,
    preArr = ['00', '01', '11', '10'];

  while (count < n) {
    const arr1 = preArr.slice().map((v) => '0' + v);
    const arr2 = preArr.reverse().map((v) => '1' + v);
    preArr = arr1.concat(arr2);

    count++;
  }
  return preArr.map((v) => parseInt(v, 2));
};
// @lc code=end
// console.log(grayCode(0));
// console.log(grayCode(1));
// console.log(grayCode(2));
// console.log(grayCode(3));
// console.log(grayCode(4));
