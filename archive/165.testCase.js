const targetFun = require('./165.compare-version-numbers');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  [['1.01', '1.001'], 0],
  [['1.0', '1.0.0'], 0],
  [['0.1', '1.1'], -1],
  [['1.0.1', '1'], 1],
  [['7.5.2.4', '7.5.3'], -1],
  [['7.5.2.4', '7.5.2.3.1'], 1],
];

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input[0], input[1]);
  if (result !== output) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
