/*
 * @lc app=leetcode id=345 lang=javascript
 *
 * [345] Reverse Vowels of a String
 */

// @lc code=start
/**
 * @param {string} s
 * @return {string}
 */
const map = {
  a: true,
  e: true,
  i: true,
  o: true,
  u: true,
  A: true,
  E: true,
  I: true,
  O: true,
  U: true,
};
var reverseVowels = function (s) {
  let arr = [];
  return s
    .split('')
    .map((v) => {
      if (map[v]) {
        arr.push(v);
        return '*';
      }
      return v;
    })
    .map((v) => {
      if (v === '*') {
        return arr.pop();
      }
      return v;
    })
    .join('');
};
// @lc code=end

// console.log(reverseVowels('leetcode'));
// console.log(reverseVowels('OE'));
// console.log(reverseVowels('abc'));
// console.log(reverseVowels('a c'));
// console.log(reverseVowels('a'));
// console.log(reverseVowels('ab'));
// console.log(reverseVowels('abc'));
// console.log(reverseVowels('!!!'));
// console.log(reverseVowels('Euston saw I was not Sue.'));
