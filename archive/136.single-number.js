/*
 * @lc app=leetcode id=136 lang=javascript
 *
 * [136] Single Number
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var singleNumber = function (nums) {
  const len = nums.length;
  if (len === 1) return nums[0];

  let count = 0,
    obj = {};
  while (count < len) {
    const ele = nums[count];
    if (Number.isInteger(obj[ele])) {
      obj[ele] = 'no';
    } else {
      obj[ele] = ele;
    }
    count++;
  }
  return Object.values(obj).find((v) => v !== 'no');
};
// @lc code=end
// console.log(singleNumber([2, 2, 1]));
// console.log(singleNumber([4, 1, 2, 1, 2]));
// console.log(singleNumber([-1, -1, 5, 66, 66]));
// console.log(singleNumber([2]));
// console.log(singleNumber([99, 8, 7, 7, 8]));
