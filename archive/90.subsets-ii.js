/*
 * @lc app=leetcode id=90 lang=javascript
 *
 * [90] Subsets II
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number[][]}
 */
const dupMap = (arr) => {
  let obj = {},
    count = 0;
  while (count < arr.length) {
    const key = arr[count];
    if (obj[key]) {
      obj[key] = obj[key] + 1;
    } else {
      obj[key] = 1;
    }
    count++;
  }

  return [obj, Object.keys(obj).map(Number)];
};

var subsetsWithDup = function (preNums) {
  if (preNums.length === 0) return [];
  let result = [];
  let result2 = [];
  const [obj, nums] = dupMap(preNums);

  const helpFn = (str) => {
    let tmpArr = [];
    for (let i = 0; i < str.length; i++) {
      const newStr = str.slice(0, i).concat(str.slice(i + 1));
      const newArr = newStr.join('|');
      if (newStr.length > 0) {
        if (!result2.includes(newArr)) {
          result.push(newStr);
          result2.push(newArr);
          tmpArr = tmpArr.concat(helpFn(newStr));
        }
      }
    }
    return tmpArr;
  };
  const range = [nums, []].concat(result.concat(helpFn(nums)));

  if (!Object.values(obj).find((v) => v > 1)) return range;
  let res = [];
  for (let i = 0; i < range.length; i++) {
    const subArr = range[i];
    let tmpRes = [];

    for (let j = 0; j < subArr.length; j++) {
      const ele = subArr[j];
      if (obj[ele] === 1) {
        if (tmpRes.length > 0) {
          tmpRes = tmpRes.map((v) => v.concat(ele));
        } else {
          tmpRes = [[ele]];
        }
      } else {
        if (tmpRes.length > 0) {
          let tmpArr = [];
          let count = 0;
          while (count < obj[ele]) {
            tmpRes.forEach((v) =>
              tmpArr.push(v.concat(Array(count + 1).fill(ele)))
            );
            count++;
          }
          tmpRes = tmpArr;
        } else {
          let count = 0;
          while (count < obj[ele]) {
            tmpRes.push(Array(count + 1).fill(ele));
            count++;
          }
        }
      }
    }
    res = res.concat(tmpRes);
  }
  res.push([]);

  return res;
};

// @lc code=end
// console.log(subsetsWithDup([1, 2, 2]));
// console.log(subsetsWithDup([1, 2]));
// console.log(subsetsWithDup([0]));
