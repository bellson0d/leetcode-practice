/*
 * @lc app=leetcode id=326 lang=javascript
 *
 * [326] Power of Three
 */

// @lc code=start
/**
 * @param {number} n
 * @return {boolean}
 */
var baseMap = {};
Array(21)
  .fill(1)
  .forEach((v, i) => {
    const n = 3 ** i;
    baseMap[n] = true;
  });
var isPowerOfThree = function (n) {
  return baseMap.hasOwnProperty(n);
};
// @lc code=end
