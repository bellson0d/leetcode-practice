/*
 * @lc app=leetcode.cn id=989 lang=javascript
 *
 * [989] 数组形式的整数加法
 */

// @lc code=start
/**
 * @param {number[]} num
 * @param {number} k
 * @return {number[]}
 */
var addToArrayForm = function (num, k) {
  if (num.length < 15)
    return String(Number(num.join('')) + k)
      .split('')
      .map(Number);
  const arr = String(k).split('').map(Number);
  const lenK = arr.length;
  let pre = 0,
    j = lenK - 1;

  for (let i = num.length - 1; i >= 0; i--) {
    let sum = pre;
    pre = 0;

    if (j >= 0) {
      sum += num[i] + arr[j];
    } else {
      sum += num[i];
    }

    if (sum > 9) {
      num[i] = sum - 10;
      pre = 1;
    } else {
      num[i] = sum;
      if (j < 0) break;
    }

    j--;
  }

  return pre ? [1].concat(num) : num;
};
// @lc code=end

// console.log(addToArrayForm([1, 2, 0, 0], 34));
// console.log(addToArrayForm([2, 7, 4], 181));
// console.log(addToArrayForm([2, 1, 5], 806));
// console.log(addToArrayForm([9, 9, 9, 9, 9, 9, 9, 9, 9, 9], 1));
// console.log(addToArrayForm([0], 23));
