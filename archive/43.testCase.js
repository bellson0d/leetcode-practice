const targetFun = require('./43.multiply-strings');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  [['2', '3'], '6'],
  [['123', '456'], '56088'],
  [['123000', '456000'], '56088000000'],
  [['9999999', '9999999'], '99999980000001'],
  [['111111111111111111', '1000'], '111111111111111111000'],
  [['9999999000', '99999990'], '999999800000010000'],
  [['"0"', '332620029'], '0'],
];

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input[0], input[1]);
  if (result !== output) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
