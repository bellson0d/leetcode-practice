/*
 * @lc app=leetcode id=71 lang=javascript
 *
 * [71] Simplify Path
 */

// @lc code=start
/**
 * @param {string} path
 * @return {string}
 */

var simplifyPath = function (path) {
  const arr = path.split('/').filter((v) => v && v != '.');
  const tmpArr = [];

  let count = 0;
  while (count < arr.length) {
    if (arr[count] !== '..') {
      tmpArr.push(arr[count]);
    } else {
      tmpArr.pop();
    }
    count++;
  }

  return '/' + tmpArr.join('/');
};
// @lc code=end

// module.exports = simplifyPath;
// simplifyPath('/home/');
