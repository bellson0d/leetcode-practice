/*
 * @lc app=leetcode.cn id=762 lang=javascript
 *
 * [762] 二进制表示中质数个计算置位
 */

// @lc code=start
/**
 * @param {number} left
 * @param {number} right
 * @return {number}
 */
var prime = {
  2: true,
  3: true,
  5: true,
  7: true,
  11: true,
  13: true,
  17: true,
  19: true,
  23: true,
  29: true,
};
var countPrimeSetBits = function (left, right) {
  let count = 0;
  for (let i = left; i <= right; i++) {
    if (prime[i.toString(2).match(/1/g).length]) count++;
  }
  return count;
};
// @lc code=end

// console.log(countPrimeSetBits(6, 10));
// console.log(countPrimeSetBits(10, 15));
