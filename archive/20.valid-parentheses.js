/*
 * @lc app=leetcode id=20 lang=javascript
 *
 * [20] Valid Parentheses
 */

// @lc code=start
/**
 * @param {string} s
 * @return {boolean}
 */
const pair = {
  ')': '(',
  ']': '[',
  '}': '{',
  '(': 1,
  '[': 1,
  '{': 1,
};

var isValid = function (s) {
  const len = s.length;
  if (len % 2 !== 0) return false;

  let count = 0,
    arr = [];
  while (count < len) {
    const ele = s[count];
    const val = pair[ele];

    if (val === 1) {
      arr.push(ele);
    } else if (val !== arr.pop()) {
      return false;
    }
    count++;
  }

  return arr.length > 0 ? false : true;
};

// console.log(isValid('{}'));
// console.log(isValid('()[]{}'));
// console.log(isValid('{[]}'));
// console.log(isValid('(]'));
// console.log(isValid('([)]'));
// @lc code=end
