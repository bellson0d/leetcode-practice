/*
 * @lc app=leetcode id=133 lang=javascript
 *
 * [133] Clone Graph
 */

// @lc code=start
/**
 * // Definition for a Node.
 * function Node(val, neighbors) {
 *    this.val = val === undefined ? 0 : val;
 *    this.neighbors = neighbors === undefined ? [] : neighbors;
 * };
 */

/**
 * @param {Node} node
 * @return {Node}
 */
var cloneGraph = function (node) {
  if (!node) return node;
  let obj = {};

  var helper = (n) => {
    if (!obj[n.val]) {
      const newNode = new Node(n.val);
      obj[n.val] = newNode;
      newNode.neighbors = n.neighbors.map(helper);
      return newNode;
    } else {
      return obj[n.val];
    }
  };

  return helper(node);
};
// @lc code=end

// function Node(val, neighbors) {
//   this.val = val === undefined ? 0 : val;
//   this.neighbors = neighbors === undefined ? [] : neighbors;
// }

// const node1 = new Node(1);
// const node2 = new Node(2);
// node1.neighbors = [node2];
// node2.neighbors = [node1];
// console.log(node1);
// console.log(cloneGraph(node1));
