/*
 * @lc app=leetcode id=191 lang=javascript
 *
 * [191] Number of 1 Bits
 */

// @lc code=start
/**
 * @param {number} n - a positive integer
 * @return {number}
 */
var hammingWeight = function (n) {
  let idx = 0,
    count = 0;
  while (idx < 32) {
    count += n & 1;
    n >>>= 1;
    idx++;
  }
  return count;
};
// @lc code=end
