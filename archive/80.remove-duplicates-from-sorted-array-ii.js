/*
 * @lc app=leetcode id=80 lang=javascript
 *
 * [80] Remove Duplicates from Sorted Array II
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var removeDuplicates = function (nums) {
  const len = nums.length;
  if (len < 3) return len;

  let count = 1,
    offset = 0,
    tmp;
  while (count + offset < len - 1) {
    if (
      nums[count] === nums[count - 1] &&
      nums[count] === nums[count + offset + 1]
    ) {
      while (nums[count] === nums[count + offset + 1]) {
        offset++;
      }
    }
    tmp = nums[count + 1];
    nums[count + 1] = nums[count + offset + 1];
    nums[count + offset + 1] = tmp;

    count++;
  }

  return len - offset;
};
// @lc code=end
// let input = [0, 0, 1, 1, 1, 1, 2, 3, 3];
// let input = [1, 1, 1, 2, 2, 3];
// console.log(removeDuplicates(input), input);
