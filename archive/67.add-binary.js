/*
 * @lc app=leetcode id=67 lang=javascript
 *
 * [67] Add Binary
 */

// @lc code=start
/**
 * @param {string} a
 * @param {string} b
 * @return {string}
 */
var addBinary = function (a, b) {
  if (a.length === 0) return b;
  if (b.length === 0) return a;

  let count = 0,
    addOne = 0,
    result = [];

  const arr1 = a.split('').reverse();
  const arr2 = b.split('').reverse();
  let val1 = arr1[count] || 0;
  let val2 = arr2[count] || 0;

  while (val1 || val2) {
    const res = ~~val1 + ~~val2 + addOne;
    if (res > 1) {
      result.push(res > 2 ? 1 : 0);
      addOne = 1;
    } else {
      result.push(res);
      addOne = 0;
    }
    count++;
    val1 = arr1[count] || 0;
    val2 = arr2[count] || 0;
  }
  if (addOne) result.push(1);

  return result.reverse().join('');
};
// @lc code=end

// console.log(addBinary('1010', '1011'));
// console.log(addBinary('101010101', ''));
// console.log(addBinary('100100100100', '111111110000'));
