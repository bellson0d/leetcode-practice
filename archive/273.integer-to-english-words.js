/*
 * @lc app=leetcode id=273 lang=javascript
 *
 * [273] Integer to English Words
 */

// @lc code=start
/**
 * @param {number} num
 * @return {string}
 */
var arr1 = [
  [1, 'One'],
  [2, 'Two'],
  [3, 'Three'],
  [4, 'Four'],
  [5, 'Five'],
  [6, 'Six'],
  [7, 'Seven'],
  [8, 'Eight'],
  [9, 'Nine'],
  [10, 'Ten'],
  [11, 'Eleven'],
  [12, 'Twelve'],
  [13, 'Thirteen'],
  [14, 'Fourteen'],
  [15, 'Fifteen'],
  [16, 'Sixteen'],
  [17, 'Seventeen'],
  [18, 'Eighteen'],
  [19, 'Nineteen'],
  [20, 'Twenty'],
  [30, 'Thirty'],
  [40, 'Forty'],
  [50, 'Fifty'],
  [60, 'Sixty'],
  [70, 'Seventy'],
  [80, 'Eighty'],
  [90, 'Ninety'],
];

var baseMap = {
  0: '',
  1: 'One',
  2: 'Two',
  3: 'Three',
  4: 'Four',
  5: 'Five',
  6: 'Six',
  7: 'Seven',
  8: 'Eight',
  9: 'Nine',
  10: 'Ten',
  11: 'Eleven',
  12: 'Twelve',
  13: 'Thirteen',
  14: 'Fourteen',
  15: 'Fifteen',
  16: 'Sixteen',
  17: 'Seventeen',
  18: 'Eighteen',
  19: 'Nineteen',
  20: 'Twenty',
  30: 'Thirty',
  40: 'Forty',
  50: 'Fifty',
  60: 'Sixty',
  70: 'Seventy',
  80: 'Eighty',
  90: 'Ninety',
};

var arr2 = [
  [1000000000, 'Billion'],
  [1000000, 'Million'],
  [1000, 'Thousand'],
  [100, 'Hundred'],
];

var baseMap2 = {
  100: 'Hundred',
  1000: 'Thousand',
  1000000: 'Million',
  1000000000: 'Billion',
};

var numberToWords = function (num) {
  if (!num) return 'Zero';
  if (num < 21) return baseMap[num];
  if (baseMap[num]) return baseMap[num];

  if (num < 100) return baseMap[~~(num / 10) * 10] + ' ' + baseMap[num % 10];

  const [n, str] = arr2.find((v) => num >= v[0]);
  const left = num % n;

  return (
    numberToWords(~~(num / n)) +
    ' ' +
    str +
    (left ? ' ' + numberToWords(left) : '')
  );
};
// @lc code=end

// console.log(numberToWords(19));
// console.log(numberToWords(25));
// console.log(numberToWords(76));
// console.log(numberToWords(123)); // "One Hundred Twenty Three"
// console.log(numberToWords(12345)); // "Twelve Thousand Three Hundred Forty Five"
// console.log(numberToWords(1234567));
// // "One Million Two Hundred Thirty Four Thousand Five Hundred Sixty Seven"
// console.log(numberToWords(1234567891));
// // "One Billion Two Hundred Thirty Four Million Five Hundred Sixty Seven Thousand Eight Hundred Ninety One"
// console.log(numberToWords(100));
// // ""One Hundred""
// console.log(numberToWords(0));
