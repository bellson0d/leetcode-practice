/*
 * @lc app=leetcode id=289 lang=javascript
 *
 * [289] Game of Life
 */

// @lc code=start
/**
 * @param {number[][]} board
 * @return {void} Do not return anything, modify board in-place instead.
 */
var helper = (x, y, board) => {
  let count = 0;

  if (board[x - 1] !== undefined) {
    if (board[x - 1][y] !== undefined) count += board[x - 1][y];
    if (board[x - 1][y - 1] !== undefined) count += board[x - 1][y - 1];
    if (board[x - 1][y + 1] !== undefined) count += board[x - 1][y + 1];
  }

  if (board[x + 1] !== undefined) {
    if (board[x + 1][y] !== undefined) count += board[x + 1][y];
    if (board[x + 1][y - 1] !== undefined) count += board[x + 1][y - 1];
    if (board[x + 1][y + 1] !== undefined) count += board[x + 1][y + 1];
  }

  if (board[x][y - 1] !== undefined) count += board[x][y - 1];
  if (board[x][y + 1] !== undefined) count += board[x][y + 1];

  if (board[x][y]) {
    if (count > 3 || count < 2) return 0;
    return 1;
  } else {
    if (count === 3) return 1;
    return 0;
  }
};

var gameOfLife = function (board) {
  const m = board.length;
  const n = board[0].length;

  const res = [];
  for (let i = 0; i < m; i++) {
    const row = [];
    for (let j = 0; j < n; j++) {
      row.push(helper(i, j, board));
    }
    res.push(row);
  }

  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      board[i][j] = res[i][j];
    }
  }
  return board;
};
// @lc code=end

// console.log(
//   gameOfLife([
//     [0, 1, 0],
//     [0, 0, 1],
//     [1, 1, 1],
//     [0, 0, 0],
//   ])
// );
// [
//   [0, 0, 0],
//   [1, 0, 1],
//   [0, 1, 1],
//   [0, 1, 0],
// ];
// console.log(
//   gameOfLife([
//     [1, 1],
//     [1, 0],
//   ])
// );
// [
//   [1, 1],
//   [1, 1],
// ];
// console.log(gameOfLife([[1], [0], [0], [1], [0], [0], [1], [0], [0], [1]]));
