const targetFun = require('./34.find-first-and-last-position-of-element-in-sorted-array');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  [
    [[5, 7, 7, 8, 8, 10], 8],
    [3, 4],
  ],
  [
    [[5, 7, 7, 8, 8, 10], 6],
    [-1, -1],
  ],
  [
    [[5, 7, 7, 8, 8, 10], 2],
    [-1, -1],
  ],
  [
    [[5, 7, 7, 8, 8, 10], 7],
    [1, 2],
  ],
  [
    [[5, 7, 7, 8, 8, 8, 10], 8],
    [3, 5],
  ],
];
let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input[0], input[1]);
  if (result.join('') !== output.join('')) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
