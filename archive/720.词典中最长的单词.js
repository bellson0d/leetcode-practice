/*
 * @lc app=leetcode.cn id=720 lang=javascript
 *
 * [720] 词典中最长的单词
 */

// @lc code=start
/**
 * @param {string[]} words
 * @return {string}
 */
var longestWord = function (words) {
  let obj = {},
    obj2 = {};
  for (const s of words) {
    obj[s] = true;
    if (obj2[s.length]) {
      obj2[s.length].push(s);
    } else {
      obj2[s.length] = [s];
    }
  }

  return Object.values(obj2)
    .concat([['']])
    .sort((a, b) => b[0].length - a[0].length)
    .map((arr) =>
      arr.filter((str) => {
        for (let i = 1; i < str.length + 1; i++) {
          const s = str.slice(0, i);
          if (!obj[s]) return false;
        }
        return true;
      })
    )
    .filter((v) => v.length)[0]
    .sort((a, b) => (a > b ? 1 : -1))[0];
};
// @lc code=end

// console.log(longestWord(['w', 'wo', 'wor', 'worl', 'world']));
// console.log(
//   longestWord(['a', 'banana', 'app', 'appl', 'ap', 'apply', 'apple'])
// );
// console.log(longestWord(['bb', 'cc']));
