/*
 * @lc app=leetcode id=35 lang=javascript
 *
 * [35] Search Insert Position
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
function findIdx(arr, val, addN) {
  const len = arr.length;
  if (len === 1) {
    if (arr[0] === val || arr[0] > val) return addN;
    if (arr[0] < val) return addN + len;
  }

  if (len === 0) {
    return addN;
  }

  const middleIdx = ~~(len / 2);
  const middle = arr[middleIdx];

  if (middle === val) return addN + middleIdx;
  if (middle > val) {
    return findIdx(arr.slice(0, middleIdx), val, addN);
  } else {
    return findIdx(arr.slice(middleIdx + 1), val, addN + middleIdx + 1);
  }
}

var searchInsert = function (nums, target) {
  return findIdx(nums, target, 0);
};
// @lc code=end
