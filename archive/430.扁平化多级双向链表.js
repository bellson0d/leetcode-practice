/*
 * @lc app=leetcode.cn id=430 lang=javascript
 *
 * [430] 扁平化多级双向链表
 */

// @lc code=start
/**
 * // Definition for a Node.
 * function Node(val,prev,next,child) {
 *    this.val = val;
 *    this.prev = prev;
 *    this.next = next;
 *    this.child = child;
 * };
 */

/**
 * @param {Node} head
 * @return {Node}
 */
var flatten = function (head) {
  if (!head) return head;
  let resHead = new Node(null, null, null, null),
    currentNode = resHead,
    tmp = [head];

  while (tmp.length) {
    const node = tmp.pop();
    currentNode.val = node.val;

    if (!node.next && !node.child && !tmp.length) break;
    const newNode = new Node(null, currentNode, null, null);
    currentNode.next = newNode;
    currentNode = newNode;

    if (node.next) tmp.push(node.next);
    if (node.child) tmp.push(node.child);
  }

  return resHead;
};
// @lc code=end
