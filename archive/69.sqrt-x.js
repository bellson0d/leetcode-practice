/*
 * @lc app=leetcode id=69 lang=javascript
 *
 * [69] Sqrt(x)
 */

// @lc code=start
/**
 * @param {number} x
 * @return {number}
 */
const baseMap = {
  0: 0,
  1: 1,
  2: 1,
  3: 1,
  4: 2,
  5: 2,
  6: 2,
  7: 2,
  8: 2,
  9: 3,
};

var mySqrt = function (x) {
  if (x === 0) return 0;
  if (x === 1) return 1;
  if (x < 4) return 1;
  if (x < 9) return 2;
  if (x < 16) return 3;
  if (x < 25) return 4;
  if (x < 36) return 5;
  if (x < 49) return 6;
  if (x < 64) return 7;
  if (x < 81) return 8;
  if (x < 100) return 9;
  if (x < 121) return 10;

  let current = 11,
    pre;

  while (current ** 2 <= x) {
    pre = current;
    current *= 2;
  }

  if (current - pre > 1) {
    while (current ** 2 > x) {
      current--;
    }
    return current;
  }

  return current - 1;
};
// @lc code=end
