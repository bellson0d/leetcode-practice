/*
 * @lc app=leetcode id=50 lang=javascript
 *
 * [50] Pow(x, n)
 */

// @lc code=start
/**
 * @param {number} x
 * @param {number} n
 * @return {number}
 */
var myPow = function (x, n) {
  let precision = 5;

  const result = x ** n;
  if (!String(result).includes('.')) {
    return result + '.' + '0'.repeat(precision);
  }
  return (x ** n).toFixed(precision);
};
// @lc code=end
// console.log(myPow(2.0, 10));
