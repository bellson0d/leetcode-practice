/*
 * @lc app=leetcode id=198 lang=javascript
 *
 * [198] House Robber
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var rob = function (nums) {
  const len = nums.length;
  if (len < 2) return Math.max(nums[0] || 0, nums[1] || 0);
  let count = 2;
  while (count < len) {
    nums[count] =
      Math.max(...nums.slice(count - 3 < 0 ? 0 : count - 3, count - 1)) +
      nums[count];
    count++;
  }
  return Math.max(nums[count - 2], nums[count - 1]);
};
// @lc code=end
// module.exports = rob;
// rob([1, 2, 3, 1]);
