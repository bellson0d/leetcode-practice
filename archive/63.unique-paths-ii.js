/*
 * @lc app=leetcode id=63 lang=javascript
 *
 * [63] Unique Paths II
 */

// @lc code=start
/**
 * @param {number[][]} obstacleGrid
 * @return {number}
 */
var uniquePathsWithObstacles = function (obstacleGrid) {
  const m = obstacleGrid.length;
  const n = obstacleGrid[0].length;
  if (m <= 0 || n <= 0) return 0;
  if (m === 1) return obstacleGrid[0].find(Boolean) ? 0 : 1;
  if (n === 1) return obstacleGrid.map((v) => v[0]).find(Boolean) ? 0 : 1;
  if (obstacleGrid[0][0] === 1 || obstacleGrid[m - 1][n - 1] === 1) return 0;

  let flag = 1;
  for (let j = 0; j < n; j++) {
    if (obstacleGrid[0][j] === 1) {
      obstacleGrid[0][j] = -1;
      flag = 0;
      continue;
    }
    obstacleGrid[0][j] = flag;
  }

  for (let i = 1; i < m; i++) {
    for (let j = 0; j < n; j++) {
      if (obstacleGrid[i][j] === 1) {
        obstacleGrid[i][j] = -1;
        continue;
      }
      const ele1 = obstacleGrid[i - 1][j] === -1 ? 0 : obstacleGrid[i - 1][j];
      const ele2 =
        j === 0
          ? 0
          : obstacleGrid[i][j - 1] === -1
          ? 0
          : obstacleGrid[i][j - 1];
      obstacleGrid[i][j] = ele1 + ele2;
    }
  }
  return obstacleGrid[m - 1][n - 1];
};
// @lc code=end
// module.exports = uniquePathsWithObstacles;
