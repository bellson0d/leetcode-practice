/*
 * @lc app=leetcode id=739 lang=javascript
 *
 * [739] Daily Temperatures
 */

// @lc code=start
/**
 * @param {number[]} temperatures
 * @return {number[]}
 */
var dailyTemperatures = function (tmp) {
  const len = tmp.length;
  let res = [],
    pre = [tmp[len - 1]];
  res[len - 1] = 0;

  for (let i = len - 2; i >= 0; i--) {
    const ele = tmp[i];
    res[i] = 0;

    let idx = 0;
    while (idx < pre.length) {
      if (pre[idx] > ele) {
        res[i] = idx + 1;
        break;
      }
      idx++;
    }
    pre.unshift(ele);
  }

  return res;
};
// @lc code=end

// console.log(dailyTemperatures([73, 74, 75, 71, 69, 72, 76, 73]));
