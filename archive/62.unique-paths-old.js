/*
 * @lc app=leetcode id=62 lang=javascript
 *
 * [62] Unique Paths
 */

// @lc code=start
/**
 * @param {number} m
 * @param {number} n
 * @return {number}
 */
var uniquePaths = function (m, n) {
  if (m <= 0 || n <= 0) return 0;
  if (m === 1 || n === 1) return 1;
  if (m === 2 && n === 2) return 2;

  let count = 0,
    tmpArr = [[0, 0]];

  const helpFn = (x, y) => {
    if (x < m - 1 && y < n - 1) {
      tmpArr.push([x + 1, y]);
      tmpArr.push([x, y + 1]);
    } else {
      count++;
    }
  };

  while (tmpArr.length > 0) {
    const args = tmpArr.pop();
    helpFn(...args);
  }

  return count;
};
// @lc code=end
module.exports = uniquePaths;

console.log(uniquePaths(17, 13));
// console.log(uniquePaths(51, 9));
