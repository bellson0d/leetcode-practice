const targetFun = require('./17.letter-combinations-of-a-phone-number');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  ['23', ['ad', 'ae', 'af', 'bd', 'be', 'bf', 'cd', 'ce', 'cf']],
];
let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(...input);
  if (result !== output) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
