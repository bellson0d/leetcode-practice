const targetFun = require('./144.binary-tree-preorder-traversal');
const { getTree } = require('./utils/treeNode');
const IS_ARRAY_RESULT = true; // 输出是否为数组
const MULTI_ARGS = false; // 输入参数是否为多个

const testStr = [
  [getTree([]), []],
  [getTree([1]), [1]],
  [getTree([1, 2]), [1, 2]],
  [getTree([1, 2, 3]), [1, 2, 3]],
  [getTree([1, 2, 3, 4, 5, 6]), [1, 2, 4, 5, 3, 6]],
  [getTree([1, null, 2, null, null, 3]), [1, 2, 3]],
  [getTree([1, null, 2]), [1, 2]],
  [getTree([4, 1, 2, null, null, 5, 3]), [4, 1, 2, 5, 3]],
];

const time1 = new Date().getTime();
console.log(time1);

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];

  let result;
  if (MULTI_ARGS) {
    result = targetFun(...input);
  } else {
    result = targetFun(input);
  }

  if (
    IS_ARRAY_RESULT ? result.join('|') !== output.join('|') : result !== output
  ) {
    console.log('Wrong !!!');
    console.log('Expected output: ', output);
    console.log('Output', result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
