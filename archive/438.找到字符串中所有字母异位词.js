/*
 * @lc app=leetcode.cn id=438 lang=javascript
 *
 * [438] 找到字符串中所有字母异位词
 */

// @lc code=start
/**
 * @param {string} s
 * @param {string} p
 * @return {number[]}
 */
var getObj = () => {
  let obj = {};
  Array(26)
    .fill(1)
    .map((_, i) => i + 97)
    .forEach((v) => (obj[String.fromCharCode(v)] = 0));
  return obj;
};
var findAnagrams = function (s, p) {
  const len = p.length;
  let obj1 = getObj(),
    obj2 = getObj();

  for (const a of p) {
    obj2[a]++;
  }

  for (const a of s.slice(0, len)) {
    obj1[a]++;
  }

  let res = [],
    i = 0;
  while (i < s.length - len + 1) {
    if (JSON.stringify(obj1) === JSON.stringify(obj2)) res.push(i);
    obj1[s[i]]--;
    obj1[s[i + len]]++;
    i++;
  }

  return res;
};
// @lc code=end

// console.log(findAnagrams('cbaebabacd', 'abc'));
// console.log(findAnagrams('abab', 'ab'));
