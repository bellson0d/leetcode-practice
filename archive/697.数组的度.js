/*
 * @lc app=leetcode.cn id=697 lang=javascript
 *
 * [697] 数组的度
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var findShortestSubArray = function (nums) {
  let obj = {},
    max = 1;
  for (let i = 0; i < nums.length; i++) {
    const n = nums[i];
    if (obj[n]) {
      obj[n][3]++;
      obj[n][2] = i - obj[n][1] + 1;
      if (obj[n][3] > max) max = obj[n][3];
    } else {
      obj[n] = [n, i, 1, 1];
    }
  }

  return Object.values(obj)
    .filter((v) => v[3] === max)
    .sort((a, b) => a[2] - b[2])[0][2];
};
// @lc code=end

// console.log(findShortestSubArray([1, 2, 2, 3, 1]));
// console.log(findShortestSubArray([1, 2, 2, 3, 1, 4, 2]));
// console.log(findShortestSubArray([1]));
