/*
 * @lc app=leetcode id=135 lang=javascript
 *
 * [135] Candy
 */

// @lc code=start
/**
 * @param {number[]} ratings
 * @return {number}
 */
var candy = function (ratings) {
  const len = ratings.length;
  let obj = {},
    arr = new Set();
  for (let i = 0; i < len; i++) {
    const key = ratings[i];
    if (obj[key]) {
      obj[key].push(i);
    } else {
      obj[key] = [i];
    }
    arr.add(key);
  }
  arr = Array.from(arr).sort((a, b) => a - b);

  let res = Array(len).fill(1);
  for (let i = 0; i < arr.length; i++) {
    const ele = arr[i];
    const tmp = obj[ele];

    for (const t of tmp) {
      const val = ratings[t];
      let left = t - 1,
        right = t + 1;

      if (left > -1) {
        const leftVal = ratings[left];
        const leftCount = res[left];
        if (val > leftVal) {
          if (!res[t] || res[t] <= leftCount) {
            res[t] = leftCount + 1;
          }
        }
      }

      if (right < len) {
        const rightVal = ratings[right];
        const rightCount = res[right];
        if (val > rightVal) {
          if (!res[t] || res[t] <= rightCount) {
            res[t] = rightCount + 1;
          }
        }
      }
    }
  }

  return res.reduce((a, b) => a + b, 0);
};
// @lc code=end

// console.log(candy([1, 0, 2])); // 5
// console.log(candy([1, 2, 2])); // 4
// console.log(candy([1, 1, 1])); // 3
// console.log(candy([1, 2, 3])); // 6
// console.log(candy([1, 2, 3, 2, 1])); // 9
// console.log(candy([1, 2, 1, 2, 1])); // 7
// console.log(candy([1, 10, 2, 1, 8])); // 9
