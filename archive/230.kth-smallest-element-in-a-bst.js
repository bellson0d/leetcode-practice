/*
 * @lc app=leetcode id=230 lang=javascript
 *
 * [230] Kth Smallest Element in a BST
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} k
 * @return {number}
 */
var kthSmallest = function (root, k) {
    let minNode = root,
        tmpArr = [root];
    while (minNode.left) {
        tmpArr.push(minNode);
        minNode = minNode.left;
    }

    let arr = [];
    while (arr.length < k) {
        if (minNode.left && !minNode.left.visited) {
            tmpArr.push(minNode);
            minNode = minNode.left;
        } else {
            arr.unshift(minNode.val);
            minNode.visited = true;
            if (minNode.right) {
                minNode = minNode.right;
            } else {
                while (minNode && minNode.visited) {
                    minNode = tmpArr.pop();
                }
            }
        }
    }
    return arr[0];
};
// @lc code=end
// module.exports = kthSmallest;
// const { getTree } = require("./utils/treeNode");
// console.log(kthSmallest(getTree([3, 1, 4, null, 2]), 1))
// console.log(kthSmallest(getTree([5, 3, 6, 2, 4, null, null, 1]), 3))
// console.log(kthSmallest(getTree([1, null, 2]), 2))
// console.log(kthSmallest(getTree([1, null, 2, null, null, null, 3]), 2))
// console.log(kthSmallest(getTree([1, null, 2, null, null, null, 3]), 3))
