/*
 * @lc app=leetcode.cn id=925 lang=javascript
 *
 * [925] 长按键入
 */

// @lc code=start
/**
 * @param {string} name
 * @param {string} typed
 * @return {boolean}
 */
var isLongPressedName = function (name, typed) {
  if (name === typed) return true;
  const len1 = name.length,
    len2 = typed.length;

  let idx = 0,
    pre = null;
  for (let i = 0; i < len1; i++) {
    let flag = false;
    const ele = name[i];
    while (idx < len2) {
      if (ele === typed[idx]) {
        pre = ele;
        idx++;
        flag = true;
        break;
      } else if (!pre || typed[idx] !== pre) return false;
      idx++;
    }
    if (!flag) return false;
  }

  while (idx < len2) {
    if (name[len1 - 1] !== typed[idx]) return false;
    idx++;
  }

  return true;
};
// @lc code=end

// console.log(isLongPressedName('alex', 'aaleex'));
// console.log(isLongPressedName('saeed', 'ssaaedd'));
// console.log(isLongPressedName('leelee', 'lleeelee'));
// console.log(isLongPressedName('laiden', 'laiden'));
// console.log(isLongPressedName('alex', 'aaleexa'));
// console.log(isLongPressedName('alex', 'aaleelx'));
