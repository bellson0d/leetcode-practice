/*
 * @lc app=leetcode id=208 lang=javascript
 *
 * [208] Implement Trie (Prefix Tree)
 */

// @lc code=start
/**
 * Initialize your data structure here.
 */
var Trie = function () {
    this.obj = {};
    this.obj._ = {};
    return this;
};

/**
 * Inserts a word into the trie.
 * @param {string} word
 * @return {void}
 */
Trie.prototype.insert = function (word) {
    let current = this.obj;
    this.obj._[word] = true;

    for (let i = 0; i < word.length; i++) {
        const ele = word[i];
        if (!current[ele]) {
            current[ele] = {};
        }
        current = current[ele];
    }
};

/**
 * Returns if the word is in the trie.
 * @param {string} word
 * @return {boolean}
 */
Trie.prototype.search = function (word) {
    return !!this.obj._[word];
};

/**
 * Returns if there is any word in the trie that starts with the given prefix.
 * @param {string} prefix
 * @return {boolean}
 */
Trie.prototype.startsWith = function (prefix) {
    let current = this.obj;
    for (let i = 0; i < prefix.length; i++) {
        const ele = prefix[i];
        if (!current[ele]) return false
        current = current[ele];
    }
    return true;
};

/**
 * Your Trie object will be instantiated and called as such:
 * var obj = new Trie()
 * obj.insert(word)
 * var param_2 = obj.search(word)
 * var param_3 = obj.startsWith(prefix)
 */
// @lc code=end

// const trie = new Trie();
// trie.insert("apple");
// console.log(trie.search("apple"));   // return True
// console.log(trie.search("app"));     // return False
// console.log(trie.startsWith("app")); // return True
// trie.insert("app");
// trie.search("app");

// const trie = new Trie();
// console.log(trie.startsWith("a")); // return True
