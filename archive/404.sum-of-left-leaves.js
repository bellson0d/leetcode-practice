/*
 * @lc app=leetcode id=404 lang=javascript
 *
 * [404] Sum of Left Leaves
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
var sumOfLeftLeaves = function (root) {
  let leftSet = new Set([]),
    nodes = [root],
    baseMap = [];
  idx = 0;
  root.idx = idx;
  baseMap[idx] = root.val;
  while (nodes.length) {
    const node = nodes.pop();
    if (node.left) {
      idx++;
      node.left.idx = idx;
      baseMap[idx] = node.left.val;
      leftSet.delete(node.idx);
      leftSet.add(node.left.idx);
      nodes.push(node.left);
    }

    if (node.right) {
      idx++;
      node.right.idx = idx;
      baseMap[idx] = node.right.val;
      leftSet.delete(node.idx);
      nodes.push(node.right);
    }
  }
  return Array.from(leftSet)
    .map((i) => baseMap[i])
    .reduce((a, b) => a + b, 0);
};
// @lc code=end

// const { getTree } = require('./utils/treeNode.js');

// console.log(sumOfLeftLeaves(getTree([3, 9, 20, null, null, 15, 7]))); // 24
// console.log(sumOfLeftLeaves(getTree([1]))); // 0
// console.log(sumOfLeftLeaves(getTree([1, null, 2]))); // 0
// console.log(sumOfLeftLeaves(getTree([1, null, 2, null, null, 3]))); // 3
// console.log(
//   sumOfLeftLeaves(
//     getTree([
//       -1,
//       -7,
//       9,
//       null,
//       null,
//       -1,
//       -7,
//       null,
//       null,
//       null,
//       null,
//       null,
//       8,
//       -9,
//     ])
//   )
// ); // -16
