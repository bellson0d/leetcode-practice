/*
 * @lc app=leetcode id=387 lang=javascript
 *
 * [387] First Unique Character in a String
 */

// @lc code=start
/**
 * @param {string} s
 * @return {number}
 */
var firstUniqChar = function (s) {
  let obj = {};
  for (let i = 0; i < s.length; i++) {
    const key = s[i];
    if (!obj[key]) {
      obj[key] = [1, i];
    } else {
      obj[key][0] = obj[key][0] + 1;
    }
  }
  const res = Object.values(obj).filter((v) => v[0] === 1);
  if (res.length === 0) return -1;
  return res.sort((a, b) => a[1] - b[1])[0][1];
};
// @lc code=end

// console.log(firstUniqChar('leetcode'));
// console.log(firstUniqChar('loveleetcode'));
// console.log(firstUniqChar('aabb'));
