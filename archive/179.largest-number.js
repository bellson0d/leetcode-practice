/*
 * @lc app=leetcode id=179 lang=javascript
 *
 * [179] Largest Number
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {string}
 */
var largestNumber = function (nums) {
  let res = nums
    .map(String)
    .sort((a, b) => {
      const str1 = a + b;
      const str2 = b + a;
      for (let i = 0; i < a.length + b.length; i++) {
        const ele1 = str1[i];
        const ele2 = str2[i];
        if (ele1 < ele2) return 1;
        if (ele1 > ele2) return -1;
      }
      return true;
    })
    .join('');
  return res[0] === '0' ? '0' : res;
};
// @lc code=end
// module.exports = largestNumber;
