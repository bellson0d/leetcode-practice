/*
 * @lc app=leetcode id=113 lang=javascript
 *
 * [113] Path Sum II
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} sum
 * @return {number[][]}
 */
var pathSum = function (root, sum) {
  if (!root) return [];
  if (!root.left && !root.right) return root.val === sum ? [[root.val]] : [];

  let tmpArr = [[[], 0, root]],
    res = [];

  while (tmpArr.length > 0) {
    const [preArr, pre, currentNode] = tmpArr.shift();

    if (!currentNode.left && !currentNode.right) {
      if (pre + currentNode.val === sum)
        res.push(preArr.concat([currentNode.val]));
    } else {
      if (currentNode.left)
        tmpArr.unshift([
          preArr.concat([currentNode.val]),
          pre + currentNode.val,
          currentNode.left,
        ]);
      if (currentNode.right)
        tmpArr.unshift([
          preArr.concat([currentNode.val]),
          pre + currentNode.val,
          currentNode.right,
        ]);
    }
  }

  return res;
};
// @lc code=end

// const { getTree } = require('./utils/treeNode');
// console.log(pathSum(getTree([]), 1));
// console.log(pathSum(getTree([1]), 1));
// console.log(pathSum(getTree([1, 2, 3]), 5));
// console.log(pathSum(getTree([3, 9, 20, null, null, 15, 7]), 30));
// console.log(pathSum(getTree([1, 2, 3, 4, 5, 6, 7]), 8));
// console.log(
//   pathSum(
//     getTree([5, 4, 8, 11, null, 13, 4, 7, 2, null, null, null, null, 5, 1]),
//     22
//   )
// );
