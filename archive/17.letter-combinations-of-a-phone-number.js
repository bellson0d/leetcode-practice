/*
 * @lc app=leetcode id=17 lang=javascript
 *
 * [17] Letter Combinations of a Phone Number
 */

// @lc code=start
/**
 * @param {string} digits
 * @return {string[]}
 */

const strMap = {
  2: ['a', 'b', 'c'],
  3: ['d', 'e', 'f'],
  4: ['g', 'h', 'i'],
  5: ['j', 'k', 'l'],
  6: ['m', 'n', 'o'],
  7: ['p', 'q', 'r', 's'],
  8: ['t', 'u', 'v'],
  9: ['w', 'x', 'y', 'z'],
};

var letterCombinations = function (digits) {
  const digitsArr = digits.split('');
  const len = digitsArr.length;
  let currentArr = strMap[digitsArr[0]] || [];

  if (len < 2) return currentArr;

  for (let i = 1; i < len; i++) {
    const arr2 = strMap[digitsArr[i]];
    let tmpArr = [];

    for (let j = 0; j < currentArr.length; j++) {
      const str1 = currentArr[j];
      for (let k = 0; k < arr2.length; k++) {
        const str2 = arr2[k];

        tmpArr.push(str1 + str2);
      }
    }
    currentArr = tmpArr;
  }

  return currentArr;
};

console.log(letterCombinations('234'));

// @lc code=end
