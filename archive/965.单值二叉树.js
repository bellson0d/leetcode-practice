/*
 * @lc app=leetcode.cn id=965 lang=javascript
 *
 * [965] 单值二叉树
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {boolean}
 */
var isUnivalTree = function (root) {
  const nodes = [root],
    baseVal = root.val;
  while (nodes.length) {
    const node = nodes.pop();
    if (node.val !== baseVal) return false;
    if (node.left) nodes.push(node.left);
    if (node.right) nodes.push(node.right);
  }
  return true;
};
// @lc code=end
