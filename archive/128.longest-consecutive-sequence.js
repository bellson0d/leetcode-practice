/*
 * @lc app=leetcode id=128 lang=javascript
 *
 * [128] Longest Consecutive Sequence
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var longestConsecutive = function (nums) {
  const len = nums.length;

  if (len < 2) return len;

  let obj = {};
  let obj2 = {};
  let count = 0;
  while (count < len) {
    const ele = nums[count];
    if (ele < 0) {
      obj2[-ele] = ele;
    } else {
      obj[ele] = ele;
    }
    count++;
  }
  const keys1 = Object.values(obj);
  const keys2 = Object.values(obj2).reverse();
  const keys = keys2.concat(keys1);

  let length = 1,
    tmpLen = 1;
  count = 0;
  while (count < len) {
    if (keys[count] + 1 === keys[count + 1]) {
      tmpLen++;
    } else {
      length = Math.max(length, tmpLen);
      tmpLen = 1;
    }
    count++;
  }

  return length;
};
// @lc code=end

// console.log(longestConsecutive([100, 4, 200, 1, 3, 2]));
// console.log(longestConsecutive([100, 4, 200, 1, 2]));
// console.log(longestConsecutive([100, 4, 200, 5, 6, 1, 2]));
// console.log(longestConsecutive([]));
// console.log(longestConsecutive([1]));
// console.log(longestConsecutive([1, 1, 2]));
// console.log(longestConsecutive([0, -1]));
// console.log(longestConsecutive([-3, -1, -2]));
