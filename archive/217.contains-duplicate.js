/*
 * @lc app=leetcode id=217 lang=javascript
 *
 * [217] Contains Duplicate
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {boolean}
 */
var containsDuplicate = function(nums) {
    let obj = {};
    for (let i = 0; i < nums.length; i++) {
        const ele = nums[i];
        if (obj[ele]) return true;
        obj[ele] = true;
    }
    return false;
};
// @lc code=end

