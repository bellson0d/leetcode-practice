/*
 * @lc app=leetcode id=389 lang=javascript
 *
 * [389] Find the Difference
 */

// @lc code=start
/**
 * @param {string} s
 * @param {string} t
 * @return {character}
 */
var helper = (s) => {
  let obj = {};
  for (let i = 0; i < s.length; i++) {
    const key = s[i];
    if (!obj[key]) {
      obj[key] = [1, key];
    } else {
      obj[key][0] = obj[key][0] + 1;
    }
  }
  return obj;
};

var findTheDifference = function (s, t) {
  if (!s) return t;
  const obj1 = helper(s);
  const obj2 = helper(t);

  return Object.keys(obj2).find((v) => !obj1[v] || obj2[v][0] !== obj1[v][0]);
};
// @lc code=end

// console.log(findTheDifference('abcd', 'abcde'));
// console.log(findTheDifference('', 'y'));
// console.log(findTheDifference('a', 'aa'));
// console.log(findTheDifference('abcqweasdqwer', 'abcqweasdqewer'));
