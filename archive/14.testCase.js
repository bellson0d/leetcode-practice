const targetFun = require('./13.roman-to-integer');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  [
    [-1, 0, 1, 2, -1, -4],
    [
      [-1, 0, 1],
      [-1, -1, 2],
    ],
  ],
];
let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input);
  if (result !== output) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
