/*
 * @lc app=leetcode id=1 lang=javascript
 *
 * [1] Two Sum
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
const log = console.log;
var nums = [-3, 4, 3, 90],
  target = 0;

var twoSum = function (nums, target) {
  for (let i = 0; i < nums.length - 1; i++) {
    const tail = target - nums[i];
    const arr2 = nums.slice(i + 1);
    for (let j = 0; j < arr2.length; j++) {
      if (tail === arr2[j]) {
        return [i, j + i + 1];
      }
    }
  }
  return null;
};

log(twoSum(nums, target));
// @lc code=end
