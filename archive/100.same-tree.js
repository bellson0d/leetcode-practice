/*
 * @lc app=leetcode id=100 lang=javascript
 *
 * [100] Same Tree
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} p
 * @param {TreeNode} q
 * @return {boolean}
 */
var isSameTree = function (p, q) {
  let arr1 = [[0, p]],
    arr2 = [[0, q]];

  while (arr1.length > 0 && arr2.length > 0) {
    const [type1, node1] = arr1.shift();
    const [type2, node2] = arr2.shift();

    if (type1 !== type2) return false;

    if (!node1 || !node2) {
      if (node1 !== node2) return false;
    } else {
      if (node1.val !== node2.val) return false;

      arr1.push([1, node1.left]);
      arr2.push([1, node2.left]);
      arr1.push([2, node1.right]);
      arr2.push([2, node2.right]);
    }
  }

  return arr1.length === arr2.length;
};

// @lc code=end

// module.exports = isSameTree;
