/*
 * @lc app=leetcode id=93 lang=javascript
 *
 * [93] Restore IP Addresses
 */

// @lc code=start
/**
 * @param {string} s
 * @return {string[]}
 */
var restoreIpAddresses = function (s) {
  const len = s.length;

  if (!/^[0-9]+$/.test(s) || s > 255255255255 || len < 4) return [];

  let res = [];
  if (len === 4) return [s.split('').join('.')];

  const helpFn = (str, arr) => {
    const sLen = str.length;
    const aLen = arr.length;

    if (sLen === 4 - aLen) {
      res.push(arr.concat(str.split('')));
    } else if (aLen === 3) {
      if (str < 256 && str[0] !== '0') res.push(arr.concat([str]));
    } else if (str[0] === '0') {
      helpFn(str.slice(1), arr.concat(['0']));
    } else {
      helpFn(str.slice(1), arr.concat([str[0]]));
      if (sLen - 2 >= 3 - aLen)
        helpFn(str.slice(2), arr.concat([str.slice(0, 2)]));

      if (sLen - 3 >= 3 - aLen && str.slice(0, 3) < 256)
        helpFn(str.slice(3), arr.concat([str.slice(0, 3)]));
    }
  };

  helpFn(s, []);

  return res.map((v) => v.join('.'));
};
// @lc code=end

// console.log(restoreIpAddresses('25525511135'));
// console.log(restoreIpAddresses('0000'));
// console.log(restoreIpAddresses('1111'));
// console.log(restoreIpAddresses('010010'));
// console.log(restoreIpAddresses('101023'));
// console.log(restoreIpAddresses('2552552552551'));
// console.log(restoreIpAddresses('1'));
// console.log(restoreIpAddresses('12'));
// console.log(restoreIpAddresses('123'));
// console.log(restoreIpAddresses('9999999'));
// console.log(restoreIpAddresses('010010'));
