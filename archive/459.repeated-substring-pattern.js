/*
 * @lc app=leetcode id=459 lang=javascript
 *
 * [459] Repeated Substring Pattern
 */

// @lc code=start
/**
 * @param {string} s
 * @return {boolean}
 */
var repeatedSubstringPattern = function (str) {
  const len = str.length;
  if (len === 1) return false;
  if (len === 2) return str[0] === str[1];

  let divide = 2;
  while (divide <= len) {
    if (len % divide === 0 && str.slice(0, len / divide).repeat(divide) === str)
      return true;

    divide++;
  }
  return false;
};
// @lc code=end

// console.log(repeatedSubstringPattern('abab')); // true
// console.log(repeatedSubstringPattern('aabb')); // false
// console.log(repeatedSubstringPattern('aabaab')); // true
// console.log(repeatedSubstringPattern('aba')); // false
// console.log(repeatedSubstringPattern('abcabcabc')); // true
// console.log(repeatedSubstringPattern('qwieuoiqwuerqwerqweq'.repeat(3))); // true
// console.log(repeatedSubstringPattern('a'.repeat(10000) + 'b')); // false
// console.log(repeatedSubstringPattern('bb')); // true
// console.log(repeatedSubstringPattern('zzz')); // true
