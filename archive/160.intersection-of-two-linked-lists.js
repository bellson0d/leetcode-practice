/*
 * @lc app=leetcode id=160 lang=javascript
 *
 * [160] Intersection of Two Linked Lists
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */

const getListLength = (head) => {
  let count = 0,
    currentNode = head;

  while (currentNode) {
    currentNode = currentNode.next;
    count++;
  }

  return count;
};

/**
 * @param {ListNode} headA
 * @param {ListNode} headB
 * @return {ListNode}
 */
var getIntersectionNode = function (headA, headB) {
  if (!headA || !headB) return null;

  let len1 = getListLength(headA),
    len2 = getListLength(headB);

  while (len1 !== len2) {
    if (len1 > len2) {
      headA = headA.next;
      len1--;
    } else {
      headB = headB.next;
      len2--;
    }
  }

  while (len1) {
    if (headA === headB) {
      return headA;
    }
    headA = headA.next;
    headB = headB.next;
    len1--;
  }

  return null;
};
// @lc code=end
