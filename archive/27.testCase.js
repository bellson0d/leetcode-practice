const targetFn = require('./27.remove-element');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  [[[3, 2, 2, 3], 3], 2],
  [[[0, 4, 4, 0, 4, 4, 4, 0, 2], 4], 4],
  [[[0, 1, 2, 2, 3, 0, 4, 2], 2], 5],
];
let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFn(input[0], input[1]);
  if (result !== output) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
