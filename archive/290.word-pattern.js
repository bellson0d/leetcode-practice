/*
 * @lc app=leetcode id=290 lang=javascript
 *
 * [290] Word Pattern
 */

// @lc code=start
/**
 * @param {string} pattern
 * @param {string} s
 * @return {boolean}
 */
var wordPattern = function (pattern, s) {
  let obj = {};
  const p = pattern.split(''),
    str = s.split(' ').map((v) => v + '@');
  if (p.length !== str.length) return false;

  for (let i = 0; i < p.length; i++) {
    const key = p[i];
    const value = str[i];
    if (obj.hasOwnProperty(key) || obj.hasOwnProperty(value)) {
      if (obj[key] !== value || obj[value] !== key) return false;
    } else {
      obj[key] = value;
      obj[value] = key;
    }
  }
  return true;
};
// @lc code=end
