/*
 * @lc app=leetcode.cn id=872 lang=javascript
 *
 * [872] 叶子相似的树
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root1
 * @param {TreeNode} root2
 * @return {boolean}
 */
var helper = (root) => {
  const arr = [root],
    res = [];
  while (arr.length) {
    const node = arr.pop();
    if (!node.right && !node.left) res.push(node.val);
    if (node.right) arr.push(node.right);
    if (node.left) arr.push(node.left);
  }
  return res.join('-');
};
var leafSimilar = function (root1, root2) {
  return helper(root1) === helper(root2);
};
// @lc code=end
