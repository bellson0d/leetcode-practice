/*
 * @lc app=leetcode.cn id=922 lang=javascript
 *
 * [922] 按奇偶排序数组 II
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number[]}
 */
var sortArrayByParityII = function (nums) {
  let i = 0,
    j = 1,
    res = [];
  for (const n of nums) {
    if (n % 2 === 0) {
      res[i] = n;
      i += 2;
    } else {
      res[j] = n;
      j += 2;
    }
  }
  return res;
};
// @lc code=end
