/*
 * @lc app=leetcode.cn id=1021 lang=javascript
 *
 * [1021] 删除最外层的括号
 */

// @lc code=start
/**
 * @param {string} s
 * @return {string}
 */
var removeOuterHelper = (str) => {
  return str.slice(1, str.length - 1);
};

var removeOuterParentheses = function (s) {
  let tmpCount = 0,
    tmpStr = "",
    res = [],
    idx = 0;

  while (idx < s.length) {
    if (s[idx] === "(") {
      tmpStr += "(";
      tmpCount++;
    } else {
      tmpStr += ")";
      tmpCount--;
      if (tmpCount === 0) {
        res.push(tmpStr);
        tmpStr = "";
      }
    }
    idx++;
  }

  return res.map(removeOuterHelper).join("");
};

// @lc code=end

// console.log(removeOuterParentheses("(()())(())"));
// console.log(removeOuterParentheses("(()())(())(()(()))"));
// console.log(removeOuterParentheses("()()"));
