/*
 * @lc app=leetcode id=12 lang=javascript
 *
 * [12] Integer to Roman
 */

// @lc code=start
/**
 * @param {number} num
 * @return {string}
 */

const numMap = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
const strMap = [
  'M',
  'CM',
  'D',
  'CD',
  'C',
  'XC',
  'L',
  'XL',
  'X',
  'IX',
  'V',
  'IV',
  'I',
];

var intToRoman = function (num) {
  let numIdx = 0;
  let leftNum = num;
  let str = '';

  while (leftNum > 0) {
    const iNum = numMap[numIdx];
    // if (leftNum >= iNum) {
    let repeatX = leftNum / iNum;
    const tmpStr = strMap[numIdx];
    while (repeatX >= 1) {
      str += tmpStr;
      repeatX--;
    }
    leftNum = leftNum % iNum;
    // }

    numIdx++;
  }

  return str;
};

// module.exports = intToRoman;

// @lc code=end
