/*
 * @lc app=leetcode id=167 lang=javascript
 *
 * [167] Two Sum II - Input array is sorted
 */

// @lc code=start
/**
 * @param {number[]} numbers
 * @param {number} target
 * @return {number[]}
 */
var twoSum = function (numbers, target) {
  const len = numbers.length;
  if (len === 2) return [1, 2];
  let obj = {},
    count = 0;

  while (count < len) {
    const ele = numbers[count];
    const left = target - ele;
    if (left < ele && obj[left]) return [obj[left], count + 1];
    obj[ele] = count + 1;
    count++;
  }

  for (let i = 0; i < len; i++) {
    const ele = numbers[i];
    const left = target - ele;
    if (obj[left]) return [i + 1, obj[left]];
  }
};
// @lc code=end
// module.exports = twoSum;
