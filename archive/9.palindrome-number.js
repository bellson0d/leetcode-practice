/*
 * @lc app=leetcode id=9 lang=javascript
 *
 * [9] Palindrome Number
 */

// @lc code=start
/**
 * @param {number} x
 * @return {boolean}
 */
var isPalindrome = function (x) {
  if (x < 10 && x > -1) return true;
  if (x < 0) return false;
  if (x % 10 === 0) return false;
  let str = '' + x;
  const len = str.length - 1;
  let i = 0;

  while (i < len / 2) {
    if (str[i] !== str[len - i]) return false;
    i++;
  }

  return true;
};

module.exports = isPalindrome;
// @lc code=end
