/*
 * @lc app=leetcode id=86 lang=javascript
 *
 * [86] Partition List
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} x
 * @return {ListNode}
 */
var partition = function (head, x) {
  if (!head || !head.next) return head;

  let head1 = null,
    head2 = null,
    current = head,
    current1,
    current2;

  while (current) {
    if (current.val < x) {
      if (!head1) {
        head1 = current;
        current1 = current;
      } else {
        current1.next = current;
        current1 = current;
      }
      current = current.next;
    } else {
      if (!head2) {
        head2 = current;
        current2 = current;
      } else {
        current2.next = current;
        current2 = current;
      }
      current = current.next;
    }
  }
  if (current1) current1.next = head2;
  if (current2) current2.next = null;

  return head1 || head2;
};
// @lc code=end
// module.exports = partition;
