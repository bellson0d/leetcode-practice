/*
 * @lc app=leetcode id=350 lang=javascript
 *
 * [350] Intersection of Two Arrays II
 */

// @lc code=start
/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @return {number[]}
 */
var intersect = function (nums1, nums2) {
  let longArr = nums1,
    shortArr = nums2;
  if (nums2.length > nums1.length) {
    longArr = nums2;
    shortArr = nums1;
  }
  let res = [];
  for (const n of longArr) {
    const idx = shortArr.indexOf(n);
    if (idx !== -1) {
      res.push(n);
      shortArr = shortArr.slice(0, idx).concat(shortArr.slice(idx + 1));
    }
  }
  return res;
};
// @lc code=end

// console.log(intersect([1, 2, 2, 1], [2, 2]));
// console.log(intersect([9, 4, 5], [9, 4, 9, 8, 4]));
