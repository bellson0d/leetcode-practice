/*
 * @lc app=leetcode.cn id=468 lang=javascript
 *
 * [468] 验证IP地址
 */

// @lc code=start
/**
 * @param {string} queryIP
 * @return {string}
 */
var validIPAddress = function (str) {
  if (str.includes('.')) {
    const arr = str.split('.');
    if (arr.length !== 4) return 'Neither';

    for (const str of arr) {
      const n = Number(str);
      if (isNaN(n)) return 'Neither';
      if (n < 0 || n > 255) return 'Neither';
      if (String(n) !== str) return 'Neither';
    }
    return 'IPv4';
  } else if (str.includes(':')) {
    const arr = str.split(':');
    if (arr.length !== 8) return 'Neither';

    for (const str of arr) {
      if (/[^0-9a-fA-F]/.test(str)) return 'Neither';
      if (!str || str.length > 4) return 'Neither';
      const n = parseInt(str, 16);

      if (n < 0 || n > 65535) return 'Neither';
    }
    return 'IPv6';
  }

  return 'Neither';
};
// @lc code=end

// console.log(validIPAddress('172.16.0.1'));
// console.log(validIPAddress('172.16.254.1'));
// console.log(validIPAddress('2001:0db8:85a3:0:0:8A2E:0370:7334'));
// console.log(validIPAddress('256.256.256.256'));
// console.log(validIPAddress('2001:0db8:85a3:0:0:8A2E:0370:7334:'));
// console.log(validIPAddress('1e1.4.5.6'));
