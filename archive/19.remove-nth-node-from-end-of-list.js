/*
 * @lc app=leetcode id=19 lang=javascript
 *
 * [19] Remove Nth Node From End of List
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} n
 * @return {ListNode}
 */
var removeNthFromEnd = function (head, n) {
  let len = 1;
  let currentNode = head;
  let arr = [currentNode];
  while (currentNode.next) {
    currentNode = currentNode.next;
    arr.push(currentNode);
    len++;
  }

  let order = len - n;
  let newHead = arr[order - 1];
  if (newHead) {
    newHead.next = arr[order + 1] || null;
  } else {
    head = head.next;
  }

  return head;
};

// function ListNode(val, next) {
//   this.val = val === undefined ? 0 : val;
//   this.next = next === undefined ? null : next;
// }

// let arr = [new ListNode(1)];
// let count = 2;
// while (count < 6) {
//   const node = new ListNode(count);
//   arr[count - 2].next = node;
//   arr.push(node);
//   count++;
// }

// console.log(removeNthFromEnd(arr[0], 2));

// @lc code=end
