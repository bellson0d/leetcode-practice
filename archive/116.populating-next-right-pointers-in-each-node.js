/*
 * @lc app=leetcode id=116 lang=javascript
 *
 * [116] Populating Next Right Pointers in Each Node
 */

// @lc code=start
/**
 * // Definition for a Node.
 * function Node(val, left, right, next) {
 *    this.val = val === undefined ? null : val;
 *    this.left = left === undefined ? null : left;
 *    this.right = right === undefined ? null : right;
 *    this.next = next === undefined ? null : next;
 * };
 */

/**
 * @param {Node} root
 * @return {Node}
 */
var connect = function (root) {
  if (!root) return null;

  let depth = 0,
    count = 0,
    tmpArr = [root];

  while (tmpArr.length > 0) {
    const currentNode = tmpArr.shift();
    count++;

    if (currentNode.left) {
      tmpArr.push(currentNode.left);
      tmpArr.push(currentNode.right);
    }

    if (count === 2 ** depth) {
      currentNode.next = null;
      depth++;
      count = 0;
    } else {
      currentNode.next = tmpArr[0];
    }
  }

  return root;
};
// @lc code=end

// const { getTree } = require('./utils/treeNode');
// console.log(connect(getTree([])));
// console.log(connect(getTree([1])));
// console.log(connect(getTree([1, 2, 3])));
// console.log(connect(getTree([1, 2, 3, 4, 5, 6, 7])));
