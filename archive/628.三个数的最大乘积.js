/*
 * @lc app=leetcode.cn id=628 lang=javascript
 *
 * [628] 三个数的最大乘积
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var calculate = (arr) => arr[0] * arr[1] * arr[2];
var maximumProduct = function (nums) {
  const len = nums.length;
  if (len === 3) return calculate(nums);

  let arr1 = [],
    arr2 = [];
  for (const n of nums) {
    if (n < 0) {
      arr1.push(n);
    } else {
      arr2.push(n);
    }
  }

  arr1.sort((a, b) => b - a);
  arr2.sort((a, b) => a - b);

  if (arr2.length === 0) return calculate(arr1.slice(0, 3));
  if (arr2.length === 1)
    return calculate(arr1.slice(arr1.length - 2).concat(arr2[0]));
  if (arr2.length === 2)
    return calculate(arr1.slice(arr1.length - 2).concat(arr2[1]));

  let max = calculate(arr2.slice(arr2.length - 3));
  if (arr1.length > 1) {
    return Math.max(
      calculate(
        arr1.slice(arr1.length - 2).concat(arr2.slice(arr2.length - 1))
      ),
      max
    );
  }
  return max;
};
// @lc code=end
// console.log(maximumProduct([1, 2, 3]));
// console.log(maximumProduct([1, 2, 3, 4]));
// console.log(maximumProduct([-1, -2, -3]));
// console.log(maximumProduct([-1, -2, -3, 1]));
// console.log(maximumProduct([-1, -2, -3, 1, 2]));
// console.log(maximumProduct([-1, -2, -2, 1, 2, 3]));
