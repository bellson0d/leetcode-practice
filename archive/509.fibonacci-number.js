/*
 * @lc app=leetcode id=509 lang=javascript
 *
 * [509] Fibonacci Number
 */

// @lc code=start
/**
 * @param {number} n
 * @return {number}
 */
var fib = function (n) {
  if (n === 0) return 0;
  if (n === 1) return 1;

  let pre1 = 0,
    pre2 = 1,
    currentN = 2;
  while (currentN < n) {
    let tmp = pre2;
    pre2 = pre1 + pre2;
    pre1 = tmp;
    currentN++;
  }
  return pre1 + pre2;
};
// @lc code=end
