/*
 * @lc app=leetcode id=88 lang=javascript
 *
 * [88] Merge Sorted Array
 */

// @lc code=start
/**
 * @param {number[]} nums1
 * @param {number} m
 * @param {number[]} nums2
 * @param {number} n
 * @return {void} Do not return anything, modify nums1 in-place instead.
 */
// 审题错误导致以为是合并 2 个有序链表
// const { getList, isSameList, logList } = require('./utils/listNode');
var merge = function (nums1, m, nums2, n) {
  let currentNode,
    head,
    node1 = nums1,
    node2 = nums2;

  if (!node1) {
    currentNode = node2;
    node2 = node2.next;
    m = 0;
  }
  if (!node2) {
    currentNode = node1;
    node1 = node1.next;
    n = 0;
  }
  if (m === 0 && n === 0) return null;

  if (!currentNode) {
    if (node1.val < node2.val) {
      currentNode = node1;
      node1 = node1.next;
      node2 = node2;
      m--;
    } else {
      currentNode = node2;
      node2 = node2.next;
      node1 = node1;
      n--;
    }
  }

  head = currentNode;

  while (m > 0 || n > 0) {
    if (m === 0) {
      currentNode.next = node2;
      currentNode = node2;
      node2 = node2 && node2.next;
      n--;
      continue;
    }

    if (n === 0) {
      currentNode.next = node1;
      currentNode = node1;
      node1 = node1 && node1.next;
      m--;
      continue;
    }

    if (!node1) {
      m = 0;
    } else if (!node2) {
      n = 0;
    } else if (node1.val < node2.val) {
      currentNode.next = node1;
      currentNode = node1;
      node1 = node1 && node1.next;
      m--;
    } else {
      currentNode.next = node2;
      currentNode = node2;
      node2 = node2 && node2.next;
      n--;
    }
  }
  if (currentNode) currentNode.next = null;

  return head;
};

// @lc code=end
module.exports = merge;
// logList(merge(getList([1, 2, 3, 0, 0, 0]), 3, getList([2, 5, 6]), 3));
