/*
 * @lc app=leetcode.cn id=661 lang=javascript
 *
 * [661] 图片平滑器
 */

// @lc code=start
/**
 * @param {number[][]} img
 * @return {number[][]}
 */
var imageSmoother = function (img) {
  const len1 = img.length;
  const len2 = img[0].length;
  let res = Array(len1)
    .fill(1)
    .map((v) => []);

  for (let i = 0; i < len1; i++) {
    for (let j = 0; j < len2; j++) {
      let sum = 0,
        count = 1;
      sum += img[i][j];
      if (j > 0) {
        sum += img[i][j - 1];
        count++;
      }
      if (j < len2 - 1) {
        sum += img[i][j + 1];
        count++;
      }

      if (i > 0 && j > 0) {
        sum += img[i - 1][j - 1];
        count++;
      }
      if (i > 0) {
        sum += img[i - 1][j];
        count++;
      }
      if (i > 0 && j < len2 - 1) {
        sum += img[i - 1][j + 1];
        count++;
      }

      if (i < len1 - 1 && j < len2 - 1) {
        sum += img[i + 1][j + 1];
        count++;
      }
      if (j > 0 && i < len1 - 1) {
        sum += img[i + 1][j - 1];
        count++;
      }
      if (i < len1 - 1) {
        sum += img[i + 1][j];
        count++;
      }

      res[i][j] = ~~(sum / count);
    }
  }
  return res;
};
// @lc code=end

// console.log(
//   imageSmoother([
//     [1, 1, 3],
//     [4, 5, 6],
//     [7, 8, 9],
//   ])
// );
// console.log(
//   imageSmoother([
//     [2, 3, 4],
//     [5, 6, 7],
//     [8, 9, 10],
//     [11, 12, 13],
//     [14, 15, 16],
//   ])
// );
