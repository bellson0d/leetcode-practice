/*
 * @lc app=leetcode id=13 lang=javascript
 *
 * [13] Roman to Integer
 */

// @lc code=start
/**
 * @param {string} s
 * @return {number}
 */
const strMap = {
  M: 1000,
  CM: 900,
  D: 500,
  CD: 400,
  C: 100,
  XC: 90,
  L: 50,
  XL: 40,
  X: 10,
  IX: 9,
  V: 5,
  IV: 4,
  I: 1,
};

var romanToInt = function (s) {
  let num = 0;

  for (let i = 0; i < s.length; i++) {
    let tmpStr = strMap[s[i] + s[i + 1]];

    if (tmpStr) {
      i++;
    } else {
      tmpStr = strMap[s[i]];
    }
    num += tmpStr;
  }

  return num;
};

// module.exports = romanToInt;

// @lc code=end
