/*
 * @lc app=leetcode id=228 lang=javascript
 *
 * [228] Summary Ranges
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {string[]}
 */
var summaryRanges = function (nums) {
    const len = nums.length;
    if (len < 2) return nums.map(String);

    let count = 1,
        tmp = nums[0],
        pre = nums[0],
        res = [];
    while (count < len) {
        const ele = nums[count];
        if (ele !== pre + 1) {
            if (tmp === pre) {
                res.push(String(tmp));
            } else {
                res.push(tmp + "->" + pre);
            }
            tmp = ele;
        }
        pre = ele;

        count++;
    }
    if (tmp === pre) {
        res.push(String(tmp));
    } else {
        res.push(tmp + "->" + pre);
    }

    return res;
};
// @lc code=end
// console.log(summaryRanges([0,1,2,4,5,7]))
// console.log(summaryRanges([0,2,3,4,6,8,9]))
// console.log(summaryRanges([]))
// console.log(summaryRanges([-1]))
// console.log(summaryRanges([0]))