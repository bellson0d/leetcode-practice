/*
 * @lc app=leetcode id=169 lang=javascript
 *
 * [169] Majority Element
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var majorityElement = function (nums) {
  let n = nums[0],
    obj = { [n]: 1 },
    count = 1;
  const len = nums.length,
    half = ~~(len / 2);
  while (count < len) {
    const ele = nums[count];
    if (!obj[ele]) obj[ele] = 0;
    obj[ele]++;
    if (obj[ele] > half) return ele;
    if (obj[ele] > obj[n]) n = ele;
    count++;
  }
  return n;
};
// @lc code=end
// 读题失误，确保有元素是过半，则可以快速定位
// console.log(majorityElement([3, 2, 3]));
// console.log(majorityElement([2, 2, 1, 1, 1, 2, 2]));
