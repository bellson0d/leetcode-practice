const targetFun = require("./203.remove-linked-list-elements");
const { getList, isSameList, logList } = require("./utils/listNode");
const MULTI_ARGS = true; // 输入参数是否为多个

const testStr = [
    [[getList([1, 2, 6, 3, 4, 5, 6]), 6], getList([1, 2, 3, 4, 5])],
    [[getList([]), 1], getList([])],
    [[getList([7, 7, 7, 7]), 7], getList([])],
];

const time1 = new Date().getTime();
console.log(time1);

let count = 0;
while (count < testStr.length) {
    const [input, output] = testStr[count];
    
    let result;
    if (MULTI_ARGS) {
        result = targetFun(...input);
    } else {
        result = targetFun(input);
    }

    if (!isSameList(result, output, true)) {
        console.log("Number " + count + " Case Wrong !!!");
        return;
    }
    count++;
}
console.log("All pass!");

const time2 = new Date().getTime();
console.log("Duration: ", time2 - time1);
