/*
 * @lc app=leetcode id=415 lang=javascript
 *
 * [415] Add Strings
 */

// @lc code=start
/**
 * @param {string} num1
 * @param {string} num2
 * @return {string}
 */
var addStrings = function (num1, num2) {
  let len = Math.max(num1.length, num2.length);
  const arr1 = num1.split('').reverse();
  const arr2 = num2.split('').reverse();
  let tmp = [],
    pre = 0,
    count = 0;
  while (count < len) {
    let sum = ~~arr1[count] + ~~arr2[count] + pre;
    if (sum > 9) {
      pre = 1;
      sum = String(sum)[1];
    } else {
      pre = 0;
    }
    tmp.unshift(sum);
    count++;
  }
  if (pre) tmp.unshift(pre);
  return tmp.join('');
};
// @lc code=end
// console.log(addStrings('456', '77'));
// console.log(addStrings('0', '0'));
// console.log(addStrings('11', '123'));
