/*
 * @lc app=leetcode id=434 lang=javascript
 *
 * [434] Number of Segments in a String
 */

// @lc code=start
/**
 * @param {string} s
 * @return {number}
 */
var countSegments = function (s) {
  const str = s.trim();
  if (!str) return 0;
  return str.split(/\s+/).filter((v) => v).length;
};
// @lc code=end
