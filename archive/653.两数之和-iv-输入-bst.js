/*
 * @lc app=leetcode.cn id=653 lang=javascript
 *
 * [653] 两数之和 IV - 输入 BST
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} k
 * @return {boolean}
 */
var findTarget = function (root, k) {
  let arr = [root],
    set = new Set(),
    need = [];
  while (arr) {
    let tmp = [];

    for (const node of arr) {
      const temp = k - node.val;
      if (set.has(temp)) return true;
      for (const n of need) {
        if (n !== k - n && set.has(n)) return true;
      }
      need.push(temp);
      if (!set.has(node.val)) set.add(node.val);

      if (node.left) tmp.push(node.left);
      if (node.right) tmp.push(node.right);
    }
    if (tmp.length) {
      arr = tmp;
    } else {
      arr = null;
    }
  }

  return false;
};
// @lc code=end
// const { getTree } = require('./utils/treeNode');
// console.log(findTarget(getTree([1, null, 2]), 2));
