/*
 * @lc app=leetcode.cn id=491 lang=javascript
 *
 * [491] 递增子序列
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number[][]}
 */
var findSubsequences = function (nums) {
  const res = [],
    obj = {};
  const helper = (pre, arr) => {
    for (let i = 0; i < arr.length; i++) {
      if (arr[i] >= pre[pre.length - 1]) {
        const newPre = pre.concat([arr[i]]);
        const newArr = arr.slice(i + 1);
        const key = newPre.join('-');
        if (!obj[key]) {
          obj[key] = true;
          res.push(newPre);
          helper(newPre, newArr);
        }
      }
    }
  };

  for (let i = 0; i < nums.length - 1; i++) {
    helper([nums[i]], nums.slice(i + 1));
  }

  return res;
};
// @lc code=end

// console.log(findSubsequences([4, 6, 7, 7]));
// console.log(findSubsequences([4, 4, 3, 2, 1]));
