const targetFun = require('./12.integer-to-roman');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  [3, 'III'],
  [4, 'IV'],
  [9, 'IX'],
  [58, 'LVIII'],
  [1994, 'MCMXCIV'],
  [4, 'IV'],
];
let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input);
  if (result !== output) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
