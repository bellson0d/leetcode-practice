/*
 * @lc app=leetcode.cn id=804 lang=javascript
 *
 * [804] 唯一摩尔斯密码词
 */

// @lc code=start
/**
 * @param {string[]} words
 * @return {number}
 */
var baseMap = {
  a: '.-',
  b: '-...',
  c: '-.-.',
  d: '-..',
  e: '.',
  f: '..-.',
  g: '--.',
  h: '....',
  i: '..',
  j: '.---',
  k: '-.-',
  l: '.-..',
  m: '--',
  n: '-.',
  o: '---',
  p: '.--.',
  q: '--.-',
  r: '.-.',
  s: '...',
  t: '-',
  u: '..-',
  v: '...-',
  w: '.--',
  x: '-..-',
  y: '-.--',
  z: '--..',
};
var uniqueMorseRepresentations = function (words) {
  const set = new Set();
  for (const w of words) {
    let str = '';
    for (const a of w) {
      str += baseMap[a];
    }
    set.add(str);
  }

  return set.size;
};
// @lc code=end
// console.log(uniqueMorseRepresentations(['gin', 'zen', 'gig', 'msg']));
