/*
 * @lc app=leetcode id=371 lang=javascript
 *
 * [371] Sum of Two Integers
 */

// @lc code=start
/**
 * @param {number} a
 * @param {number} b
 * @return {number}
 */

var getSum = function (a, b) {
  let [max, min] = Math.abs(a) > Math.abs(b) ? [a, b] : [b, a];
  let flag = '',
    isAdd = true,
    bi1 = max.toString(2),
    bi2 = min.toString(2);
  if (max < 0 && min < 0) {
    flag = '-';
    bi1 = bi1.slice(1);
    bi2 = bi2.slice(1);
  } else if (max < 0 || min < 0) {
    isAdd = false;
    if (max < 0) {
      flag = '-';
      bi1 = bi1.slice(1);
    } else {
      bi2 = bi2.slice(1);
    }
  }

  len = bi1.length;
  bi2 = '0'.repeat(len - bi2.length) + bi2;
  bi1 = bi1.split('');
  bi2 = bi2.split('');

  let idx = len - 1,
    pre = '0',
    res = '';
  while (idx >= 0) {
    const ele1 = bi1[idx];
    const ele2 = bi2[idx];
    if (!isAdd) {
      if (ele2 === '0') {
        res = ele1 + res;
      } else if (ele1 === '1') {
        res = '0' + res;
      } else {
        res = '1' + res;
        let i = idx - 1;
        while (bi1[i] === '0' && i >= 0) {
          bi1[i] = '1';
          i--;
        }
        bi1[i] = '0';
      }
    } else {
      const sum = [pre, ele1, ele2].reduce((a, b) => ~~a + ~~b, 0);
      let tmp = '0';
      if (sum === 3) {
        tmp = '1';
        pre = '1';
      } else if (sum === 2) {
        tmp = '0';
        pre = '1';
      } else if (sum === 1) {
        tmp = '1';
        pre = '0';
      } else {
        tmp = '0';
        pre = '0';
      }
      res = tmp + res;
    }
    idx--;
  }
  res = pre + res;
  return parseInt(flag + res, 2);
};
// @lc code=end

// console.log(getSum(1, 2));
// console.log(getSum(2, 3));
// console.log(getSum(999, 1000));
// console.log(getSum(-1, 1));
// console.log(getSum(-100, -200));
// console.log(getSum(-99, 80));
// console.log(getSum(-14, 16));
