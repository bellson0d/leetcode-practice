/*
 * @lc app=leetcode id=3 lang=javascript
 *
 * [3] Longest Substring Without Repeating Characters
 */

// @lc code=start
/**
 * @param {string} s
 * @return {number}
 */

function findRepeat(str) {
  let tmpArr = [str[0]];

  for (let i = 1; i < str.length; i++) {
    const ele = str[i];
    const repeatIdx = tmpArr.indexOf(ele);
    if (repeatIdx !== -1) return [repeatIdx, i];
    tmpArr.push(ele);
  }

  return null;
}

var lengthOfLongestSubstring = function (s) {
  if (s.length < 2) return s.length;
  let maxLen = 1;

  function sliceStr(str) {
    const repeatArr = findRepeat(str);
    if (!Array.isArray(repeatArr)) {
      if (str.length > maxLen) {
        maxLen = str.length;
      }
      return;
    } else {
      const part1 = str.slice(0, repeatArr[1]);
      const part2 = str.slice(repeatArr[0] + 1);
      // console.log(part1, part2);
      if (part1.length > maxLen) {
        sliceStr(part1);
      }
      if (part2.length > maxLen) {
        sliceStr(part2);
      }
    }
  }
  sliceStr(s);

  return maxLen;
};

const time1 = new Date().getTime();
console.log(time1);

// Test Part
const testStr = [
  ['bvbj', 3],
  ['cdd', 2],
  ['abababababab', 2],
  ['aqwerpaj', 7],
  ['aaaaa', 1],
  ['', 0],
  ['pfbhmipx', 7],
];
let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = lengthOfLongestSubstring(input);
  if (result !== output) {
    console.log('Wrong !');
    console.log(input, result);
    return;
  }
  count++;
}
console.log('All pass!');

// const testStr =
//   'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!\\"#$%&\'()*+,-./:;<=>?@[\\\\]^_`{|}~ abcdefghijklmnopqrstuvwxyzABCD';
// console.log(
//   lengthOfLongestSubstring(
//     Array(130)
//       // Array(250)
//       .fill(testStr.slice(0, 120))
//       .join('') + testStr
//   )
// );

const time2 = new Date().getTime();
console.log(time2, time2 - time1);

// @lc code=end
