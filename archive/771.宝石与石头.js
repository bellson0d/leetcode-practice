/*
 * @lc app=leetcode.cn id=771 lang=javascript
 *
 * [771] 宝石与石头
 */

// @lc code=start
/**
 * @param {string} jewels
 * @param {string} stones
 * @return {number}
 */
var numJewelsInStones = function (jewels, stones) {
  let set = new Set();
  for (const j of jewels) {
    set.add(j);
  }

  let count = 0;
  for (const s of stones) {
    if (set.has(s)) count++;
  }
  return count;
};
// @lc code=end

// console.log(numJewelsInStones('aA', 'aAAbbbb'));
