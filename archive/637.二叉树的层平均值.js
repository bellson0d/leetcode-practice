/*
 * @lc app=leetcode.cn id=637 lang=javascript
 *
 * [637] 二叉树的层平均值
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
var averageOfLevels = function (root) {
  let res = [],
    nodes = [root];
  do {
    let tmp = [],
      count = 0,
      sum = 0;
    for (const node of nodes) {
      sum += node.val;
      count++;
      if (node.left) tmp.push(node.left);
      if (node.right) tmp.push(node.right);
    }
    res.push(sum / count);
    if (tmp.length) {
      nodes = tmp;
    } else {
      nodes = null;
    }
  } while (nodes);

  return res;
};
// @lc code=end
