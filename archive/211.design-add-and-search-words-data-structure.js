/*
 * @lc app=leetcode id=211 lang=javascript
 *
 * [211] Design Add and Search Words Data Structure
 */

// @lc code=start
/**
 * Initialize your data structure here.
 */
var WordDictionary = function () {
    this.obj = {};
    this.obj2 = {};
    this.arr = [];
    return this;
};

/**
 * @param {string} word
 * @return {void}
 */
WordDictionary.prototype.addWord = function (word) {
    this.obj[word] = word.length;
    this.obj2 = {}; // Magic trick!
    this.arr.push(word);
};

/**
 * @param {string} word
 * @return {boolean}
 */
WordDictionary.prototype.search = function (word) {
    if (this.obj2.hasOwnProperty(word)) return this.obj2[word];
    if (word.includes(".")) {
        let len = word.length,
            tmpArr = this.arr.filter((v) => {
                if (v.length !== len) {
                    this.obj2[word] = false;
                    return false;
                }
                for (let i = 0; i < len; i++) {
                    if (word[i] !== "." && word[i] !== v[i]) {
                        this.obj2[word] = false;
                        return false;
                    }
                }
                this.obj2[word] = true;
                return true;
            });
        return !!tmpArr.length;
    } else {
        return this.obj.hasOwnProperty(word);
    }
};

/**
 * Your WordDictionary object will be instantiated and called as such:
 * var obj = new WordDictionary()
 * obj.addWord(word)
 * var param_2 = obj.search(word)
 */
// @lc code=end

// const wordDictionary = new WordDictionary();
// wordDictionary.addWord("bad");
// wordDictionary.addWord("dad");
// wordDictionary.addWord("mad");
// console.log(wordDictionary.search("pad")); // return False
// console.log(wordDictionary.search("bad")); // return True
// console.log(wordDictionary.search(".ad")); // return True
// console.log(wordDictionary.search("b..")); // return True
// console.log(wordDictionary.search("...."));
