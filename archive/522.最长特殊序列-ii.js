/*
 * @lc app=leetcode.cn id=522 lang=javascript
 *
 * [522] 最长特殊序列 II
 */

// @lc code=start
/**
 * @param {string[]} strs
 * @return {number}
 */
var helper = (s1, s2) => {
  let len1 = s1.length,
    len2 = s2.length,
    idx1 = 0;
  idx2 = 0;

  while (idx2 < len2) {
    if (s1[idx1] === s2[idx2]) idx1++;
    idx2++;
  }

  return idx1 < len1;
};

var findLUSlength = function (strs) {
  let obj = {};
  for (const s of strs) {
    if (obj[s]) {
      obj[s][0]++;
    } else {
      obj[s] = [1, s];
    }
  }

  const res = Object.values(obj)
    .filter((v) => v[0] === 1)
    .map((v) => v[1])
    .sort((a, b) => b.length - a.length);

  if (!res.length) return -1;

  for (let i = 0; i < res.length; i++) {
    const s1 = res[i];
    let flag = s1;
    for (const s2 of strs) {
      if (s1 !== s2 && s1.length <= s2.length && !helper(s1, s2)) {
        flag = null;
        break;
      }
    }
    if (flag) return flag.length;
  }
  return -1;
};
// @lc code=end

// console.log(findLUSlength(['aba', 'cdc', 'eae']));
// console.log(findLUSlength(['bd', 'e', 'abc', 'ab', 'abc']));
// console.log(findLUSlength(['aaa', 'aaa', 'aa']));
// console.log(findLUSlength(['aaa', 'aaa', '']));
// console.log(findLUSlength(['aaa', 'aaa', 'bbb']));
// console.log(findLUSlength(['aabbcc', 'aabbcc', 'b', 'bc']));
// console.log(findLUSlength(['aabbcc', 'aabbcc', 'bc', 'bcc', 'aabbccc']));
