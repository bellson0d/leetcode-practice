/*
 * @lc app=leetcode.cn id=451 lang=javascript
 *
 * [451] 根据字符出现频率排序
 */

// @lc code=start
/**
 * @param {string} s
 * @return {string}
 */
var frequencySort = function (s) {
  let obj = {};
  for (const str of s) {
    if (obj[str]) {
      obj[str][1]++;
    } else {
      obj[str] = [str, 1];
    }
  }

  return Object.values(obj)
    .sort((a, b) => b[1] - a[1])
    .map((v) => v[0].repeat(v[1]))
    .join('');
};
// @lc code=end
// console.log(frequencySort('Aabb'));
