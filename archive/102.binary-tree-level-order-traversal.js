/*
 * @lc app=leetcode id=102 lang=javascript
 *
 * [102] Binary Tree Level Order Traversal
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[][]}
 */
var levelOrder = function (root) {
  if (!root) return [];

  let tmpArr = [],
    nextArr = [],
    res = [],
    result = [[root.val]];
  if (root.left) tmpArr.push(root.left);
  if (root.right) tmpArr.push(root.right);

  while (tmpArr.length > 0) {
    const currentNode = tmpArr.shift();

    if (currentNode) {
      res.push(currentNode.val);

      if (currentNode.left) nextArr.push(currentNode.left);
      if (currentNode.right) nextArr.push(currentNode.right);
    }

    if (tmpArr.length === 0) {
      result.push(res);
      res = [];
      if (nextArr.length > 0) {
        tmpArr = nextArr.slice();
        nextArr = [];
      }
    }
  }

  return result;
};
// @lc code=end

// const { getTree } = require('./utils/treeNode');
// console.log(levelOrder(getTree([])));
// console.log(levelOrder(getTree([1])));
// console.log(levelOrder(getTree([1, 2, 3])));
// console.log(levelOrder(getTree([3, 9, 20, null, null, 15, 7])));
// console.log(levelOrder(getTree([1, 2, 3, 4, 5, 6, 7])));
