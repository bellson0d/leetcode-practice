const targetFun = require('./147.insertion-sort-list');
const { getList, isSameList } = require('./utils/listNode');
const MULTI_ARGS = false; // 输入参数是否为多个

const testStr = [
  [getList([]), getList([])],
  [getList([1]), getList([1])],
  [getList([2, 1]), getList([1, 2])],
  [getList([1, 4, 3, 2, 5, 2]), getList([1, 2, 2, 3, 4, 5])],
  [getList([5, 4, 3, 2, 1]), getList([1, 2, 3, 4, 5])],
];

const time1 = new Date().getTime();
console.log(time1);

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];

  let result;
  if (MULTI_ARGS) {
    result = targetFun(...input);
  } else {
    result = targetFun(input);
  }

  if (!isSameList(result, output, true)) {
    console.log('Number ' + count + ' Case Wrong !!!');
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
