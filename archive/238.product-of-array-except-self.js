/*
 * @lc app=leetcode id=238 lang=javascript
 *
 * [238] Product of Array Except Self
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number[]}
 */
var productExceptSelf = function (nums) {
    const len = nums.length;
    const z1 = nums.indexOf(0);
    const z2 = nums.lastIndexOf(0);
    if (z1 !== -1) {
        if (z1 === z2) {
            const res = nums
                .slice(0, z1)
                .concat(nums.slice(z1 + 1))
                .reduce((a, b) => a * b, 1);
            return nums.map((v) => (v ? 0 : res));
        } else {
            return Array(len).fill(0);
        }
    }

    const all = nums.reduce((a, b) => a * b, 1);
    return nums.map((v) => all / v);
};
// @lc code=end

// console.log(productExceptSelf([1, 2, 3, 4])); // [24,12,8,6]
// console.log(productExceptSelf([-1, 1, 0, -3, 3])); // [0,0,9,0,0]
// console.log(productExceptSelf([-1, 1, 0, -3, 0])); // [0,0,0,0,0]
