/*
 * @lc app=leetcode id=344 lang=javascript
 *
 * [344] Reverse String
 */

// @lc code=start
/**
 * @param {character[]} s
 * @return {void} Do not return anything, modify s in-place instead.
 */
var reverseString = function (s) {
  let count = 0,
    len = s.length;
  while (count < len / 2) {
    let tmp = s[len - 1 - count];
    s[len - 1 - count] = s[count];
    s[count] = tmp;
    count++;
  }
};
// @lc code=end
