const targetFun = require('./71.simplify-path');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  ['/home/', '/home'],
  ['/../', '/'],
  ['/home//foo/', '/home/foo'],
  ['/a/./b/../../c/', '/c'],
  ['/a/../../b/../c//.//', '/c'],
  ['/a//b////c/d//././/..', '/a/b/c'],
  ['/...', '/...'],
];

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input);
  if (result !== output) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
