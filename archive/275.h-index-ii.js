/*
 * @lc app=leetcode id=275 lang=javascript
 *
 * [275] H-Index II
 */

// @lc code=start
/**
 * @param {number[]} citations
 * @return {number}
 */
var hIndex = function (citations) {
    let h = 0;
    citations.reverse();

    for (let i = 0; i < citations.length; i++) {
        const ele = citations[i];
        if (ele > i) {
            h = i + 1;
        } else {
            break;
        }
    }

    return h;
};
// @lc code=end
