/*
 * @lc app=leetcode id=3 lang=javascript
 *
 * [3] Longest Substring Without Repeating Characters
 */

// @lc code=start
/**
 * @param {string} s
 * @return {number}
 */

function noRepeat(str) {
  let tmpArr = [str[0]];

  for (let i = 1; i < str.length; i++) {
    const ele = str[i];
    if (tmpArr.includes(ele)) return false;
    tmpArr.push(ele);
  }

  return true;
}

function findFirst(str) {
  const s = str[str.length - 1];
  return str.indexOf(s);
}

function validateLength(len, s, preIdx) {
  let tmpLen = len;
  let newIdx = 0;

  for (let i = 0; i < s.length - tmpLen; i++) {
    while (tmpLen < s.length - len) {
      const subStr = s.slice(i, tmpLen + i + 1);
      // console.log(tmpLen, i);
      // console.log(s, subStr, tmpLen, i);
      if (noRepeat(subStr) && subStr.length > tmpLen) {
        tmpLen = subStr.length;
      } else {
        newIdx = findFirst(subStr) + i;
        break;
      }
    }
  }
  // console.log('newIdx', newIdx, tmpLen, preIdx, s.length);
  if (!newIdx || newIdx - preIdx + 1 >= tmpLen) {
    return [tmpLen, s.slice(preIdx + 1), preIdx + 1];
  }
  return [tmpLen, s.slice(newIdx), newIdx];
}

var lengthOfLongestSubstring = function (s) {
  if (s.length < 2) return s.length;
  let str = s;
  let len = 0;
  let currentIdx = 0;

  while (currentIdx + len <= str.length) {
    [len, str, currentIdx] = validateLength(len, str, currentIdx);
  }

  return len;
};

const time1 = new Date().getTime();
console.log(time1);

// Test Part
// const testStr = [
//   ['bvbj', 3],
//   ['cdd', 2],
//   ['aqwerpaj', 7],
//   ['aaaaa', 1],
//   ['', 0],
//   ['pfbhmipx', 7],
// ];
// let count = 0;
// while (count < testStr.length) {
//   const [input, output] = testStr[count];
//   const result = lengthOfLongestSubstring(input);
//   if (result !== output) {
//     console.log('Wrong !');
//     console.log(input, result);
//     return;
//   }
//   count++;
// }
// console.log('All pass!');

const testStr =
  'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!\\"#$%&\'()*+,-./:;<=>?@[\\\\]^_`{|}~ abcdefghijklmnopqrstuvwxyzABCD';
console.log(
  lengthOfLongestSubstring(
    Array(3).fill(testStr.slice(0, 120)).join('') + testStr
  )
);

const time2 = new Date().getTime();
console.log(time2, time2 - time1);

// @lc code=end
