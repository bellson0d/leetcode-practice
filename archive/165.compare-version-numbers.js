/*
 * @lc app=leetcode id=165 lang=javascript
 *
 * [165] Compare Version Numbers
 */

// @lc code=start
/**
 * @param {string} version1
 * @param {string} version2
 * @return {number}
 */
var getArr = (s1, s2) => {
  let arr1 = s1.split('.').map(Number),
    len1 = arr1.length;
  let arr2 = s2.split('.').map(Number),
    len2 = arr2.length;

  if (len1 !== len2) {
    const diff = Math.abs(len1 - len2);
    if (len1 > len2) {
      arr2 = arr2.concat('0'.repeat(diff).split(''));
    } else {
      arr1 = arr1.concat('0'.repeat(diff).split(''));
    }
  }

  return [arr1, arr2];
};

var compareVersion = function (version1, version2) {
  const [arr1, arr2] = getArr(version1, version2);
  for (let i = 0; i < arr1.length; i++) {
    const ele = arr1[i];
    const ele2 = arr2[i];
    if (ele > ele2) return 1;
    if (ele < ele2) return -1;
  }
  return 0;
};
// @lc code=end

module.exports = compareVersion;
