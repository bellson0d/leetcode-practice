/*
 * @lc app=leetcode id=162 lang=javascript
 *
 * [162] Find Peak Element
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var findPeakElement = function (nums) {
  const len = nums.length;
  if (len < 2) return 0;
  if (len === 2) return nums[0] > nums[1] ? 0 : 1;

  let idx = 0;
  while (idx < len) {
    const pre = nums[idx - 1],
      current = nums[idx],
      next = nums[idx + 1];

    if (idx === 0) {
      if (current > next) return idx;
    } else if (idx === len - 1) {
      if (current > pre) return idx;
    } else if (pre < current && current > next) return idx;
    idx++;
  }

  return null;
};
// @lc code=end
// console.log(findPeakElement([]));
// console.log(findPeakElement([1, 2, 3, 1]));
// console.log(findPeakElement([1, 2, 1, 3, 5, 6, 4]));
