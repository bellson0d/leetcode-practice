/*
 * @lc app=leetcode id=441 lang=javascript
 *
 * [441] Arranging Coins
 */

// @lc code=start
/**
 * @param {number} n
 * @return {number}
 */
var arrangeCoins = function (n) {
  let range = 1;
  while (n >= range) {
    n -= range;
    range++;
  }

  return range - 1;
};
// @lc code=end
// 除了公式外，最优二分法 n / 2 , 就不实现了
// console.log(arrangeCoins(1)); // 1
// console.log(arrangeCoins(2)); // 1
// console.log(arrangeCoins(3)); // 2
// console.log(arrangeCoins(4)); // 2
// console.log(arrangeCoins(5)); // 2
// console.log(arrangeCoins(6)); // 3
// console.log(arrangeCoins(7)); // 3
// console.log(arrangeCoins(8)); // 3
