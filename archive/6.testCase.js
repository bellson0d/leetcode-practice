const convert = require('./6.zig-zag-conversion');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  [['PAYPALISHIRING', 1], 'PAYPALISHIRING'],
  [['1234', 2], '1324'],
  [['PAYPALISHIRING', 3], 'PAHNAPLSIIGYIR'],
  [['PAYPALISHIRING', 4], 'PINALSIGYAHRPI'],
  [['A', 2], 'A'],
  [['AB', 2], 'AB'],
];
let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = convert(input[0], input[1]);
  if (result !== output) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
