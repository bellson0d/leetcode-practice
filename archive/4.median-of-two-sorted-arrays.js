/*
 * @lc app=leetcode id=4 lang=javascript
 *
 * [4] Median of Two Sorted Arrays
 */

// @lc code=start
/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @return {number}
 */

var findMedianSortedArrays = function (nums1, nums2) {
  const tmpIdx = (nums1.length + nums2.length) / 2;
  let mediumIdx = tmpIdx % 1 !== 0 ? ~~tmpIdx : [tmpIdx - 1, tmpIdx];

  const newArr = nums1.concat(nums2).sort((a, b) => a - b);
  //   console.log(tmpIdx, mediumIdx, newArr);
  if (Array.isArray(mediumIdx)) {
    return (newArr[mediumIdx[0]] + newArr[mediumIdx[1]]) / 2;
  }
  return newArr[mediumIdx];
};

module.exports = findMedianSortedArrays;
// @lc code=end
