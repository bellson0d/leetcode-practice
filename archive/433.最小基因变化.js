/*
 * @lc app=leetcode.cn id=433 lang=javascript
 *
 * [433] 最小基因变化
 */

// @lc code=start
/**
 * @param {string} start
 * @param {string} end
 * @param {string[]} bank
 * @return {number}
 */
var oneDiff = (str1, str2) => {
  let count = 0;
  for (let i = 0; i < 8; i++) {
    if (str1[i] !== str2[i]) {
      count++;
      if (count > 1) return false;
    }
  }
  return count === 1;
};
var minMutation = function (start, end, bank) {
  if (!bank.includes(end)) return -1;
  let temp = [];

  for (let i = 0; i < bank.length; i++) {
    if (oneDiff(start, bank[i])) {
      if (bank[i] === end) return 1;
      const newArr = bank.slice(0, i).concat(bank.slice(i + 1));
      if (newArr.length) temp.push([bank[i], newArr]);
    }
  }

  while (temp.length) {
    const [s, arr] = temp.shift();

    for (let i = 0; i < arr.length; i++) {
      if (oneDiff(s, arr[i])) {
        if (arr[i] === end) return bank.length - arr.length + 1;
        const newArr = arr.slice(0, i).concat(arr.slice(i + 1));
        if (newArr.length) temp.push([arr[i], newArr]);
      }
    }
  }

  return -1;
};
// @lc code=end

// console.log(minMutation('AACCGGTT', 'AACCGGTA', ['AACCGGTA']));
// console.log(
//   minMutation('AACCGGTT', 'AAACGGTA', ['AACCGGTA', 'AACCGCTA', 'AAACGGTA'])
// );
// console.log(
//   minMutation('AAAAACCC', 'AACCCCCC', ['AAAACCCC', 'AAACCCCC', 'AACCCCCC'])
// );
// console.log(
//   minMutation('AACCGGTT', 'AAACGGTA', [
//     'AACCGATT',
//     'AACCGATA',
//     'AAACGATA',
//     'AAACGGTA',
//   ])
// );
