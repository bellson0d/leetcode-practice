const targetFun = require('./44.wildcard-matching');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  [['aa', 'a'], false],
  [['aa', '*'], true],
  [['cb', '?a'], false],
  [['adceb', '*a*b'], true],
  [['acdcb', 'a*c?b'], false],
];

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input[0], input[1]);
  if (result !== output) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
