/*
 * @lc app=leetcode.cn id=905 lang=javascript
 *
 * [905] 按奇偶排序数组
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number[]}
 */
var sortArrayByParity = function (nums) {
  const len = nums.length;
  const arr1 = [],
    arr2 = [];

  for (let i = 0; i < len; i++) {
    if (nums[i] % 2 === 0) {
      arr1.push(nums[i]);
    } else {
      arr2.push(nums[i]);
    }
  }

  return [...arr1, ...arr2];
};
// @lc code=end

// console.log(sortArrayByParity([1, 2, 4, 5, 6]));
