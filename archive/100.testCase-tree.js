const targetFun = require('./100.same-tree');
const { getTree } = require('./utils/treeNode');
const IS_ARRAY_RESULT = false; // 输出是否为数组
const MULTI_ARGS = true; // 输入参数是否为多个

const testStr = [
  [[getTree([]), getTree([])], true],
  [[getTree([1]), getTree([])], false],
  [[getTree([]), getTree([1])], false],
  [[getTree([1, 2, null]), getTree([1, null, 2])], false],
  [[getTree([1, 2, null]), getTree([1, 2, null])], true],
  [[getTree([1, 2, 3]), getTree([1, 2, 3])], true],
  [[getTree([1, 2, 3, 4, 5, 6]), getTree([1, 2, 3, 4, 5, 6])], true],
  [[getTree([1, 2, 3, 5, 5, 6]), getTree([1, 2, 3, 4, 5, 6])], false],
  [[getTree([10, 5, 15]), getTree([10, 5, null, null, 15])], false],
];

const time1 = new Date().getTime();
console.log(time1);

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];

  let result;
  if (MULTI_ARGS) {
    result = targetFun(...input);
  } else {
    result = targetFun(input);
  }

  if (
    IS_ARRAY_RESULT ? result.join('|') !== output.join('|') : result !== output
  ) {
    console.log('Number ' + count + ' Case Wrong !!!');
    console.log('Expected output: ', output);
    console.log('Output', result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
