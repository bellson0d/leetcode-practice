/*
 * @lc app=leetcode.cn id=599 lang=javascript
 *
 * [599] 两个列表的最小索引总和
 */

// @lc code=start
/**
 * @param {string[]} list1
 * @param {string[]} list2
 * @return {string[]}
 */
var findRestaurant = function (list1, list2) {
  let obj1 = {},
    obj2 = {};
  list1.forEach((v, i) => (obj1[v] = i));
  list2.forEach((v, i) => {
    if (Number.isInteger(obj1[v])) {
      obj2[v] = i;
    }
  });

  let min = Infinity,
    res = [];
  for (const key of Object.keys(obj2)) {
    const sum = obj1[key] + obj2[key];
    if (sum < min) {
      min = sum;
      res = [key];
    } else if (sum === min) {
      res.push(key);
    }
  }
  return res;
};
// @lc code=end
