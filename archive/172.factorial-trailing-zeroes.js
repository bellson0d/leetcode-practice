/*
 * @lc app=leetcode id=172 lang=javascript
 *
 * [172] Factorial Trailing Zeroes
 */

// @lc code=start
/**
 * @param {number} n
 * @return {number}
 */
var trailingZeroes = function (n) {
  let count = 0;
  while (n) {
    count += ~~(n / 5);
    n /= 5; // root seeking
  }
  return count;
};
// @lc code=end

// module.exports = trailingZeroes;
// 5 * 2 === 10
// 2 always more than 5
// count all 5 times
// 5*5 = 25 5*5*5 = 125 5**4 = 625
