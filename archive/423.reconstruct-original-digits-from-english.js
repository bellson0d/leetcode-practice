/*
 * @lc app=leetcode id=423 lang=javascript
 *
 * [423] Reconstruct Original Digits from English
 */

// @lc code=start
/**
 * @param {string} s
 * @return {string}
 */
const NUM_MAP_1 = [
  { key: 'z', num: 0, strArr: ['z', 'e', 'r', 'o'] },
  { key: 'w', num: 2, strArr: ['t', 'w', 'o'] },
  { key: 'u', num: 4, strArr: ['f', 'o', 'u', 'r'] },
  { key: 'x', num: 6, strArr: ['s', 'i', 'x'] },
  { key: 'g', num: 8, strArr: ['e', 'i', 'g', 'h', 't'] },
];
const NUM_MAP_2 = [
  { key: 'o', num: 1, strArr: ['o', 'n', 'e'] },
  { key: 'h', num: 3, strArr: ['t', 'h', 'r', 'e', 'e'] },
  { key: 'f', num: 5, strArr: ['f', 'i', 'v', 'e'] },
  { key: 's', num: 7, strArr: ['s', 'e', 'v', 'e', 'n'] },
];
const NUM_MAP_3 = [{ key: 'i', num: 9, strArr: ['n', 'i', 'n', 'e'] }];
const NUM_MAPS = [NUM_MAP_1, NUM_MAP_2, NUM_MAP_3];

const removeNum = (s, arr) => {
  while (arr.length) {
    const idx = s.indexOf(arr[0]);
    arr = arr.slice(1);
    s = s.slice(0, idx) + s.slice(idx + 1);
  }
  return s;
};

var originalDigits = function (s) {
  let arr = [];
  const helper = (s) => {
    for (let i = 0; i < NUM_MAPS.length; i++) {
      let numMap = NUM_MAPS[i];
      if (i === 2) {
        arr.push('9'.repeat(s.match(/i/g).length));
        return arr.sort((a, b) => a - b).join('');
      } else {
        while (numMap.length) {
          while (s.includes(numMap[0].key)) {
            arr.push(numMap[0].num);
            s = removeNum(s, numMap[0].strArr);
            if (!s) return arr.sort((a, b) => a - b).join('');
          }
          numMap = numMap.slice(1);
        }
      }
    }
  };
  return helper(s);
};
// @lc code=end
// 更进一步叠加，则是先取到唯一值以后、逐个剔除之前占据的值
// 核心找到唯一值就能解。快慢罢了
// console.log(originalDigits('owoztneoer')); // 012
// console.log(originalDigits('fviefuro')); // 45
// console.log(originalDigits('neoneo')); // 11
// console.log(originalDigits('eeotnwetohr')); // 123
