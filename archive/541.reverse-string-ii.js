/*
 * @lc app=leetcode id=541 lang=javascript
 *
 * [541] Reverse String II
 */

// @lc code=start
/**
 * @param {string} s
 * @param {number} k
 * @return {string}
 */
var reverseStr = function (s, k) {
  return s
    .match(new RegExp(`.{1,${k}}`, 'g'))
    .map((v, i) => (i % 2 === 0 ? v.split('').reverse().join('') : v))
    .join('');
};
// @lc code=end
// console.log(reverseStr('abcdefg', 2));
// console.log(reverseStr('abcd', 2));
