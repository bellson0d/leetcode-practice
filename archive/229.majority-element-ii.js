/*
 * @lc app=leetcode id=229 lang=javascript
 *
 * [229] Majority Element II
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number[]}
 */
var majorityElement = function (nums) {
    const len = nums.length;
    if (len < 2) return nums;
    let obj = {};

    const t = ~~(len / 3);
    nums.forEach((v) => {
        if (obj[v]) {
            obj[v][0]++;
        } else {
            obj[v] = [1, v];
        }
    });
    return Object.values(obj).filter(v => v[0] > t).map(v => v[1]);
};
// @lc code=end

// console.log(majorityElement([3,2,3]))
// console.log(majorityElement([1]))
// console.log(majorityElement([1,2]))
// console.log(majorityElement([2,2]))