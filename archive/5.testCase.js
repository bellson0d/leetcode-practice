const longestPalindrome = require('./5.longest-palindromic-substring');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  ['babad', 'bab'],
  ['cbbd', 'bb'],
  ['ac', 'a'],
  ['b', 'b'],
];
let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = longestPalindrome(input);
  if (result !== output) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
