/*
 * @lc app=leetcode.cn id=860 lang=javascript
 *
 * [860] 柠檬水找零
 */

// @lc code=start
/**
 * @param {number[]} bills
 * @return {boolean}
 */
var lemonadeChange = function (bills) {
  let pre = [0, 0, 0];
  for (const b of bills) {
    if (b === 5) {
      pre[0]++;
    } else if (b === 10) {
      if (pre[0] === 0) return false;
      pre[0]--;
      pre[1]++;
    } else {
      if (pre[0] === 0) return false;
      if (pre[1]) {
        pre[0]--;
        pre[1]--;
      } else {
        if (pre[0] < 3) return false;
        pre[0] -= 3;
      }
      pre[2]++;
    }
  }
  return true;
};
// @lc code=end

// console.log(lemonadeChange([5, 5, 5, 10, 20])); //t
// console.log(lemonadeChange([5])); //t
// console.log(lemonadeChange([5, 5, 10])); //t
// console.log(
//   lemonadeChange([
//     5, 5, 10, 20, 5, 5, 5, 5, 5, 5, 5, 5, 5, 10, 5, 5, 20, 5, 20, 5,
//   ])
// ); //t
// console.log(lemonadeChange([5, 5, 10, 10, 20])); //f
// console.log(lemonadeChange([10, 10])); //f
// console.log(lemonadeChange([10])); //f
