/*
 * @lc app=leetcode id=530 lang=javascript
 *
 * [530] Minimum Absolute Difference in BST
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
var getMinimumDifference = function (root) {
  let set = new Set(),
    nodes = [root];
  while (nodes.length) {
    const { left, right, val } = nodes.pop();
    if (set.has(val)) return 0;
    set.add(val);
    if (left) nodes.push(left);
    if (right) nodes.push(right);
  }

  const arr = Array.from(set).sort((a, b) => a - b);
  let min = Infinity;
  for (let i = 1; i < arr.length; i++) {
    const diff = arr[i] - arr[i - 1];
    if (diff === 1) return 1;
    min = Math.min(diff, min);
  }
  return min;
};
// @lc code=end
