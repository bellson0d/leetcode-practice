/*
 * @lc app=leetcode id=34 lang=javascript
 *
 * [34] Find First and Last Position of Element in Sorted Array
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
function findIdx(arr, val, addN) {
  const len = arr.length;
  if (len < 2) {
    return arr[0] !== val ? -1 : addN;
  }

  const middleIdx = ~~(len / 2);
  const middle = arr[middleIdx];

  if (middle === val) return addN + middleIdx;
  if (middle > val) {
    return findIdx(arr.slice(0, middleIdx), val, addN);
  } else {
    return findIdx(arr.slice(middleIdx + 1), val, addN + middleIdx + 1);
  }
}

var searchRange = function (nums, target) {
  const result = findIdx(nums, target, 0);

  if (result === -1) return [-1, -1];

  let pointer1 = result,
    pointer2 = result;

  while (nums[pointer1] === target || nums[pointer2] === target) {
    if (nums[pointer1] === target) pointer1--;
    if (nums[pointer2] === target) pointer2++;
  }

  pointer1++;
  pointer2--;

  return [pointer1, pointer2];
};

// @lc code=end
module.exports = searchRange;
