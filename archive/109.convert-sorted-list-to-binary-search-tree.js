/*
 * @lc app=leetcode id=109 lang=javascript
 *
 * [109] Convert Sorted List to Binary Search Tree
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {ListNode} head
 * @return {TreeNode}
 */
var helper = (nums) => {
  const len = nums.length;
  if (len === 0) return null;
  if (len === 1) return new TreeNode(nums[0]);

  const mid = ~~(len / 2);
  const root = new TreeNode(nums[mid]);

  root.left = helper(nums.slice(0, mid));
  root.right = helper(nums.slice(mid + 1));

  return root;
};

var sortedListToBST = function (head) {
  let nums = [],
    current = head;
  while (current) {
    nums.push(current.val);
    current = current.next;
  }

  return helper(nums);
};
// @lc code=end
