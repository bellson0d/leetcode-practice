/*
 * @lc app=leetcode id=92 lang=javascript
 *
 * [92] Reverse Linked List II
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} m
 * @param {number} n
 * @return {ListNode}
 */
// const { getList } = require('./utils/listNode');
var reverseBetween = function (head, m, n) {
  if (m === n) return head;

  let count = 1,
    currentNode = head,
    nextNode = currentNode.next,
    tmpNext = null,
    reverseHead = null,
    reverseTail = null;

  while (count <= n) {
    if (count < m) {
      reverseHead = currentNode;
      currentNode = nextNode;
      nextNode = currentNode && currentNode.next;
    } else if (count === m) {
      reverseTail = currentNode;
      tmpNext = currentNode;
      currentNode = nextNode;
      nextNode = currentNode && currentNode.next;
      currentNode.next = tmpNext;
    } else if (count < n) {
      tmpNext = currentNode;
      currentNode = nextNode;
      nextNode = currentNode && currentNode.next;
      currentNode.next = tmpNext;
    } else {
      if (reverseHead) {
        reverseHead.next = currentNode;
      } else {
        reverseHead = currentNode;
      }
      currentNode = nextNode;
      nextNode = currentNode && currentNode.next;
      reverseTail.next = currentNode;
    }

    count++;
  }

  return m === 1 ? reverseHead : head;
};
// @lc code=end
// module.exports = reverseBetween;
// reverseBetween(getList([3, 5]), 1, 2);
