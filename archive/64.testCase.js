const targetFun = require('./64.minimum-path-sum');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  [
    [
      [1, 3, 1],
      [1, 5, 1],
      [4, 2, 1],
    ],
    7,
  ],
  [
    [
      [3, 6],
      [1, 1],
      [2, 1],
    ],
    6,
  ],
  [[[1, 2]], 3],
  [[[]], 0],
  [[[1, 1, 1]], 3],
  [
    [
      [1, 2, 3],
      [4, 5, 6],
    ],
    12,
  ],
];

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input);
  if (result !== output) {
    console.log('Wrong No : ', count);
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
