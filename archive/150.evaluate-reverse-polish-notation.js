/*
 * @lc app=leetcode id=150 lang=javascript
 *
 * [150] Evaluate Reverse Polish Notation
 */

// @lc code=start
/**
 * @param {string[]} tokens
 * @return {number}
 */
var baseMap = {
  '+': (a, b) => Number(a) + Number(b),
  '-': (a, b) => Number(a) - Number(b),
  '*': (a, b) => Number(a) * Number(b),
  '/': (a, b) => ~~(Number(a) / Number(b)),
};
var evalRPN = function (tokens) {
  if (tokens.length < 3) return Number(tokens[0]);
  let idx = 0;
  while (tokens.length >= 3) {
    while (!baseMap[tokens[idx]]) {
      idx++;
    }
    const fn = baseMap[tokens[idx]];
    const a = tokens[idx - 2],
      b = tokens[idx - 1];

    tokens.splice(idx - 2, 3, fn(a, b));
    idx -= 2;
  }

  return tokens[0];
};
// @lc code=end

// module.exports = evalRPN;
// evalRPN(['4', '13', '5', '/', '+']);
