/*
 * @lc app=leetcode.cn id=437 lang=javascript
 *
 * [437] 路径总和 III
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} targetSum
 * @return {number}
 */

var pathSum = function (root, targetSum) {
  if (!root) return 0;
  let count = 0;

  var helper = (node) => {
    if (node.val === targetSum) {
      count++;
    }

    let tmp = [node.val];
    if (!node.left && !node.right) return tmp;
    if (node.left) {
      const leftValues = helper(node.left);

      leftValues.forEach((v) => {
        if (node.val + v === targetSum) {
          count++;
        }
      });

      tmp.push(...leftValues.map((v) => v + node.val));
    }
    if (node.right) {
      const rightValues = helper(node.right);

      rightValues.forEach((v) => {
        if (node.val + v === targetSum) {
          count++;
        }
      });

      tmp.push(...rightValues.map((v) => v + node.val));
    }
    return tmp;
  };
  helper(root);

  return count;
};
// @lc code=end
// const { getTree } = require('./utils/treeNode');
// console.log(pathSum(getTree([10, 5, -3, 3, 2, null, 11, 3, -2, null, 1]), 8));
// console.log(
//   pathSum(getTree([5, 4, 8, 11, null, 13, 4, 7, 2, null, null, 5, 1]), 22)
// );
// console.log(pathSum(getTree([]), 22));
// console.log(pathSum(getTree([1]), 1));
// console.log(pathSum(getTree([1, 2]), 1));
// console.log(pathSum(getTree([1, 2, null, -1, -1]), 1)); // 3
// console.log(pathSum(getTree([1, 1, 1, 1, 1, 2]), 3)); // 3
