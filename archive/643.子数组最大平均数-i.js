/*
 * @lc app=leetcode.cn id=643 lang=javascript
 *
 * [643] 子数组最大平均数 I
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number}
 */
var findMaxAverage = function (nums, k) {
  const len = nums.length;
  let sum = nums.slice(0, k).reduce((a, b) => a + b),
    max = sum / k;

  for (let i = k; i < len; i++) {
    sum += nums[i];
    sum -= nums[i - k];
    max = Math.max(sum / k, max);
  }

  return max;
};
// @lc code=end

// console.log(findMaxAverage([1, 12, -5, -6, 50, 3], 4));
// console.log(findMaxAverage([5], 1));
