/*
 * @lc app=leetcode id=414 lang=javascript
 *
 * [414] Third Maximum Number
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var thirdMax = function (arr) {
  let set = new Set([]);
  for (const a of arr) {
    set.add(a);
  }
  const nums = Array.from(set);

  const len = nums.length;
  if (len === 1) return nums[0];
  if (len === 2) return Math.max(...nums);
  if (len === 3) return Math.min(...nums);
  let [m3, m2, m1] = nums.slice(0, 3).sort((a, b) => a - b);

  for (let i = 3; i < len; i++) {
    const n = nums[i];
    if (n > m1) {
      m3 = m2;
      m2 = m1;
      m1 = n;
    } else if (n > m2) {
      m3 = m2;
      m2 = n;
    } else if (n > m3) {
      m3 = n;
    }
  }
  return m3;
};
// @lc code=end

// console.log(thirdMax([3, 2, 1]));
// console.log(thirdMax([1, 2]));
// console.log(thirdMax([2, 2, 3, 1]));
// console.log(thirdMax([1, 2, 2, 5, 3, 5])); // 2
