/*
 * @lc app=leetcode id=203 lang=javascript
 *
 * [203] Remove Linked List Elements
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} val
 * @return {ListNode}
 */
var removeElements = function (head, val) {
    if (!head) return head;

    let current = head,
        pre = null,
        next = head.next,
        newHead = null;
    while (current) {
        if (current.val === val) {
            current = next;
            pre && (pre.next = current);
        } else {
            pre = current;
            current = next;
        }
        if (!newHead) newHead = pre;
        next = next ? next.next : next;
    }
    return newHead;
};

// @lc code=end
// module.exports = removeElements;
