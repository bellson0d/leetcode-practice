/*
 * @lc app=leetcode id=64 lang=javascript
 *
 * [64] Minimum Path Sum
 */

// @lc code=start
/**
 * @param {number[][]} grid
 * @return {number}
 */
var minPathSum = function (grid) {
  const m = grid.length;
  if (m === 0) return 0;
  const n = grid[0].length;
  if (n === 0) return 0;

  let tmpArr = Array(m)
    .fill(1)
    .map((v) => []);
  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      if (!i && !j) {
        tmpArr[i][j] = grid[0][0];
        continue;
      }
      const self = grid[i][j];

      if (!i) {
        tmpArr[i][j] = self + tmpArr[i][j - 1];
      } else if (!j) {
        tmpArr[i][j] = self + tmpArr[i - 1][j];
      } else {
        const top = tmpArr[i - 1][j];
        const left = tmpArr[i][j - 1];
        tmpArr[i][j] = Math.min(self + left, self + top);
      }
    }
  }

  return tmpArr[m - 1][n - 1];
};
// @lc code=end
// module.exports = minPathSum;
// minPathSum([
//   [1, 3, 1],
//   [1, 5, 1],
//   [4, 2, 1],
// ]);
