/*
 * @lc app=leetcode.cn id=859 lang=javascript
 *
 * [859] 亲密字符串
 */

// @lc code=start
/**
 * @param {string} s
 * @param {string} goal
 * @return {boolean}
 */
var buddyStrings = function (s, goal) {
  if (s.length !== goal.length) return false;
  let diff = 0,
    obj1 = {},
    obj2 = {};
  for (let i = 0; i < s.length; i++) {
    if (s[i] !== goal[i]) diff++;
    if (obj1[s[i]]) {
      obj1[s[i]][0]++;
    } else {
      obj1[s[i]] = [1, s[i]];
    }

    if (obj2[goal[i]]) {
      obj2[goal[i]][0]++;
    } else {
      obj2[goal[i]] = [1, goal[i]];
    }
  }

  const values1 = Object.values(obj1).sort((a, b) => (a[1] > b[1] ? 1 : -1)),
    values2 = Object.values(obj2).sort((a, b) => (a[1] > b[1] ? 1 : -1));
  if (JSON.stringify(values1) !== JSON.stringify(values2)) return false;
  if (diff === 0 && values1.find((v) => v[0] > 1)) return true;
  return diff === 2;
};
// @lc code=end

// console.log(buddyStrings('b', 'a'));
// console.log(buddyStrings('ab', 'ba'));
// console.log(buddyStrings('ab', 'ab'));
// console.log(buddyStrings('aa', 'aa'));
// console.log(buddyStrings('aaaaaaabc', 'aaaaaaacb'));
// console.log(buddyStrings('abcaa', 'abcbb'));
