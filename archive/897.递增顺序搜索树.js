/*
 * @lc app=leetcode.cn id=897 lang=javascript
 *
 * [897] 递增顺序搜索树
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {TreeNode}
 */
var increasingBST = function (root) {
  let arr = [root],
    res = [];
  while (arr.length) {
    const node = arr.pop();
    if (node.left && !node.left.visited) {
      arr.push(node, node.left);
    } else {
      res.push(node.val);
      node.visited = true;
      if (node.right && !node.right.visited) {
        arr.push(node.right);
      }
    }
  }

  const head = new TreeNode(res[0]);
  let current = head;
  for (const n of res.slice(1)) {
    const tmpNode = new TreeNode(n);
    current.right = tmpNode;
    current = tmpNode;
  }
  return head;
};
// @lc code=end

// const { getTree } = require('./utils/treeNode');

// console.log(
//   increasingBST(getTree([5, 3, 6, 2, 4, null, 8, 1, null, null, null, 7, 9]))
// );
