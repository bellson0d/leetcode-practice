/*
 * @lc app=leetcode id=260 lang=javascript
 *
 * [260] Single Number III
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number[]}
 */
var singleNumber = function(nums) {
    const len = nums.length;
    if (len < 4) return nums;

    let obj = {};
    for (const ele of nums) {
        if (obj.hasOwnProperty(ele)) {
            delete obj[ele]
        } else {
            obj[ele] = ele;
        }
    }

    return Object.values(obj)
};
// @lc code=end

// singleNumber([0,0,1,2])