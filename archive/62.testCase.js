const targetFun = require('./62.unique-paths');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  [[3, 2], 3],
  [[7, 3], 28],
  [[8, 3], 36],
  [[9, 4], 165],
  [[11, 6], 3003],
];

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input[0], input[1]);
  if (result !== output) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
