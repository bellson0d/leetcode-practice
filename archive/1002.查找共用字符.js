/*
 * @lc app=leetcode.cn id=1002 lang=javascript
 *
 * [1002] 查找共用字符
 */

// @lc code=start
/**
 * @param {string[]} words
 * @return {string[]}
 */
var commonChars = function (words) {
  let res = {};
  words[0].split('').map((v) => {
    if (res[v]) {
      res[v]++;
    } else {
      res[v] = 1;
    }
  });

  for (let i = 1; i < words.length; i++) {
    let obj = {};
    for (const s of words[i]) {
      if (res[s]) {
        res[s]--;
        if (obj[s]) {
          obj[s]++;
        } else {
          obj[s] = 1;
        }
      }
    }
    if (!Object.keys(obj).length) return [];
    res = obj;
  }

  let tmp = [];
  Object.keys(res).map((k) => tmp.push(...Array(res[k]).fill(k)));
  return tmp;
};
// @lc code=end

// console.log(commonChars(['bella', 'label', 'roller']));
// console.log(commonChars(['cool', 'lock', 'cook']));
// console.log(commonChars(['cool']));
// console.log(commonChars(['abc', 'efg']));
