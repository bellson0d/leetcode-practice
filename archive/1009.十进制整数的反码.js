/*
 * @lc app=leetcode.cn id=1009 lang=javascript
 *
 * [1009] 十进制整数的反码
 */

// @lc code=start
/**
 * @param {number} n
 * @return {number}
 */
var bitwiseComplement = function (n) {
  let str = '';
  for (const s of n.toString(2)) {
    str += s === '0' ? '1' : '0';
  }
  return parseInt(str, 2);
};
// @lc code=end
