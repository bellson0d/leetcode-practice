/*
 * @lc app=leetcode id=14 lang=javascript
 *
 * [14] Longest Common Prefix
 */

// @lc code=start
/**
 * @param {string[]} strs
 * @return {string}
 */
var longestCommonPrefix = function (strs) {
  const len = strs.length;
  if (len === 0) return '';
  const firstStr = strs[0];
  if (len === 1) return firstStr;
  if (!firstStr[0]) return '';

  let str = '';
  let count = 0;

  while (count < firstStr.length) {
    for (let i = 1; i < len; i++) {
      if (firstStr[count] !== strs[i][count]) return str;
    }
    str += firstStr[count];
    count++;
  }

  return str;
};
// @lc code=end
