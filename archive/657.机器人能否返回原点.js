/*
 * @lc app=leetcode.cn id=657 lang=javascript
 *
 * [657] 机器人能否返回原点
 */

// @lc code=start
/**
 * @param {string} moves
 * @return {boolean}
 */
var judgeCircle = function (moves) {
  const len = moves.length;
  if (len % 2 !== 0) return false;

  if (len === 0) return true;
  let obj = { R: 0, L: 0, U: 0, D: 0 };
  for (const m of moves) {
    obj[m]++;
  }
  if (obj.R === obj.L && obj.U === obj.D) return true;
  return false;
};
// @lc code=end
