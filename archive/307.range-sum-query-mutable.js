/*
 * @lc app=leetcode id=307 lang=javascript
 *
 * [307] Range Sum Query - Mutable
 */

// @lc code=start
/**
 * @param {number[]} nums
 */
var NumArray = function (nums) {
  this.arr = nums;
  return this;
};

/**
 * @param {number} index
 * @param {number} val
 * @return {void}
 */
NumArray.prototype.update = function (index, val) {
  this.arr[index] = val;
};

/**
 * @param {number} left
 * @param {number} right
 * @return {number}
 */
NumArray.prototype.sumRange = function (left, right) {
  let mid = (right - left) / 2 + left;
  let sum = 0,
    leftP = ~~mid,
    rightP = ~~mid + 1;
  if (mid === 0) return this.arr[left];

  if (Number.isInteger(mid)) {
    sum += this.arr[mid];
    leftP--;
  }
  while (leftP >= left) {
    sum += this.arr[leftP] + this.arr[rightP];
    leftP--;
    rightP++;
  }
  return sum;
};

/**
 * Your NumArray object will be instantiated and called as such:
 * var obj = new NumArray(nums)
 * obj.update(index,val)
 * var param_2 = obj.sumRange(left,right)
 */
// @lc code=end

// const numArray = new NumArray([1, 3, 5]);
// console.log(numArray.sumRange(0, 2)); // return 1 + 3 + 5 = 9
// numArray.update(1, 2); // nums = [1, 2, 5]
// console.log(numArray.sumRange(0, 2)); // return 1 + 2 + 5 = 8
