/*
 * @lc app=leetcode id=543 lang=javascript
 *
 * [543] Diameter of Binary Tree
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, right, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
var helper = (arr1, arr2) => {
  let idx = 0;
  while (arr1[idx] === arr2[idx]) {
    idx++;
  }
  return arr1.slice(idx).length + arr2.slice(idx).length;
};

var diameterOfBinaryTree = function (root) {
  let nodes = [[root, []]],
    arr = [],
    count = 0,
    max = 0;
  if ((!root.left && root.right) || (!root.right && root.left))
    arr.push([count]);

  while (nodes.length) {
    const [node, pre] = nodes.pop();
    const { val, left, right } = node;
    const newArr = pre.concat([count]);
    count++;
    if (!left && !right) {
      arr.push(newArr);
      continue;
    }
    if (left) nodes.push([left, newArr]);
    if (right) nodes.push([right, newArr]);
  }

  for (let i = 0; i < arr.length - 1; i++) {
    for (let j = i + 1; j < arr.length; j++) {
      max = Math.max(helper(arr[i], arr[j]), max);
    }
  }
  return max;
};
// @lc code=end

// 记录 parent 路径，求公共 -> 暴力比较 O(N**2)
// DFS 求左右子树深度 -> O(N) 复杂度，正解
