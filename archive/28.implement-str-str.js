/*
 * @lc app=leetcode id=28 lang=javascript
 *
 * [28] Implement strStr()
 */

// @lc code=start
/**
 * @param {string} haystack
 * @param {string} needle
 * @return {number}
 */
var strStr = function (haystack, needle) {
  if (!needle) return 0;

  let i = 0;
  const len = haystack.length,
    len2 = needle.length;
  while (i < len) {
    if (haystack[i] === needle[0] && haystack.slice(i, i + len2) === needle)
      return i;
    i++;
  }
  return -1;
};

// console.log(strStr('hello', 'll'));
// console.log(strStr('aaaaaaaa', 'bba'));
// console.log(strStr('sayhelloyoyocheck', 'yoyo'));
// @lc code=end
