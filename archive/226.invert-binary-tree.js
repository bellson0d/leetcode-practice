/*
 * @lc app=leetcode id=226 lang=javascript
 *
 * [226] Invert Binary Tree
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {TreeNode}
 */
var invertTree = function (root) {
    if (!root) return root;
    let arr = [root];

    while (arr.length) {
        const node = arr.pop();
        const { left, right } = node;
        node.left = right;
        node.right = left;
        if (left) arr.unshift(left);
        if (right) arr.unshift(right);
    }
    return root;
};
// @lc code=end
