/*
 * @lc app=leetcode id=384 lang=javascript
 *
 * [384] Shuffle an Array
 */

// @lc code=start
/**
 * @param {number[]} nums
 */

var Solution = function (nums) {
  this.arr = nums.slice();
};

/**
 * Resets the array to its original configuration and return it.
 * @return {number[]}
 */
Solution.prototype.reset = function () {
  return this.arr;
};

var shuffle = (arr) => {
  let n = arr.length - 1,
    i;
  while (n > 0) {
    i = Math.floor((n + 1) * Math.random());
    [arr[i], arr[n]] = [arr[n], arr[i]];
    n--;
  }
  return arr;
};
/**
 * Returns a random shuffling of the array.
 * @return {number[]}
 */
Solution.prototype.shuffle = function () {
  return shuffle(this.arr.slice());
};

// Your Solution object will be instantiated and called as such:
// var obj = new Solution([1, 2, 3]);
// var param_1 = obj.reset();
// var param_2 = obj.shuffle();
// console.log(param_1, param_2);
// @lc code=end
