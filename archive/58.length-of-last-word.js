/*
 * @lc app=leetcode id=58 lang=javascript
 *
 * [58] Length of Last Word
 */

// @lc code=start
/**
 * @param {string} s
 * @return {number}
 */
var lengthOfLastWord = function (preS) {
  const s = preS.trim();
  const len = s.length;
  let count = len - 1;

  while (count >= 0) {
    if (s[count] === ' ') return len - 1 - count;

    count--;
  }

  return len;
};
// @lc code=end
// console.log(lengthOfLastWord('hello world'));
// console.log(lengthOfLastWord('hello world 123'));
// console.log(lengthOfLastWord('helloworld'));
// console.log(lengthOfLastWord('hello world e'));
// console.log(lengthOfLastWord(' helloworld'));
