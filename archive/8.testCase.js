const targetFun = require('./8.string-to-integer-atoi');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  ['42', 42],
  ['   -42', -42],
  ['4193 with words', 4193],
  ['words and 987', 0],
  ['-91283472332', -2147483648],
  ['  -0012a42', -12],
  ['-5-', -5],
  ['-13+8', -13],
  ['+1', 1],
];
let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input);
  if (result !== output) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
