/*
 * @lc app=leetcode id=129 lang=javascript
 *
 * [129] Sum Root to Leaf Numbers
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
var sumNumbers = function (root) {
  if (!root) return 0;
  if (!root.left && !root.right) return root.val;

  let tmpArr = [[root, root.val + '']],
    res = [];

  while (tmpArr.length > 0) {
    const [currentNode, val] = tmpArr.shift();

    if (!currentNode.left && !currentNode.right) {
      res.push(val);
    } else {
      if (currentNode.left)
        tmpArr.unshift([currentNode.left, val + currentNode.left.val]);
      if (currentNode.right)
        tmpArr.unshift([currentNode.right, val + currentNode.right.val]);
    }
  }

  return res.reduce((a, b) => ~~a + ~~b, 0);
};

// @lc code=end
// module.exports = sumNumbers;
// const { getTree } = require('./utils/treeNode');
// console.log(getTree([4, 9, 0, 5, 1]));
// console.log(sumNumbers(getTree([4, 9, 0, 5, 1])));
