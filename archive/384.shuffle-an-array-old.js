/*
 * @lc app=leetcode id=384 lang=javascript
 *
 * [384] Shuffle an Array
 */

// @lc code=start
/**
 * @param {number[]} nums
 */
function ListNode(val, next) {
  this.val = val === undefined ? 0 : val;
  this.next = next === undefined ? null : next;
}

var linkArr = (arr) => {
  let head = new ListNode(arr[0]),
    currentNode = head;
  for (let i = 1; i < arr.length; i++) {
    const ele = arr[i];
    const node = new ListNode(ele);
    currentNode.next = node;
    currentNode = node;
  }
  currentNode.next = head;
  return head;
};

var Solution = function (nums) {
  this.arr = nums;
};

/**
 * Resets the array to its original configuration and return it.
 * @return {number[]}
 */
Solution.prototype.reset = function () {
  return this.arr;
};

var shuffle = (arr) => {
  const len = arr.length;
  let preNode = linkArr(arr),
    currentNode = preNode.next,
    res = [],
    count = 1;

  while (count <= len) {
    let steps = ~~(Math.random() * (len - count));
    while (steps) {
      preNode = currentNode;
      currentNode = currentNode.next;
      steps--;
    }
    res.push(currentNode.val);
    preNode.next = currentNode.next;
    currentNode = currentNode.next;
    count++;
  }
  return res;
};
/**
 * Returns a random shuffling of the array.
 * @return {number[]}
 */
Solution.prototype.shuffle = function () {
  return shuffle(this.arr);
};

// Your Solution object will be instantiated and called as such:
// var obj = new Solution([1, 2, 3]);
// var param_1 = obj.reset();
// var param_2 = obj.shuffle();
// console.log(param_1, param_2);
// @lc code=end
