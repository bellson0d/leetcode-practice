// @lc code=start
/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */
const log = console.log;

function ListNode(val) {
  this.val = val;
  this.next = null;
}

const addTwo = (a, b, c) => {
  const sum = Number(a) + Number(b) + c;
  return sum > 9 ? [sum % 10, 1] : [sum, 0];
};

var addTwoNumbers = function (a, b) {
  let node1 = a;
  let node2 = b;
  let nodes = [new ListNode(0)];
  let count = 0;
  let carry = 0;

  while (node1 || node2 || carry) {
    const currentNode = nodes[count];
    const nextNode = new ListNode(0);
    if (count > 0) {
      nodes[count - 1].next = currentNode;
    }

    const [val, carr] = addTwo(node1?.val ?? 0, node2?.val ?? 0, carry);
    currentNode.val = val;

    count++;
    nodes[count] = nextNode;
    carry = carr;
    node1 = node1?.next ?? null;
    node2 = node2?.next ?? null;
  }

  return nodes[0];
};

// @lc code=end
