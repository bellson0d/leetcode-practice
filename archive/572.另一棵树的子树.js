/*
 * @lc app=leetcode.cn id=572 lang=javascript
 *
 * [572] 另一棵树的子树
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {TreeNode} subRoot
 * @return {boolean}
 */
var helper = (equalTree = (tree1, tree2) => {
  let arr = [tree1];
  let arr2 = [tree2];
  while (arr.length) {
    const node1 = arr.pop();
    const node2 = arr2.pop();

    if (node1.val !== node2.val) return false;
    if (node1.left && !node2.left) return false;
    if (node1.right && !node2.right) return false;
    if (node2.left && !node1.left) return false;
    if (node2.right && !node1.right) return false;

    if (node1.left) {
      arr.push(node1.left);
      arr2.push(node2.left);
    }
    if (node1.right) {
      arr.push(node1.right);
      arr2.push(node2.right);
    }
  }
  return true;
});
var isSubtree = function (root, subRoot) {
  let arr = [root];
  while (arr.length) {
    const node = arr.pop();
    if (helper(node, subRoot)) return true;

    if (node.left) {
      arr.push(node.left);
    }
    if (node.right) {
      arr.push(node.right);
    }
  }
  return false;
};
// @lc code=end

// const { getTree } = require('./utils/treeNode');
// console.log(isSubtree(getTree([3, 4, 5, 1, 2]), getTree([4, 1, 2]))); // true
// console.log(
//   isSubtree(
//     getTree([3, 4, 5, 1, 2, null, null, null, null, 0]),
//     getTree([4, 1, 2])
//   )
// ); // false
