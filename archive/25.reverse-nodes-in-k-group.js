/*
 * @lc app=leetcode id=25 lang=javascript
 *
 * [25] Reverse Nodes in k-Group
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} k
 * @return {ListNode}
 */

function ListNode(val, next) {
  this.val = val === undefined ? 0 : val;
  this.next = next === undefined ? null : next;
}

var reverseKGroup = function (head, k) {
  if (k < 2 || !head || !head.next) return head;

  let tmpNode = head,
    tmpArr = [tmpNode];
  while (tmpNode.next) {
    tmpNode = tmpNode.next;
    if (tmpNode) {
      tmpArr.push(tmpNode);
    }
  }

  const preLen = tmpArr.length;
  const len = Math.ceil(preLen / k);
  let tmpArr2 = Array(len),
    count = 0;
  while (count < preLen) {
    const idx = ~~(count / k);
    if (tmpArr2[idx]) {
      tmpArr2[idx].push(tmpArr[count]);
    } else {
      tmpArr2[idx] = [tmpArr[count]];
    }
    count++;
  }

  let result = null;
  let tail = null;
  for (let i = 0; i < tmpArr2.length; i++) {
    let nodes = tmpArr2[i];
    const nLen = nodes.length;
    if (nLen === k) {
      nodes.reverse();
    } else {
      if (i === 0) return nodes[0];
    }

    if (tail) {
      tail.next = nodes[0];
      if (nLen < k) {
        tail = nodes[nLen - 1];
        break;
      }
    }
    if (!result) {
      result = nodes[0];
    }

    let count = 0;
    while (count < nodes.length - 1) {
      nodes[count].next = nodes[count + 1];
      count++;
    }

    tail = nodes[nLen - 1];
  }
  tail.next = null;

  return result;
};

// let arr = [new ListNode(1)];
// let count = 2;
// while (count < 6) {
//   const node = new ListNode(count);
//   arr[count - 2].next = node;
//   arr.push(node);
//   count++;
// }

// let result = reverseKGroup(arr[0], 2);
// while (result) {
//   console.log(result.val);
//   result = result.next;
// }

// @lc code=end
