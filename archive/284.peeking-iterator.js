/*
 * @lc app=leetcode id=284 lang=javascript
 *
 * [284] Peeking Iterator
 */

// @lc code=start
/**
 * // This is the Iterator's API interface.
 * // You should not implement it, or speculate about its implementation.
 * function Iterator() {
 *    @ return {number}
 *    this.next = function() { // return the next number of the iterator
 *       ...
 *    };
 *
 *    @return {boolean}
 *    this.hasNext = function() { // return true if it still has numbers
 *       ...
 *    };
 * };
 */

/**
 * @param {Iterator} iterator
 */
var PeekingIterator = function (iterator) {
  this.pointer = -1;
  this.arr = [];
  this.iterator = iterator;
  return this;
};

/**
 * @return {number}
 */
PeekingIterator.prototype.peek = function () {
  if (!this.arr[this.pointer + 1]) {
    this.arr[this.pointer + 1] = this.iterator.next();
  }
  return this.arr[this.pointer + 1];
};

/**
 * @return {number}
 */
PeekingIterator.prototype.next = function () {
  this.pointer++;
  if (!this.arr[this.pointer]) {
    this.arr[this.pointer] = this.iterator.next();
  }
  return this.arr[this.pointer];
};

/**
 * @return {boolean}
 */
PeekingIterator.prototype.hasNext = function () {
  if (!this.arr[this.pointer + 1]) {
    return this.iterator.hasNext();
  } else {
    return true;
  }
};

/**
 * Your PeekingIterator object will be instantiated and called as such:
 * var obj = new PeekingIterator(arr)
 * var param_1 = obj.peek()
 * var param_2 = obj.next()
 * var param_3 = obj.hasNext()
 */
// @lc code=end

// const peekingIterator = new PeekingIterator([1, 2, 3]); // [1,2,3]
// console.log(peekingIterator.next()); // return 1, the pointer moves to the next element [1,2,3].
// console.log(peekingIterator.peek()); // return 2, the pointer does not move [1,2,3].
// console.log(peekingIterator.next()); // return 2, the pointer moves to the next element [1,2,3]
// console.log(peekingIterator.next()); // return 3, the pointer moves to the next element [1,2,3]
// console.log(peekingIterator.hasNext()); // return False
