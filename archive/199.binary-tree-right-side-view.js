/*
 * @lc app=leetcode id=199 lang=javascript
 *
 * [199] Binary Tree Right Side View
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
var rightSideView = function (root) {
  if (!root) return [];
  let res = [root.val],
    arr = [root],
    next = [];
  while (arr) {
    const node = arr.shift();
    if (node.right) next.push(node.right);
    if (node.left) next.push(node.left);
    if (arr.length === 0) {
      if (next.length === 0) {
        return res;
      } else {
        res.push(next[0].val);
        arr = next;
        next = [];
      }
    }
  }
};
// @lc code=end
// const { getTree } = require('./utils/treeNode');
// console.log(rightSideView(getTree([1, 2, 3, null, 5, null, 4]))); // [1,3,4]
// console.log(rightSideView(getTree([1, null, 3]))); // [1,3]
// console.log(rightSideView(getTree([1, 2, null, 3]))); // [1,2,3]
// console.log(rightSideView(getTree([]))); // []
