/*
 * @lc app=leetcode.cn id=746 lang=javascript
 *
 * [746] 使用最小花费爬楼梯
 */

// @lc code=start
/**
 * @param {number[]} cost
 * @return {number}
 */
var minCostClimbingStairs = function (cost) {
  const len = cost.length;
  if (len === 2) return Math.min(...cost);
  if (len === 3) return Math.min(cost[0] + cost[2], cost[1]);

  const arr = [0, 0];
  for (let i = 2; i < len; i++) {
    const val1 = cost[i - 1] + arr[i - 1];
    const val2 = cost[i - 2] + arr[i - 2];
    if (val1 < val2) {
      arr[i] = val1;
    } else {
      arr[i] = val2;
    }
  }

  return Math.min(arr[len - 1] + cost[len - 1], arr[len - 2] + cost[len - 2]);
};
// @lc code=end

// console.log(minCostClimbingStairs([2, 1]));
// console.log(minCostClimbingStairs([1, 2]));
// console.log(minCostClimbingStairs([10, 15, 20])); // 15
// console.log(minCostClimbingStairs([10, 31, 20]));
// console.log(minCostClimbingStairs([10, 28, 20, 1]));
// console.log(minCostClimbingStairs([1, 100, 1, 1, 1, 100, 1, 1, 100, 1])); // 6
// console.log(minCostClimbingStairs([1, 100, 1, 1, 1, 100, 1, 1, 100, 101])); // 6
