/*
 * @lc app=leetcode id=268 lang=javascript
 *
 * [268] Missing Number
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var missingNumber = function (nums) {
    let arr = [];
    for (const n of nums) {
        arr[n] = n;
    }

    for (let i = 0; i < arr.length; i++) {
        if (arr[i] === undefined) return i;
    }
    return nums.length;
};
// @lc code=end

// console.log(missingNumber([3, 0, 1]));
