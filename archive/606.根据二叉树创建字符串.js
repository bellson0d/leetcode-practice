/*
 * @lc app=leetcode.cn id=606 lang=javascript
 *
 * [606] 根据二叉树创建字符串
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {string}
 */
var tree2str = function (node) {
  if (!node) return '';
  if (!node.left && !node.right) return `${node.val}`;
  if (!node.left) return `${node.val}()(${tree2str(node.right)})`;
  if (!node.right) return `${node.val}(${tree2str(node.left)})`;
  return `${node.val}(${tree2str(node.left)})(${tree2str(node.right)})`;
};
// @lc code=end
// const { getTree } = require('./utils/treeNode');

// console.log(tree2str(getTree([])));
// console.log(tree2str(getTree([1])));
// console.log(tree2str(getTree([1, 2, 3])));
// console.log(tree2str(getTree([1, 2, null, 4])));
// console.log(tree2str(getTree([1, 2, 3, 4])));
// console.log(tree2str(getTree([1, 2, 3, null, 4])));
// console.log(tree2str(getTree([1, 2, 3, null, 4, 9])));
// console.log(tree2str(getTree([1, 2, 3, null, 4, null, 9])));
// console.log(tree2str(getTree([1, 2, 3, null, 4, 8, 9])));
