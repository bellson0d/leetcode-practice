/*
 * @lc app=leetcode id=118 lang=javascript
 *
 * [118] Pascal's Triangle
 */

// @lc code=start
/**
 * @param {number} numRows
 * @return {number[][]}
 */
var generate = function (numRows) {
  if (numRows === 0) return [];
  if (numRows === 1) return [[1]];
  if (numRows === 2) return [[1], [1, 1]];

  let result = [[1], [1, 1]];

  let count = 2;
  while (count < numRows) {
    const tmpArr = result[count - 1];
    let newArr = [];

    for (let i = 0; i < tmpArr.length - 1; i++) {
      newArr[i] = tmpArr[i] + tmpArr[i + 1];
    }
    newArr.unshift(1);
    newArr.push(1);

    result.push(newArr);
    count++;
  }

  return result;
};
// @lc code=end

// console.log(generate(5));
