/*
 * @lc app=leetcode.cn id=443 lang=javascript
 *
 * [443] 压缩字符串
 */

// @lc code=start
/**
 * @param {character[]} chars
 * @return {number}
 */
var compress = function (chars) {
  const len = chars.length;
  if (len === 1) return 1;

  let count = 1,
    s = chars[0],
    idx = 1,
    res = '';
  while (idx < len) {
    if (s != chars[idx]) {
      res += s;
      if (count > 1) res += count;
      s = chars[idx];
      count = 1;
    } else {
      count++;
    }
    idx++;
  }
  res += s;
  if (count > 1) res += count;

  res.split('').forEach((v, i) => (chars[i] = v));
  chars.splice(res.length);
  return res.length;
};
// @lc code=end

// console.log(compress(['a', 'a', 'b', 'b', 'c', 'c', 'c']));
// console.log(compress(['a']));
// console.log(
//   compress(['a', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b'])
// );
// console.log(compress(['a', 'a', 'b', 'b', 'c', 'c', 'c']));
