/*
 * @lc app=leetcode id=23 lang=javascript
 *
 * [23] Merge k Sorted Lists
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode[]} lists
 * @return {ListNode}
 */

function ListNode(val, next) {
  this.val = val === undefined ? 0 : val;
  this.next = next === undefined ? null : next;
}

var mergeTwoLists = function (l1, l2) {
  if (!l1) return l2;
  if (!l2) return l1;

  let head, currentNode, node1, node2;
  if (l1.val < l2.val) {
    head = currentNode = l1;
    node1 = l1.next;
    node2 = l2;
  } else {
    head = currentNode = l2;
    node2 = l2.next;
    node1 = l1;
  }

  while (node1 || node2) {
    if (!node1) {
      currentNode.next = node2;
      node2 = null;
      continue;
    }
    if (!node2) {
      currentNode.next = node1;
      node1 = null;
      continue;
    }
    if (node1.val < node2.val) {
      currentNode.next = node1;
      currentNode = node1;
      node1 = node1.next;
    } else {
      currentNode.next = node2;
      currentNode = node2;
      node2 = node2.next;
    }
  }

  return head;
};

var mergeKLists = function (lists) {
  const len = lists.length;
  if (len === 0) return null;
  if (len === 1) return lists[0];

  let count = 0;
  let l1 = lists[count];
  let l2 = lists[count + 1];

  while (count < len - 1) {
    count++;
    l1 = mergeTwoLists(l1, l2);
    l2 = lists[count + 1];
  }

  return l1;
};
// @lc code=end
