/*
 * @lc app=leetcode id=101 lang=javascript
 *
 * [101] Symmetric Tree
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {boolean}
 */

var isSymmetric = function (root) {
  if (!root) return true;

  let leftArr = [root.left],
    rightArr = [root.right];

  while (leftArr.length > 0 || rightArr.length > 0) {
    const leftNode = leftArr.pop();
    const rightNode = rightArr.pop();

    if (!leftNode || !rightNode) {
      if (leftNode !== rightNode) return false;
      continue;
    } else {
      if (leftNode.val !== rightNode.val) return false;

      leftArr.push(leftNode.left);
      leftArr.push(leftNode.right);
      rightArr.push(rightNode.right);
      rightArr.push(rightNode.left);
    }
  }

  return true;
};
// @lc code=end
// module.exports = isSymmetric;
