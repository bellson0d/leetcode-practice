/*
 * @lc app=leetcode id=258 lang=javascript
 *
 * [258] Add Digits
 */

// @lc code=start
/**
 * @param {number} num
 * @return {number}
 */
var addDigits = function(num) {
    while (num > 9) {
        num = String(num).split("").reduce((a, b) => ~~a + ~~b, 0);
    }
    return num;
};
// @lc code=end

