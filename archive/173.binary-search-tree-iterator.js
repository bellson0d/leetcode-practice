/*
 * @lc app=leetcode id=173 lang=javascript
 *
 * [173] Binary Search Tree Iterator
 */

// @lc code=start
// Definition for a binary tree node.
// function TreeNode(val, left, right) {
//   this.val = val === undefined ? 0 : val;
//   this.left = left === undefined ? null : left;
//   this.right = right === undefined ? null : right;
// }

/**
 * @param {TreeNode} root
 */
var BSTIterator = function (root) {
  this.arr = [];
  while (root) {
    this.arr.unshift(root);
    root = root.left;
  }
  return this;
};

/**
 * @return {number}
 */
BSTIterator.prototype.next = function () {
  let { val, right } = this.arr.shift();
  while (right) {
    this.arr.unshift(right);
    right = right.left;
  }
  return val;
};

/**
 * @return {boolean}
 */
BSTIterator.prototype.hasNext = function () {
  return this.arr.length > 0;
};

/**
 * Your BSTIterator object will be instantiated and called as such:
 * var obj = new BSTIterator(root)
 * var param_1 = obj.next()
 * var param_2 = obj.hasNext()
 */
// @lc code=end

// const { getTree } = require('./utils/treeNode');
// let bSTIterator = new BSTIterator(getTree([7, 3, 15, null, null, 9, 20]));
// console.log(bSTIterator.next()); // return 3
// console.log(bSTIterator.next()); // return 7
// console.log(bSTIterator.hasNext()); // return True
// console.log(bSTIterator.next()); // return 9
// console.log(bSTIterator.hasNext()); // return True
// console.log(bSTIterator.next()); // return 15
// console.log(bSTIterator.hasNext()); // return True
// console.log(bSTIterator.next()); // return 20
// console.log(bSTIterator.hasNext()); // return False
