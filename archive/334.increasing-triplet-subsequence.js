/*
 * @lc app=leetcode id=334 lang=javascript
 *
 * [334] Increasing Triplet Subsequence
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {boolean}
 */
var increasingTriplet = function (nums) {
  let res = [],
    min = Infinity;
  for (const n of nums) {
    let flag = false;
    res = res.map((v) => {
      if (v[0] < n) {
        if (v.length === 2) flag = true;
        return [n].concat(v);
      } else if (v.length === 2 && v[v.length - 1] < n) {
        return [n, v[1]];
      }
      return v;
    });
    if (n < min) {
      min = n;
      res.push([n]);
    }
    if (flag) return true;
  }
  return false;
};
// @lc code=end

// console.log(increasingTriplet([1, 2, 3, 4, 5]));
// console.log(increasingTriplet([5, 4, 3, 2, 1]));
// console.log(increasingTriplet([2, 1, 5, 0, 4, 6]));
// console.log(increasingTriplet([1, 5, 0, 4, 1, 3]));
