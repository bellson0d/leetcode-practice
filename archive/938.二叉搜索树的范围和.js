/*
 * @lc app=leetcode.cn id=938 lang=javascript
 *
 * [938] 二叉搜索树的范围和
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} low
 * @param {number} high
 * @return {number}
 */
var rangeSumBST = function (root, low, high) {
  let sum = 0,
    nodes = [root];
  while (nodes.length) {
    const node = nodes.pop();

    if (!node.visited) {
      node.visited = true;
      if (node.left && node.right) {
        nodes.push(node.right, node, node.left);
        continue;
      } else if (node.left) {
        nodes.push(node, node.left);
        continue;
      } else if (node.right) {
        nodes.push(node.right, node);
        continue;
      }
    }
    if (node.val >= low && node.val <= high) sum += node.val;
  }

  return sum;
};
// @lc code=end

// const { getTree } = require('./utils/treeNode');

// console.log(rangeSumBST(getTree([10, 5, 15, 3, 7, null, 18]), 7, 15));
// console.log(rangeSumBST(getTree([10, 5, 15, 3, 7, 13, 18, 1, null, 6]), 6, 10));
