/*
 * @lc app=leetcode id=44 lang=javascript
 *
 * [44] Wildcard Matching
 */

// @lc code=start
/**
 * @param {string} s
 * @param {string} p
 * @return {boolean}
 */
const compare = (str1, str2) => {
  if (str2 === '*') return 1;
  if (str2 === str1 || str2 === '?') return 0;
  return -1;
};

const allStar = /^[\*]+$/;

let tmpArr = [];

var isMatch = function (s, p) {
  const len1 = s.length;
  const len2 = p.length;

  for (let i = 0; i < len1; i++) {
    const ele1 = s[i];

    for (let j = 0; j < len2; j++) {
      const ele2 = p[j];
      const type = compare(ele1, ele2);

      if (type === -1) {
        const backTrace = tmpArr.pop();
        if (!backTrace) return false;

        return isMatch();
      }

      //   if (j < len2 - 1 && i < len1 - 1) {
      //     if (type === 0) isMatch(s.slice(i + 1), p.slice(j + 1));
      //     if (type === 1) isMatch(s.slice(i + 1), p);
      //   } else {
      //     if (type === 0) {
      //       if (j === len2 - 1 && i === len1 - 1) return true;
      //     }
      //     if (type === 1) {
      //       if (i === len1 - 1) {
      //         if (j === len2 - 1 || allStar(p.slice(j))) {
      //           return true;
      //         }
      //       } else {
      //           isMatch(s.slice(i + 1), p);
      //       }
      //     }
      //   }
    }
  }
};
// @lc code=end

module.exports = isMatch;
