/*
 * @lc app=leetcode id=65 lang=javascript
 *
 * [65] Valid Number
 */

// @lc code=start
/**
 * @param {string} s
 * @return {boolean}
 */
var isNumber = function (preS) {
  const s = preS.trim();
  if (!s) return false;
  return !isNaN(Number(s));
};
// @lc code=end
