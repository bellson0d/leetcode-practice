/*
 * @lc app=leetcode id=299 lang=javascript
 *
 * [299] Bulls and Cows
 */

// @lc code=start
/**
 * @param {string} secret
 * @param {string} guess
 * @return {string}
 */
var getHint = function (secret, guess) {
  let str1 = '',
    str2 = [],
    count1 = 0,
    count2 = 0;

  for (let i = 0; i < secret.length; i++) {
    const ele1 = secret[i];
    const ele2 = guess[i];
    if (ele1 === ele2) {
      count1++;
    } else {
      str1 += ele1;
      str2.push(ele2);
    }
  }

  for (let i = 0; i < str1.length; i++) {
    const ele = str1[i];
    const idx = str2.indexOf(ele);
    if (idx !== -1) {
      count2++;
      str2 = str2.slice(0, idx).concat(str2.slice(idx + 1));
    }
  }

  return count1 + 'A' + count2 + 'B';
};
// @lc code=end

// console.log(getHint('1807', '7810')); // "1A3B"
// console.log(getHint('1123', '0111')); // "1A1B"
// console.log(getHint('1', '0')); // "0A0B"
// console.log(getHint('1', '1')); // "1A0B"
