const targetFun = require('./42.trapping-rain-water');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  [[0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1], 6],
  [[0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0], 8],
  [[0, 2, 0, 4, 0, 2, 0, 4, 0, 2, 0], 14],
];

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input);
  if (result !== output) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
