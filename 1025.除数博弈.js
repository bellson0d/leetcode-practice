/*
 * @lc app=leetcode.cn id=1025 lang=javascript
 *
 * [1025] 除数博弈
 */

// @lc code=start
/**
 * @param {number} n
 * @return {boolean}
 */
var divisorGame = function (n) {
  return n % 2 === 0;
};
// @lc code=end

// console.log(divisorGame(2)); // true
// console.log(divisorGame(3)); // false
// console.log(divisorGame(4)); // true
// console.log(divisorGame(5)); // false
// console.log(divisorGame(6)); // true
// console.log(divisorGame(7)); // false
// console.log(divisorGame(8)); // true
// console.log(divisorGame(9)); // false
// console.log(divisorGame(10)); // true
// console.log(divisorGame(11)); // false
// console.log(divisorGame(12)); // true
// console.log(divisorGame(13)); // false
// console.log(divisorGame(14)); // true
// console.log(divisorGame(15)); // false
