/*
 * @lc app=leetcode id=84 lang=javascript
 *
 * [84] Largest Rectangle in Histogram
 */

// @lc code=start
/**
 * @param {number[]} heights
 * @return {number}
 */

var largestRectangleArea = function (height) {
  const hLen = height.length;
  if (hLen < 2) return height[0] || 0;

  let max = 0,
    i = 0,
    stack = [];

  while (i < hLen + 1) {
    if (stack.length === 0 || height[i] >= height[stack[stack.length - 1]]) {
      stack.push(i);
      i++;
    } else {
      const idx = stack.pop();
      const h = height[idx];
      const w = stack.length > 0 ? i - stack[stack.length - 1] - 1 : i;
      if (h * w > max) max = h * w;
    }
  }

  while (stack.length > 0) {
    const idx = stack.pop();
    const h = height[idx];
    const w = stack.length > 0 ? i - stack[stack.length - 1] - 1 : i;
    if (h * w > max) max = h * w;
  }

  return max;
};
// @lc code=end

module.exports = largestRectangleArea;
// console.log(largestRectangleArea([2, 1, 5, 6, 2, 3]));
// console.log(largestRectangleArea([5, 5, 1, 7, 1, 1, 5, 2, 7, 6]));
// console.log(largestRectangleArea([1, 1]));
