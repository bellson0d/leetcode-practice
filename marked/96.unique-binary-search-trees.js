/*
 * @lc app=leetcode id=96 lang=javascript
 *
 * [96] Unique Binary Search Trees
 */

// @lc code=start
/**
 * @param {number} n
 * @return {number}
 */
var numTrees = function (n) {
  if (n === 1) return 1;
  let baseMap = Array(n + 1).fill(0);
  baseMap[0] = 1;
  baseMap[1] = 1;

  for (let i = 2; i <= n; i++) {
    for (let j = 1; j <= i; j++) {
      baseMap[i] += baseMap[j - 1] * baseMap[i - j];
    }
  }

  return baseMap[n];
};
// @lc code=end
// DPPPPPPPPPPPPPPPPPPPPPPPPPP
// console.log(numTrees(1));
// console.log(numTrees(2));
// console.log(numTrees(3));
// console.log(numTrees(19));
