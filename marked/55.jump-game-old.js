/*
 * @lc app=leetcode id=55 lang=javascript
 *
 * [55] Jump Game
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {boolean}
 */

var canJump = function (preNums) {
  const len = preNums.length;
  let currentEle = preNums[0];
  if (len === 1) return true;
  if (len === 0 || currentEle === 0) return false;

  if (currentEle >= len - 1) return true;

  let preVal = preNums[0];
  let nums = [preVal];
  let count = 1;
  while (count < len) {
    const ele = preNums[count];
    if (ele !== 1 || preVal !== 1) {
      nums.push(ele);
    }
    preVal = ele;
    count++;
  }

  let tmpArr = [0];

  const helpFn = (nums, i) => {
    const len = nums.length;
    let currentEle = nums[0];
    if (len === 1) return true;
    if (len === 0 || currentEle === 0) return false;

    if (currentEle >= len - 1) return true;

    for (let j = currentEle; j > 0; j--) {
      const newIdx = i + j;
      if (!tmpArr.includes(newIdx)) {
        tmpArr.push(newIdx);
        let res = helpFn(nums.slice(j), newIdx);
        if (res) return true;
      }
    }

    return false;
  };

  return helpFn(nums, 0);
};

// @lc code=end
// module.exports = canJump;
