/*
 * @lc app=leetcode id=146 lang=javascript
 *
 * [146] LRU Cache
 */

// @lc code=start
/**
 * @param {number} capacity
 */
function ListNode(key, val, pre, next) {
  this.key = key === undefined ? 0 : key;
  this.val = val === undefined ? 0 : val;
  this.pre = pre === undefined ? null : pre;
  this.next = next === undefined ? null : next;
  return this;
}

var LRUCache = function (capacity) {
  this.headNode = null;
  this.tailNode = null;
  this.obj = {};
  this.count = 0;
  this.maxCount = capacity;
};

/**
 * @param {number} key
 * @return {number}
 */
LRUCache.prototype.get = function (key) {
  let currentNode = this.obj[key];
  if (currentNode) {
    let val = currentNode.val;
    if (this.headNode.key === key) {
      let newHead = this.headNode.next || this.headNode;
      newHead.pre = null;
      this.tailNode.next = this.headNode;
      this.headNode.next = null;
      this.headNode.pre = this.tailNode;
      this.tailNode = this.headNode;
      this.headNode = newHead;
    } else if (this.tailNode.key !== key) {
      let preNode = currentNode.pre;
      let nextNode = currentNode.next;
      preNode.next = nextNode;
      nextNode.pre = preNode;
      currentNode.next = null;
      currentNode.pre = this.tailNode;
      this.tailNode.next = currentNode;
      this.tailNode = currentNode;
    }

    return val;
  }
  return -1;
};

/**
 * @param {number} key
 * @param {number} value
 * @return {void}
 */
LRUCache.prototype.put = function (key, value) {
  let currentNode = this.obj[key];
  if (currentNode) {
    currentNode.val = value;
    if (this.headNode.key === key) {
      let newHead = this.headNode.next || this.headNode;
      newHead.pre = null;
      this.tailNode.next = this.headNode;
      this.headNode.next = null;
      this.headNode.pre = this.tailNode;
      this.tailNode = this.headNode;
      this.headNode = newHead;
    } else if (this.tailNode.key !== key) {
      let preNode = currentNode.pre;
      let nextNode = currentNode.next;
      preNode.next = nextNode;
      nextNode.pre = preNode;
      currentNode.next = null;
      currentNode.pre = this.tailNode;
      this.tailNode.next = currentNode;
      this.tailNode = currentNode;
    }
  } else {
    let newNode = new ListNode(key, value);
    this.obj[key] = newNode;
    if (this.count === 0) {
      this.headNode = newNode;
      this.tailNode = newNode;
      this.count = 1;
      return;
    }

    if (this.count === this.maxCount) {
      delete this.obj[this.headNode.key];
      this.headNode = this.headNode.next || this.headNode;
      this.headNode.pre = null;
    } else {
      this.count++;
    }

    newNode.pre = this.tailNode;
    this.tailNode.next = newNode;
    this.tailNode = newNode;
  }
};

/**
 * Your LRUCache object will be instantiated and called as such:
 * var obj = new LRUCache(capacity)
 * var param_1 = obj.get(key)
 * obj.put(key,value)
 */
// @lc code=end

// let lRUCache = new LRUCache(2);
// lRUCache.put(1, 1); // cache is {1=1}
// lRUCache.put(2, 2); // cache is {1=1, 2=2}
// console.log(lRUCache.get(1)); // return 1
// lRUCache.put(3, 3); // LRU key was 2, evicts key 2, cache is {1=1, 3=3}
// console.log(lRUCache.get(2)); // returns -1 (not found)
// lRUCache.put(4, 4); // LRU key was 1, evicts key 1, cache is {4=4, 3=3}
// console.log(lRUCache.get(1)); // return -1 (not found)
// console.log(lRUCache.get(3)); // return 3
// console.log(lRUCache.get(4)); // return 4

// let lRUCache = new LRUCache(1);
// lRUCache.put(2, 1); // cache is {1=1}
// console.log(lRUCache.get(2)); // return 1

// let lRUCache = new LRUCache(1);
// lRUCache.put(2, 1); // cache is {1=1}
// console.log(lRUCache.get(2)); // return 1
// lRUCache.put(3, 2); // cache is {1=1, 2=2}
// console.log(lRUCache.get(2)); // return 1
// console.log(lRUCache.get(3)); // return 1

// let lRUCache = new LRUCache(2);
// lRUCache.put(2, 1); // cache is {1=1}
// lRUCache.put(2, 2); // cache is {1=1, 2=2}
// console.log(lRUCache.get(2)); // return 1
// lRUCache.put(1, 1); // LRU key was 2, evicts key 2, cache is {1=1, 3=3}
// lRUCache.put(4, 1); // LRU key was 2, evicts key 2, cache is {1=1, 3=3}
// console.log(lRUCache.get(2)); // returns -1 (not found)
