/*
 * @lc app=leetcode id=10 lang=javascript
 *
 * [10] Regular Expression Matching
 */

// @lc code=start
/**
 * @param {string} s
 * @param {string} p
 * @return {boolean}
 */

function findLastIdx(arr) {
  for (let i = arr.length - 1; i >= 0; i--) {
    const ele = arr[i];
    if (!ele[1]) return i;
  }
  return -1;
}

var isMatch = function (s, p) {
  const sLen = s.length;
  const pLen = p.length;

  let tmpArr = [];
  for (let i = 0; i < pLen; i++) {
    const ele = p[i];
    if (p[i + 1] === '*') {
      tmpArr.push(ele + '*');
      i++;
    } else {
      tmpArr.push(ele);
    }
  }
  // tmpArr Sample: ["a*", "b", "c", "d*"]
  const tmpLen = tmpArr.filter((v) => v[1] !== '*').length; // arr length without * char

  if (tmpLen > sLen) return false; // Test case: aa bbca*
  if (tmpLen === pLen && sLen !== pLen) return false; // Test case: aa a

  let tmpArr2 = [];
  let tmpVal = '';
  for (let i = 0; i < tmpArr.length; i++) {
    const ele = tmpArr[i];

    if (!ele[1]) {
      if (ele === '.') {
        tmpVal = '';
      } else {
        tmpVal += ele;
      }
    } else if (tmpVal) {
      tmpArr2.push(tmpVal);
      tmpVal = '';
    }
  }

  for (let i = 0; i < tmpArr2.length; i++) {
    const ele = tmpArr2[i];

    if (!s.includes(ele)) return false;
  }

  let extraCount = sLen - tmpLen;
  let fillNum = extraCount;
  let j = 0;
  let jVal, hasMore;

  for (let i = 0; i < sLen; i++) {
    const ele = s[i];
    jVal = tmpArr[j];
    if (!jVal) {
      return false; // Test case: aaab a*
    }
    hasMore = jVal[1];

    if (ele !== jVal[0] && jVal[0] !== '.') {
      if (!hasMore) {
        if (extraCount < fillNum) {
          i = i - 2;
          extraCount++;
        } else {
          return false;
        }
      } else {
        if (extraCount < 0) return false;
        i--;
        j++;
      }
    } else {
      if (!hasMore) {
        if (i === sLen - 1) break;
        if (extraCount < 0) return false;
        j++;
      } else if (extraCount > 0) {
        extraCount--;
      } else {
        i--;
        j++;
      }
    }
  }

  const lastSolidNum = findLastIdx(tmpArr);
  if (lastSolidNum === -1) return true;

  return j >= lastSolidNum ? true : false;
};

// console.log(isMatch('aasdfasdfasdfasdfas', 'aasdf.*asdf.*asdf.*asdf.*s'));
console.log(isMatch('bbcbbcbcbbcaabcacb', 'a*.*ac*a*a*.a..*.*'));

module.exports = isMatch;
// @lc code=end
