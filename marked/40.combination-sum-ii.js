/*
 * @lc app=leetcode id=40 lang=javascript
 *
 * [40] Combination Sum II
 */

// @lc code=start
/**
 * @param {number[]} candidates
 * @param {number} target
 * @return {number[][]}
 */

function filterDuplicate(arr) {
  let tmpArr = [];
  let count = 0;
  while (count < arr.length) {
    const ele = arr[count].join('|');
    if (!tmpArr.includes(ele)) {
      tmpArr.push(ele);
    }

    count++;
  }

  return tmpArr.map((v) => v.split('|').map((n) => Number(n)));
}

function helpFn(arr, val, num, preVal) {
  if (num === 1) {
    const ele = arr.find((v) => v === val);
    if (ele) {
      return [preVal.concat([ele])];
    } else {
      return null;
    }
  }

  let result = [];
  for (let i = 0; i < arr.length; i++) {
    const ele = arr[i];

    if (ele < val) {
      const ele2 = helpFn(
        arr.slice(i + 1),
        val - ele,
        num - 1,
        preVal.concat([ele])
      );

      if (ele2 && ele2.length > 0) {
        result = result.concat(ele2);
      }
    }
  }

  return result;
}

var combinationSum2 = function (c, target) {
  const candidates = c.sort((a, b) => a - b);
  if (target < candidates[0]) return [];
  let maxNums = 1;

  while (candidates[0] * maxNums <= target) {
    maxNums++;
  }
  maxNums--;

  let count = 1,
    result = [];
  while (count <= maxNums) {
    const ele2 = helpFn(candidates, target, count, []);
    if (ele2 && ele2.length > 0) {
      result = result.concat(ele2);
    }
    count++;
  }

  return filterDuplicate(result);
};

// console.log(combinationSum2([10, 1, 2, 7, 6, 1, 5], 8));
// console.log(combinationSum2([2, 5, 2, 1, 2], 5));
// @lc code=end
