/*
 * @lc app=leetcode.cn id=435 lang=javascript
 *
 * [435] 无重叠区间
 */

// @lc code=start
/**
 * @param {number[][]} intervals
 * @return {number}
 */

var eraseOverlapIntervals = function (intervals) {
  let count = 0,
    point = 0;
  const len = intervals.length;
  intervals.sort((a, b) => a[1] - b[1]);

  var removeOverlapping = () => {
    for (let i = point + 1; i < len; i++) {
      if (intervals[i][0] >= intervals[point][1]) {
        point = i;
      } else {
        count++;
      }
    }
    point = len;
  };

  while (point < len - 1) {
    removeOverlapping();
  }

  return count;
};
// @lc code=end
// https://pic.leetcode-cn.com/1631930017-fYYUAr-file_1631930017753
// 一图道破玄机，学习了

// console.log(
//   eraseOverlapIntervals([
//     [1, 2],
//     [2, 3],
//     [3, 4],
//     [1, 3],
//   ])
// ); // 1
// console.log(
//   eraseOverlapIntervals([
//     [1, 2],
//     [1, 2],
//     [1, 2],
//   ])
// ); // 2
// console.log(
//   eraseOverlapIntervals([
//     [1, 2],
//     [2, 3],
//   ])
// ); // 0
// console.log(
//   eraseOverlapIntervals([
//     [1, 100],
//     [11, 22],
//     [1, 11],
//     [2, 12],
//   ])
// ); // 2
// console.log(
//   eraseOverlapIntervals([
//     [-52, 31],
//     [-73, -26],
//     [82, 97],
//     [-65, -11],
//     [-62, -49],
//     [95, 99],
//     [58, 95],
//     [-31, 49],
//     [66, 98],
//     [-63, 2],
//     [30, 47],
//     [-40, -26],
//   ])
// ); // 7
