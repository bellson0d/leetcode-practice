/*
 * @lc app=leetcode id=84 lang=javascript
 *
 * [84] Largest Rectangle in Histogram
 */

// @lc code=start
/**
 * @param {number[]} heights
 * @return {number}
 */
const groupArr = (arr) => {
  let count = 0,
    tmpArr = [],
    result = [];
  while (count < arr.length) {
    if (arr[count] > 0) {
      tmpArr.push(arr[count]);
    } else {
      if (tmpArr.length > 0) result.push(tmpArr);
      tmpArr = [];
    }
    count++;
  }
  if (tmpArr.length > 0) result.push(tmpArr);

  return result;
};

var largestRectangleArea = function (heights) {
  let hArr = groupArr(heights);
  const hLen = hArr.length;
  if (hLen === 0) return 0;

  let maxVal = 0;

  const helpFn = (h) => {
    const len = h.length;
    if (len < 2) {
      if (h[0] > maxVal) maxVal = h[0];
      return;
    }

    let pointerLeft = 0,
      pointerRight = 1;

    while (pointerRight < len) {
      const x = pointerRight - pointerLeft + 1;
      const y = Math.min(...h.slice(pointerLeft, pointerRight + 1));
      const val = x * y;
      if (val > maxVal) maxVal = val;

      let ll = pointerLeft + 1;

      if (h[pointerRight + 1] < h[pointerRight] || pointerRight === len - 1) {
        while (ll <= pointerRight) {
          const x1 = pointerRight - ll + 1;
          const y1 = Math.min(...h.slice(ll, pointerRight + 1));
          const val1 = x1 * y1;
          if (val1 > maxVal) {
            maxVal = val1;
          }
          ll++;
        }
      }

      pointerRight++;
    }
  };

  while (hArr.length > 0) {
    helpFn(hArr.pop());
  }

  return maxVal;
};
// @lc code=end

module.exports = largestRectangleArea;
// console.log(largestRectangleArea([2, 1, 5, 6, 2, 3]));
