/*
 * @lc app=leetcode id=53 lang=javascript
 *
 * [53] Maximum Subarray
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */

var maxSubArray = function (nums) {
  const baseLen = nums.length;

  if (baseLen === 0) return 0;
  if (baseLen === 1) return nums[0];

  let tmpMax = nums[0],
    result = nums[0];

  for (let i = 1; i < baseLen; i++) {
    const ele = nums[i];

    tmpMax = Math.max(ele, ele + tmpMax);
    result = Math.max(result, tmpMax);
  }

  return result;
};
// @lc code=end

module.exports = maxSubArray;
// maxSubArray([-1, 1, -1, 1, -1]);
