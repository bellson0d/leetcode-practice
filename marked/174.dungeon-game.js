/*
 * @lc app=leetcode id=174 lang=javascript
 *
 * [174] Dungeon Game
 */

// @lc code=start
/**
 * @param {number[][]} dungeon
 * @return {number}
 */
var calculateMinimumHP = function (d) {
  const x = d.length,
    y = d[0].length;
  for (let i = x - 1; i >= 0; i--) {
    for (let j = y - 1; j >= 0; j--) {
      const ele = d[i][j];
      if (i === x - 1 && j === y - 1) {
        d[i][j] = ele < 0 ? 1 - ele : 1;
      } else if (i === x - 1) {
        const sum = d[i][j + 1] - ele;
        d[i][j] = sum < 1 ? 1 : sum;
      } else if (j === y - 1) {
        const sum = d[i + 1][j] - ele;
        d[i][j] = sum < 1 ? 1 : sum;
      } else {
        const sum = Math.min(d[i][j + 1], d[i + 1][j]) - ele;
        d[i][j] = sum < 1 ? 1 : sum;
      }
    }
  }

  return d[0][0];
};
// @lc code=end
// module.exports = calculateMinimumHP;
// Inspired by https://blog.csdn.net/musechipin/article/details/84972257
// 困在了习惯性 DP 从左上至右下的顺序，导致转换方程太复杂且难处理
// 反向右下至左上则清晰明了
// Mark!
