/*
 * @lc app=leetcode id=310 lang=javascript
 *
 * [310] Minimum Height Trees
 */

// @lc code=start
/**
 * @param {number} n
 * @param {number[][]} edges
 * @return {number[]}
 */
function GraphNode(val, arr) {
  if (val === undefined) throw Error('No initial value!');
  this.val = val;
  this.neighbor = arr || [];

  this.addNeighbor = (node) => this.neighbor.push(node);
}

var findMinHeightTrees = function (n, edges) {
  let obj = {},
    nodes = [];
  for (let i = 0; i < n; i++) {
    const node = new GraphNode(i);
    obj[i] = node;
    nodes.push(node);
  }

  edges.forEach((v) => {
    obj[v[0]].addNeighbor(obj[v[1]]);
    obj[v[1]].addNeighbor(obj[v[0]]);
  });

  let minH = Infinity,
    res = [];
  for (const node of nodes) {
    let baseMap = {},
      tmpArr = [[node]],
      tmpH = 0,
      subArr = new Set();
    while (tmpArr.length && tmpH <= minH) {
      const currentNodes = tmpArr.pop();
      let flag = false;
      for (const currentNode of currentNodes) {
        if (!baseMap[currentNode.val]) {
          baseMap[currentNode.val] = true;
          flag = true;
          for (const nn of currentNode.neighbor) {
            subArr.add(nn);
          }
        }
      }
      if (subArr.size) tmpArr.push(Array.from(subArr));
      subArr.clear();
      if (flag) tmpH++;
    }
    if (tmpH < minH) {
      minH = tmpH;
      res = [node.val];
    } else if (tmpH === minH) {
      res.push(node.val);
    }
  }

  return res;
};
// @lc code=end

console.log(
  findMinHeightTrees(6, [
    [3, 0],
    [3, 1],
    [3, 2],
    [3, 4],
    [5, 4],
  ])
);
console.log(
  findMinHeightTrees(7, [
    [3, 0],
    [3, 1],
    [3, 2],
    [3, 4],
    [5, 4],
    [5, 6],
  ])
);
console.log(
  findMinHeightTrees(7, [
    [3, 0],
    [3, 1],
    [3, 2],
    [3, 4],
    [5, 4],
    [2, 6],
  ])
);
console.log(findMinHeightTrees(1, []));
console.log(findMinHeightTrees(2, [[0, 1]]));
