const targetFun = require('./95.unique-binary-search-trees-ii');
const { getTree } = require('./utils/treeNode');
const IS_ARRAY_RESULT = false; // 输出是否为数组
const MULTI_ARGS = false; // 输入参数是否为多个

const testStr = [
  [getTree([1, 2, 2, 3, 4, 4, 3]), true],
  [getTree([]), true],
  [getTree([1]), true],
  [getTree([1, 2]), false],
  [getTree([1, 2, 2, null, 3, null, 3]), false],
];

const time1 = new Date().getTime();
console.log(time1);

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];

  let result;
  if (MULTI_ARGS) {
    result = targetFun(...input);
  } else {
    result = targetFun(input);
  }

  if (
    IS_ARRAY_RESULT ? result.join('|') !== output.join('|') : result !== output
  ) {
    console.log('Number ' + count + ' Case Wrong !!!');
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
