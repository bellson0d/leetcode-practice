/*
 * @lc app=leetcode id=10 lang=javascript
 *
 * [10] Regular Expression Matching
 */

// @lc code=start
/**
 * @param {string} s
 * @param {string} p
 * @return {boolean}
 */
var isMatch = function (s, p) {
  let tmpArr = [];
  for (let i = 0; i < p.length; i++) {
    const ele = p[i];
    if (p[i + 1] === '*') {
      tmpArr.push(ele + '*');
      i++;
    } else {
      tmpArr.push(ele);
    }
  }
  const pLen = tmpArr.filter((v) => !v[1]).length;
  const strLen = s.length;
  if (pLen > strLen) return false;

  let j = 0;
  let jVal, hasMore;
  tmpArr.reverse();
  const reverseStr = s.split('').reverse().join('');
  const reserveLen = strLen - pLen;

  for (let i = 0; i < strLen; i++) {
    // console.log(i, j, reverseStr);
    jVal = tmpArr[j];
    if (!jVal) {
      return false;
    }
    hasMore = jVal[1];
    const ele = reverseStr[i];

    if (ele !== jVal[0] && jVal[0] !== '.') {
      if (!hasMore) return false;
      i--;
      j++;
    } else if (ele === jVal[0] || jVal[0] === '.') {
      if (!hasMore) {
        j++;
      }
    }
  }

  if (j >= tmpArr.length - 1) return true;

  for (let k = j; k < tmpArr.length; k++) {
    const ele = tmpArr[k];
    if (!ele[1] && ele[0] !== tmpArr[j][0]) return false;
  }
  return true;
};

isMatch('aasdfasdfasdfasdfas', 'aasdf.*asdf.*asdf.*asdf.*s');
// isMatch('aaa', 'ab*a*c*a');

module.exports = isMatch;
// @lc code=end
