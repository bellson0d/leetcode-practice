/*
 * @lc app=leetcode id=224 lang=javascript
 *
 * [224] Basic Calculator
 */

// @lc code=start
/**
 * @param {string} s
 * @return {number}
 */
 var helper2 = (left, right, operator) => {
    switch (operator) {
        case "+":
            return left + right;
        case "-":
            return left - right;
        case "*":
            return left * right;
        default:
            return ~~(left / right);
    }
};

const reg = /[\*\/]/;

var helper = (str) => {
    let s = str.replace(/--/g, '+').split(/([\*\/\+\-])/).map(v => v[0] === '@' ? -Number(v.slice(1)) : v);
    while (s.length > 1) {
        let idx = 1;
        if (reg.test(s)) {
            while (true) {
                if (s[idx] === "*" || s[idx] === "/") break;
                idx += 2;
            }
            const res = helper2(Number(s[idx - 1]), Number(s[idx + 1]), s[idx]);
            s = s
                .slice(0, idx - 1)
                .concat([res])
                .concat(s.slice(idx + 2));
        } else {
            let res = Number(s[idx-1]);
            while (idx < s.length) {
                res = helper2(res, Number(s[idx + 1]), s[idx]);
                idx += 2;
            }
            return res;
        }
    }
    return Number(s[0]);
};

var calculate = function (str) {
    if (str[0] === '+') {
        str = str.slice(1)
    }
    if (str[0] === '-') {
        str = "@" + str.slice(1);
    }
    let s = str
        .replace(/ /g, "")
        .replace(/([\+\/\*\-\(])\+(\d+)/g, "$1$2")
        .replace(/([\+\/\*\-\(])\-(\d+)/g, "$1@$2")
        .split(/([\(\)])/)
        .filter(Boolean);
    while (s.includes("(")) {
        const idx = s.indexOf(")");
        let res = helper(s[idx - 1]),
            leftP = idx - 3;
        while (s[leftP] !== "(" && s[leftP] !== ")"  && leftP >= 0) {
            res = s[leftP] + res;
            leftP--;
        }
        
        let rightP = idx + 1;
        while (s[rightP] !== ")" && s[rightP] !== "(" && rightP < s.length) {
            res += s[rightP];
            rightP++;
        }

        s = s
            .slice(0, leftP + 1)
            .concat([res])
            .concat(s.slice(rightP));
    }
    return helper(s.join(""));
};
// @lc code=end
// Edge Case 太多
// console.log(calculate("1 + 1")); // 2
// console.log(calculate(" 2-1 + 2 ")); // 3
// console.log(calculate("(1+(4+5+2)-3)+(6+8)")); // 23
// console.log(calculate("+48 + -48")); // 0
// console.log(calculate("+48 + (+45 - -48) +(-33 + 1)")); // 109
// console.log(calculate("(3)+1")); // 4
// console.log(calculate("(3)+(-1)")); // 2
// console.log(calculate("(-3)+(-1)")); // -4
// console.log(calculate("5")); // 5
// console.log(calculate("2-(5-6)")); // 3