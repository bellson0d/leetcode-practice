/*
 * @lc app=leetcode id=36 lang=javascript
 *
 * [36] Valid Sudoku
 */

// @lc code=start
/**
 * @param {character[][]} board
 * @return {boolean}
 */
const GROUP = [
  [0, 0, 0],
  [0, 1, 1],
  [0, 2, 2],
  [1, 0, 3],
  [1, 1, 4],
  [1, 2, 5],
  [2, 0, 6],
  [2, 1, 7],
  [2, 2, 8],
];

function findMatrix(x, y, board) {
  const [xRange, yRange] = GROUP.find(
    (v) => x < (v[0] + 1) * 3 && y < (v[1] + 1) * 3
  );

  for (let i = xRange * 3; i < xRange * 3 + 3; i++) {
    for (let j = yRange * 3; j < yRange * 3 + 3; j++) {
      const ele = board[i][j];
      if (ele === '.') continue;
      if (i !== x && j !== y && ele === board[x][y]) return false;
    }
  }

  return true;
}

var isValidSudoku = function (board) {
  for (let i = 0; i < 9; i++) {
    for (let j = 0; j < 9; j++) {
      const ele = board[i][j];
      if (ele === '.') continue;

      let count = j + 1;
      while (count < 9) {
        const ele2 = board[i][count];
        if (ele2 !== '.' && ele2 === ele) return false;
        count++;
      }

      count = i + 1;
      while (count < 9) {
        const ele2 = board[count][j];
        if (ele2 !== '.' && ele2 === ele) return false;
        count++;
      }

      if (!findMatrix(i, j, board)) return false;
    }
  }

  return true;
};

// @lc code=end

// module.exports = isValidSudoku;

// isValidSudoku([
//   ['5', '3', '.', '.', '7', '.', '.', '.', '.'],
//   ['6', '.', '.', '1', '9', '5', '.', '.', '.'],
//   ['.', '9', '8', '.', '.', '.', '.', '6', '.'],
//   ['8', '.', '.', '.', '6', '.', '.', '.', '3'],
//   ['4', '.', '.', '8', '.', '3', '.', '.', '1'],
//   ['7', '.', '.', '.', '2', '.', '.', '.', '6'],
//   ['.', '6', '.', '.', '.', '.', '2', '8', '.'],
//   ['.', '.', '.', '4', '1', '9', '.', '.', '5'],
//   ['.', '.', '.', '.', '8', '.', '.', '7', '9'],
// ]);
