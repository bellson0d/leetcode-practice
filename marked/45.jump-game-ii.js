/*
 * @lc app=leetcode id=45 lang=javascript
 *
 * [45] Jump Game II
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
const helpFn = (nums, preStep) => {
  let len = nums.length;
  let ele = nums[0];
  let maxNext = nums[1],
    idx = 1,
    count = 0;

  let step = preStep;

  while (nums[count] === 1 && count < len - 1) {
    count++;
  }
  step = step + count;
  if (count === len - 1) return [true, step];

  step++;
  ele = nums[count];
  idx = idx + count;
  maxNext = nums[idx];
  nums = nums.slice(count);
  len = nums.length;

  if (len < 3 || ele >= len - 1) return [true, step];

  for (let j = 2; j <= ele; j++) {
    const ele2 = nums[j];

    if (j + ele2 < len - 1) {
      if (idx + maxNext < j + ele2) {
        maxNext = ele2;
        idx = j;
      }
    } else {
      return [true, step + 1];
    }
  }

  return [false, step, nums.slice(idx)];
};

var jump = function (nums) {
  const len = nums.length;

  if (len === 1) return 0;
  if (len === 2 || nums[0] >= len - 1) return 1;

  let n = nums,
    step = 0,
    isFinal = false;

  while (!isFinal && n.length > 0) {
    [isFinal, step, n] = helpFn(n, step);
  }

  return step;
};
// @lc code=end
