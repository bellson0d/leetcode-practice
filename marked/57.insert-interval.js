/*
 * @lc app=leetcode id=57 lang=javascript
 *
 * [57] Insert Interval
 */

// @lc code=start
/**
 * @param {number[][]} intervals
 * @param {number[]} newInterval
 * @return {number[][]}
 */
var insert = function (arr1, arr2) {
  const len = arr1.length;
  const len2 = arr2.length;
  if (len === 0) return [arr2];
  if (len2 === 0) return arr1;

  let result = [],
    count = 0,
    insertArr = arr2;

  while (count < len) {
    let currentArr = arr1[count];

    const preFirst = currentArr[0];
    const preLast = currentArr[1];
    const nextFirst = insertArr[0];
    const nextLast = insertArr[1];

    if (nextFirst > preLast) {
      result.push(currentArr);
      count++;
      continue;
    }

    if (nextLast < preFirst) {
      result.push(insertArr);
      result = result.concat(arr1.slice(count));
      insertArr = null;
      break;
    }

    insertArr = [Math.min(preFirst, nextFirst), Math.max(preLast, nextLast)];

    count++;
  }
  if (insertArr) result.push(insertArr);

  return result;
};
// @lc code=end
// module.exports = insert;
// console.log(
//   insert(
//     [
//       [1, 7],
//       [0, 3],
//     ],
//     [0, 0]
//   )
// );
