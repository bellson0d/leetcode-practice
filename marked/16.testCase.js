const targetFun = require('./16.3-sum-closest');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  [[[-1, 2, 1, -4], 1], 2],
  [[[0, 0, 0], 1], 0],
  [[[1, 1, 1, 1], 3], 3],
  [[[1, 1, 1, 0], 100], 3],
  [[[1, 2, 4, 8, 16, 32, 64, 128], 82], 82],
];
let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(...input);
  if (result !== output) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
