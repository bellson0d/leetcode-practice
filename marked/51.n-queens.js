/*
 * @lc app=leetcode id=51 lang=javascript
 *
 * [51] N-Queens
 */

// @lc code=start
/**
 * @param {number} n
 * @return {string[][]}
 */
const baseRange = (n) =>
  Array(n)
    .fill(1)
    .map((v, i) => i);

const setN = (idx, n) => '.'.repeat(idx) + 'Q' + '.'.repeat(n - idx);

const getRange = (level, arr, n) => {
  if (level === 0) return baseRange(n);

  let tmpRange = [];
  for (let i = 0; i < n; i++) {
    let count = level - 1,
      j = i - 1,
      k = i + 1,
      flag = true;

    while (count >= 0) {
      if (arr[count][i] !== '.') {
        flag = false;
        break;
      }
      if (j >= 0) {
        if (arr[count][j] !== '.') {
          flag = false;
          break;
        }
        j--;
      }
      if (k < n) {
        if (arr[count][k] !== '.') {
          flag = false;
          break;
        }
        k++;
      }

      count--;
    }

    if (flag) tmpRange.push(i);
  }

  return tmpRange;
};

const helpFu = (arr, range, n, idx) => {
  let result = [];
  let newArr = arr.slice();

  for (let i = 0; i < range.length; i++) {
    const ele = range[i];
    newArr[idx] = setN(ele, n);

    const newRange = getRange(idx + 1, newArr, n + 1);
    if (idx < n) {
      if (newRange.length > 0) {
        result = result.concat(helpFu(newArr, newRange, n, idx + 1));
      }
    } else {
      result.push(newArr);
    }
  }

  return result;
};

var solveNQueens = function (n) {
  let result = [];

  result = result.concat(
    helpFu(Array(n).fill('.'.repeat(n)), baseRange(n), n - 1, 0)
  );

  return result;
};
// @lc code=end

// console.log(solveNQueens(8));
// console.log(getRange(1, ['Q...', '....', '....', '....'], 4));
