const targetFun = require('./143.reorder-list');
const { getList, isSameList, logList } = require('./utils/listNode');
const MULTI_ARGS = false; // 输入参数是否为多个

const testStr = [
  [getList([1]), getList([1])],
  [getList([1, 2]), getList([1, 2])],
  [getList([1, 2, 3]), getList([1, 3, 2])],
  [getList([1, 2, 3, 4]), getList([1, 4, 2, 3])],
  [getList([1, 2, 3, 4, 5]), getList([1, 5, 2, 4, 3])],
  [getList([1, 2, 3, 4, 5, 6]), getList([1, 6, 2, 5, 3, 4])],
  [getList([1, 2, 3, 4, 5, 6, 7]), getList([1, 7, 2, 6, 3, 5, 4])],
];

const time1 = new Date().getTime();
console.log(time1);

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];

  let result;
  if (MULTI_ARGS) {
    result = targetFun(...input);
  } else {
    result = targetFun(input);
  }

  if (!isSameList(result, output, true)) {
    console.log('Number ' + count + ' Case Wrong !!!');
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
