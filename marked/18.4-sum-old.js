/*
 * @lc app=leetcode id=18 lang=javascript
 *
 * [18] 4Sum
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[][]}
 */
var fourSum = function (nums, target) {
  const len = nums.length;
  if (len < 4) return [];
  if (len === 4)
    return nums.reduce((a, b) => a + b, 0) === target ? [nums] : [];
  nums.sort((a, b) => a - b);

  let result = [];
  let leftIdx1 = 0,
    leftIdx2 = 1,
    rightIdx1 = len - 2,
    rightIdx2 = len - 1;

  while (leftIdx2 < rightIdx1) {
    const leftVal1 = nums[leftIdx1];
    const leftVal2 = nums[leftIdx2];
    const rightVal1 = nums[rightIdx1];
    const rightVal2 = nums[rightIdx2];
    const sumVal = leftVal1 + leftVal2 + rightVal1 + rightVal2;

    if (sumVal < target) {
      if (leftIdx1 + 1 < leftIdx2) {
        leftIdx1++;
      } else {
        leftIdx2++;
      }
    } else if (sumVal > target) {
      if (rightIdx1 + 1 < rightIdx2) {
        rightIdx2--;
      } else {
        rightIdx1--;
      }
    } else {
      result.push([leftVal1, leftVal2, rightVal1, rightVal2]);

      if (
        leftIdx1 + 1 === leftIdx2 &&
        leftIdx2 + 1 === rightIdx1 &&
        rightIdx1 + 1 === rightIdx2
      )
        break;

      while (leftIdx2 + 1 < rightIdx1 && nums[leftIdx2] === nums[leftIdx2 + 1])
        leftIdx2++;
      while (leftIdx1 + 1 < leftIdx2 && nums[leftIdx1] === nums[leftIdx1 + 1])
        leftIdx1++;
      while (
        rightIdx1 - 1 > leftIdx2 &&
        nums[rightIdx1] === nums[rightIdx1 - 1]
      )
        rightIdx1--;
      while (
        rightIdx2 - 1 > rightIdx1 &&
        nums[rightIdx2] === nums[rightIdx2 - 1]
      )
        rightIdx2--;

      if (leftIdx1 + 1 < leftIdx2) {
        leftIdx1++;
      } else if (rightIdx2 - 1 > rightIdx1) {
        rightIdx2--;
      } else {
        leftIdx2++;
      }
    }
  }

  if (len > 5) {
    leftIdx1 = 0;
    leftIdx2 = 1;
    rightIdx1 = len - 2;
    rightIdx2 = len - 1;

    while (leftIdx2 < rightIdx1) {
      if (
        leftIdx1 + 1 === leftIdx2 &&
        leftIdx2 + 1 === rightIdx1 &&
        rightIdx1 + 1 === rightIdx2
      )
        break;

      const leftVal1 = nums[leftIdx1];
      const leftVal2 = nums[leftIdx2];
      const rightVal1 = nums[rightIdx1];
      const rightVal2 = nums[rightIdx2];
      const sumVal = leftVal1 + leftVal2 + rightVal1 + rightVal2;

      if (sumVal < target) {
        if (leftIdx2 + 1 < rightIdx1) {
          leftIdx2++;
        } else if (leftIdx1 + 1 < leftIdx2) {
          leftIdx1++;
        } else {
          break;
        }
      } else if (sumVal > target) {
        if (rightIdx1 - 1 > leftIdx2) {
          rightIdx1--;
        } else if (rightIdx2 - 1 > rightIdx1) {
          rightIdx2--;
        } else {
          break;
        }
      } else {
        const tmpArr = [leftVal1, leftVal2, rightVal1, rightVal2];
        if (
          !result.find(
            (v) =>
              v[0] == tmpArr[0] &&
              v[1] == tmpArr[1] &&
              v[2] == tmpArr[2] &&
              v[3] == tmpArr[3]
          )
        )
          result.push([leftVal1, leftVal2, rightVal1, rightVal2]);

        while (
          leftIdx2 + 1 < rightIdx1 &&
          nums[leftIdx2] === nums[leftIdx2 + 1]
        )
          leftIdx2++;
        while (leftIdx1 + 1 < leftIdx2 && nums[leftIdx1] === nums[leftIdx1 + 1])
          leftIdx1++;
        while (
          rightIdx1 - 1 > leftIdx2 &&
          nums[rightIdx1] === nums[rightIdx1 - 1]
        )
          rightIdx1--;
        while (
          rightIdx2 - 1 > rightIdx1 &&
          nums[rightIdx2] === nums[rightIdx2 - 1]
        )
          rightIdx2--;

        if (leftIdx2 + 1 < rightIdx1) {
          leftIdx2++;
        } else if (leftIdx1 + 1 < leftIdx2) {
          leftIdx1++;
        } else {
          rightIdx2--;
        }
      }
    }
  }

  return result;
};

// console.log(fourSum([1, 0, -1, 0, -2, 2], 0));
// console.log(fourSum([-3, -1, 0, 2, 4, 5], 1));
console.log(fourSum([-3, -2, -1, 0, 0, 1, 2, 3], 0));

// @lc code=end
