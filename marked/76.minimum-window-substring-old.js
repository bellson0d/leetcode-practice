/*
 * @lc app=leetcode id=76 lang=javascript
 *
 * [76] Minimum Window Substring
 */

// @lc code=start
/**
 * @param {string} s
 * @param {string} t
 * @return {string}
 */
var addOne = (k, o) => {
  o[k] = (o[k] || 0) + 1;
  return o;
};

var isEqual = (o1, o2) => {
  const k2 = Object.keys(o2);
  for (let i = 0; i < k2.length; i++) {
    const k = k2[i];
    const ele1 = o1[k];
    const ele2 = o2[k];
    if (!ele1 || ele1 < ele2) return false;
  }
  return true;
};

var helper = (s, o, type = true) => {
  const len = Object.keys(o).length;
  const sLen = s.length;

  let i = 0,
    obj = {};
  while (i < sLen) {
    obj = addOne(s[type ? i : sLen - 1 - i], obj);
    if (i >= len - 1) {
      if (isEqual(obj, o))
        return type ? s.slice(0, i + 1) : s.slice(sLen - 1 - i);
    }
    i++;
  }
  return '';
};

var minWindow = function (s, t) {
  if (!s || !t || s.length < t.length) return '';
  if (s.includes(t)) return t;
  const tLen = t.length;

  let i = 0,
    obj = {};
  while (i < tLen) {
    obj = addOne(t[i], obj);
    i++;
  }
  const [s21, s22] = [helper(s, obj), helper(s, obj, false)];
  if (s21.length === tLen || s21.length === 0) return s21;
  if (s22.length === tLen || s22.length === 0) return s22;
  const [s31, s32] = [helper(s21, obj, false), helper(s22, obj)];
  return s31.length > s32.length ? s32 : s31;
};
// @lc code=end

module.exports = minWindow;
// console.log('???', minWindow('ADOBECODEBANC', 'ABC'), '---');
