/*
 * @lc app=leetcode id=236 lang=javascript
 *
 * [236] Lowest Common Ancestor of a Binary Tree
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @param {TreeNode} p
 * @param {TreeNode} q
 * @return {TreeNode}
 */
var lowestCommonAncestor = function (root, p, q) {
    let arr1 = [],
        arr2 = [],
        node1 = root,
        node2 = root;

    if (p.val === root.val || q.val === root.val) return root;

    while (node1) {
        if (!node1.visited1) {
            node1.visited1 = true;
            arr1.unshift(node1);
            if (node1.val === p.val) {
                break;
            } else {
                if (node1.left) arr1.unshift(node1.left);
                if (node1.right) arr1.unshift(node1.right);
            }
        }
        node1 = arr1.shift();
    }
    arr1 = arr1.filter(v => v.visited1);

    while (node2) {
        if (!node2.visited2) {
            node2.visited2 = true;
            arr2.unshift(node2);
            if (node2.val === q.val) {
                break;
            } else {
                if (node2.left) arr2.unshift(node2.left);
                if (node2.right) arr2.unshift(node2.right);
            }
        }
        node2 = arr2.shift();
    }
    arr2 = arr2.filter(v => v.visited2);

    return arr1.find((v) => arr2.find(val => val.val === v.val));
};
// @lc code=end

// const { getTree } = require("./utils/treeNode");
// const mockNode = (n) => {
//     return { val: n };
// };
// console.log(
//     lowestCommonAncestor(
//         getTree([3, 5, 1, 6, 2, 0, 8, null, null, 7, 4]),
//         mockNode(5),
//         mockNode(1)
//     )
// ); // 3
// console.log(
//     lowestCommonAncestor(
//         getTree([3, 5, 1, 6, 2, 0, 8, null, null, 7, 4]),
//         mockNode(5),
//         mockNode(4)
//     )
// ); // 5
// console.log(lowestCommonAncestor(getTree([1, 2]), mockNode(1), mockNode(2))); // 1
