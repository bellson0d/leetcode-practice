const targetFun = require('./31.next-permutation');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  [
    [1, 2, 3],
    [1, 3, 2],
  ],
  [
    [3, 2, 1],
    [1, 2, 3],
  ],
  [
    [1, 1, 5],
    [1, 5, 1],
  ],
  [
    [1, 5, 1],
    [5, 1, 1],
  ],
  [
    [5, 1, 1],
    [1, 1, 5],
  ],
  [
    [1, 2, 3, 7, 6, 5, 2, 1],
    [1, 2, 5, 1, 2, 3, 6, 7],
  ],
  [
    [
      11,
      12,
      0,
      27,
      3,
      11,
      21,
      9,
      0,
      15,
      26,
      27,
      17,
      24,
      0,
      16,
      4,
      17,
      14,
      8,
      15,
      8,
      2,
      16,
      10,
      6,
      6,
      24,
      16,
      2,
      18,
      19,
      6,
      10,
      17,
      10,
      21,
      0,
      11,
      13,
      7,
      7,
      2,
      16,
      24,
      25,
      2,
      20,
      12,
      9,
      20,
      19,
    ],
    [
      11,
      12,
      0,
      27,
      3,
      11,
      21,
      9,
      0,
      15,
      26,
      27,
      17,
      24,
      0,
      16,
      4,
      17,
      14,
      8,
      15,
      8,
      2,
      16,
      10,
      6,
      6,
      24,
      16,
      2,
      18,
      19,
      6,
      10,
      17,
      10,
      21,
      0,
      11,
      13,
      7,
      7,
      2,
      16,
      24,
      25,
      2,
      20,
      12,
      19,
      9,
      20,
    ],
  ],
];
let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input);
  if (result.join('') !== output.join('')) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
