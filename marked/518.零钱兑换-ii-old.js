/*
 * @lc app=leetcode.cn id=518 lang=javascript
 *
 * [518] 零钱兑换 II
 */

// @lc code=start
/**
 * @param {number} amount
 * @param {number[]} coins
 * @return {number}
 */
var change = function (amount, coins) {
  if (amount === 0) return 1;
  const len = coins.length;
  const emptyArr = coins.map((v) => 0);
  let res = [[emptyArr]];

  let idx = 1,
    obj = {};
  while (idx <= amount) {
    let tmpArr = [];
    for (let i = 0; i < len; i++) {
      const pre = idx - coins[i];
      if (pre >= 0) {
        let arr = res[pre];
        for (let j = 0; j < arr.length; j++) {
          const newArr = arr[j].slice();
          newArr[i]++;
          // const key = newArr.join('-');
          // if (!obj[key]) {
          // obj[key] = true;
          tmpArr.push(newArr);
          // }
        }
      }
    }
    res[idx] = tmpArr;
    idx++;
  }

  return res[amount].length;
};
// @lc code=end

console.log(change(5, [1, 2, 5])); // 4
console.log(change(3, [2])); // 0
console.log(change(10, [10])); // 1
console.log(change(4, [2])); // 1
console.log(change(1, [2])); // 0
console.log(change(0, [7])); // 1
console.log(change(50, [1, 2, 5])); // 146
console.log(change(500, [1, 2, 5])); // 12701
