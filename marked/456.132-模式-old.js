/*
 * @lc app=leetcode.cn id=456 lang=javascript
 *
 * [456] 132 模式
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {boolean}
 */
var find132pattern = function (nums) {
  const len = nums.length;
  if (len < 3) return false;

  for (let i = 0; i < len - 2; i++) {
    let stack = [];
    let idx = i;
    while (idx < len) {
      const len2 = stack.length;
      if (len2 === 0) {
        stack.push(nums[idx]);
      } else if (len2 === 1) {
        if (nums[idx] <= stack[0]) {
          break;
        } else {
          stack.push(nums[idx]);
        }
      } else {
        if (nums[idx] >= stack[1]) {
          stack[1] = nums[idx];
        } else if (nums[idx] > stack[0]) {
          return true;
        }
      }
      idx++;
    }
  }

  return false;
};
// @lc code=end
// 这个方法直观、本能，但是 N**2 大用例超时，正解单调栈，Cool~~
// Inspired by https://leetcode-cn.com/problems/132-pattern/solution/dan-diao-zhan-jie-132-by-zn_-s8xh/
module.exports = find132pattern;
// console.log(find132pattern([1, 2, 3, 4]));
// console.log(find132pattern([3, 1, 4, 2]));
// console.log(find132pattern([-1, 3, 2, 0]));
