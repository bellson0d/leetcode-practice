/*
 * @lc app=leetcode id=214 lang=javascript
 *
 * [214] Shortest Palindrome
 */

// @lc code=start
/**
 * @param {string} s
 * @return {string}
 */
var shortestPalindrome = function (s) {
    const len = s.length;
    if (len < 2) return s;

    let prefix = "",
        half = len / 2,
        leftPointer,
        rightPointer,
        offset = 0;

    if (Number.isInteger(half)) {
        leftPointer = half - 1;
        rightPointer = half;
    } else {
        leftPointer = ~~half - 1;
        rightPointer = ~~half + 1;
    }

    while (rightPointer < s.length) {
        if (leftPointer === -1) {
            prefix = s.slice(rightPointer).split("").reverse().join("");
            break;
        } else {
            if (s[leftPointer] !== s[rightPointer]) {
                offset++;
                half = ((len + offset) / 2) -offset;
                if (Number.isInteger(half)) {
                    leftPointer = half - 1;
                    rightPointer = half;
                } else {
                    leftPointer = ~~half - 1;
                    rightPointer = ~~half + 1;
                }
            } else {
                leftPointer--;
                rightPointer++;
            }
        }
    }

    return prefix + s;
};
// @lc code=end

// console.log(
//     1,
//     shortestPalindrome("aacecaaa"),
//     "aaacecaaa",
//     shortestPalindrome("aacecaaa") === "aaacecaaa"
// );
// console.log(
//     2,
//     shortestPalindrome("abcd"),
//     "dcbabcd",
//     shortestPalindrome("abcd") === "dcbabcd"
// );
// console.log(3, shortestPalindrome("a"), "a", shortestPalindrome("a") === "a");
// console.log(4, shortestPalindrome(""), "", shortestPalindrome("") === "");
// console.log(
//     5,
//     shortestPalindrome("ab"),
//     "bab",
//     shortestPalindrome("ab") === "bab"
// );
// console.log(
//     6,
//     shortestPalindrome("abc"),
//     "cbabc",
//     shortestPalindrome("abc") === "cbabc"
// );
// console.log(
//     7,
//     shortestPalindrome("abab"),
//     "babab",
//     shortestPalindrome("abab") === "babab"
// );
// console.log(
//     8,
//     shortestPalindrome("baab"),
//     "baab",
//     shortestPalindrome("baab") === "baab"
// );
