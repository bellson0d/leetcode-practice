const targetFun = require('./166.fraction-to-recurring-decimal');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  [[1, 2], '0.5'],
  [[2, 1], '2'],
  [[2, 3], '0.(6)'],
  [[4, 333], '0.(012)'],
  [[1, 5], '0.2'],
  [[13, 3], '4.(3)'],
  [[1, 6], '0.1(6)'],
  [[7, -12], '-0.58(3)'],
  [[0, 9999], '0'],
  [[0, -5], '0'],
  [
    [1, 214748364],
    '0.00(000000465661289042462740251655654056577585848337359161441621040707904997124914069194026549138227660723878669455195477065427143370461252966751355553982241280310754777158628319049732085502639731402098131932683780538602845887105337854867197032523144157689601770377165713821223802198558308923834223016478952081795603341592860749337303449725)',
  ],
  [[-1, -2147483648], '0.0000000004656612873077392578125'],
  [[-2147483648, 1], '-2147483648'],
];

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input[0], input[1]);
  if (result !== output) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
