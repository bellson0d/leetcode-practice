/*
 * @lc app=leetcode id=85 lang=javascript
 *
 * [85] Maximal Rectangle
 */

// @lc code=start
/**
 * @param {character[][]} matrix
 * @return {number}
 */
const findRec = (position, w, h, arr) => {
  const [x1, y1] = position;
  for (let i = y1; i < w + y1; i++) {
    for (let j = x1; j < h + x1; j++) {
      if (arr[i][j] === '0') return false;
    }
  }

  return w * h;
};

var maximalRectangle = function (matrix) {
  const yLen = matrix.length;
  if (yLen === 0) return 0;
  const xLen = matrix[0].length;
  if (xLen === 0) return 0;

  let max = 0;

  const helpFn = (x, y, m) => {
    for (let i = 0; i < yLen - y + 1; i++) {
      for (let j = 0; j < xLen - x + 1; j++) {
        const res = findRec([j, i], y, x, m);
        if (res && res > max) {
          max = res;
        }
      }
    }
    if (x > 1) {
      helpFn(x - 1, y, m);
    }
    if (y > 1) {
      helpFn(x, y - 1, m);
    }
    if (x > 1 && y > 1) {
      helpFn(x - 1, y - 1, m);
    }
  };
  helpFn(xLen, yLen, matrix);

  return max;
};
// @lc code=end

// console.log(
//   maximalRectangle([
//     ['1', '0', '1', '0', '0'],
//     ['1', '0', '1', '1', '1'],
//     ['1', '1', '1', '1', '1'],
//     ['1', '0', '0', '1', '0'],
//   ])
// );
