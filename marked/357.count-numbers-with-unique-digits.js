/*
 * @lc app=leetcode id=357 lang=javascript
 *
 * [357] Count Numbers with Unique Digits
 */

// @lc code=start
/**
 * @param {number} n
 * @return {number}
 */
var baseMap = {
  0: 1,
  1: 10,
  2: 91,
  3: 739,
};
var countNumbersWithUniqueDigits = function (n) {
  if (baseMap[n]) return baseMap[n];

  let tmp = 4;
  while (tmp <= n) {
    let res = 9;
    for (let i = 1; i < tmp; i++) {
      res *= 10 - i;
    }
    baseMap[tmp] = res + baseMap[tmp - 1];
    tmp++;
  }
  return baseMap[n];
};
// @lc code=end
// console.log(countNumbersWithUniqueDigits(0));
// console.log(countNumbersWithUniqueDigits(1));
// console.log(countNumbersWithUniqueDigits(2));
// console.log(countNumbersWithUniqueDigits(3));
// console.log(countNumbersWithUniqueDigits(4));
// console.log(countNumbersWithUniqueDigits(5));
// console.log(countNumbersWithUniqueDigits(6));
// console.log(countNumbersWithUniqueDigits(7));
// console.log(countNumbersWithUniqueDigits(8));
