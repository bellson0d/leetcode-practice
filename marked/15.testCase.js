const targetFun = require('./15.3-sum-version1');
const { longArr } = require('./15.helpFun');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  // [longArr, []],
  [
    [-1, 0, 1, 2, -1, -4],
    [
      [-1, 0, 1],
      [-1, -1, 2],
    ],
  ],
];
let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input);
  if (result !== output) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
