/*
 * @lc app=leetcode id=32 lang=javascript
 *
 * [32] Longest Valid Parentheses
 */

// @lc code=start
/**
 * @param {string} s
 * @return {number}
 */
var longestValidParentheses = function (str) {
  let s = str;
  if (s[0] === ')') {
    s = s.slice(1);
  }
  if (s[s.length - 1] === '(') {
    s = s.slice(0, s.length - 1);
  }
  const len = s.length;
  let tmpResult = 0;
  let result = 0;
  let count = 0;
  let tmpArr = [];

  while (count < len) {
    const ele = s[count];
    if (ele === '(') {
      tmpArr.push(count);
    } else {
      if (tmpArr.length > 0) {
        tmpArr.pop();
        result += 2;
        if (tmpArr.length === 0 && result > tmpResult) {
          tmpResult = result;
        }
      } else {
        if (result > tmpResult) {
          tmpResult = result;
        }
        result = 0;
      }
    }
    count++;
  }

  if (result !== 0) {
    const tmpLen = tmpArr.length;
    if (tmpLen > 0) {
      let end = count;
      for (let i = 0; i < tmpLen; i++) {
        let start = tmpArr.pop();
        const value = end - start - 1;
        if (value > tmpResult) {
          tmpResult = value;
        }
        end = start;
      }
    } else {
      if (result > tmpResult) {
        tmpResult = result;
      }
    }
  }

  return tmpResult;
};

// longestValidParentheses('))))((()((');
// module.exports = longestValidParentheses;
// @lc code=end
