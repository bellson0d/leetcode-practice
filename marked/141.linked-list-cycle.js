/*
 * @lc app=leetcode id=141 lang=javascript
 *
 * [141] Linked List Cycle
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */

/**
 * @param {ListNode} head
 * @return {boolean}
 */
var hasCycle = function (head) {
  if (!head || !head.next) return false;

  let arr = [],
    current = head;

  while (current.next) {
    if (
      arr.find((v) => v.val === current.val && v.next.val === current.next.val)
    )
      return true;
    arr.push(current);
    current = current.next;
  }

  return false;
};
// @lc code=end
