/*
 * @lc app=leetcode id=32 lang=javascript
 *
 * [32] Longest Valid Parentheses
 */

// @lc code=start
/**
 * @param {string} s
 * @return {number}
 */
var helpFun = function (s) {
  const len = s.length;
  if (s[0] === ')' || s[len - 1] === '(') return false;
  let count = 0;
  let tmpArr = 0;

  while (count < len) {
    const ele = s[count];
    if (ele === '(') {
      tmpArr++;
    } else {
      if (tmpArr !== 0) {
        tmpArr--;
      } else {
        return false;
      }
    }
    count++;
  }

  return tmpArr > 0 ? false : true;
};
// 暴力遍历、超时
var longestValidParentheses = function (s) {
  const len = s.length;
  let count = len;
  while (count > 0) {
    for (let i = 0; i < len - count + 1; i++) {
      const ele = s.slice(i, i + count);
      const result = helpFun(ele);
      if (result) {
        return count;
      }
    }
    count--;
  }

  return count;
};

module.exports = longestValidParentheses;
// @lc code=end
