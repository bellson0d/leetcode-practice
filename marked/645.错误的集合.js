/*
 * @lc app=leetcode.cn id=645 lang=javascript
 *
 * [645] 错误的集合
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number[]}
 */
var findErrorNums = function (nums) {
  nums.sort((a, b) => a - b);
  let idx = 0,
    offset = 0,
    repeat = null,
    defect = null;

  while ((!repeat || !defect) && idx < nums.length) {
    if (nums[idx] !== idx + 1 + offset) {
      defect = idx + 1 + offset;
      offset++;
    }

    if (nums[idx] === nums[idx + 1]) {
      repeat = nums[idx];
      idx++;
      offset--;
    }

    idx++;
  }

  return [repeat, defect || nums.length];
};
// @lc code=end
// module.exports = findErrorNums;
// console.log(findErrorNums([5, 3, 6, 1, 5, 4, 7, 8]));
