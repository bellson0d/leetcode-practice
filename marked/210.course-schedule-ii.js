/*
 * @lc app=leetcode id=210 lang=javascript
 *
 * [210] Course Schedule II
 */

// @lc code=start
/**
 * @param {number} numCourses
 * @param {number[][]} prerequisites
 * @return {number[]}
 */
var findOrder = function (n, p) {
    if (p.length === 0 || n === 1)
        return Array(n)
            .fill(1)
            .map((v, i) => i);

    let obj = {},
        hasLearn = {},
        res = [];
    p.forEach((v) => {
        const [pre, next] = v;
        if (obj[next]) {
            obj[next].push(pre);
        } else {
            obj[next] = [pre];
        }
    });
    let newP = Object.keys(obj).map((v) => [obj[v], v]);

    for (let i = 0; i < n; i++) {
        if (!obj[i]) {
            hasLearn[i] = true;  
            res.unshift(i);
        } 
    }

    while (newP.length) {
        let flag = false;
        for (let i = 0; i < newP.length; i++) {
            const ele = newP[i];
            const [pre, next] = ele;
            if (pre.findIndex((v) => !hasLearn[v]) === -1) {
                hasLearn[next] = true;
                res.unshift(~~next);
                newP = newP.slice(0, i).concat(newP.slice(i + 1));
                flag = true;
                break;
            }
        }
        if (!flag) return [];
    }

    return res;
};
// @lc code=end

// console.log(
//     findOrder(3, [
//         [0, 2],
//         [1, 2],
//         [2, 0],
//     ])
// );
// console.log(
//     findOrder(2, [[1,0]])
// );
// console.log(
//     findOrder(4, [[1,0],[2,0],[3,1],[3,2]])
// );
// console.log(
//     findOrder(1, [])
// );
