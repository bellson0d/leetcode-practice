/*
 * @lc app=leetcode id=99 lang=javascript
 *
 * [99] Recover Binary Search Tree
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {void} Do not return anything, modify root in-place instead.
 */
var recoverTree = function (root) {
  let tmp = [root],
    arr = [];

  while (tmp.length) {
    const node = tmp[0];

    if (!node.lVisited && node.left) {
      node.lVisited = true;
      tmp.unshift(node.left);
    } else {
      tmp.shift();
      if (node.right) {
        tmp.unshift(node.right);
      }
      arr.push(node);
    }
  }

  const sorted = [...arr].sort((a, b) => a.val - b.val);
  const res = [];
  for (let i = 0; i < arr.length; i++) {
    if (arr[i].val !== sorted[i].val) {
      res.push(arr[i]);
    }
  }

  const [a, b] = res;
  let temp = a.val;
  a.val = b.val;
  b.val = temp;
  return root;
};
// @lc code=end
// Inspired by https://leetcode-cn.com/problems/recover-binary-search-tree/solution/zhong-xu-bian-li-de-jian-dan-xie-fa-kong-ggou/
// 关键点在于 Leetcode 以数组形式呈现的树构造出来是错的，导致迟迟无法验证
// const { getTree } = require('./utils/treeNode');
// console.log(recoverTree(getTree([1, 3, null, null, 2])));
// console.log(recoverTree(getTree([3, 1, 4, null, null, 2])));
// console.log(recoverTree(getTree([2, 3, 1])));
// recoverTree(
//   getTree([
//     146,
//     71,
//     -13,
//     55,
//     null,
//     231,
//     399,
//     321,
//     null,
//     null,
//     null,
//     null,
//     null,
//     -33,
//   ])
// );
