/*
 * @lc app=leetcode id=91 lang=javascript
 *
 * [91] Decode Ways
 */

// @lc code=start
/**
 * @param {string} s
 * @return {number}
 */

const splitArr = (s) => {
  let tmpArr = [];

  let count = 0,
    tmpStr = '';

  while (count < s.length) {
    const ele = s[count];
    const eleNext = s[count + 1];

    if (tmpStr.length < 1) {
      tmpStr += ele;
    } else {
      if (ele > 2 || ele === '0') {
        tmpStr += ele;
        tmpArr.push(tmpStr);
        tmpStr = '';
      } else if (ele < 2) {
        if (eleNext > 2) {
          tmpStr += ele + eleNext;
          tmpArr.push(tmpStr);
          tmpStr = '';
          count++;
        } else {
          tmpStr += ele;
        }
      } else {
        if (eleNext < 7) {
          if (eleNext > 2) {
            tmpStr += ele + eleNext;
            tmpArr.push(tmpStr);
            tmpStr = '';
            count++;
          } else {
            tmpStr += ele;
          }
        } else {
          tmpStr += ele;
          tmpArr.push(tmpStr);
          tmpStr = '';
        }
      }
    }
    count++;
  }
  if (tmpStr) tmpArr.push(tmpStr);

  return tmpArr;
};

var singleDecoding = function (s) {
  const len = s.length;
  if (len === 1) return s !== '0' ? 1 : 0;
  if (s[0] === '0' || s.includes('00')) return 0;

  let res = [],
    cacheArr = [[s, []]];

  const helpFn = (str, arr) => {
    const len = str.length;

    if (len < 3) {
      if (len === 1 || str[1] === '0') {
        res.push(arr.concat([str]));
      } else if (str[0] > 2) {
        res.push(arr.concat([str[0], str[1]]));
      } else if (str[0] < 2) {
        res.push(arr.concat([str[0], str[1]]));
        res.push(arr.concat([str]));
      } else {
        if (str[1] < 7) {
          res.push(arr.concat([str[0], str[1]]));
          res.push(arr.concat([str]));
        } else {
          res.push(arr.concat([str[0], str[1]]));
        }
      }
    } else {
      if (str[2] === '0') {
        cacheArr.unshift([str.slice(1), arr.concat([str.slice(0, 1)])]);
      } else if (str[1] === '0') {
        cacheArr.unshift([str.slice(2), arr.concat([str.slice(0, 2)])]);
      } else if (str[0] < 2) {
        cacheArr.unshift([str.slice(1), arr.concat([str.slice(0, 1)])]);
        cacheArr.unshift([str.slice(2), arr.concat([str.slice(0, 2)])]);
      } else if (str[0] > 2) {
        cacheArr.unshift([str.slice(1), arr.concat([str.slice(0, 1)])]);
      } else {
        if (str[1] < 7) {
          cacheArr.unshift([str.slice(1), arr.concat([str.slice(0, 1)])]);
          cacheArr.unshift([str.slice(2), arr.concat([str.slice(0, 2)])]);
        } else {
          cacheArr.unshift([str.slice(1), arr.concat([str.slice(0, 1)])]);
        }
      }
    }
  };

  while (cacheArr.length > 0) {
    helpFn(...cacheArr.pop());
  }

  return res.filter((arr) => arr.every((v) => v >= 1 && v <= 26)).length;
};

const numDecodings = (s) => {
  const arr = splitArr(s);
  return arr.map((v) => singleDecoding(v)).reduce((a, b) => a * b, 1);
};

// @lc code=end
// console.log(numDecodings('12'));
// console.log(numDecodings('226'));
// console.log(numDecodings('27'));
// console.log(numDecodings('299'));
// console.log(numDecodings('00'));
// console.log(numDecodings('99'));
// console.log(numDecodings('101'));
// console.log(numDecodings('17'));
// console.log(numDecodings('1111111'));
// console.log(numDecodings('01'));
// console.log(
//   numDecodings(
//     '4757562545844617494555774581341211511296816786586787755257741178599337186486723247528324612117156948'
//   )
// );
// console.log(splitArr('1122112211221122'));
