/*
 * @lc app=leetcode id=220 lang=javascript
 *
 * [220] Contains Duplicate III
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @param {number} k
 * @param {number} t
 * @return {boolean}
 */
var containsNearbyAlmostDuplicate = function (nums, k, t) {
    if (k === 0) return false;
    if (k === 1) {
        for (let i = 0; i < nums.length - 1; i++) {
            const ele = nums[i];
            const ele2 = nums[i + 1];
            if (Math.abs(ele - ele2) <= t) return true;
        }
        return false;
    }

    nums = nums.map((v, i) => [v, i]).sort((a, b) => b[0] - a[0]);
    const len = nums.length;
    let leftP = 0,
        rightP = 1;
    while (leftP < len - 1) {
        while (
            rightP < len &&
            Math.abs(nums[leftP][0] - nums[rightP][0]) <= t
        ) {
            if (Math.abs(nums[leftP][1] - nums[rightP][1]) <= k) return true;
            rightP++;
        }
        leftP++;
        rightP = leftP + 1;
    }

    return false;
};
// @lc code=end
// 双指针好用，NB！ hhhhhhhhh
// console.log(containsNearbyAlmostDuplicate([1, 2, 3, 1], 3, 0)); // true
// console.log(containsNearbyAlmostDuplicate([1, 0, 1, 1], 1, 2)); // true
// console.log(containsNearbyAlmostDuplicate([1, 5, 9, 1, 5, 9], 2, 3)); // false
// console.log(containsNearbyAlmostDuplicate([2147483646, 2147483647], 3, 3)); // true
// console.log(containsNearbyAlmostDuplicate([-5,5,5,5,5,15], 6, 6)); // true
// console.log(containsNearbyAlmostDuplicate([-2147483648,2147483647], 1, 1)); // false
// console.log(
//     containsNearbyAlmostDuplicate([2147483647, -1, 2147483647], 1, 2147483647)
// ); // false
