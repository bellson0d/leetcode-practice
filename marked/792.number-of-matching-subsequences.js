/*
 * @lc app=leetcode id=792 lang=javascript
 *
 * [792] Number of Matching Subsequences
 */

// @lc code=start
/**
 * @param {string} s
 * @param {string[]} words
 * @return {number}
 */
var numMatchingSubseq = function (s, words) {
    const len = s.length;
    let obj = {},
        objMap = new Set(s.split("")),
        res = 0;
    for (const ele of words) {
        if (ele.length > len) continue;
        if (obj[ele]) {
            res++;
            continue;
        }

        let j = 0,
            k = 0;
        while (j < len && k < ele.length) {
            if (!objMap.has(ele[k])) break;
            if (s[j] !== ele[k]) {
                j++;
            } else {
                j++;
                k++;
            }
        }

        if (k === ele.length) {
            obj[ele] = true;
            res++;
        }
    }
    return res;
};
// @lc code=end

// console.log(numMatchingSubseq("abcde", ["a", "bb", "acd", "ace"])); // 3
// console.log(
//     numMatchingSubseq("dsahjpjauf", [
//         "ahjpjau",
//         "ja",
//         "ahbwzgqnuk",
//         "tnmlanowax",
//     ])
// ); // 2
