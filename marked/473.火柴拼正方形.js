/*
 * @lc app=leetcode.cn id=473 lang=javascript
 *
 * [473] 火柴拼正方形
 */

// @lc code=start
/**
 * @param {number[]} matchsticks
 * @return {boolean}
 */
var makesquare = function (matchsticks) {
  const len = matchsticks.length;
  if (len < 4) return false;

  const sum = matchsticks.reduce((a, b) => a + b);
  if (sum % 4 !== 0) return false;
  const width = sum / 4;
  let obj = {};

  let tmp = [[0, matchsticks.slice(), width]];
  while (tmp.length) {
    let [count, arr, n] = tmp.pop();
    const key = arr.join('') + '-' + n;
    if (obj[key]) {
      continue;
    } else {
      obj[key] = true;
    }

    for (let i = 0; i < arr.length; i++) {
      if (n === arr[i]) {
        if (count === 3 && arr.length === 1) return true;
        if (count < 3) {
          tmp.push([
            count + 1,
            arr.slice(0, i).concat(arr.slice(i + 1)),
            width,
          ]);
        }
      } else if (n > arr[i]) {
        tmp.push([count, arr.slice(0, i).concat(arr.slice(i + 1)), n - arr[i]]);
      }
    }
  }

  return false;
};
// @lc code=end

// console.log(makesquare([1, 1, 2, 2, 2])); // true
// console.log(makesquare([3, 3, 3, 3, 4])); // false
// console.log(makesquare([4, 2, 3, 4, 3, 4])); // false
// console.log(makesquare([1, 2, 2, 1, 4, 2, 3, 5])); // true
// console.log(makesquare([10, 6, 5, 5, 5, 3, 3, 3, 2, 2, 2, 2])); // true
// console.log(makesquare([5, 5, 5, 5, 16, 4, 4, 4, 4, 4, 3, 3, 3, 3, 4])); // false
