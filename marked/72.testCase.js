const targetFun = require('./Pro/72.edit-distance');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  [['aa', 'a'], 1],
  [['ba', 'a'], 1],
  [['', ''], 0],
  [['abcdef', 'avc'], 4],
  [['a', 'bcd'], 3],
  [['a', 'abc'], 2],
  [['', 'ros'], 3],
  [['abc', ''], 3],
  [['abc', 'ros'], 3],
  [['abcd', 'ros'], 4],
  [['abcd', ''], 4],
  [['aadd', 'ad'], 2],
  [['axbxc', 'abc'], 2],
  [['horse', 'ros'], 3],
  [['orsre', 'ros'], 4],
  [['intention', 'execution'], 5],
  [
    [
      'qwoeiruqoweuroqiuerouqoqwpoieuroqiwueq',
      'qwoeiruqoweuroqiuerouqoqwpoieuroqiwuep',
    ],
    1,
  ],
  [
    [
      'qwoeiruqoweuroqiuerouqoqwpoieuroqiwueq',
      'qwoeiruqoweuroqiuerauqoqwpoieuroqiwueq',
    ],
    1,
  ],
  [
    [
      'qwoeiruqoweuroqiuerouqoqwpoieuro',
      'qwoeiruqoweuroqiuerouqoqwpoieuroqiwueq',
    ],
    6,
  ],
  [
    ['pneumonoultramicroscopicsilicovolcanoconiosis', 'ultramicroscopically'],
    27,
  ],
  [['x'.repeat(100), 'y'.repeat(100)], 100],
];

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input[0], input[1]);
  if (result !== output) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
