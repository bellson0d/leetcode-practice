/*
 * @lc app=leetcode id=15 lang=javascript
 *
 * [15] 3Sum
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number[][]}
 */
// const { longArr } = require('./15.helpFun');

// Inspired by https://medium.com/@hanjiang_54259/leetcode-15-3sum-in-javascript-ea39705e283d

var threeSum = function (nums) {
  const len = nums.length;
  nums.sort((a, b) => a - b);
  let result = [];

  for (let i = 0; i < len - 2; i++) {
    const ele = nums[i];
    if (ele > 0 || nums[len - 1] < 0) return result;
    if (ele === nums[i - 1]) continue;

    let leftIdx = i + 1,
      rightIdx = nums.length - 1;

    while (leftIdx < rightIdx) {
      const leftVal = nums[leftIdx];
      const rightVal = nums[rightIdx];
      const sumVal = ele + leftVal + rightVal;

      if (sumVal < 0) {
        leftIdx++;
      } else if (sumVal > 0) {
        rightIdx--;
      } else {
        result.push([ele, leftVal, rightVal]);
        while (leftIdx < rightIdx && leftVal === nums[leftIdx + 1]) leftIdx++;
        while (leftIdx < rightIdx && rightVal === nums[rightIdx - 1])
          rightIdx--;
        leftIdx++;
        rightIdx--;
      }
    }
  }

  return result;
};

// const time1 = new Date().getTime();
// console.log(time1);

// console.log(threeSum(longArr));

// const time2 = new Date().getTime();
// console.log('Duration: ', time2 - time1);

// console.log(threeSum([1, 1, -2]));
// console.log(threeSum([0]));
// console.log(threeSum([1, -1, -2, 0]));

module.exports = threeSum;
// @lc code=end
