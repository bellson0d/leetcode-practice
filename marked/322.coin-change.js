/*
 * @lc app=leetcode id=322 lang=javascript
 *
 * [322] Coin Change
 */

// @lc code=start
/**
 * @param {number[]} coins
 * @param {number} amount
 * @return {number}
 */
var coinChange = function (coins, amount) {
  if (!amount) return 0;

  let obj = {},
    level = 1,
    arr = [],
    arr2 = [];
  for (const c of coins) {
    if (c === amount) return 1;
    if (!obj[amount - c]) {
      arr.push(c);
      arr2.push(amount - c);
      obj[amount - c] = true;
    }
  }
  coins = arr.slice();
  const len = coins.length;
  if (len === 1) return amount % coins[0] === 0 ? amount / coins[0] : -1;

  while (arr2.length) {
    let tmp = [];
    for (const val of arr2) {
      for (const c of coins) {
        const res = val - c;
        if (res === 0) return level + 1;
        if (res > 0 && !obj[res]) {
          obj[res] = true;
          tmp.push(res);
        }
      }
    }
    arr2 = tmp.slice();
    level++;
  }

  return -1;
};
// @lc code=end

// console.log(coinChange([1, 2, 5], 11)); // 3
// console.log(coinChange([1, 2, 5], 19)); // 5
// console.log(coinChange([2], 3)); // -1
// console.log(coinChange([1], 0)); // 0
// console.log(coinChange([1], 1)); // 1
// console.log(coinChange([1], 2)); // 2
// console.log(coinChange([2, 17], 88)); // 14
// console.log(coinChange([2], 11)); // -1
// console.log(coinChange([474, 83, 404, 3], 264)); // 8
// console.log(coinChange([281, 20, 251, 251], 7323)); // 66
// console.log(coinChange([1, 2], 3)); // 2
