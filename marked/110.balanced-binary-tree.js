/*
 * @lc app=leetcode id=110 lang=javascript
 *
 * [110] Balanced Binary Tree
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {boolean}
 */

var isBalanced = function (root) {
  if (!root || (!root.left && !root.right)) return true;

  var helper = (node, level) => {
    if (!node) return level;
    level++;
    if (!node.left && !node.right) {
      return level;
    } else {
      return Math.max(helper(node.left, level), helper(node.right, level));
    }
  };

  return (
    Math.abs(helper(root.left, 1) - helper(root.right, 1)) < 2 &&
    isBalanced(root.left) &&
    isBalanced(root.right)
  );
};
// @lc code=end
// 平衡二叉树概念不清晰，漏考虑了子树也需要满足！！！
// const { getTree } = require('./utils/treeNode');
// console.log(isBalanced(getTree([3, 9, 20, null, null, 15, 7])));
// console.log(isBalanced(getTree([1, 2, 2, 3, 3, null, null, 4, 4])));
// console.log(isBalanced(getTree([1, null, 2, null, 3])));
// console.log(isBalanced(getTree([1, 2])));
// const tree = getTree([1, 2, 2, 3, null, null, 3, 4, null, null, 4]);
// console.log(isBalanced(tree));
