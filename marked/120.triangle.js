/*
 * @lc app=leetcode id=120 lang=javascript
 *
 * [120] Triangle
 */

// @lc code=start
/**
 * @param {number[][]} triangle
 * @return {number}
 */
var minimumTotal = function (triangle) {
  const len = triangle.length;

  if (len === 0) return 0;
  if (len === 1) return triangle[0][0];

  let tmpArr = [triangle[0]];
  for (let i = 1; i < len; i++) {
    const arr = triangle[i];
    const preArr = tmpArr[0];
    let subTmp = [];

    for (let j = 0; j < arr.length; j++) {
      const ele = arr[j];
      if (j === 0) {
        subTmp[j] = ele + preArr[j];
      } else if (j === arr.length - 1) {
        subTmp[j] = ele + preArr[j - 1];
      } else {
        subTmp[j] = ele + Math.min(preArr[j - 1], preArr[j]);
      }
    }
    tmpArr.unshift(subTmp);
  }

  return Math.min(...tmpArr[0]);
};
// @lc code=end

// console.log(minimumTotal([[2], [3, 4], [6, 5, 7], [4, 1, 8, 3]])); // 11
// console.log(minimumTotal([[-1], [2, 3], [1, -1, -3]])); // -1
// console.log(minimumTotal([[1], [-5, -2], [3, 6, 1], [-1, 2, 4, -3]])); // -3
// 动态规划典型应用，简单清晰 Mark
