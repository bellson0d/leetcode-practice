/*
 * @lc app=leetcode id=827 lang=javascript
 *
 * [827] Making A Large Island
 */

// @lc code=start
/**
 * @param {number[][]} grid
 * @return {number}
 */
var largestIsland = function (grid) {
  const m = grid.length;
  const n = grid[0].length;
  let pool1 = new Set([]),
    pool0 = [];

  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      if (grid[i][j]) {
        pool1.add(i + '-' + j);
      } else {
        pool0.push(i + '-' + j);
      }
    }
  }

  let islands = [],
    maxLand = 1;
  while (pool1.size) {
    let island = new Set([]),
      tmp = [pool1.values().next().value];

    while (tmp.length) {
      const position = tmp.pop();
      const [x, y] = position.split('-').map(Number);

      if (!island.has(position)) {
        island.add(position);
        pool1.delete(position);
      }

      if (x > 0) {
        const p = x - 1 + '-' + y;
        if (pool1.has(p) && !island.has(p)) {
          island.add(p);
          pool1.delete(p);
          tmp.push(p);
        }
      }

      if (y > 0) {
        const p = x + '-' + (y - 1);
        if (pool1.has(p) && !island.has(p)) {
          island.add(p);
          pool1.delete(p);
          tmp.push(p);
        }
      }

      if (x < m - 1) {
        const p = x + 1 + '-' + y;
        if (pool1.has(p) && !island.has(p)) {
          island.add(p);
          pool1.delete(p);
          tmp.push(p);
        }
      }

      if (y < n - 1) {
        const p = x + '-' + (y + 1);
        if (pool1.has(p) && !island.has(p)) {
          island.add(p);
          pool1.delete(p);
          tmp.push(p);
        }
      }
    }
    islands.push(island);
    if (island.size > maxLand) maxLand = island.size;
  }

  if (islands.length === 0) return maxLand;
  if (islands.length === 1)
    return islands[0].size < m * n ? islands[0].size + 1 : m * n;

  islands.map((v, i) => {
    let area = v.size;
    Array.from(v).map((p) => {
      const [x, y] = p.split('-').map(Number);
      grid[x][y] = [area, i];
    });
  });

  for (const position of pool0) {
    const [x, y] = position.split('-').map(Number);
    let area = 1,
      idxs = [];

    if (x > 0) {
      const p = grid[x - 1][y];
      if (p) {
        const [tmpArea, idx] = p;
        if (!idxs.includes(idx)) {
          (area += tmpArea), idxs.push(idx);
        }
      }
    }

    if (y > 0) {
      const p = grid[x][y - 1];
      if (p) {
        const [tmpArea, idx] = p;
        if (!idxs.includes(idx)) {
          (area += tmpArea), idxs.push(idx);
        }
      }
    }

    if (x < m - 1) {
      const p = grid[x + 1][y];
      if (p) {
        const [tmpArea, idx] = p;
        if (!idxs.includes(idx)) {
          (area += tmpArea), idxs.push(idx);
        }
      }
    }

    if (y < n - 1) {
      const p = grid[x][y + 1];
      if (p) {
        const [tmpArea, idx] = p;
        if (!idxs.includes(idx)) {
          (area += tmpArea), idxs.push(idx);
        }
      }
    }

    if (area > maxLand) maxLand = area;
  }

  return maxLand;
};
// @lc code=end

// console.log(
//   largestIsland([
//     [1, 0],
//     [0, 1],
//   ])
// ); // 3
// console.log(
//   largestIsland([
//     [1, 1],
//     [0, 1],
//   ])
// ); // 4
// console.log(
//   largestIsland([
//     [1, 1],
//     [1, 1],
//   ])
// ); // 4
// console.log(
//   largestIsland([
//     [1, 0, 1],
//     [0, 1, 0],
//     [1, 0, 1],
//   ])
// ); // 4
// console.log(
//   largestIsland([
//     [0, 1, 0],
//     [1, 0, 1],
//     [0, 1, 0],
//   ])
// ); // 5

// console.log(
//   largestIsland([
//     [1, 1, 0],
//     [1, 0, 1],
//     [0, 1, 1],
//   ])
// ); // 7

// console.log(
//   largestIsland([
//     [0, 0, 0],
//     [0, 0, 0],
//     [0, 0, 0],
//   ])
// ); // 1

// console.log(
//   largestIsland([
//     [1, 0, 0, 1, 1],
//     [1, 0, 0, 1, 0],
//     [1, 1, 1, 1, 1],
//     [1, 1, 1, 0, 1],
//     [0, 0, 0, 1, 0],
//   ])
// ); // 16
