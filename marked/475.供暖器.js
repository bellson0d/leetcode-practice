/*
 * @lc app=leetcode.cn id=475 lang=javascript
 *
 * [475] 供暖器
 */

// @lc code=start
/**
 * @param {number[]} houses
 * @param {number[]} heaters
 * @return {number}
 */
var findRadius = function (houses, heaters) {
  houses.sort((a, b) => a - b);
  heaters.sort((a, b) => a - b);
  let p1 = 0,
    p2 = 0,
    maxRadius = 0;
  const len1 = houses.length,
    len2 = heaters.length;
  while (p1 < len1) {
    let range = Math.abs(heaters[p2] - houses[p1]);
    while (p2 < len2) {
      const diff = Math.abs(heaters[p2] - houses[p1]);
      if (heaters[p2] < houses[p1]) {
        range = diff;
      } else if (heaters[p2] === houses[p1]) {
        range = 0;
        break;
      } else {
        if (diff > range) {
          p2--;
        } else {
          range = Math.min(range, diff);
        }
        break;
      }
      p2++;
    }
    p2 = Math.min(len2 - 1, p2);
    maxRadius = Math.max(maxRadius, range);

    p1++;
  }

  return maxRadius;
};
// @lc code=end

module.exports = findRadius;

// console.log(findRadius([1, 5], [2])); // 3
