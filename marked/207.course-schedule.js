/*
 * @lc app=leetcode id=207 lang=javascript
 *
 * [207] Course Schedule
 */

// @lc code=start
/**
 * @param {number} numCourses
 * @param {number[][]} prerequisites
 * @return {boolean}
 */
var canFinish = function (n, p) {
    if (p.length === 0 || n === 1) return true;

    let obj = {}, hasLearn = {};
    p.forEach((v) => {
        const [pre, next] = v;
        if (obj[next]) {
            obj[next].push(pre);
        } else {
            obj[next] = [pre];
        }
    });
    let newP = Object.keys(obj).map((v) => [obj[v], v]);

    for (let i = 0; i < n; i++) {
        if (!obj[i]) hasLearn[i] = true;
    }

    while (newP.length) {
        let flag = false;
        for (let i = 0; i < newP.length; i++) {
            const ele = newP[i];
            const [pre, next] = ele;
            if (pre.findIndex((v) => !hasLearn[v]) === -1) {
                hasLearn[next] = true;
                newP = newP.slice(0, i).concat(newP.slice(i + 1));
                flag = true;
                break;
            }
        }
        if (!flag) return false;
    }

    return true;
};
// @lc code=end
module.exports = canFinish;
// console.log(
//     canFinish(3, [
//         [0, 2],
//         [1, 2],
//         [2, 0],
//     ])
// );
