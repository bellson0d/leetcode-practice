/*
 * @lc app=leetcode id=310 lang=javascript
 *
 * [310] Minimum Height Trees
 */

// @lc code=start
/**
 * @param {number} n
 * @param {number[][]} edges
 * @return {number[]}
 */
function GraphNode(val, arr) {
  if (val === undefined) throw Error('No initial value!');
  this.val = val;
  this.neighbor = arr || [];

  this.addNeighbor = (node) => this.neighbor.push(node);
}

var validate = (arr1, arr2) => {
  let count = 0;
  for (let i = 0; i < Math.min(arr1.length, arr2.length); i++) {
    if (arr1[i] === arr2[i]) {
      count++;
    } else {
      break;
    }
  }
  return count;
};

var findMinHeightTrees = function (n, edges) {
  let obj = {},
    nodes = [];
  for (let i = 0; i < n; i++) {
    const node = new GraphNode(i);
    obj[i] = node;
    nodes.push(node);
  }

  edges.forEach((v) => {
    obj[v[0]].addNeighbor(obj[v[1]]);
    obj[v[1]].addNeighbor(obj[v[0]]);
  });

  const visited = {};
  var helper = (node, pre) => {
    if (!visited[node.val]) {
      visited[node.val] = true;
      let next = [];
      for (const n of node.neighbor) {
        if (!visited[n.val]) next.push(n);
      }

      const newPre = [...pre, node.val];
      let res = [];
      if (next.length) {
        for (const n of next) {
          res.push(...helper(n, newPre));
        }
        return res;
      } else {
        return [newPre];
      }
    }
  };
  const arr = helper(nodes[0], []).sort((a, b) => b.length - a.length);

  let long = arr[0].length,
    tmp = arr.filter((v) => v.length === long);
  for (let i = 0; i < arr.length - 1; i++) {
    const length1 = arr[i].length;
    for (let j = i + 1; j < arr.length; j++) {
      const length2 = arr[j].length;
      if (length1 + length2 < long) break;

      const cutLen = validate(arr[i], arr[j]);

      const len = length1 + length2 - cutLen * 2 + 1;
      if (len === long) {
        tmp.push([
          ...arr[j].slice(cutLen).reverse(),
          arr[j][cutLen - 1],
          ...arr[i].slice(cutLen),
        ]);
      }
      if (len > long) {
        long = len;
        tmp = [
          [
            ...arr[j].slice(cutLen).reverse(),
            arr[j][cutLen - 1],
            ...arr[i].slice(cutLen),
          ],
        ];
      }
    }
  }

  const mid = long / 2;
  if (Number.isInteger(mid)) {
    const idx1 = mid - 1;
    const idx2 = mid;
    let res = [...tmp[0].slice(idx1, idx2 + 1)];
    for (let i = 1; i < tmp.length; i++) {
      if (!res.includes(tmp[i][idx1])) return [tmp[i][idx2]];
      if (!res.includes(tmp[i][idx2])) return [tmp[i][idx1]];
    }
    return res;
  } else {
    return [tmp[0][~~mid]];
  }
};
// @lc code=end

module.exports = findMinHeightTrees;
// 这个版本想到了一次遍历完全图，找到最长一条再取中值的形式
// 无奈还是超时，因为在判定最长条时循环还是多了
// 还是借鉴 LC 直接叶子节点逆向
// console.log(
//   findMinHeightTrees(7, [
//     [0, 1],
//     [1, 2],
//     [1, 3],
//     [2, 4],
//     [3, 5],
//     [4, 6],
//   ])
// );
