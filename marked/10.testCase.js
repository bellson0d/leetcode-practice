const targetFun = require('./10.regular-expression-matching');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  [['cc', 'bcdc*'], false],
  [['aa', 'a'], false],
  [['aa', 'a*'], true],
  [['ab', '.*'], true],
  [['aab', 'c*a*b'], true],
  [['aab', 'cc*a*b'], false],
  [['mississippi', 'mis*is*p*.'], false],
  [['ab', '.*c'], false],
  [['aaa', 'aaaa'], false],
  [['aaa', 'a*a'], true],
  [['aabcd', 'a*abcd'], true],
  [['aabcd', '.*d'], true],
  [['abcd', '.*abcd'], true],
  [['abcd', '.*abc'], false],
  [['a', 'a*aa'], false],
  [['aaa', 'ab*a*c*a'], true],
  [['aasdfasdfasdfasdfas', 'aasdf.*asdf.*asdf.*asdf.*s'], true],
  [['aabaa', 'a*aba*'], true],
  [['aabaa', 'a*aba*c*'], true],
  [['aaaaabaaaaa', 'a*aa*baaa*'], true],
  [['cabcbabbacabbbba', 'b*.*aa.*c*c*aa*b*'], false],
  [['bbcbbcbcbbcaabcacb', 'a*.*ac*a*a*.a..*.*'], false],
  [['ab', '.*..c*'], true],
  [['cabbbbcbcacbabc', '.*b.*.ab*.*b*a*c'], true],
  [['aabccbcbacabaab', '.*c*a*b.*a*ba*bb*'], true],
];
let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input[0], input[1]);
  if (result !== output) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
