/*
 * @lc app=leetcode id=416 lang=javascript
 *
 * [416] Partition Equal Subset Sum
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {boolean}
 */
var canPartition = function (nums) {
  const sum = nums.reduce((a, b) => a + b, 0);
  if (sum % 2 !== 0) return false;
  nums.sort((a, b) => b - a);
  if (nums[0] > sum / 2) return false;
  const len = nums.length;
  if (len === 2) return nums[0] === nums[1];

  let left = [nums[0]],
    sumL = nums[0],
    right = [nums[1], nums[2]],
    sumR = nums[1] + nums[2];
  for (let i = 3; i < len; i++) {
    const ele = nums[i];
    if (sumL < sumR) {
      left.push(ele);
      sumL += ele;
    } else {
      right.push(ele);
      sumR += ele;
    }
  }
  if (sumL === sumR) return true;
  const diff = Math.abs(sumL - sumR);
  if (diff % 2 !== 0) return false;
  if (sumL < sumR) {
    return right.includes(diff / 2);
  } else {
    return left.includes(diff / 2);
  }
};
// @lc code=end

// console.log(canPartition([1, 5, 11, 5])); // true
// console.log(canPartition([1, 5, 11, 5, 1])); // false
// console.log(canPartition([1, 2, 3, 4, 5])); // false
// console.log(canPartition([1, 2, 3, 4])); // true
// console.log(canPartition([1, 2, 3, 5])); // false
// console.log(canPartition([2, 2, 2, 2, 10])); // false
// console.log(canPartition([2, 2, 2, 2, 2, 8])); // false
console.log(canPartition([3, 3, 3, 4, 5])); // true
