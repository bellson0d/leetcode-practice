/*
 * @lc app=leetcode id=143 lang=javascript
 *
 * [143] Reorder List
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {void} Do not return anything, modify head in-place instead.
 */
var reorderList = function (head) {
  if (!head) return head;
  let count = 1,
    next = head.next,
    tail = head;
  while (next) {
    next.pre = tail;
    tail = next;
    next = next.next;
    count++;
  }
  if (count < 3) return head;

  let preNode = head.next,
    currentNode = tail,
    flag = true;
  head.next = currentNode;
  while (count > 0) {
    if (flag) {
      let tmp = currentNode.pre;
      currentNode.next = preNode;
      currentNode = preNode;
      preNode = tmp;
    } else {
      let tmp = currentNode.next;
      currentNode.next = preNode;
      currentNode = preNode;
      preNode = tmp;
    }
    flag = !flag;
    count--;
  }
  currentNode.next.next = null;

  return head;
};
// @lc code=end
// module.exports = reorderList;
// const { getList } = require('./utils/listNode');
// console.log(reorderList(getList([1, 2, 3, 4, 5, 6])));
