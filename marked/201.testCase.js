const targetFun = require("./201.bitwise-and-of-numbers-range");
const IS_ARRAY_RESULT = false; // 输出是否为数组
const MULTI_ARGS = true; // 输入参数是否为多个

const testStr = [
    [[0, 0], 0],
    [[0, 1231231], 0],
    [[5, 7], 4],
    [[1, 2147483647], 0],
    [[1, 2], 0],
    [[2, 4], 0],
    [[3, 4], 0],
    [[111, 111], 111],
    [[1, 128], 0],
    [[64, 127], 64],
    [[77, 127], 64],
    [[151, 170], 128],
    [[2147483646, 2147483647], 2147483646],
];

const time1 = new Date().getTime();
console.log(time1);

let count = 0;
while (count < testStr.length) {
    const [input, output] = testStr[count];
    let result;
    if (MULTI_ARGS) {
        result = targetFun(...input);
    } else {
        result = targetFun(input);
    }

    if (
        IS_ARRAY_RESULT
            ? result.join("|") !== output.join("|")
            : result !== output
    ) {
        console.log("Number " + count + " Case Wrong !!!");
        console.log("Input: ", input);
        console.log("Expected output: ", output);
        console.log("Output", result);
        return;
    }
    count++;
}
console.log("All pass!");

const time2 = new Date().getTime();
console.log("Duration: ", time2 - time1);
