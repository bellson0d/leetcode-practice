const targetFun = require('./57.insert-interval');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  [
    [
      [
        [1, 3],
        [6, 9],
      ],
      [2, 5],
    ],
    [
      [1, 5],
      [6, 9],
    ],
  ],
  [
    [
      [
        [1, 2],
        [3, 5],
        [6, 7],
        [8, 10],
        [12, 16],
      ],
      [4, 8],
    ],
    [
      [1, 2],
      [3, 10],
      [12, 16],
    ],
  ],
  [
    [
      [
        [0, 4],
        [1, 4],
      ],
      [2, 9],
    ],
    [[0, 9]],
  ],
  [
    [
      [
        [0, 3],
        [1, 7],
      ],
      [0, 0],
    ],
    [[0, 7]],
  ],
  [
    [
      [
        [0, 0],
        [1, 4],
      ],
      [5, 6],
    ],
    [
      [0, 0],
      [1, 4],
      [5, 6],
    ],
  ],
  [
    [
      [
        [1, 10],
        [2, 3],
        [4, 5],
        [6, 7],
        [8, 9],
      ],
      [9, 12],
    ],
    [[1, 12]],
  ],
];

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input[0], input[1]);
  if (result.join('') !== output.join('')) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
