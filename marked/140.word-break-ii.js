/*
 * @lc app=leetcode id=140 lang=javascript
 *
 * [140] Word Break II
 */

// @lc code=start
/**
 * @param {string} s
 * @param {string[]} wordDict
 * @return {string[]}
 */
var wordBreak = function (s, arr) {
  let map = new Set();
  for (const a of arr.join('')) {
    map.add(a);
  }
  for (const b of s) {
    if (!map.has(b)) return [];
  }

  let obj = {},
    res = [];
  var helper = (s, p) => {
    for (let i = 1; i <= s.length; i++) {
      let pre = p.slice();
      const word = s.slice(0, i);
      if (arr.indexOf(word) !== -1) {
        pre = pre.concat([word]);
        const str = s.slice(word.length);
        if (str === '') {
          res.push(pre.join(' '));
        } else {
          if (!obj.hasOwnProperty(str)) {
            helper(str, pre);
          }
        }
      }
    }
  };

  helper(s, []);
  return res;
};
// @lc code=end

// console.log(wordBreak('leetcode', ['leet', 'code'])); // true
// console.log(wordBreak('applepenapple', ['apple', 'pen'])); // true
// console.log(wordBreak('catsandog', ['cats', 'dog', 'sand', 'and', 'cat'])); // false
// console.log(wordBreak('catsandog', ['cats', 'and'])); // false
// console.log(wordBreak('catsandog', ['cats', 'and', 'og'])); // true
// console.log(wordBreak('bb', ['a', 'b', 'bbb', 'bbbb'])); // true
// console.log(wordBreak('catsanddog', ['cat', 'cats', 'and', 'sand', 'dog'])); // true
