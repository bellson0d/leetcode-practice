/*
 * @lc app=leetcode id=310 lang=javascript
 *
 * [310] Minimum Height Trees
 */

// @lc code=start
/**
 * @param {number} n
 * @param {number[][]} edges
 * @return {number[]}
 */
function GraphNode(val, arr) {
  if (val === undefined) throw Error('No initial value!');
  this.val = val;
  this.neighbor = arr || [];

  this.addNeighbor = (node) => this.neighbor.push(node);
}

var findMinHeightTrees = function (n, edges) {
  let obj = {},
    nodes = [];
  for (let i = 0; i < n; i++) {
    const node = new GraphNode(i);
    obj[i] = node;
    nodes.push(node);
  }

  edges.forEach((v) => {
    obj[v[0]].addNeighbor(obj[v[1]]);
    obj[v[1]].addNeighbor(obj[v[0]]);
  });

  while (nodes.length > 2) {
    const tmp = nodes.filter((v) => v.neighbor.length === 1).map((v) => v.val);
    nodes = nodes
      .filter((v) => v.neighbor.length > 1)
      .map((v) => {
        v.neighbor = v.neighbor.filter((v) => !tmp.includes(v.val));
        return v;
      });
  }

  return nodes.map((v) => v.val);
};
// @lc code=end

module.exports = findMinHeightTrees;
// Inspired by https://leetcode-cn.com/problems/minimum-height-trees/solution/zui-rong-yi-li-jie-de-bfsfen-xi-jian-dan-zhu-shi-x/
// console.log(
//   findMinHeightTrees(7, [
//     [0, 1],
//     [1, 2],
//     [1, 3],
//     [2, 4],
//     [3, 5],
//     [4, 6],
//   ])
// );
