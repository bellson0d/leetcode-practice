/*
 * @lc app=leetcode.cn id=435 lang=javascript
 *
 * [435] 无重叠区间
 */

// @lc code=start
/**
 * @param {number[][]} intervals
 * @return {number}
 */
var eraseOverlapIntervals = function (intervals) {
  intervals.sort((a, b) => {
    if (a[0] === b[0]) {
      return a[1] - b[1];
    } else {
      return a[0] - b[0];
    }
  });

  let idx = 0,
    remove = 0;
  while (idx < intervals.length - 1) {
    const arr1 = intervals[idx];
    const arr2 = intervals[idx + 1];

    if (arr1[1] > arr2[0]) {
      const range1 = arr1[1] - arr1[0];
      const range2 = arr2[1] - arr2[0];
      let i = idx;
      if ((range2 === range1 && arr2[0] > arr1[0]) || range2 > range1)
        i = idx + 1;
      intervals = intervals.slice(0, i).concat(intervals.slice(i + 1));
      remove++;
    } else {
      idx++;
    }
  }

  return remove;
};
// @lc code=end
// https://pic.leetcode-cn.com/1631930017-fYYUAr-file_1631930017753
// 一图道破玄机，学习了

// console.log(
//   eraseOverlapIntervals([
//     [1, 2],
//     [2, 3],
//     [3, 4],
//     [1, 3],
//   ])
// );
// console.log(
//   eraseOverlapIntervals([
//     [1, 2],
//     [1, 2],
//     [1, 2],
//   ])
// );
// console.log(
//   eraseOverlapIntervals([
//     [1, 2],
//     [2, 3],
//   ])
// );
// console.log(
//   eraseOverlapIntervals([
//     [1, 100],
//     [11, 22],
//     [1, 11],
//     [2, 12],
//   ])
// );
console.log(
  eraseOverlapIntervals([
    [-52, 31],
    [-73, -26],
    [82, 97],
    [-65, -11],
    [-62, -49],
    [95, 99],
    [58, 95],
    [-31, 49],
    [66, 98],
    [-63, 2],
    [30, 47],
    [-40, -26],
  ])
);
