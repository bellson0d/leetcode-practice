/*
 * @lc app=leetcode id=16 lang=javascript
 *
 * [16] 3Sum Closest
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var threeSumClosest = function (nums, target) {
  const len = nums.length;
  if (len === 3) return nums[0] + nums[1] + nums[2];
  nums.sort((a, b) => a - b);

  let closestOffset = null;
  let closestVal = null;

  for (let i = 0; i < nums.length; i++) {
    const centerIdx = i;
    let leftIdx = centerIdx === 0 ? 1 : 0,
      rightIdx = centerIdx === len - 1 ? len - 2 : len - 1;

    while (leftIdx < rightIdx) {
      const leftVal = nums[leftIdx];
      const rightVal = nums[rightIdx];
      const centerVal = nums[centerIdx];
      const sumVal = centerVal + leftVal + rightVal;

      if (sumVal === target) return sumVal;
      if (sumVal < target) {
        const offSet = target - sumVal;
        if (offSet < closestOffset || !closestOffset) {
          closestOffset = offSet;
          closestVal = sumVal;
        }
        leftIdx++;
        if (leftIdx === centerIdx) {
          leftIdx++;
        }
      } else {
        const offSet = sumVal - target;
        if (offSet < closestOffset || !closestOffset) {
          closestOffset = offSet;
          closestVal = sumVal;
        }
        rightIdx--;
        if (rightIdx === centerIdx) {
          rightIdx--;
        }
      }
    }
  }

  return closestVal;
};

module.exports = threeSumClosest;

console.log(threeSumClosest([1, 1, 1, 1], 3));
// @lc code=end
