/*
 * @lc app=leetcode id=152 lang=javascript
 *
 * [152] Maximum Product Subarray
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var maxProduct = function (nums) {
  const len = nums.length;
  if (len === 1) return nums[0];

  let max = nums[0],
    preMin = nums[0],
    preMax = nums[0],
    idx = 1;
  while (idx < len) {
    const val1 = preMin * nums[idx];
    const val2 = preMax * nums[idx];
    preMin = Math.min(val1, nums[idx], val2);
    preMax = Math.max(val1, nums[idx], val2);
    if (preMax > max) max = preMax;
    idx++;
  }

  return max;
};
// @lc code=end
// Inspired by https://leetcode-cn.com/problems/maximum-product-subarray/solution/wa-ni-zhe-ti-jie-shi-xie-gei-bu-hui-dai-ma-de-nu-p/
// 又找笨猪学习了
// console.log(maxProduct([2, 3, -2, 4]));
// console.log(maxProduct([-2, 0, -1]));
// console.log(maxProduct([0, 2]));
// console.log(maxProduct([1, 2, 3, 0, 2, 3, 4]));
// console.log(maxProduct([3, -1, 4]));
// console.log(maxProduct([1, 0, -1, 2, 3, -5, -2]));
