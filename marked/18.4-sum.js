/*
 * @lc app=leetcode id=18 lang=javascript
 *
 * [18] 4Sum
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[][]}
 */

var threeSum = function (nums, target) {
  const len = nums.length;
  nums.sort((a, b) => a - b);
  let result = [];

  for (let i = 0; i < len - 2; i++) {
    const ele = nums[i];
    if (ele === nums[i - 1]) continue;

    let leftIdx = i + 1,
      rightIdx = nums.length - 1;

    while (leftIdx < rightIdx) {
      const leftVal = nums[leftIdx];
      const rightVal = nums[rightIdx];
      const sumVal = ele + leftVal + rightVal;

      if (sumVal < target) {
        leftIdx++;
      } else if (sumVal > target) {
        rightIdx--;
      } else {
        result.push([ele, leftVal, rightVal]);
        while (leftIdx < rightIdx && leftVal === nums[leftIdx + 1]) leftIdx++;
        while (leftIdx < rightIdx && rightVal === nums[rightIdx - 1])
          rightIdx--;
        leftIdx++;
        rightIdx--;
      }
    }
  }

  return result;
};

var fourSum = function (nums, target) {
  const len = nums.length;
  nums.sort((a, b) => a - b);
  let result = [];

  for (let i = 0; i < len - 3; i++) {
    const ele = nums[i];

    if (i > 0 && ele === nums[i - 1]) continue;

    const tmpArr = threeSum(nums.slice(i + 1), target - ele);
    if (tmpArr.length > 0) {
      result = result.concat(tmpArr.map((v) => [ele].concat(v)));
    }
  }

  return result;
};

// console.log(fourSum([-3, -2, -1, 0, 0, 1, 2, 3], 0));
// console.log(fourSum([2, 1, 0, -1], 2));

// @lc code=end
