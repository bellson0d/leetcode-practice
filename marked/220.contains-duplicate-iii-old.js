/*
 * @lc app=leetcode id=220 lang=javascript
 *
 * [220] Contains Duplicate III
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @param {number} k
 * @param {number} t
 * @return {boolean}
 */
var helper = (nums, t) => {
    nums.sort((a, b) => a - b);
    for (let i = 0; i < nums.length - 1; i++) {
        if (nums[i + 1] - nums[i] <= t) return true;
    }
    return false;
};

var containsNearbyAlmostDuplicate = function (nums, k, t) {
    if (k === 0) return false;

    let newN = [];
    for (let i = 0; i < nums.length - k; i++) {
        newN.push(nums.slice(i, i + k + 1));
    }
    if (nums.length <= k) newN = [nums];

    for (let j = 0; j < newN.length; j++) {
        const n = newN[j];
        if (helper(n, t)) return true;
    }
    return false;
};
// @lc code=end
// 这种解法通不过 big case
// console.log(containsNearbyAlmostDuplicate([1, 2, 3, 1], 3, 0)); // true
// console.log(containsNearbyAlmostDuplicate([1, 0, 1, 1], 1, 2)); // true
// console.log(containsNearbyAlmostDuplicate([1, 5, 9, 1, 5, 9], 2, 3)); // false
// console.log(containsNearbyAlmostDuplicate([2147483646, 2147483647], 3, 3)); // true
// console.log(containsNearbyAlmostDuplicate([-5,5,5,5,5,15], 6, 6)); // true
