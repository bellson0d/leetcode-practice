/*
 * @lc app=leetcode id=56 lang=javascript
 *
 * [56] Merge Intervals
 */

// @lc code=start
/**
 * @param {number[][]} intervals
 * @return {number[][]}
 */
var merge = function (intervals) {
  const len = intervals.length;
  if (len < 2) return intervals;

  const nums = intervals.sort((a, b) => a[0] - b[0]);

  let result = [],
    count = 1,
    currentArr = nums[0];

  while (count < len) {
    const nextArr = nums[count];
    const preFirst = currentArr[0];
    const preLast = currentArr[1];
    const nextFirst = nextArr[0];
    const nextLast = nextArr[1];
    if (preLast >= nextFirst && preFirst <= nextLast) {
      currentArr = [Math.min(preFirst, nextFirst), Math.max(preLast, nextLast)];
    } else {
      result.push(currentArr);
      currentArr = nextArr;
    }

    count++;
  }
  result.push(currentArr);

  return result;
};
// @lc code=end
// module.exports = merge;
// console.log(
//   merge([
//     [1, 3],
//     [2, 6],
//     [8, 10],
//     [15, 18],
//   ])
// );
