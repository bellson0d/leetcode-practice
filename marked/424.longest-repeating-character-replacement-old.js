/*
 * @lc app=leetcode id=424 lang=javascript
 *
 * [424] Longest Repeating Character Replacement
 */

// @lc code=start
/**
 * @param {string} s
 * @param {number} k
 * @return {number}
 */
var characterReplacement = function (s, k) {
  const len = s.length;
  if (len === 1 || len - 2 < k) return len;

  let leftP = 0,
    rightP = 1,
    obj = { [s[0]]: 1 },
    maxCharCount = 1,
    maxChar = s[0],
    remindK = k,
    tmpMax = 1;

  while (rightP < len) {
    if (s[rightP] === maxChar) {
      obj[s[rightP]]++;
      maxCharCount++;
    } else {
      if (!obj[s[rightP]]) {
        obj[s[rightP]] = 1;
      } else {
        obj[s[rightP]]++;
      }

      obj[s[leftP]]--;
      if (s[leftP] !== maxChar) {
        remindK++;
      } else {
        obj[maxChar]--;
        maxCharCount--;
      }
      leftP++;

      if (obj[s[rightP]] > obj[maxChar]) {
        const diff = rightP - leftP + 1 - obj[s[rightP]];
        if (leftP === 0) {
          maxCharCount = obj[s[rightP]] + diff;
          maxChar = s[rightP];
          remindK = k - diff;
        } else {
          if (diff < k) {
            leftP--;
          }
          maxCharCount = obj[s[rightP]] + k;
          maxChar = s[rightP];
          remindK = 0;
        }
      } else {
        if (remindK > 0) {
          maxCharCount++;
          remindK--;
        } else {
          maxCharCount--;
        }
      }
    }

    if (maxCharCount > tmpMax) {
      console.log('-'.repeat(100));
      // console.log(leftP, rightP);
      console.log(s.slice(leftP, rightP + 1));
      console.log('-'.repeat(100));
      tmpMax = maxCharCount;
    }

    rightP++;
  }

  return tmpMax > len ? len : tmpMax;
};
// @lc code=end

// console.log(characterReplacement('ABAB', 2)); // 4
// console.log(characterReplacement('AABABBA', 1)); // 4
// console.log(characterReplacement('AABABBBA', 3)); // 7
// console.log(characterReplacement('AABABCCBA', 3)); // 6
// console.log(characterReplacement('ABBB', 2)); // 4
// console.log(
//   characterReplacement(
//     'IMNJJTRMJEGMSOLSCCQICIHLQIOGBJAEHQOCRAJQMBIBATGLJDTBNCPIFRDLRIJHRABBJGQAOLIKRLHDRIGERENNMJSDSSMESSTR',
//     2
//   )
// ); // 6
console.log(characterReplacement('NMJSDSSMESS', 2)); // 6

// @after-stub-for-debug-begin
module.exports = characterReplacement;
// @after-stub-for-debug-end
