const targetFun = require('./76.minimum-window-substring');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  [['', ''], ''],
  [['', 'ros'], ''],
  [['abc', ''], ''],
  [['aa', 'ab'], ''],
  [['a', 'bcd'], ''],
  [['aa', 'a'], 'a'],
  [['ba', 'a'], 'a'],
  [['abcdefv', 'avc'], 'abcdefv'],
  [['abcdefav', 'avc'], 'cdefav'],
  [['ADOBECODEBANC', 'ABC'], 'BANC'],
  [['ABABABCCC', 'ABC'], 'ABC'],
  [['ABABABCCC', 'ABBC'], 'BABC'],
  [['cabwefgewcwaefgcf', 'cae'], 'cwae'],
  [['cabwefgewcwaefgcf', 'xxx'], ''],
  [['abcd'.repeat(10000) + 'abd', 'aabd'], 'abcda'],
];

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input[0], input[1]);
  if (result !== output) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
