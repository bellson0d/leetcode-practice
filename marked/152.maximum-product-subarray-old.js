/*
 * @lc app=leetcode id=152 lang=javascript
 *
 * [152] Maximum Product Subarray
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var splitArr = (arr) => {
  let idx = 0,
    tmp = [];
  for (const a of arr) {
    if (a) {
      if (tmp[idx]) {
        tmp[idx].push(a);
      } else {
        tmp[idx] = [a];
      }
    } else {
      idx++;
    }
  }
  return tmp.filter((v) => v);
};

var helper = function (nums) {
  const len = nums.length;
  if (len === 1) return nums[0];

  let max = nums[0],
    pre = nums[0],
    idx = 1;
  while (idx < len) {
    let tmp = pre * nums[idx],
      tmpMax = tmp,
      i = 0;
    pre = tmp;
    while (i < idx) {
      tmp = tmp / nums[i];
      if (tmp > tmpMax) {
        tmpMax = tmp;
      }
      i++;
    }
    if (tmpMax > max) max = tmpMax;
    idx++;
  }

  return max || 0;
};

var maxProduct = function (nums) {
  if (nums.includes(0)) {
    return Math.max(...splitArr(nums).map(helper), 0);
  } else {
    return helper(nums);
  }
};
// @lc code=end
// Inspired by https://leetcode-cn.com/problems/maximum-product-subarray/solution/wa-ni-zhe-ti-jie-shi-xie-gei-bu-hui-dai-ma-de-nu-p/
// 又找笨猪学习了
// console.log(maxProduct([2, 3, -2, 4]));
// console.log(maxProduct([-2, 0, -1]));
// console.log(maxProduct([0, 2]));
// console.log(maxProduct([1, 2, 3, 0, 2, 3, 4]));
// console.log(maxProduct([3, -1, 4]));
// console.log(maxProduct([1, 0, -1, 2, 3, -5, -2]));
