/*
 * @lc app=leetcode id=301 lang=javascript
 *
 * [301] Remove Invalid Parentheses
 */

// @lc code=start
/**
 * @param {string} s
 * @return {string[]}
 */
var trimS = (s) => {
  let str = '',
    removeIdx = [],
    idx1 = 0,
    idx2 = s.length - 1;

  while (idx1 < s.length && s[idx1] !== '(') {
    if (s[idx1] === ')') removeIdx.push(idx1);
    idx1++;
  }
  while (idx2 > idx1 - 1 && s[idx2] !== ')') {
    if (s[idx2] === '(') removeIdx.push(idx2);
    idx2--;
  }

  for (let i = 0; i < s.length; i++) {
    if (!removeIdx.includes(i)) str += s[i];
  }

  return str;
};

var countBrackets = (s) => {
  let left = 0,
    right = 0;

  for (let i = 0; i < s.length; i++) {
    if (s[i] === '(') left++;
    if (s[i] === ')') right++;
  }

  return [left, right];
};

const BASE_MAP = {
  '(': ')',
  ')': '(',
};
var reverseS = (s) => {
  return s
    .split('')
    .reverse()
    .map((v) => (BASE_MAP[v] ? BASE_MAP[v] : v))
    .join('');
};

var helper = (s, arr) => {
  let res = [];
  for (const idx of arr) {
    const newStr = s.slice(0, idx) + s.slice(idx + 1);
    if (!res.find((v) => v[0] === newStr)) {
      let newArr = [];
      for (let j = 0; j < arr.length; j++) {
        const ele = arr[j];
        if (ele !== idx) {
          newArr.push(ele > idx ? ele - 1 : ele);
        }
      }
      res.push([newStr, newArr]);
    }
  }
  return res;
};

var removeInvalidParentheses = function (s) {
  s = trimS(s);
  if (s.indexOf('(') === -1 || s.indexOf(')') === -1) return [s];
  const [left, right] = countBrackets(s);

  if (left > right) s = reverseS(s);

  let leftP = 0,
    rightP = 0,
    lArr = [],
    rArr = [],
    tmpArr = [['', []]],
    count = 0;

  while (rightP < s.length) {
    const ele = s[rightP];
    if (ele === '(') lArr.push(rightP);
    if (ele === ')') rArr.push(rightP);

    if (rArr.length > lArr.length) {
      let tempArr = [];
      for (const [preS, preRArr] of tmpArr) {
        const newArr = helper(
          preS + s.slice(leftP, rightP + 1),
          preRArr.concat(rArr.map((v) => v - count))
        );
        for (const newItem of newArr) {
          if (!tempArr.find((v) => v[0] === newItem[0])) {
            tempArr.push(newItem);
          }
        }
      }
      count++;
      lArr = [];
      rArr = [];
      leftP = rightP + 1;
      rightP = leftP;
      tmpArr = tempArr;
    } else {
      rightP++;
    }
  }

  tmpArr = tmpArr.map((v) => v[0]);

  if (leftP < s.length) {
    const tailS = s.slice(leftP, rightP + 1);
    const [l, r] = countBrackets(tailS);
    if (l !== r) {
      const tmpArr2 = removeInvalidParentheses(s.slice(leftP, rightP + 1));
      let tmpArr3 = [];
      for (let i = 0; i < tmpArr.length; i++) {
        const s1 = tmpArr[i];
        for (let j = 0; j < tmpArr2.length; j++) {
          const s2 = tmpArr2[j];
          tmpArr3.push(s1 + s2);
        }
      }
      tmpArr = tmpArr3;
    } else {
      tmpArr = tmpArr.map((v) => v + tailS);
    }
  }

  if (left > right) {
    tmpArr = tmpArr.map(reverseS);
  }
  return tmpArr;
};

// @lc code=end

module.exports = removeInvalidParentheses;

// console.log(removeInvalidParentheses('())))(((()'));
// console.log(removeInvalidParentheses('(r(()()('));
