/*
 * @lc app=leetcode id=301 lang=javascript
 *
 * [301] Remove Invalid Parentheses
 */

// @lc code=start
/**
 * @param {string} s
 * @return {string[]}
 */
var trimS = (s) => {
  while (s[0] === ')') {
    s = s.slice(1);
  }
  while (s[s.length - 1] === '(') {
    s = s.slice(0, s.length - 1);
  }
  return s;
};

var countBrackets = (s) => {
  let left = [],
    right = [];

  for (let i = 0; i < s.length; i++) {
    if (s[i] === '(') left.push(i);
    if (s[i] === ')') right.push(i);
  }

  return [left, right];
};

var validS = (s) => {
  return s[0] !== ')' && s[s.length - 1] !== '(';
};

var removeInvalidParentheses = function (s) {
  s = trimS(s);

  const [left, right] = countBrackets(s);

  let set = new Set([]),
    tmp = [['', s, left, right, 0, 0]],
    res = [];
  while (tmp.length) {
    let [preS, leftS, left, right, preLeftCount, preRightCount] = tmp.shift();
    if (!leftS) {
      const finalS = preS.replace(/@/g, '');
      if (validS(finalS) && !res.includes(finalS)) res.push(finalS);

      continue;
    }

    const ele = leftS[0];
    if (ele === '(') {
      if (preLeftCount >= right.length - preRightCount) {
        preS += ele;
        for (let i = 0; i < left.length; i++) {
          const idx = Number(left[i]);
          if (idx < preS.length) {
            const tmpStr = preS.slice(0, idx) + '@' + preS.slice(idx + 1);
            if (!set.has(tmpStr)) {
              set.add(tmpStr);
              tmp.push([
                tmpStr,
                leftS.slice(1),
                left.slice(0, i).concat(left.slice(i + 1)),
                right,
                preLeftCount,
                preRightCount,
              ]);
            }
          }
        }
      } else {
        preS += ele;
        tmp.push([
          preS,
          leftS.slice(1),
          left,
          right,
          preLeftCount + 1,
          preRightCount,
        ]);
      }
    } else if (ele === ')') {
      if (preRightCount === preLeftCount) {
        preS += ele;
        for (let i = 0; i < right.length; i++) {
          const idx = Number(right[i]);
          if (idx < preS.length) {
            const tmpStr = preS.slice(0, idx) + '@' + preS.slice(idx + 1);
            if (!set.has(tmpStr)) {
              set.add(tmpStr);
              tmp.push([
                tmpStr,
                leftS.slice(1),
                left,
                right.slice(0, i).concat(right.slice(i + 1)),
                preLeftCount,
                preRightCount,
              ]);
            }
          }
        }
      } else {
        preS += ele;
        tmp.push([
          preS,
          leftS.slice(1),
          left,
          right,
          preLeftCount,
          preRightCount + 1,
        ]);
      }
    } else {
      preS += ele;
      tmp.push([
        preS,
        leftS.slice(1),
        left,
        right,
        preLeftCount,
        preRightCount + 1,
      ]);
    }
  }

  return res;
};
// @lc code=end

// console.log(removeInvalidParentheses('()())()'));
// console.log(removeInvalidParentheses('(a)())()'));
// console.log(removeInvalidParentheses('(a(())()'));
// console.log(removeInvalidParentheses(')('));
// console.log(removeInvalidParentheses('('));
// console.log(removeInvalidParentheses('))))((((('));
// console.log(removeInvalidParentheses('))))()((((('));
// console.log(removeInvalidParentheses('()(((((()'));
// console.log(removeInvalidParentheses('()(((((()()))'));
console.log(removeInvalidParentheses('()()()(((()'));
// console.log(removeInvalidParentheses('n'));
// console.log(removeInvalidParentheses('())(())('));
// console.log(removeInvalidParentheses('())))(((()'));
// console.log(removeInvalidParentheses('((((()'));
// console.log(removeInvalidParentheses('(((k()(('));
