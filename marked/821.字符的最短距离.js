/*
 * @lc app=leetcode.cn id=821 lang=javascript
 *
 * [821] 字符的最短距离
 */

// @lc code=start
/**
 * @param {string} s
 * @param {character} c
 * @return {number[]}
 */
var shortestToChar = function (s, c) {
  let res = [],
    pre = Infinity;
  for (let i = 0; i < s.length; i++) {
    const ele = s[i];
    if (ele === c) {
      res.push(0);
      pre = i;
      j = i - 1;
      while (j >= 0) {
        const range = pre - j;
        if (range < res[j]) {
          res[j] = range;
        } else {
          break;
        }
        j--;
      }
    } else {
      res.push(Math.abs(i - pre));
    }
  }
  return res;
};
// @lc code=end

// console.log(shortestToChar('loveleetcode', 'e'));
// console.log(shortestToChar('aaab', 'b'));
// console.log(shortestToChar('b', 'b'));
// console.log(shortestToChar('bqwerzxcv', 'b'));
