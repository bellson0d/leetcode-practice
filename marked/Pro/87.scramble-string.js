/*
 * @lc app=leetcode id=87 lang=javascript
 *
 * [87] Scramble String
 */

// @lc code=start
/**
 * @param {string} s1
 * @param {string} s2
 * @return {boolean}
 */
var isScramble = function (s1, s2) {
  const len = s1.length;
  if (s1 === s2) return true;
  if (len < 2) return s1 === s2;

  var hasLove = (s1, s2) => {
    for (const s11 of s1) {
      if (!s2.includes(s11)) return false;
    }
    for (const s12 of s2) {
      if (!s1.includes(s12)) return false;
    }
    return true;
  };
  let obj = {};

  var helper = (s1, s2) => {
    const key = s1 + '-' + s2;
    if (obj.hasOwnProperty(key)) {
      return obj[key];
    }

    if (s1 === s2) {
      obj[key] = true;
      return true;
    }
    const len = s1.length;
    if (len === 1) {
      if (s1 === s2) {
        obj[key] = true;
        return true;
      } else {
        obj[key] = false;
        return false;
      }
    }

    if (!hasLove(s1, s2)) {
      obj[key] = false;
      return false;
    }

    for (let i = 1; i < len; i++) {
      const left = s1.slice(0, i);
      const right = s1.slice(i);
      const left2 = s2.slice(0, i);
      const right2 = s2.slice(i);
      const left3 = s2.slice(len - i);
      const right3 = s2.slice(0, len - i);

      if (helper(left, left2) && helper(right, right2)) {
        obj[key] = true;
        return true;
      }
      if (helper(left, left3) && helper(right, right3)) {
        obj[key] = true;
        return true;
      }
    }
    obj[key] = false;
    return false;
  };

  return helper(s1, s2);
};

// @lc code=end
module.exports = isScramble;
// 理解和谐的概念才能解~
// FXXK FXXK FXXK!!!
// 记忆化写错一度以为自己是 SB
// 魔鬼在细节 !!!
// console.log(isScramble('abc', 'cba')); // true
// console.log(isScramble('great', 'rgtae')); // true
// console.log(isScramble('eat', 'tae')); // true
// console.log(isScramble('abcd', 'acbd')); // true
