/*
 * @lc app=leetcode id=282 lang=javascript
 *
 * [282] Expression Add Operators
 */

// @lc code=start
/**
 * @param {string} num
 * @param {number} target
 * @return {string[]}
 */
const addOperators = (num, target) => {
  let res = [],
    ans = [];
  const backTrack = (num0, val, pre, k) => {
    if (num0.length < 1) {
      if (val === target) {
        ans.push(res.slice(0, k).join(''));
      }
      return;
    }
    for (let i = 0; i < num0.length; i++) {
      if (k === 0) {
        res[k] = num0.slice(0, i + 1);
        backTrack(num0.slice(i + 1), Number(res[k]), Number(res[k]), k + 1);
      } else {
        res[k] = '+';
        res[k + 1] = num0.slice(0, i + 1);
        backTrack(
          num0.slice(i + 1),
          val + Number(res[k + 1]),
          Number(res[k + 1]),
          k + 2
        );
        res[k] = '-';
        res[k + 1] = num0.slice(0, i + 1);
        backTrack(
          num0.slice(i + 1),
          val - Number(res[k + 1]),
          -Number(res[k + 1]),
          k + 2
        );
        res[k] = '*';
        res[k + 1] = num0.slice(0, i + 1);
        backTrack(
          num0.slice(i + 1),
          val - pre + pre * Number(res[k + 1]),
          pre * Number(res[k + 1]),
          k + 2
        );
      }
      if (num0[0] === '0') break;
    }
  };
  backTrack(num, 0, 0, 0);
  return ans;
};

// @lc code=end
// const time1 = new Date().getTime();
// console.log(time1);
console.log(addOperators('123', 6)); // ["1*2*3","1+2+3"]
// console.log(addOperators('232', 8)); // ["2*3+2","2+3*2"]
// console.log(addOperators('105', 5)); // ["1*0+5","10-5"]
// console.log(addOperators('00', 0)); // ["0*0","0+0","0-0"]
// console.log(addOperators('0', 0));
// console.log(addOperators('9', 9));
// console.log(addOperators('3456237490', 9191)); // []
// console.log(addOperators('111', 1)); // ["1*1*1", "1+1-1", "1-1+1"]
// console.log(addOperators('1000000009', 9));
// console.log(addOperators('1000000009', 9).length); // 3280
// console.log(addOperators('2147483647', 2147483647));
// console.log(addOperators('2147483647', -2147483647));
// console.log(addOperators('123456789', 45)); // 121
// console.log(addOperators('123456789', 45).length); // 121
// const time2 = new Date().getTime();
// console.log('Duration: ', time2 - time1);
