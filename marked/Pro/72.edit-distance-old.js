/*
 * @lc app=leetcode id=72 lang=javascript
 *
 * [72] Edit Distance
 */

// @lc code=start
/**
 * @param {string} w1
 * @param {string} w2
 * @return {number}
 */
var findEqualLength = (s1, s2, type) => {
  const len1 = s1.length,
    len2 = s2.length;
  if (type === 1) {
    let p1 = (p2 = 0),
      tmp = 0,
      idx = 0;
    while (p1 < len1 && p2 < len2) {
      if (s1[p1] === s2[p2]) {
        idx = p2;
        tmp++;
        p1++;
        p2++;
      } else if (p1 === len1 - 1) {
        if (p2 < len2 - 1) {
          p2++;
        } else {
          return tmp;
        }
      } else {
        if (p2 < len2 - 1) {
          p2++;
        } else {
          p1++;
          p2 = idx + 1;
        }
      }
    }
    return tmp;
  }

  let tmp2 = 0,
    idx2 = 0;
  while (idx2 < len1) {
    if (s1[idx2] === s2[idx2]) tmp2++;
    idx2++;
  }
  return len1 === len2 ? tmp2 : tmp;
};

var minDistance = function (w1, w2) {
  const len1 = w1.length,
    len2 = w2.length;
  if (!w1) return len2;
  if (!w2) return len1;
  if (w1 === w2) return 0;
  if (w1.includes(w2) || w2.includes(w1)) return Math.abs(len1 - len2);

  let [minW, maxW] = len1 < len2 ? [w1, w2] : [w2, w1];
  const constantNum = maxW.length - minW.length;
  let idx = 0,
    maxCount = 0,
    minLen = minW.length;

  if (len1 === len2) {
    while (idx <= constantNum) {
      maxCount = Math.max(
        findEqualLength(minW, maxW.slice(idx, idx + minLen)),
        maxCount
      );
      idx++;
    }
  } else {
    while (idx < minLen && minLen - idx > maxCount) {
      maxCount = Math.max(findEqualLength(minW.slice(idx), maxW, 1), maxCount);
      idx++;
    }
  }
  console.log(maxCount);

  return minLen - maxCount + constantNum;
};
// @lc code=end

module.exports = minDistance;
