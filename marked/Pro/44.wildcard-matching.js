/*
 * @lc app=leetcode id=44 lang=javascript
 *
 * [44] Wildcard Matching
 */

// @lc code=start
/**
 * @param {string} s
 * @param {string} p
 * @return {boolean}
 */
const allStar = (s) => {
  let count = 0;
  while (count < s.length) {
    if (s[count] !== '*') return false;
    count++;
  }
  return s.length > 0;
};

const getMinLen = (p) => {
  return p.split('').filter((v) => v !== '*').length;
};

const collapse = (s) => {
  let count = 0,
    res = [];
  while (count < s.length) {
    if (res[res.length - 1] !== '*' || s[count] !== '*') res.push(s[count]);
    count++;
  }
  return res.join('');
};

var isMatch = function (s, pre) {
  const p = collapse(pre);
  if (allStar(p)) return true;

  if (!s && !p) return true;
  if (s && !p) return false;

  const len1 = s.length;
  const minLen2 = getMinLen(p);

  if (minLen2 > len1) return false;

  if (minLen2 === len1) {
    let count = 0;
    for (let i = 0; i < len1; i++) {
      const ele = s[i];
      const ele2 = p[i + count];

      if (ele2 === '*') {
        i--;
        count++;
      } else if (ele !== ele2 && ele2 !== '?') return false;
    }
    return true;
  }

  let fillCount = len1 - minLen2;

  let count = 0,
    pointer = 0,
    tmpArr = [];

  while (count < len1) {
    const ele = s[count];
    const ele2 = p[pointer];

    if (!ele2) return false;

    if (ele === ele2 || ele2 === '?') {
      count++;
      pointer++;
    } else if (ele2 === '*') {
      if (fillCount > 0) {
        count = count + fillCount - 1;
        if (count > -1) tmpArr.push([count, pointer, fillCount]);
        pointer++;
        count++;
        fillCount = 0;
      } else {
        pointer++;
      }
    } else if (tmpArr.length > 0) {
      let [preCount, po, fill] = tmpArr.pop();

      if (fill > 0) {
        fillCount++;
        preCount--;
        fill--;
        tmpArr.push([preCount, po, fill]);
        count = preCount + 1;
        pointer = po + 1;
      }
    } else {
      return false;
    }
  }

  return true;
};
// @lc code=end

module.exports = isMatch;
// console.log(isMatch('adceb', '*a*b'));
// console.log(isMatch('bbaa', '*a*?'));
// console.log(isMatch('mississippi', 'm*iss*iss*'));
// console.log(isMatch('mississippi', 'm*i*si*si*pis'));

// Note:
// 该题一直卡着的原因是没有觉察到匹配规则的特性，从一开始定位成回溯，然后自然联想到
// 每个 * 优先匹配最多个数，然后不符合再回溯减少，这样复杂度直逼 N^2 自然是大用例通不过
// 的。
// 看题解其实是 * 优先匹配 0 位，然后就继续下一位，如果不符合，只记录最近一次 * 号位置
// 然后增长，这样在处理长用例时依然高效。核心点在于从低位开始匹配基本就断定了是最小长度，
// 所以优先耗尽 s 之后仅做剩余 p 串的判断即可。
// 想象 A........B........C , 如果只有 * 肯定通过，如果中间有个 *M* ，如果跨不过 M 则直接
// 可以否掉，不需要先 * 贪婪匹配所有再回溯减少。
