/*
 * @lc app=leetcode id=124 lang=javascript
 *
 * [124] Binary Tree Maximum Path Sum
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */

var maxPathSum = function (node) {
  let subMax = -Infinity;
  var helper = (node) => {
    if (!node.left && !node.right) {
      if (node.val > subMax) subMax = node.val;
      return Math.max(-Infinity, node.val);
    }

    let left = 0,
      right = 0;
    if (node.left) left = Math.max(0, helper(node.left));
    if (node.right) right = Math.max(0, helper(node.right));
    const tmpMax = left + right + node.val;
    if (tmpMax > subMax) subMax = tmpMax;

    return Math.max(0, node.val, node.val + left, node.val + right);
  };
  helper(node);
  return subMax;
};
// @lc code=end
// Inspired by https://leetcode-cn.com/problems/binary-tree-maximum-path-sum/solution/shou-hui-tu-jie-hen-you-ya-de-yi-dao-dfsti-by-hyj8/
// 关键点是以下两条注释

// const innerMaxSum = left + root.val + right; // 当前子树内部的最大路径和
// const outputMaxSum = root.val + Math.max(left, right); // 当前子树对外提供的最大和

// const { getTree } = require('./utils/treeNode');
// console.log(maxPathSum(getTree([1, 2, 3])));
// console.log(maxPathSum(getTree([-1, -2, -3])));
// console.log(maxPathSum(getTree([-1, -2, -3, 1])));
// console.log(maxPathSum(getTree([-10, 9, 20, null, null, 15, 7])));
// console.log(
//   maxPathSum(getTree([5, 4, 8, 11, null, 13, 4, 7, 2, null, null, null, 1]))
// );
// console.log(maxPathSum(getTree([-3])));
// console.log(maxPathSum(getTree([-1, -2, 10, -6, null, -3, -6])));
