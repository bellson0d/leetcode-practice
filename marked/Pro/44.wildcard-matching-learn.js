/*
 * @lc app=leetcode id=44 lang=javascript
 *
 * [44] Wildcard Matching
 */

// @lc code=start
/**
 * @param {string} s
 * @param {string} p
 * @return {boolean}
 */
var isMatch = function (s, p) {
  var starS = null,
    starP = null;
  var i = 0,
    j = 0;
  while (s[i]) {
    if (s[i] === p[j] || p[j] === '?') {
      i++;
      j++;
      continue;
    }
    if (p[j] === '*') {
      while (p[j + 1] && p[j + 1] === '*') {
        j++;
      }
      starS = i;
      starP = j;
      j++;
      continue;
    }
    if (starS !== null) {
      i = starS + 1;
      j = starP + 1;
      starS++;
      continue;
    }
    return false;
  }
  while (p[j] === '*') {
    j++;
  }

  return !p[j];
};
// @lc code=end

module.exports = isMatch;
// console.log(isMatch('adceb', '*a*b'));
// console.log(isMatch('bbaa', '*a*?'));
// console.log(isMatch('mississippi', 'm*iss*iss*'));
console.log(isMatch('mississippi', 'm*i*si*si*pis'));
