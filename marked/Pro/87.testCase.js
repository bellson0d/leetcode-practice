const targetFun = require('./87.scramble-string');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  [['great', 'rgeat'], true],
  [['great', 'rgtae'], true],
  [['abcde', 'caebd'], false],
  [['abcde', 'cdeba'], true],
  [['abb', 'bab'], true],
  [['aa', 'aa'], true],
  [['a', 'a'], true],
  [['abcd', 'dcba'], true],
  [['abcdefgh', 'hgfedcab'], true],
  [['abab', 'aabb'], true],
  [['abcdbdacbdac', 'bdacabcdbdac'], true],
  [['abc', 'bac'], true],
  [['abcd', 'acbd'], true],
  [['ccabcbabcbabbbbcbb', 'bbbbabccccbbbabcba'], false],
];

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input[0], input[1]);
  if (result !== output) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
