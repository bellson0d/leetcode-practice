const targetFun = require('./44.wildcard-matching');
const IS_ARRAY_RESULT = false; // 输出是否为数组
const MULTI_ARGS = true; // 输入参数是否为多个

const testStr = [
  [['', 'a'], false],
  [['', ''], true],
  [['aa', '*a*'], true],
  [['aa', '*'], true],
  [['cb', '?a'], false],
  [['adceb', '*a*b'], true],
  [['acdcb', 'a*c?b'], false],
  [['mississippi', 'm??*ss*?i*pi'], false],
  [['c', '*?*'], true],
  [['mississippi', 'm*iss*'], true],
  [['mississippi', 'm*iss*iss*'], true],
  [['mississippi', 'm*i*si*si*pis'], false],
  [['bbaa', '*a*?'], true],
  // [
  //   [
  //     'abbabaaabbabbaababbabbbbbabbbabbbabaaaaababababbbabababaabbababaabbbbbbaaaabababbbaabbbbaabbbbababababbaabbaababaabbbababababbbbaaabbbbbabaaaabbababbbbaababaabbababbbbbababbbabaaaaaaaabbbbbaabaaababaaaabb',
  //     '**aa*****ba*a*bb**aa*ab****a*aaaaaa***a*aaaa**bbabb*b*b**aaaaaaaaa*a********ba*bbb***a*ba*bb*bb**a*b*bb',
  //   ],
  //   false,
  // ],
];

const time1 = new Date().getTime();
console.log(time1);

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  let result;
  if (MULTI_ARGS) {
    result = targetFun(...input);
  } else {
    result = targetFun(input);
  }

  if (
    IS_ARRAY_RESULT ? result.join('|') !== output.join('|') : result !== output
  ) {
    console.log('Number ' + count + ' Case Wrong !!!');
    console.log('Input: ', input);
    console.log('Expected output: ', output);
    console.log('Output', result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
