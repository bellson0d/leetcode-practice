/*
 * @lc app=leetcode id=282 lang=javascript
 *
 * [282] Expression Add Operators
 */

// @lc code=start
/**
 * @param {string} num
 * @param {number} target
 * @return {string[]}
 */
var baseMap = {
  '+': (a, b) => Number(a) + Number(b),
  '-': (a, b) => Number(a) - Number(b),
  '*': (a, b) => Number(a) * Number(b),
};
let obj = {};
var calc = (str) => {
  if (obj.hasOwnProperty(str)) return obj[str];
  let p = '',
    arr = [];
  for (let i = 0; i < str.length; i++) {
    if (baseMap[str[i]] && i > 0) {
      arr.push(p);
      arr.push(str[i]);
      p = '';
    } else {
      p += str[i];
    }
  }
  arr.push(p);
  if (arr.length === 3) {
    const res = baseMap[arr[1]](arr[0], arr[2]);
    obj[str] = res;
    return res;
  }

  while (arr.indexOf('*') !== -1) {
    const idx = arr.indexOf('*');
    const t1 = arr[idx - 1];
    const t2 = arr[idx + 1];
    const key = t1 + '*' + t2;
    let res = 0;
    if (obj.hasOwnProperty(key)) {
      res = obj[key];
    } else {
      res = calc(key);
      obj[key] = res;
    }
    arr = arr
      .slice(0, idx - 1)
      .concat([res])
      .concat(arr.slice(idx + 2));
  }

  let pre = Number(arr[0]);
  for (let i = 1; i < arr.length; i = i + 2) {
    const key = pre + arr[i] + arr[i + 1];
    if (obj.hasOwnProperty(key)) {
      pre = obj[key];
    } else {
      pre = calc(key);
      obj[key] = pre;
    }
  }
  return pre;
};

var addOperators = function (num, target) {
  const len = num.length;
  if (len === 1) return Number(num) === target ? [num] : [];
  if (num[0] !== '0' && Number(num) === target) return [num];
  if (num[0] !== '0' && -Number(num) === target) return [];

  const tmp = num.split('');
  const str = Array(len * 2 - 1)
    .fill('')
    .map((v, i) => (i % 2 === 0 ? tmp[i / 2] : ' '));
  let res = [[str[0], str[0]]];

  for (let i = 1; i < len * 2 - 1; i = i + 2) {
    let tmp = [];
    for (const item of res) {
      const [pre, preOne] = item;
      const v2 = str[i + 1];
      if (preOne[0] !== '0') tmp.push([pre + v2, pre + v2]);
      tmp.push([pre + '+' + v2, v2]);
      tmp.push([pre + '-' + v2, v2]);
      tmp.push([pre + '*' + v2, v2]);
    }
    res = tmp.slice();
  }

  return res.map((v) => v[0]).filter((str) => calc(str) === target);
};
// @lc code=end
// const time1 = new Date().getTime();
// 之所以做了这么长时间，写了 4-5 个版本，记忆计算过程都无效
// 本质是因为回溯在处理 * 号优先级的时候没有处理好导致绕过去解决成本太高
// 处理 * 号只需要回溯多带一个变量存储再上一步结果即可
// Inspired by https://leetcode-cn.com/problems/expression-add-operators/solution/hui-su-fa-you-hua-chu-li-cheng-fa-by-peanutbutter/
// console.log(time1);
// console.log(addOperators('123', 6)); // ["1*2*3","1+2+3"]
// console.log(addOperators('232', 8)); // ["2*3+2","2+3*2"]
// console.log(addOperators('105', 5)); // ["1*0+5","10-5"]
// console.log(addOperators('00', 0)); // ["0*0","0+0","0-0"]
// console.log(addOperators('0', 0));
// console.log(addOperators('9', 9));
// console.log(addOperators('3456237490', 9191)); // []
// console.log(addOperators('111', 1)); // ["1*1*1", "1+1-1", "1-1+1"]
// console.log(addOperators('1000000009', 9));
// console.log(addOperators('1000000009', 9).length); // 3280
// console.log(addOperators('2147483647', 2147483647));
// console.log(addOperators('2147483647', -2147483647));
// console.log(addOperators('123456789', 45)); // 121
// console.log(addOperators('123456789', 45).length); // 121
// const time2 = new Date().getTime();
// console.log('Duration: ', time2 - time1);
