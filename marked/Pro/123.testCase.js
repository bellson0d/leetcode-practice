const targetFun = require('./123.best-time-to-buy-and-sell-stock-iii');
const IS_ARRAY_RESULT = false; // 输出是否为数组
const MULTI_ARGS = false; // 输入参数是否为多个

const testStr = [
  [[3, 3, 5, 0, 0, 3, 1, 4], 6],
  [[1, 2, 3, 4, 5], 4],
  [[0], 0],
  [[], 0],
  [[1, 2, 3], 2],
  [[7, 6, 4, 3, 2, 1], 0],
  [[1, 2, 4, 2, 5, 7, 2, 4, 9, 0], 13],
  [[6, 1, 6, 4, 3, 0, 2], 7],
  [[8, 3, 6, 2, 8, 8, 8, 4, 2, 0, 7, 2, 9, 4, 9], 15],
  [[2, 1, 2, 0, 1], 2],
];

const time1 = new Date().getTime();
console.log(time1);

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  let result;
  if (MULTI_ARGS) {
    result = targetFun(...input);
  } else {
    result = targetFun(input);
  }

  if (
    IS_ARRAY_RESULT ? result.join('|') !== output.join('|') : result !== output
  ) {
    console.log('Number ' + count + ' Case Wrong !!!');
    console.log('Input: ', input);
    console.log('Expected output: ', output);
    console.log('Output', result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
