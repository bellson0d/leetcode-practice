/*
 * @lc app=leetcode id=106 lang=javascript
 *
 * [106] Construct Binary Tree from Inorder and Postorder Traversal
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {number[]} inorder
 * @param {number[]} postorder
 * @return {TreeNode}
 */
var buildTree = function (inorder, postorder) {
  const preorder = postorder.reverse();
  const maxLen = preorder.length - 1;
  if (maxLen === 0) return new TreeNode(preorder[0]);

  var helper = (preStart, inStart, inEnd, preOr, inOr) => {
    const val = preOr[preStart];
    const root = new TreeNode(val);
    if (preStart > maxLen) return null;
    if (inStart === inEnd) return root;
    let idx = inOr.indexOf(val);

    if (idx > inStart) {
      root.left = helper(
        preStart + 1 + inEnd - idx,
        inStart,
        Math.max(idx - 1, 0),
        preOr,
        inOr
      );
    }

    if (idx < inEnd) {
      root.right = helper(
        preStart + 1,
        Math.min(idx + 1, inEnd),
        inEnd,
        preOr,
        inOr
      );
    }

    return root;
  };

  return helper(0, 0, maxLen, preorder, inorder);
};
// @lc code=end

// Inspired by https://leetcode-cn.com/problems/construct-binary-tree-from-preorder-and-inorder-traversal/solution/xiang-xi-tong-su-de-si-lu-fen-xi-duo-jie-fa-by--22/
// function TreeNode(val, left, right) {
//   this.val = val === undefined ? 0 : val;
//   this.left = left === undefined ? null : left;
//   this.right = right === undefined ? null : right;
// }

// console.log(buildTree([9, 3, 15, 20, 7], [9, 15, 7, 20, 3])); //[3,9,20,null,null,15,7]
