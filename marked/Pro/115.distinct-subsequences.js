/*
 * @lc app=leetcode id=115 lang=javascript
 *
 * [115] Distinct Subsequences
 */

// @lc code=start
/**
 * @param {string} s
 * @param {string} t
 * @return {number}
 */
var numDistinct = function (s, t) {
  const len1 = s.length,
    len2 = t.length;

  if (len2 > len1) return 0;
  if (len2 === 0) return 1;
  if (len1 === len2) return s === t ? 1 : 0;
  let res = Array(len1)
    .fill(1)
    .map((v) =>
      Array(len2)
        .fill(1)
        .map((v) => -1)
    );

  var helper = (i, j) => {
    if (j < 0) return 1;
    if (i < 0) return 0;
    if (res[i][j] !== -1) return res[i][j];

    if (s[i] === t[j]) {
      res[i][j] = helper(i - 1, j - 1) + helper(i - 1, j);
    } else {
      res[i][j] = helper(i - 1, j);
    }
    return res[i][j];
  };

  return helper(len1 - 1, len2 - 1);
};

// @lc code=end
// Inspired by https://leetcode-cn.com/problems/distinct-subsequences/solution/shou-hua-tu-jie-xiang-jie-liang-chong-ji-4r2y/
// console.log(numDistinct('rabbbit', 'rabbit')); // 3
// console.log(numDistinct('babgbag', 'bag')); // 5
// console.log(numDistinct('', '123')); // 0
// console.log(numDistinct('123', '123')); // 1
// console.log(numDistinct('213', '123')); // 0
// console.log(numDistinct('123123123', '1234')); // 0
// console.log(
//   numDistinct(
//     'aabdbaabeeadcbbdedacbbeecbabebaeeecaeabaedadcbdbcdaabebdadbbaeabdadeaabbabbecebbebcaddaacccebeaeedababedeacdeaaaeeaecbe',
//     'bddabdcae'
//   )
// ); // 10582116
