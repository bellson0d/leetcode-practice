/*
 * @lc app=leetcode id=72 lang=javascript
 *
 * [72] Edit Distance
 */

// @lc code=start
/**
 * @param {string} w1
 * @param {string} w2
 * @return {number}
 */
var minDistance = function (w1, w2) {
  const len1 = w1.length,
    len2 = w2.length;
  if (!w1) return len2;
  if (!w2) return len1;
  if (w1 === w2) return 0;

  let dp = Array(len1 + 1)
    .fill(1)
    .map((_, i) =>
      i === 0
        ? Array(len2 + 1)
            .fill(1)
            .map((__, idx) => idx)
        : [i]
    );

  let x = 1,
    y = 1;
  while (x < len1 + 1) {
    y = 1;
    while (y < len2 + 1) {
      if (w1[x - 1] === w2[y - 1]) {
        dp[x][y] = dp[x - 1][y - 1];
      } else {
        dp[x][y] = Math.min(dp[x - 1][y - 1], dp[x][y - 1], dp[x - 1][y]) + 1;
      }
      y++;
    }
    x++;
  }
  return dp[x - 1][y - 1];
};

// @lc code=end

// module.exports = minDistance;
