/*
 * @lc app=leetcode id=123 lang=javascript
 *
 * [123] Best Time to Buy and Sell Stock III
 */

// @lc code=start
/**
 * @param {number[]} prices
 * @return {number}
 */
var helper = (arr) => {
  const len = arr.length;
  let max = arr[1],
    min = max < arr[0] ? max : arr[0],
    res = Math.max(max - min, 0);

  for (let i = 2; i < len; i++) {
    const ele = arr[i];
    if (ele < min) {
      min = ele;
      max = ele;
    } else if (ele > max) {
      max = ele;
    }
    res = Math.max(max - min, res);
  }
  return res;
};

var maxProfit = function (prices) {
  const len = prices.length;
  if (len < 2) return 0;
  if (len === 3) return helper(prices);

  let max = helper(prices);
  for (let i = 2; i < len - 1; i++) {
    const res = helper(prices.slice(0, i)) + helper(prices.slice(i));
    if (res > max) max = res;
  }

  return max;
};

// @lc code=end
module.exports = maxProfit;
