/*
 * @lc app=leetcode id=282 lang=javascript
 *
 * [282] Expression Add Operators
 */

// @lc code=start
/**
 * @param {string} num
 * @param {number} target
 * @return {string[]}
 */
var baseMap = {
  '+': (a, b) => a + b,
  '-': (a, b) => a - b,
  '*': (a, b) => a * b,
};
var calc = (str, target) => {
  let p = '',
    arr = [];
  for (let i = 0; i < str.length; i++) {
    if (baseMap[str[i]]) {
      arr.push(p);
      arr.push(str[i]);
      p = '';
    } else {
      p += str[i];
    }
  }
  arr.push(p);

  while (arr.indexOf('*') !== -1) {
    const idx = arr.indexOf('*');
    arr = arr
      .slice(0, idx - 1)
      .concat([baseMap['*'](arr[idx - 1], arr[idx + 1])])
      .concat(arr.slice(idx + 2));
  }
  let pre = Number(arr[0]);
  for (let i = 1; i < arr.length; i = i + 2) {
    pre = baseMap[arr[i]](pre, Number(arr[i + 1]));
  }
  return pre === target;
};

var addOperators = function (num, target) {
  const len = num.length;
  if (len === 1) return Number(num) === target ? [num] : [];

  var helper = (num, isLeft = false) => {
    const len = num.length;
    let tmp = [];
    if (len === 1) return [[num]];
    if (!isLeft || num[0] !== '0') tmp.push([num]);
    if (len === 2) {
      tmp.push([num[0], '+', num[1]]);
      tmp.push([num[0], '-', num[1]]);
      tmp.push([num[0], '*', num[1]]);
    } else {
      const half = len - 1;
      const left = num.slice(0, half);
      const right = num.slice(half);
      const leftArr = helper(left, true);
      const rightArr = helper(right);

      leftArr.forEach((v1) =>
        rightArr.forEach((v2) => {
          const v1Last = v1[v1.length - 1];
          if (v1Last[0] !== '0')
            tmp.push([
              ...v1.slice(0, v1.length - 1),
              v1Last + v2[0],
              ...v2.slice(1),
            ]);
          if (v2[0].length === 1 || v2[0][0] !== '0') {
            tmp.push([...v1, '+', ...v2]);
            tmp.push([...v1, '-', ...v2]);
            tmp.push([...v1, '*', ...v2]);
          }
        })
      );
    }

    return tmp;
  };

  const res = helper(num, true);
  return res.filter((arr) => calc(arr, target));
};
// @lc code=end

// console.log(addOperators('123', 6)); // ["1*2*3","1+2+3"]
// console.log(addOperators('232', 8)); // ["2*3+2","2+3*2"]
// console.log(addOperators('105', 5)); // ["1*0+5","10-5"]
// console.log(addOperators('00', 0)); // ["0*0","0+0","0-0"]
// console.log(addOperators('0', 0));
// console.log(addOperators('9', 9));
// console.log(addOperators('3456237490', 9191)); // []
// console.log(addOperators('111', 1)); // ["1*1*1", "1+1-1", "1-1+1"]
// console.log(addOperators('1000000009', 9));
// console.log(addOperators('1000000009', 9).length); // 3280
// console.log(addOperators('2147483647', 2147483647));
