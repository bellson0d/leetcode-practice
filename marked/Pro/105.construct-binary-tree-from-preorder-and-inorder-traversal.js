/*
 * @lc app=leetcode id=105 lang=javascript
 *
 * [105] Construct Binary Tree from Preorder and Inorder Traversal
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {number[]} preorder
 * @param {number[]} inorder
 * @return {TreeNode}
 */

var buildTree = function (preorder, inorder) {
  const maxLen = preorder.length - 1;
  if (maxLen === 0) return new TreeNode(preorder[0]);

  var helper = (preStart, inStart, inEnd, preOr, inOr) => {
    const val = preOr[preStart];
    const root = new TreeNode(val);
    if (preStart > maxLen) return null;
    if (inStart === inEnd) return root;
    let idx = inOr.indexOf(val);

    if (idx > inStart) {
      root.left = helper(
        preStart + 1,
        inStart,
        Math.max(idx - 1, 0),
        preOr,
        inOr
      );
    }

    if (idx < inEnd) {
      root.right = helper(
        preStart + 1 + idx - inStart,
        Math.min(idx + 1, inEnd),
        inEnd,
        preOr,
        inOr
      );
    }

    return root;
  };

  return helper(0, 0, maxLen, preorder, inorder);
};
// @lc code=end
// Inspired by https://leetcode-cn.com/problems/construct-binary-tree-from-preorder-and-inorder-traversal/solution/xiang-xi-tong-su-de-si-lu-fen-xi-duo-jie-fa-by--22/
// function TreeNode(val, left, right) {
//   this.val = val === undefined ? 0 : val;
//   this.left = left === undefined ? null : left;
//   this.right = right === undefined ? null : right;
// }

// // const { getTree } = require('./utils/treeNode');
// console.log(buildTree([3, 9, 20, 15, 7], [9, 3, 15, 20, 7])); //[3,9,20,null,null,15,7]
// console.log(buildTree([1, 2], [2, 1])); //[1,2]
// console.log(buildTree([1, 2], [1, 2])); //[1,2]
// console.log(buildTree([1, 2, 3], [3, 2, 1])); //[1,2,null,3]
