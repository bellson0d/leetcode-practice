/*
 * @lc app=leetcode id=115 lang=javascript
 *
 * [115] Distinct Subsequences
 */

// @lc code=start
/**
 * @param {string} s
 * @param {string} t
 * @return {number}
 */
var numDistinct = function (str, tar) {
  let obj = {};
  const len1 = str.length,
    len2 = tar.length;

  if (len2 > len1) return 0;
  if (len2 === 0) return 1;
  if (len1 === len2) return str === tar ? 1 : 0;

  for (let i = 0; i < str.length; i++) {
    const s = str[i];
    if (obj[s]) {
      obj[s].push(i);
    } else {
      obj[s] = [i];
    }
  }

  let res = [],
    count = 0;
  while (count < len2) {
    const t = tar[count];
    if (!obj[t]) return 0;
    const tmpArr = obj[t];

    if (!res.length) {
      for (const idx of tmpArr) {
        if (len2 - count <= len1 - idx) res.push([idx]);
      }
    } else {
      let newRes = [];
      for (const arr of res) {
        const preIdx = arr[arr.length - 1];
        for (const idx of tmpArr) {
          if (idx > preIdx && len2 - count <= len1 - idx)
            newRes.push(arr.concat([idx]));
        }
      }
      res = newRes;
    }
    if (!res.length) return 0;
    count++;
  }

  return res.length;
};
// @lc code=end

// console.log(numDistinct('rabbbit', 'rabbit')); // 3
// console.log(numDistinct('babgbag', 'bag')); // 5
// console.log(numDistinct('', '123')); // 0
// console.log(numDistinct('123', '123')); // 1
// console.log(numDistinct('213', '123')); // 0
// console.log(numDistinct('123123123', '1234')); // 0
console.log(
  numDistinct(
    'aabdbaabeeadcbbdedacbbeecbabebaeeecaeabaedadcbdbcdaabebdadbbaeabdadeaabbabbecebbebcaddaacccebeaeedababedeacdeaaaeeaecbe',
    'bddabdcae'
  )
); // 10582116
