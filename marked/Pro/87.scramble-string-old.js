/*
 * @lc app=leetcode id=87 lang=javascript
 *
 * [87] Scramble String
 */

// @lc code=start
/**
 * @param {string} s1
 * @param {string} s2
 * @return {boolean}
 */
var isScramble = function (s1, s2) {
  const len = s1.length;
  if (s1 === s2) return true;
  if (len < 2) return s1 === s2;
  let obj = {};

  var helper = (s) => {
    const len = s.length;
    if (len === 1) return [s];
    if (len === 2) return [s, s[1] + s[0]];

    let res = [];
    for (let i = 1; i < len; i++) {
      const left = s.slice(0, i);
      const right = s.slice(i);
      let part1 = [],
        part2 = [];
      if (!obj.hasOwnProperty(left)) {
        obj[left] = helper(left);
      }
      part1 = obj[left];
      if (!obj.hasOwnProperty(right)) {
        obj[right] = helper(right);
      }
      part2 = obj[right];

      part1.forEach((v1) => {
        part2.forEach((v2) => {
          res.push(v1 + v2);
          res.push(v2 + v1);
        });
      });
    }
    return res;
  };

  return helper(s1).includes(s2);
};

// @lc code=end
module.exports = isScramble;
// 暴力超时
// console.log(isScramble('abc', 'cba')); // true
// console.log(isScramble('great', 'rgtae')); // true
// console.log(isScramble('eat', 'tae')); // true
// console.log(isScramble('abcd', 'acbd')); // true
