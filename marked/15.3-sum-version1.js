/*
 * @lc app=leetcode id=15 lang=javascript
 *
 * [15] 3Sum
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number[][]}
 */
const { longArr } = require('./15.helpFun');

function findTwo(n, arr) {
  let tmpArr = [];

  for (let i = 0; i < arr.length - 1; i++) {
    const ele = arr[i];

    if (ele)
      for (let j = i + 1; j < arr.length; j++) {
        const ele2 = arr[j];

        if (
          -n === ele + ele2 &&
          !tmpArr.find((v) => v[1] === ele && v[2] === ele2)
        ) {
          tmpArr.push([n, ele, ele2]);
        }
      }
  }

  return tmpArr;
}

var threeSum = function (nums) {
  let result = [];
  let positives = [];
  let zeros = [];
  let negatives = [];
  let empty = [];
  let empty2 = [];
  nums.sort((a, b) => a - b);

  for (let i = 0; i < nums.length; i++) {
    const ele = nums[i];

    const hasEle = empty.find((v) => v === ele);
    if (hasEle && ele !== 0) {
      const hasEle2 = empty2.find((v) => v === ele);
      if (hasEle2) continue;
      empty2.push(ele);
    } else {
      empty.push(ele);
    }

    if (ele > 0) {
      positives.push(ele);
    } else if (ele < 0) {
      negatives.push(ele);
    } else {
      if (zeros.length < 3) zeros.push(ele);
    }
  }

  const zLen = zeros.length;
  if (zLen === 3) result.push([0, 0, 0]);

  if (positives.length === 0 || negatives.length === 0) return result;

  if (zLen > 0) {
    let tmpArr = [];

    for (let i = 0; i < positives.length; i++) {
      const ele = positives[i];

      for (let j = 0; j < negatives.length; j++) {
        const lastIdx = tmpArr.length - 1;
        const ele2 = negatives[j];

        if (ele === -ele2 && (!tmpArr[lastIdx] || tmpArr[lastIdx][2] !== ele2))
          tmpArr.push([ele, 0, ele2]);
      }
    }

    result = result.concat(tmpArr);
  }

  let p = [];
  for (let i = 0; i < positives.length; i++) {
    const ele = positives[i];
    if (!p.includes(ele)) {
      p.push(ele);
      const tempArr = findTwo(ele, negatives);
      if (tempArr.length > 0) {
        result = result.concat(tempArr);
      }
    }
  }

  let n = [];
  for (let i = 0; i < negatives.length; i++) {
    const ele = negatives[i];
    if (!n.includes(ele)) {
      n.push(ele);
      const tempArr = findTwo(ele, positives);
      if (tempArr.length > 0) {
        result = result.concat(tempArr);
      }
    }
  }

  return result;
};

console.log(threeSum(longArr));

module.exports = threeSum;
// @lc code=end
