/*
 * @lc app=leetcode id=416 lang=javascript
 *
 * [416] Partition Equal Subset Sum
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {boolean}
 */
var canPartition = function (nums) {
  const sum = nums.reduce((a, b) => a + b, 0);
  if (sum % 2 !== 0) return false;
  const target = sum / 2;
  const len = nums.length;
  if (len === 2) return nums[0] === nums[1];

  let obj = {};
  var dfs = (preSum, idx) => {
    const key = preSum + '-' + idx;
    if (obj.hasOwnProperty(key)) return obj[key];
    if (preSum > target || idx > len - 1) return false;
    const ele = nums[idx];
    const newSum = preSum + ele;
    if (newSum === target) return true;

    idx++;
    const d = dfs(newSum, idx) || dfs(preSum, idx);
    obj[key] = d;
    return d;
  };

  return dfs(0, 0);
};
// @lc code=end

// console.log(canPartition([1, 5, 11, 5])); // true
// console.log(canPartition([1, 5, 11, 5, 1])); // false
// console.log(canPartition([1, 2, 3, 4, 5])); // false
// console.log(canPartition([1, 2, 3, 4])); // true
// console.log(canPartition([1, 2, 3, 5])); // false
// console.log(canPartition([2, 2, 2, 2, 10])); // false
// console.log(canPartition([2, 2, 2, 2, 2, 8])); // false
// console.log(canPartition([3, 3, 3, 4, 5])); // true
