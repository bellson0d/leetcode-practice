/*
 * @lc app=leetcode id=74 lang=javascript
 *
 * [74] Search a 2D Matrix
 */

// @lc code=start
/**
 * @param {number[][]} matrix
 * @param {number} target
 * @return {boolean}
 */
var searchMatrix = function (matrix, target) {
  const len = matrix.length;
  if (len === 0) return false;
  if (len === 1) return matrix[0].includes(target);

  let x = len - 1;

  while (x > 0) {
    const ele = matrix[x][0];
    const ele2 = matrix[x - 1][0];

    if (ele === target || ele2 === target) return true;

    if (ele < target) {
      return matrix[x].includes(target);
    }
    if (ele2 < target) {
      return matrix[x - 1].includes(target);
    }
    if (x > 2) x--;
    x--;
  }

  return false;
};
// @lc code=end
// console.log(
//   searchMatrix(
//     [
//       [1, 3, 5, 7],
//       [8, 9, 10, 11],
//       [12, 13, 14, 15],
//     ],
//     3
//   )
// );
