/*
 * @lc app=leetcode id=190 lang=javascript
 *
 * [190] Reverse Bits
 */

// @lc code=start
/**
 * @param {number} n - a positive integer
 * @return {number} - a positive integer
 */
var reverseBits = function (n) {
  let s = 0;
  for (let i = 31; i >= 0; i--) {
    if (i) {
      s += (n & 1) << (i - 1);
    } else {
      s *= 2;
      s += n & 1;
    }
    n >>= 1;
  }
  return s;
};
// @lc code=end.
// module.exports = reverseBits;
// reverseBits(parseInt('11111111111111111111111111111101', 2));
// ref: https://www.w3school.com.cn/js/js_bitwise.asp
// ref: https://blog.csdn.net/weixin_41806099/article/details/96421896
