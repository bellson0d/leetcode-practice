/*
 * @lc app=leetcode id=357 lang=javascript
 *
 * [357] Count Numbers with Unique Digits
 */

// @lc code=start
/**
 * @param {number} n
 * @return {number}
 */
var isValid = (str) => {
  let set = new Set();
  for (const s of str) {
    if (set.has(s)) return false;
    set.add(s);
  }
  return true;
};
var countNumbersWithUniqueDigits = function (n) {
  let i = 0,
    count = 0;
  while (i < 10 ** n) {
    if (isValid(String(i))) {
      // console.log(i);
      count++;
    }
    i++;
  }
  return count;
};
// @lc code=end
console.log(countNumbersWithUniqueDigits(0));
console.log(countNumbersWithUniqueDigits(1));
console.log(countNumbersWithUniqueDigits(2));
console.log(countNumbersWithUniqueDigits(3));
console.log(countNumbersWithUniqueDigits(4));
console.log(countNumbersWithUniqueDigits(5));
// console.log(countNumbersWithUniqueDigits(6));
// console.log(countNumbersWithUniqueDigits(7));
// console.log(countNumbersWithUniqueDigits(8));
