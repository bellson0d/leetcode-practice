/*
 * @lc app=leetcode id=216 lang=javascript
 *
 * [216] Combination Sum III
 */

// @lc code=start
/**
 * @param {number} k
 * @param {number} n
 * @return {number[][]}
 */
const baseArr = [1, 2, 3, 4, 5, 6, 7, 8, 9];

var helper = (arr) =>
    arr.map((v) => {
        let filterArr = baseArr.slice();
        v.forEach((v) => {
            filterArr.splice(filterArr.indexOf(v), 1);
        });
        return filterArr;
    });

var combinationSum3 = function (k, n) {
    if (n > 45) return [];
    if (k === 1 && n > 9) return [];
    if (k === 2 && (n > 17 || n < 3)) return [];
    if (k === 3 && (n > 24 || n < 6)) return [];
    if (k === 4 && (n > 30 || n < 10)) return [];
    if (k === 5 && (n > 35 || n < 15)) return [];

    let flag = true;
    if (k > 5) {
        k = 9 - k;
        n = 45 - n;
        flag = false;
    }

    let count = 0,
        tmpArr = [{ sum: 0, arr: baseArr, res: [] }];
    while (count < k) {
        let tmp = [];
        while (tmpArr.length) {
            const { sum, arr, res } = tmpArr.shift();
            for (let i = 0; i < arr.length; i++) {
                const ele = arr[i];
                if (sum + ele <= n) {
                    tmp.push({
                        sum: sum + ele,
                        arr: arr.slice(0, i).concat(arr.slice(i + 1)),
                        res: res.concat([ele]),
                    });
                }
            }
        }
        tmpArr = tmp;
        count++;
    }

    let finalArr = Array.from(
        new Set(
            tmpArr.filter((v) => v.sum === n).map((v) => v.res.sort().join(""))
        )
    ).map((v) => v.split("").map(Number));

    return flag ? finalArr : helper(finalArr);
};
// @lc code=end

// console.log(combinationSum3(3, 7)); // [[1,2,4]]
// console.log(combinationSum3(3, 9)); // [[1,2,6],[1,3,5],[2,3,4]]
// console.log(combinationSum3(4, 1)); // []
// console.log(combinationSum3(3, 2)); // []
// console.log(combinationSum3(9, 45)); // [[1,2,3,4,5,6,7,8,9]]
