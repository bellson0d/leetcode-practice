/*
 * @lc app=leetcode id=76 lang=javascript
 *
 * [76] Minimum Window Substring
 */

// @lc code=start
/**
 * @param {string} s
 * @param {string} t
 * @return {string}
 */
var arrToObj = (arr) => {
  let i = 0,
    obj = {};
  while (i < arr.length) {
    if (obj[arr[i]]) {
      obj[arr[i]]++;
    } else {
      obj[arr[i]] = 1;
    }
    i++;
  }
  return obj;
};

var minWindow = function (s, t) {
  if (!s || !t || s.length < t.length) return '';
  if (s.includes(t)) return t;
  const sLen = s.length;
  const tLen = t.length;
  let max = '',
    count = tLen,
    obj = arrToObj(t),
    lIdx = 0,
    rIdx = 0;

  while (rIdx < sLen) {
    const ele = s[rIdx];
    if (obj.hasOwnProperty(ele)) {
      if (obj[ele] > 0) {
        count--;
      }
      obj[ele]--;
      while (count === 0) {
        max =
          !max || max.length > rIdx - lIdx + 1 ? s.slice(lIdx, rIdx + 1) : max;

        const ele2 = s[lIdx];
        if (obj.hasOwnProperty(ele2)) {
          if (obj[ele2] === 0) {
            count++;
          }
          obj[ele2]++;
        }
        lIdx++;
      }
    }
    rIdx++;
  }
  return max;
};
// @lc code=end
// Inspired by https://zhuanlan.zhihu.com/p/96974928
// module.exports = minWindow;
// console.log(minWindow('ABABABCCC', 'ABBC'));
// console.log(minWindow('ADOBECODEBANC', 'ABC'));
