const targetFun = require("./218.the-skyline-problem");
const IS_ARRAY_RESULT = true; // 输出是否为数组
const MULTI_ARGS = false; // 输入参数是否为多个

const testStr = [
    [
        [
            [2, 9, 10],
            [3, 7, 15],
            [5, 12, 12],
            [15, 20, 10],
            [19, 24, 8],
        ],
        [
            [2, 10],
            [3, 15],
            [7, 12],
            [12, 0],
            [15, 10],
            [20, 8],
            [24, 0],
        ],
    ],
    [
        [
            [0, 2, 3],
            [2, 5, 3],
        ],
        [
            [0, 3],
            [5, 0],
        ],
    ],
    [
        [
            [1, 2, 1],
            [2147483646, 2147483647, 2147483647],
        ],
        [
            [1, 1],
            [2, 0],
            [2147483646, 2147483647],
            [2147483647, 0],
        ],
    ],
];

const time1 = new Date().getTime();
console.log(time1);

let count = 0;
while (count < testStr.length) {
    const [input, output] = testStr[count];
    let result;
    if (MULTI_ARGS) {
        result = targetFun(...input);
    } else {
        result = targetFun(input);
    }

    if (
        IS_ARRAY_RESULT
            ? result.join("|") !== output.join("|")
            : result !== output
    ) {
        console.log("Number " + count + " Case Wrong !!!");
        console.log("Input: ", input);
        console.log("Expected output: ", output);
        console.log("Output", result);
        return;
    }
    count++;
}
console.log("All pass!");

const time2 = new Date().getTime();
console.log("Duration: ", time2 - time1);
