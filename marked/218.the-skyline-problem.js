/*
 * @lc app=leetcode id=218 lang=javascript
 *
 * [218] The Skyline Problem
 */

// @lc code=start
/**
 * @param {number[][]} buildings
 * @return {number[][]}
 */
var filterB = (idx, buildings) => {
    for (let j = 0; j < buildings.length; j++) {
        const building = buildings[j];
        if (building[1] >= idx) {
            return buildings.slice(j);
        }
    }
    return [];
};

var findB = (idx, buildings) => {
    let res = [];
    for (let j = 0; j < buildings.length; j++) {
        const building = buildings[j];
        if (building[1] > idx && building[0] <= idx) {
            res.push(building);
        } else if(building[0] > idx){
            break;
        }
    }
    return res;
};

var getSkyline = function (buildings) {
    const len = buildings.length;
    if (len === 1)
        return [
            [buildings[0][0], buildings[0][2]],
            [buildings[0][1], 0],
        ];

    const steps = Array.from(
        new Set(buildings.map((v) => v.slice(0, 2)).flat())
    ).sort((a, b) => a - b);
    let res = [],
        tmpMax = 0;

    for (let i = 0; i < steps.length; i++) {
        const idx = steps[i];

        buildings = filterB(idx, buildings);
        const b = findB(idx, buildings);

        if (b.length) {
            const max = Math.max(...b.map((v) => v[2]));
            if (max !== tmpMax) {
                res.push([idx, max]);
                tmpMax = max;
            }
        } else if (tmpMax !== 0) {
            res.push([idx, 0]);
            tmpMax = 0;
        }
    }

    return res;
};
// @lc code=end
module.exports = getSkyline;
// console.log(
//     getSkyline([
//         [2, 9, 10],
//         [3, 7, 15],
//         [5, 12, 12],
//         [15, 20, 10],
//         [19, 24, 8],
//     ])
// );

// console.log(
//     getSkyline([[1,2,1],[2147483646,2147483647,2147483647]])
// );
