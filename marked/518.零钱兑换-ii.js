/*
 * @lc app=leetcode.cn id=518 lang=javascript
 *
 * [518] 零钱兑换 II
 */

// @lc code=start
/**
 * @param {number} amount
 * @param {number[]} coins
 * @return {number}
 */
var change = function (amount, coins) {
  const dp = Array(amount + 1).fill(0);
  dp[0] = 1;

  for (const c of coins) {
    for (let i = c; i <= amount; i++) {
      dp[i] += dp[i - c];
    }
  }

  return dp[amount];
};
// @lc code=end
// Inspired by: https://leetcode-cn.com/problems/coin-change-2/solution/ling-qian-dui-huan-ii-by-leetcode-soluti-f7uh/
// 爬梯子变形，然而自己的解法还是太蠢
// console.log(change(5, [1, 2, 5])); // 4
// console.log(change(3, [2])); // 0
// console.log(change(10, [10])); // 1
// console.log(change(4, [2])); // 1
// console.log(change(1, [2])); // 0
// console.log(change(0, [7])); // 1
// console.log(change(50, [1, 2, 5])); // 146
// console.log(change(500, [1, 2, 5])); // 12701
