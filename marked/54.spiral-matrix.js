/*
 * @lc app=leetcode id=54 lang=javascript
 *
 * [54] Spiral Matrix
 */

// @lc code=start
/**
 * @param {number[][]} matrix
 * @return {number[]}
 */
var spiralOrder = function (matrix) {
  const yLen = matrix.length;
  if (yLen === 0) return [];
  if (yLen === 1) return matrix[0];

  const xLen = matrix[0].length;
  const totalEle = xLen * yLen;

  if (xLen === 1) return matrix.map((v) => v[0]);

  let count = 0,
    i = 0,
    j = 0,
    result = [matrix[i][j]],
    currentDirection = 0,
    addCount = 0;

  while (count < totalEle - 1) {
    if (currentDirection === 0) {
      result.push(matrix[i][++j]);
      if (j === xLen - 1 - addCount)
        currentDirection = (currentDirection + 1) % 4;
    } else if (currentDirection === 1) {
      result.push(matrix[++i][j]);
      if (i === yLen - 1 - addCount)
        currentDirection = (currentDirection + 1) % 4;
    } else if (currentDirection === 2) {
      result.push(matrix[i][--j]);
      if (j === addCount) {
        currentDirection = (currentDirection + 1) % 4;
        addCount++;
      }
    } else if (currentDirection === 3) {
      result.push(matrix[--i][j]);
      if (i === addCount) currentDirection = (currentDirection + 1) % 4;
    }

    count++;
  }

  return result;
};
// @lc code=end
// module.exports = spiralOrder;
// spiralOrder([
//   [1, 2, 3],
//   [4, 5, 6],
//   [7, 8, 9],
// ]);
