/*
 * @lc app=leetcode id=187 lang=javascript
 *
 * [187] Repeated DNA Sequences
 */

// @lc code=start
/**
 * @param {string} s
 * @return {string[]}
 */
var findRepeatedDnaSequences = function (s) {
  const len = s.length,
    arr = [],
    obj = {};
  if (len < 11) return arr;
  for (let i = 0; i <= len - 10; i++) {
    const ele = s.slice(i, i + 10);
    if (obj.hasOwnProperty(ele)) {
      obj[ele]++;
    } else {
      obj[ele] = 0;
    }
  }
  Object.keys(obj).forEach((v) => {
    if (obj[v]) arr.push(v);
  });
  return arr;
};
// @lc code=end
// module.exports = findRepeatedDnaSequences;
// More efficient https://zhuanlan.zhihu.com/p/95244199
