const targetFun = require('./85.maximal-rectangle');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  [[[]], 0],
  [[['0']], 0],
  [[['1']], 1],
  [[['1', '1']], 2],
  [
    [
      ['1', '1'],
      ['1', '1'],
    ],
    4,
  ],
  [
    [
      ['1', '1'],
      ['0', '1'],
    ],
    2,
  ],
  [
    [
      ['1', '0'],
      ['0', '1'],
    ],
    1,
  ],
  [
    [
      ['1', '0'],
      ['0', '0'],
    ],
    1,
  ],
  [
    [
      ['1', '0', '1', '0', '0'],
      ['1', '0', '1', '1', '1'],
      ['1', '1', '1', '1', '1'],
      ['1', '0', '0', '1', '0'],
    ],
    6,
  ],
  [
    [
      ['1', '0', '1', '0', '0'],
      ['1', '0', '1', '0', '1'],
      ['1', '1', '1', '1', '1'],
      ['1', '0', '0', '1', '0'],
    ],
    5,
  ],
  [
    [
      ['1', '0', '1', '0', '0'],
      ['1', '1', '1', '0', '1'],
      ['1', '1', '1', '1', '1'],
      ['1', '1', '1', '1', '0'],
    ],
    9,
  ],
  [
    [
      ['1', '0', '1', '0', '0'],
      ['1', '1', '1', '1', '1'],
      ['0', '1', '1', '1', '0'],
      ['1', '1', '1', '1', '0'],
      ['1', '1', '0', '1', '0'],
    ],
    9,
  ],
];

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input[0], input[1]);
  if (result !== output) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
