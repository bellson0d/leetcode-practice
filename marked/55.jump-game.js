/*
 * @lc app=leetcode id=55 lang=javascript
 *
 * [55] Jump Game
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {boolean}
 */

var canJump = function (nums) {
  const len = nums.length;
  let currentEle = nums[0];
  if (len === 1) return true;
  if (len === 0 || currentEle === 0) return false;

  if (currentEle >= len - 1) return true;

  let idx = 0;
  while (idx < len - 1) {
    if (currentEle === 1) {
      idx++;
      currentEle = nums[idx];
    } else {
      let tmpIdx = idx,
        tmpVal = currentEle;

      for (let i = 1; i <= currentEle; i++) {
        if (nums[idx + i] + i > tmpVal) {
          tmpIdx = idx + i;
          tmpVal = nums[tmpIdx] + i;
        }
      }

      if (tmpIdx === idx) return false;
      idx = tmpIdx;
      currentEle = nums[tmpIdx];
    }
    if (idx + currentEle >= len - 1) return true;
    if (currentEle === 0) return false;
  }

  return idx >= len - 1;
};

// @lc code=end
module.exports = canJump;
// console.log(canJump([2, 3, 1, 1, 4]));
// console.log(canJump([4, 0, 2, 2, 2, 1, 0, 1, 4, 2, 1, 0]));
// console.log(canJump([5, 9, 3, 2, 1, 0, 2, 3, 3, 1, 0, 0]));
