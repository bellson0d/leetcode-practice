/*
 * @lc app=leetcode id=97 lang=javascript
 *
 * [97] Interleaving String
 */

// @lc code=start
/**
 * @param {string} s1
 * @param {string} s2
 * @param {string} s3
 * @return {boolean}
 */
var isInterleave = function (s1, s2, s3) {
  if (!s1) return s3 === s2;
  if (!s2) return s3 === s1;

  const len1 = s1.length;
  const len2 = s2.length;
  const len3 = s3.length;
  if (len1 + len2 !== len3) return false;

  let tmpArr = Array(len1);

  for (let i = 0; i < len1 + 1; i++) {
    for (let j = 0; j < len2 + 1; j++) {
      if (!tmpArr[i]) {
        tmpArr[i] = [];
        if (i === 0) {
          tmpArr[0][0] = true;
          continue;
        }
      }

      if (s3[i + j - 1] === s2[j - 1] && (j === 0 || tmpArr[i][j - 1])) {
        tmpArr[i][j] = true;
      } else if (s3[i + j - 1] === s1[i - 1] && (i === 0 || tmpArr[i - 1][j])) {
        tmpArr[i][j] = true;
      } else {
        tmpArr[i][j] = false;
      }
    }
  }

  return tmpArr[len1][len2];
};
// @lc code=end
// module.exports = isInterleave;

// console.log(isInterleave('aabcc', 'dbbca', 'aadbbcbcac'));
// console.log(isInterleave('aabcc', 'dbbca', 'aadbbbaccc'));
