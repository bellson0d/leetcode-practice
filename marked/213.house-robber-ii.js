/*
 * @lc app=leetcode id=213 lang=javascript
 *
 * [213] House Robber II
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var helper = function (nums) {
    const len = nums.length;
    let maxArr = Array(len).fill(false);
    let preArr = Array(len).fill(false);

    let pre = 0,
        max = nums[0];
    maxArr[0] = true;

    for (let i = 1; i < len; i++) {
        if (maxArr[i - 1]) {
            if (nums[i] + pre > max) {
                const tmp = pre;
                pre = max;
                max = nums[i] + tmp;

                preArr[i] = true;
                const tmpArr = maxArr.slice();
                maxArr = preArr.slice();
                preArr = tmpArr;
            }
        } else {
            pre = max;
            preArr = maxArr.slice();
            max += nums[i];
            maxArr[i] = true;
        }
    }

    return max;
};

var rob = function (nums) {
    const len = nums.length;
    if (len < 4) return Math.max(...nums);
    if (len === 4) return Math.max(nums[0] + nums[2], nums[1] + nums[3]);
    // Trick!!
    return Math.max(helper(nums.slice(1)), helper(nums.slice(0, len - 1)));
};
// @lc code=end

// console.log(rob([2, 3, 2])); // 3
// console.log(rob([1, 2, 3, 1])); // 4
// console.log(rob([0])); // 0
// console.log(rob([1, 2, 3, 1, 4, 2, 1, 1, 3, 4])); // 12
// console.log(rob([1, 2, 1, 1])); // 3
// console.log(rob([1, 1, 1, 2])); // 3
// console.log(rob([1,1,3,6,7,10,7,1,8,5,9,1,4,4,3])); // 41
// console.log(rob([6, 6, 4, 8, 4, 3, 3, 10])); // 27
// console.log(rob([2, 2, 4, 3, 2, 5])); // 10
