/*
 * @lc app=leetcode id=264 lang=javascript
 *
 * [264] Ugly Number II
 */

// @lc code=start
/**
 * @param {number} n
 * @return {number}
 */

var nthUglyNumber = function (n) {
    // let count = 1,
    //     obj = { 1: 1 },
    //     tmpArr = [1],
    //     num = 2;

    // while (count < n) {
    //     if (obj[num / 2]) {
    //         obj[num] = num;
    //         tmpArr.push(num);
    //         count++;
    //     } else if (obj[num / 3]) {
    //         obj[num] = num;
    //         tmpArr.push(num);
    //         count++;
    //     } else if (obj[num / 5]) {
    //         obj[num] = num;
    //         tmpArr.push(num);
    //         count++;
    //     }
    //     num++;
    // }

    // return tmpArr[tmpArr.length - 1];

    var dp = [1];
    var t2 = 0;
    var t3 = 0;
    var t5 = 0;

    for (var i = 1; i < n; i++) {
        dp[i] = Math.min(dp[t2] * 2, dp[t3] * 3, dp[t5] * 5);
        if (dp[i] === dp[t2] * 2) t2++;
        if (dp[i] === dp[t3] * 3) t3++;
        if (dp[i] === dp[t5] * 5) t5++;
    }

    return dp[n - 1];
};
// @lc code=end
// https://baffinlee.com/leetcode-javascript/problem/ugly-number-ii.html
// 参考
// console.log(nthUglyNumber(27)); // 64
// console.log(nthUglyNumber(1690));
// console.log(nthUglyNumber(1000));
// console.log(nthUglyNumber(100));
// console.log(nthUglyNumber(10));
// console.log(nthUglyNumber(11));
// console.log(nthUglyNumber(12));
// console.log(nthUglyNumber(1))
// console.log(nthUglyNumber(2))
// console.log(nthUglyNumber(3))
// console.log(nthUglyNumber(4))
// console.log(nthUglyNumber(5))
// console.log(nthUglyNumber(6))
