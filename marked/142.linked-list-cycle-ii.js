/*
 * @lc app=leetcode id=142 lang=javascript
 *
 * [142] Linked List Cycle II
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */

/**
 * @param {ListNode} head
 * @return {ListNode}
 */
var detectCycle = function (head) {
  if (!head || !head.next) return null;

  let step1 = head,
    step2 = head,
    tmp = [];

  while (step1) {
    if (!step1 || !step2) return null;
    if (tmp.includes(step1)) {
      return step1;
    } else {
      tmp.push(step1);
    }

    step1 = step1.next;
    step2 = step2.next ? step2.next.next : step2.next;
    // if (step1 === step2) {
    //   return tmp[0];
    // }
  }

  return null;
};
// @lc code=end
// const { getList, logList } = require('./utils/listNode');
// console.log(
//   detectCycle(getList([3, 2, 0, -4], { isCycle: true, cycleIdx: 1 }))
// );
