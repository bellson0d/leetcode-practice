/*
 * @lc app=leetcode id=306 lang=javascript
 *
 * [306] Additive Number
 */

// @lc code=start
/**
 * @param {string} num
 * @return {boolean}
 */
var reg = /^0+[1-9]/;
var isAdditiveNumber = function (num) {
  const len = num.length;
  if (len < 3) return false;
  let count = 1;
  while (count * 2 <= len - 1) {
    let firstNum = num.slice(0, count);
    if (!reg.test(firstNum)) {
      let count2 = 1;

      while (len - 1 >= 2 * Math.max(count, count2)) {
        let second = num.slice(count, count + count2);
        if (!reg.test(second)) {
          let res = num.slice(count + count2);
          let first = firstNum;

          while (res.length) {
            let sum = +first + +second;
            if (res.indexOf(sum) === 0) {
              res = res.slice(String(sum).length);
              first = second;
              second = sum;
              if (res.length === 0) {
                return true;
              }
            } else {
              break;
            }
          }
        }
        count2++;
      }
    }

    count++;
  }
  return false;
};
// @lc code=end

// console.log(isAdditiveNumber('112132538')); // true
// console.log(isAdditiveNumber('112132244')); // true
// console.log(isAdditiveNumber('112334')); // true
// console.log(isAdditiveNumber('112358')); // true
// console.log(isAdditiveNumber('11235812359')); // true
// console.log(isAdditiveNumber('199100199')); // true
// console.log(isAdditiveNumber('1100101')); // true
// console.log(isAdditiveNumber('1100111001')); // false
// console.log(isAdditiveNumber('110011002')); // true
// console.log(isAdditiveNumber('1100111111')); // true
// console.log(isAdditiveNumber('110111122')); // true
// console.log(isAdditiveNumber('000')); // true
// console.log(isAdditiveNumber('0000000000000')); // true
// console.log(isAdditiveNumber('0235813')); // false
// console.log(isAdditiveNumber('199001200')); // false
// console.log(isAdditiveNumber('11111111111011111111111')); // true
