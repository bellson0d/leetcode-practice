/*
 * @lc app=leetcode id=322 lang=javascript
 *
 * [322] Coin Change
 */

// @lc code=start
/**
 * @param {number[]} coins
 * @param {number} amount
 * @return {number}
 */
var coinChange = function (co, amount) {
  if (!amount) return 0;
  const len = co.length;
  if (len === 1) return amount % co[0] === 0 ? amount / co[0] : -1;
  co.sort((a, b) => a - b);
  let obj = {},
    coins = [];
  for (const c of co) {
    if (!obj[c]) {
      obj[c] = 1;
      if (c <= amount) coins.push(c);
    }
  }
  if (amount < coins[0]) return -1;

  let max = coins[coins.length - 1],
    flag = false;
  while (!flag) {
    let arr = Object.keys(obj).map(Number);
    let p1 = arr.length - 1,
      p2 = p1;
    if (max > amount) {
      flag = true;
      let p = arr.length - 1;
      while (p >= 0) {
        if (arr[p] === amount) return obj[amount];
        if (arr[p] < amount) {
          p1 = p;
          p2 = p1;
          break;
        }
        p--;
      }
    }

    while (p2 >= 0) {
      let sum = arr[p1] + arr[p2];
      if (!obj[sum]) {
        if (sum > max) max = sum;
        obj[sum] = obj[arr[p1]] + obj[arr[p2]];
      }

      if (p1 !== 0) {
        p1--;
      } else {
        p2--;
        p1 = p2;
      }
    }
  }

  return obj[amount] || -1;
};
// @lc code=end

// console.log(coinChange([1, 2, 5], 11)); // 3
// console.log(coinChange([1, 2, 5], 19)); // 5
// console.log(coinChange([2], 3)); // -1
// console.log(coinChange([1], 0)); // 0
// console.log(coinChange([1], 1)); // 1
// console.log(coinChange([1], 2)); // 2
// console.log(coinChange([2, 17], 88)); // 44
// console.log(coinChange([2], 11)); // -1
// console.log(coinChange([474, 83, 404, 3], 11)); // 8
console.log(coinChange([474, 83, 404, 3], 264)); // 8
console.log(coinChange([281, 20, 251, 251], 7323)); // 66
