/*
 * @lc app=leetcode id=300 lang=javascript
 *
 * [300] Longest Increasing Subsequence
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var lengthOfLIS = function (nums) {
  //   const len = nums.length;
  //   if (len === 1) return 1;

  //   let min = nums[0],
  //     arr = [[nums[0]]],
  //     max = nums[0];

  //   for (let i = 1; i < len; i++) {
  //     const ele = nums[i];

  //     if (ele < min) {
  //       min = ele;
  //       arr.push([ele]);
  //     } else if (ele > max) {
  //       max = ele;
  //       arr.forEach((v) => v.unshift(ele));
  //     } else {
  //       let flag = false,
  //         tmp = [];
  //       arr.forEach((v) => {
  //         if (ele > v[0]) {
  //           flag = true;
  //           v.unshift(ele);
  //         }
  //       });

  //       arr.forEach((v) => {
  //         const res = v.filter((v) => v < ele);
  //         if (res.length > tmp.length || res[0] < tmp[0]) {
  //           tmp = [ele].concat(res);
  //         }
  //       });

  //       if (tmp.length) arr.push(tmp);
  //     }
  //   }

  //   return Math.max(...arr.map((v) => v.length));

  var max = 0,
    dp = [],
    i,
    j,
    curMax;
  for (i = 0; i < nums.length; i++) {
    curMax = 0;
    for (j = 0; j < i; j++) {
      if (nums[i] > nums[j] && dp[j] > curMax) {
        curMax = dp[j];
      }
    }
    curMax++;
    if (curMax > max) {
      max = curMax;
    }
    dp[i] = curMax;
  }
  return max;
};
// @lc code=end

// console.log(lengthOfLIS([10, 9, 2, 5, 3, 7, 101, 18]));
// console.log(lengthOfLIS([0, 1, 0, 3, 2, 3]));
// console.log(lengthOfLIS([7, 7, 7, 7, 7, 7, 7]));
// console.log(lengthOfLIS([6, 7, 8, 1, 2, 3, 4]));
// console.log(lengthOfLIS([6, 7, 8, 9, 1, 2, 3, 1]));
// console.log(lengthOfLIS([5, 7, -24, 12, 13, 2, 3, 12, 5, 6, 35])); // 6
console.log(lengthOfLIS([5, 7, -24, 1, 12, 13, 2, 3, 12, 5, 6, 35])); // 7
// console.log(
//   lengthOfLIS([
//     5, 7, -24, 1, 12, 13, 2, 3, 12, 5, 6, 35, -111, 1, 2, 3, 4, 5, 6, 7,
//   ])
// ); // 8
// console.log(lengthOfLIS([3, 5, 6, 2, 5, 4, 19, 5, 6, 7, 12])); // 6
// console.log(
//   lengthOfLIS([
//     -813, 82, -728, -82, -432, 887, -551, 324, -315, 306, -164, -499, -873,
//     -613, 932, 177, 61, 52, 1000, -710, 372, -306, -584, -332, -500, 407, 399,
//     -648, 290, -866, 222, 562, 993, -338, -590, 303, -16, -134, 226, -648, 909,
//     582, 177, 899, -343, 55, 629, 248, 333, 1, -921, 143, 629, 981, -435, 681,
//     844, 349, 613, 457, 797, 695, 485, 15, 710, -450, -775, 961, -445, -905,
//     466, 942, 995, -289, -397, 434, -14, 34, -903, 314, 862, -441, 507, -966,
//     525, 624, -706, 39, 152, 536, 874, -364, 747, -35, 446, -608, -554, -411,
//     987, -354, -700, -34, 395, -977, 544, -330, 596, 335, -612, 28, 586, 228,
//     -664, -841, -999, -100, -620, 718, 489, 346, 450, 772, 941, 952, -560, 58,
//     999, -879, 396, -101, 897, -1000, -566, -296, -555, 938, 941, 475, -260,
//     -52, 193, 379, 866, 226, -611, -177, 507, 910, -594, -856, 156, 71, -946,
//     -660, -716, -295, -927, 148, 620, 201, 706, 570, -659, 174, 637, -293, 736,
//     -735, 377, -687, -962, 768, 430, 576, 160, 577, -329, 175, 51, 699, -113,
//     950, -364, 383, 5, 748, -250, -644, -576, -227, 603, 832, -483, -237, 235,
//     893, -336, 452, -526, 372, -418, 356, 325, -180, 134, -698,
//   ])
// ); // 25
