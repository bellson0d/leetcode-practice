/*
 * @lc app=leetcode id=201 lang=javascript
 *
 * [201] Bitwise AND of Numbers Range
 */

// @lc code=start
/**
 * @param {number} left
 * @param {number} right
 * @return {number}
 */

const rangeBitwiseAnd = function (left, right) {
    if (left === 0) return 0;
    if (left === right) return right;

    const s1 = right.toString(2);
    const len = s1.length;
    let s2 = left.toString(2);
    s2 = s2.padStart(len, "0");
    let newS = "";
    for (let i = 0; i < len; i++) {
        const ele1 = s1[i];
        const ele2 = s2[i];
        if (ele1 === ele2) {
            newS += ele1;
        } else {
            newS = newS.padEnd(len, "0");
            break;
        }
    }

    return parseInt(newS, 2);
};

// @lc code=end
// module.exports = rangeBitwiseAnd;
