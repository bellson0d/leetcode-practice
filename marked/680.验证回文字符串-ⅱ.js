/*
 * @lc app=leetcode.cn id=680 lang=javascript
 *
 * [680] 验证回文字符串 Ⅱ
 */

// @lc code=start
/**
 * @param {string} s
 * @return {boolean}
 */
var helper = (s, isLeft = true) => {
  let p1 = 0,
    p2 = s.length - 1,
    flag = false;

  while (p1 < p2) {
    if (s[p1] === s[p2]) {
      p1++;
      p2--;
    } else if (!flag) {
      if (p1 + 1 === p2) return true;
      flag = true;

      if (isLeft) {
        if (s[p1 + 1] === s[p2]) {
          p1++;
        } else if (s[p1] === s[p2 - 1]) {
          p2--;
        } else {
          return false;
        }
      } else {
        if (s[p1] === s[p2 - 1]) {
          p2--;
        } else if (s[p1 + 1] === s[p2]) {
          p1++;
        } else {
          return false;
        }
      }
    } else {
      return false;
    }
  }
  return true;
};
var validPalindrome = function (s) {
  return helper(s) || helper(s, false);
};
// @lc code=end

// console.log(validPalindrome('aba')); // true
// console.log(validPalindrome('abca')); // true
// console.log(validPalindrome('abc')); // false
// console.log(validPalindrome('aacbccbccaa')); // true
// console.log(validPalindrome('acxcybycxcxa')); // true
