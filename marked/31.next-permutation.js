/*
 * @lc app=leetcode id=31 lang=javascript
 *
 * [31] Next Permutation
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {void} Do not return anything, modify nums in-place instead.
 */
function swap(idx1, idx2, n) {
  let tmp = n[idx1];
  n[idx1] = n[idx2];
  n[idx2] = tmp;
}

function setArr(idx, n1, n2) {
  for (let i = 0; i < n1.length; i++) {
    n2[idx + i] = n1[i];
  }
}

var nextPermutation = function (nums) {
  let flag = false;
  const len = nums.length;

  for (let i = len - 1; i > 0; i--) {
    const ele1 = nums[i];
    const ele2 = nums[i - 1];
    if (ele2 < ele1) {
      flag = true;
      if (i === len - 1) {
        swap(i - 1, i, nums);
      } else {
        let j = len - 1;
        while (nums[j] <= ele2) {
          j--;
        }
        swap(i - 1, j, nums);
        let arr = nums.slice(i).sort((a, b) => a - b);
        setArr(i, arr, nums);
      }
      break;
    }
  }

  if (!flag) nums.reverse();
  return nums;
};

// nextPermutation([1, 5, 1]);
module.exports = nextPermutation;
// @lc code=end
