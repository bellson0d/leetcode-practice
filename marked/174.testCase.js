const targetFun = require('./174.dungeon-game');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  [
    [
      [-2, -3, 3],
      [-5, -10, 1],
      [10, 30, -5],
    ],
    7,
  ],
  [[[-200]], 201],
  [
    [
      [1, -3, 3],
      [0, -2, 0],
      [-3, -3, -3],
    ],
    3,
  ],
  [
    [
      [3, 0, -3],
      [-3, -2, -2],
      [3, 1, -3],
    ],
    1,
  ],
  [
    [
      [0, 0, 0],
      [-1, 0, 0],
      [2, 0, -2],
    ],
    2,
  ],
];

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const originInput = JSON.parse(JSON.stringify(input));
  const result = targetFun(input);
  if (result !== output) {
    console.log('Number ' + count + ' Case Wrong !!!');
    console.log('Input: ');
    originInput.forEach((v) => console.log(v));
    console.log('After transform ----------------');
    input.forEach((v) => console.log(v));
    console.log('Expected output: ', output);
    console.log('Output', result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
