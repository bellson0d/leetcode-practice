/*
 * @lc app=leetcode id=241 lang=javascript
 *
 * [241] Different Ways to Add Parentheses
 */

// @lc code=start
/**
 * @param {string} expression
 * @return {number[]}
 */
var baseMap = {
  '+': (a, b) => a + b,
  '-': (a, b) => a - b,
  '*': (a, b) => a * b,
};

var diffWaysToCompute = function (expression) {
  const len = expression.length;

  let obj = {},
    arr = [],
    tmp = '';
  for (let i = 0; i < len; i++) {
    const ele = expression[i];
    if (baseMap[expression[i]]) {
      arr.push(Number(tmp));
      tmp = '';
      arr.push(ele);
    } else {
      tmp += ele;
    }
  }
  arr.push(Number(tmp));

  var helper = (arr, arr2) => {
    const len = arr.length;
    if (len === 1) return [Number(arr[0])];
    if (len === 3) return [Number(baseMap[arr[1]](arr[0], arr[2]))];

    let tmp = [];
    for (let i = 0; i < len; i++) {
      const ele = arr[i];
      if (baseMap[ele]) {
        const result = baseMap[ele](arr[i - 1], arr[i + 1]);
        const strResult = '(' + arr2[i - 1] + arr2[i] + arr2[i + 1] + ')';

        const newArr = arr
          .slice(0, i - 1)
          .concat([result])
          .concat(arr.slice(i + 2));
        const newArr2 = arr2
          .slice(0, i - 1)
          .concat([strResult])
          .concat(arr2.slice(i + 2));
        const key = newArr2.join('');
        if (!obj[key]) {
          obj[key] = true;
          tmp.push(...helper(newArr, newArr2));
        }
      }
    }
    return tmp;
  };

  return helper(arr, arr);
};
// @lc code=end

// console.log(diffWaysToCompute('2-1-1'));
// console.log(diffWaysToCompute('2*3-4*5'));
// console.log(diffWaysToCompute('0'));
// console.log(diffWaysToCompute('123'));
// console.log(diffWaysToCompute('-123'));
// console.log(diffWaysToCompute('-123-222'));
// console.log(diffWaysToCompute('15*1*4'));
// console.log(diffWaysToCompute('2-1-1-1-1'));
