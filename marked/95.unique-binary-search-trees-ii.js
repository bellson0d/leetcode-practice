/*
 * @lc app=leetcode id=95 lang=javascript
 *
 * [95] Unique Binary Search Trees II
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {number} n
 * @return {TreeNode[]}
 */
var helper = (arr) => {
  const len = arr.length;

  if (len === 0) return [null];
  if (len === 1) return [new TreeNode(arr[0])];

  let res = [];
  for (let i = 0; i < len; i++) {
    const leftNode = helper(arr.slice(0, i));
    const rightNode = helper(arr.slice(i + 1));

    for (const left of leftNode) {
      for (const right of rightNode) {
        const root = new TreeNode(arr[i]);
        root.left = left;
        root.right = right;
        res.push(root);
      }
    }
  }
  return res;
};

var generateTrees = function (n) {
  if (n === 1) return [new TreeNode(1)];

  const arr = Array(n)
    .fill(1)
    .map((v, i) => i + 1);

  return helper(arr);
};
// @lc code=end

// function TreeNode(val, left, right) {
//   this.val = val === undefined ? 0 : val;
//   this.left = left === undefined ? null : left;
//   this.right = right === undefined ? null : right;
//   return this;
// }

// console.log(generateTrees(3));
