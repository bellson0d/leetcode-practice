/*
 * @lc app=leetcode id=235 lang=javascript
 *
 * [235] Lowest Common Ancestor of a Binary Search Tree
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */

/**
 * @param {TreeNode} root
 * @param {TreeNode} p
 * @param {TreeNode} q
 * @return {TreeNode}
 */
var lowestCommonAncestor = function (root, p, q) {
    let arr1 = [],
        arr2 = [],
        node1 = root,
        node2 = root;

    if (p.val === root.val || q.val === root.val) return root;

    while (node1) {
        arr1.unshift(node1);
        if (node1.val > p.val) {
            node1 = node1.left;
        } else if (node1.val < p.val) {
            node1 = node1.right;
        } else {
            break;
        }
    }

    while (node2) {
        arr2.unshift(node2.val);
        if (node2.val > q.val) {
            node2 = node2.left;
        } else if (node2.val < q.val) {
            node2 = node2.right;
        } else {
            break;
        }
    }

    return arr1.find((v) => arr2.includes(v.val));
};
// @lc code=end
// 每次这种题总是会被用例卡死几次，无论输出还是输入都是 Tree 的 Node 节点！！！
// const { getTree } = require("./utils/treeNode");
// const mockNode = (n) => {
//     return { val: n };
// };
// console.log(lowestCommonAncestor(getTree([6,2,8,0,4,7,9,null,null,3,5]), 2, 8)) // 6
// console.log(
//     lowestCommonAncestor(getTree([6, 2, 8, 0, 4, 7, 9, null, null, 3, 5]), 2, 4)
// ); // 2
// console.log(lowestCommonAncestor(getTree([2,1]), 2, 1)) // 2
// console.log(lowestCommonAncestor(getTree([6,2,8,0,4,7,9,null,null,3,5]), 2, 4)) // 2
// console.log(
//     lowestCommonAncestor(getTree([6, 2, 8, 0, 4, 7, 9, null, null, 3, 5]),mockNode(3), mockNode(5))
// ); // 4
