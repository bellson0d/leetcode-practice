/*
 * @lc app=leetcode id=24 lang=javascript
 *
 * [24] Swap Nodes in Pairs
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
function ListNode(val, next) {
  this.val = val === undefined ? 0 : val;
  this.next = next === undefined ? null : next;
}

var swapPairs = function (head) {
  if (!head || !head.next) return head;

  let node1 = head,
    node2 = head.next,
    newHead = null,
    currentNode = newHead;

  while (1) {
    const node11 = node2 ? node2.next : null;
    const node22 = node11 ? node11.next : null;

    if (newHead) {
      currentNode.next = node2;
      node2.next = node1;
      currentNode = node1;
    } else {
      newHead = node2;
      currentNode = node1;
      node2.next = currentNode;
    }

    if (!node11) {
      currentNode.next = null;
      break;
    } else if (!node22) {
      currentNode.next = node11;
      break;
    }

    node1 = node11;
    node2 = node22;
  }

  return newHead;
};

// let arr = [new ListNode(1)];
// let count = 2;
// while (count < 4) {
//   const node = new ListNode(count);
//   arr[count - 2].next = node;
//   arr.push(node);
//   count++;
// }

// let result = swapPairs(arr[0]);
// while (result) {
//   console.log(result.val);
//   result = result.next;
// }

// @lc code=end
