/*
 * @lc app=leetcode id=768 lang=javascript
 *
 * [768] Max Chunks To Make Sorted II
 */

// @lc code=start
/**
 * @param {number[]} arr
 * @return {number}
 */
var maxChunksToSorted = function (arr) {
  const len = arr.length;
  if (len === 1) return 1;

  let preArr = [],
    current = [0, 0, arr[0], arr[0]],
    idx = 1;
  while (idx < len) {
    const val = arr[idx];

    if (val < current[2]) {
      current[1]++;
      current[2] = val;
    }
    if (val >= current[2] || idx === len - 1) {
      while (preArr.length && current[2] < preArr[0][3]) {
        const [preStart, preEnd, preMin, preMax] = preArr.shift();
        current = [
          preStart,
          current[1],
          Math.min(preMin, current[2]),
          Math.max(preMax, current[3]),
        ];
      }
      preArr.unshift(current);
      current = [idx, idx, val, val];
    }
    idx++;
  }
  if (preArr.length && current[2] < preArr[0][3]) return preArr.length;

  return preArr.length + 1;
};
// @lc code=end

// console.log(maxChunksToSorted([5, 4, 3, 2, 1])); //1
// console.log(maxChunksToSorted([5, 4, 3, 2, 1, 2, 3, 4, 5])); //2
// console.log(maxChunksToSorted([2, 1, 3, 4, 4])); //4
// console.log(maxChunksToSorted([2, 1, 3, 2, 4, 4])); //4
// console.log(maxChunksToSorted([2, 1, 3, 2, 1, 4, 4])); //3
// console.log(maxChunksToSorted([2, 1, 3, 2, 1, 4, 4, 5, 8, 6, 4])); //4
// console.log(maxChunksToSorted([1, 1, 1, 0, 1, 0, 0, 0, 1, 0])); //1
// console.log(maxChunksToSorted([0, 0, 1, 1, 0, 1, 1, 1, 1, 0])); //3
