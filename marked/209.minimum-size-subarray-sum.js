/*
 * @lc app=leetcode id=209 lang=javascript
 *
 * [209] Minimum Size Subarray Sum
 */

// @lc code=start
/**
 * @param {number} target
 * @param {number[]} nums
 * @return {number}
 */
var minSubArrayLen = function (target, nums) {
    let sum = target,
        minLen = Infinity,
        leftP = 0,
        rightP = 0;

    while (rightP < nums.length) {
        const ele = nums[rightP];

        if (ele >= target) {
            return 1;
        } else {
              sum = sum - ele;
            if (sum <= 0) {
                while (leftP < rightP && sum <= 0) {
                    minLen = Math.min(minLen, rightP - leftP + 1);
                    sum = sum + nums[leftP];
                    leftP++;
                }
            }
        }
        rightP++;
    }
    return minLen === Infinity ? 0 : minLen;
};

// Greater than or equal !!!! 坑爹，还按照 Equal 做
// @lc code=end
// console.log(minSubArrayLen(7, [2, 3, 1, 2, 4, 3]));
// console.log(minSubArrayLen(4, [1, 4, 4]));
// console.log(minSubArrayLen(11, [1, 1, 1, 1, 1, 1, 1, 1]));
// console.log(minSubArrayLen(11, [1, 2, 3, 4, 5]));
