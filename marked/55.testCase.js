const targetFun = require('./55.jump-game');

const testArr = [
  2,
  0,
  6,
  9,
  8,
  4,
  5,
  0,
  8,
  9,
  1,
  2,
  9,
  6,
  8,
  8,
  0,
  6,
  3,
  1,
  2,
  2,
  1,
  2,
  6,
  5,
  3,
  1,
  2,
  2,
  6,
  4,
  2,
  4,
  3,
  0,
  0,
  0,
  3,
  8,
  2,
  4,
  0,
  1,
  2,
  0,
  1,
  4,
  6,
  5,
  8,
  0,
  7,
  9,
  3,
  4,
  6,
  6,
  5,
  8,
  9,
  3,
  4,
  3,
  7,
  0,
  4,
  9,
  0,
  9,
  8,
  4,
  3,
  0,
  7,
  7,
  1,
  9,
  1,
  9,
  4,
  9,
  0,
  1,
  9,
  5,
  7,
  7,
  1,
  5,
  8,
  2,
  8,
  2,
  6,
  8,
  2,
  2,
  7,
  5,
  1,
  7,
  9,
  6,
];

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  [[2, 3, 1, 1, 4], true],
  [[3, 2, 1, 0, 4], false],
  [[1, 1, 1, 0, 4], false],
  [[2, 0, 1, 2, 0], true],
  [[], false],
  [[0], true],
  [[1], true],
  [testArr, false],
  [[1, 2, 3], true],
  [Array(25000).fill(1), true],
  [
    Array(25000)
      .fill(25000)
      .map((v, i) => v - i)
      .concat([0, 0, 0, 0]),
    false,
  ],
  [[1, 1, 1, 0], true],
  [[3, 0, 8, 2, 0, 0, 1], true],
  [[5, 9, 3, 2, 1, 0, 2, 3, 3, 1, 0, 0], true],
  [[4, 0, 2, 2, 2, 1, 0, 1, 4, 2, 1, 0], false],
];

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input);
  if (result !== output) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
