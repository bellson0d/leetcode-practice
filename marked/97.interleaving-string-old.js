/*
 * @lc app=leetcode id=97 lang=javascript
 *
 * [97] Interleaving String
 */

// @lc code=start
/**
 * @param {string} s1
 * @param {string} s2
 * @param {string} s3
 * @return {boolean}
 */
var isInter = (restStr, str2, matchStr) => {
  let tmpArr = [[restStr, matchStr, str2]];

  while (tmpArr.length > 0) {
    const [s1, str, crossStr] = tmpArr.shift();
    const ele = s1[0];

    const len = str.length;
    const len2 = s1.length;

    for (let i = 0; i < str.length; i++) {
      if (ele === str[i]) {
        const idx = i;
        if (len - idx < len2) break;

        const newS1 = s1.slice(1);

        if (newS1) {
          const idx2 = crossStr.slice(0, idx) === str.slice(0, idx);
          if (idx2) {
            tmpArr.unshift([
              newS1,
              str.slice(idx + 1),
              crossStr.slice(idx === 0 ? 0 : idx),
            ]);
          }
        } else {
          if (crossStr === str.slice(0, idx) + str.slice(idx + 1)) return true;
        }
      }
    }
  }

  return false;
};

var isInterleave = function (s1, s2, s3) {
  if (!s1) return s3 === s2;
  if (!s2) return s3 === s1;

  return isInter(s1, s2, s3);
};
// @lc code=end
module.exports = isInterleave;

// isInterleave(
//   'bbbbbabbbbabaababaaaabbababbaaabbabbaaabaaaaababbbababbbbbabbbbababbabaabababbbaabababababbbaaababaa',
//   'babaaaabbababbbabbbbaabaabbaabbbbaabaaabaababaaaabaaabbaaabaaaabaabaabbbbbbbbbbbabaaabbababbabbabaab',
//   'babbbabbbaaabbababbbbababaabbabaabaaabbbbabbbaaabbbaaaaabbbbaabbaaabababbaaaaaabababbababaababbababbbababbbbaaaabaabbabbaaaaabbabbaaaabbbaabaaabaababaababbaaabbbbbabbbbaabbabaabbbbabaaabbababbabbabbab'
// );
