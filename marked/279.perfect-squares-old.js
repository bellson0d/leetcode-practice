/*
 * @lc app=leetcode id=279 lang=javascript
 *
 * [279] Perfect Squares
 */

// @lc code=start
/**
 * @param {number} n
 * @return {number}
 */
var obj = [
  1, 4, 9, 16, 25, 36, 49, 64, 81, 100, 121, 144, 169, 196, 225, 256, 289, 324,
  361, 400, 441, 484, 529, 576, 625, 676, 729, 784, 841, 900, 961, 1024, 1089,
  1156, 1225, 1296, 1369, 1444, 1521, 1600, 1681, 1764, 1849, 1936, 2025, 2116,
  2209, 2304, 2401, 2500, 2601, 2704, 2809, 2916, 3025, 3136, 3249, 3364, 3481,
  3600, 3721, 3844, 3969, 4096, 4225, 4356, 4489, 4624, 4761, 4900, 5041, 5184,
  5329, 5476, 5625, 5776, 5929, 6084, 6241, 6400, 6561, 6724, 6889, 7056, 7225,
  7396, 7569, 7744, 7921, 8100, 8281, 8464, 8649, 8836, 9025, 9216, 9409, 9604,
  9801, 10000, 10201,
];

var numSquares = function (n) {
  let res = n,
    count = 0,
    i = obj.length - 1;

  while (i >= 0) {
    const ele = obj[i];
    if (res === ele || obj[i - 1] === res) {
      return count + 1;
    } else if (ele < res && obj[i - 1] < res) {
      let j = i,
        tmp = Infinity;

      while (j > 0) {
        const ele2 = obj[j];
        if (res % ele2 === 0) {
          tmp = Math.min(tmp, res / ele2);
        } else {
          tmp = Math.min(tmp, ~~(res / ele2) + numSquares(res % ele2));
        }
        j--;
      }
      return count + tmp;
    }
    i--;
  }
  return count + res;
};
// @lc code=end

// console.log(numSquares(10)); //2
// console.log(numSquares(12)); //3
// console.log(numSquares(13)); //2
// console.log(numSquares(75)); //3
// console.log(numSquares(1)); // 1
// console.log(numSquares(728)); // 3
// console.log(numSquares(19)); // 3
// console.log(numSquares(23)); // 4
// console.log(numSquares(7168)); // 4
console.log(numSquares(7115)); // 3
