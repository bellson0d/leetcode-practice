/*
 * @lc app=leetcode id=221 lang=javascript
 *
 * [221] Maximal Square
 */

// @lc code=start
/**
 * @param {character[][]} matrix
 * @return {number}
 */
var helper = (x, y, m, n, matrix) => {
    for (let i = x; i < m; i++) {
        for (let j = y; j < n; j++) {
            if (matrix[i][j] !== "1") return false;
        }
    }
    return true;
};

var maximalSquare = function (matrix) {
    const m = matrix.length;
    if (m === 1) return Math.max(...matrix[0]);

    const n = matrix[0].length,
        arrMap = [];
    for (let i = 0; i < m; i++) {
        for (let j = 0; j < n; j++) {
            if (matrix[i][j] === "1") arrMap.push([i, j]);
        }
    }
    if (!arrMap.length) return 0;
    if (arrMap.length < 4) return 1;

    let tmpN = Math.min(m, n);

    while (tmpN > 1) {
        for (let i = 0; i < arrMap.length; i++) {
            const [x, y] = arrMap[i];
            if (
                x + tmpN <= m &&
                y + tmpN <= n &&
                helper(x, y, x + tmpN, y + tmpN, matrix)
            )
                return tmpN ** 2;
        }
        tmpN--;
    }

    return tmpN;
};
// @lc code=end
// DP 版本思路 从每一个 1 开始往外扩，扩到最大为止，后续如果做不到当前最大
// 值则直接放弃，会比这个版本好些
// console.log(
//     maximalSquare([
//         ["1", "0", "1", "0", "0"],
//         ["1", "0", "1", "1", "1"],
//         ["1", "1", "1", "1", "1"],
//         ["1", "0", "0", "1", "0"],
//     ])
// ); // 4
// console.log(
//     maximalSquare(
//         [
//             ["0", "1"],
//             ["1", "0"],
//         ],
//     )
// ); // 1
// console.log(maximalSquare([["0"]])); // 0
