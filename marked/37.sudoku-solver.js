/*
 * @lc app=leetcode id=37 lang=javascript
 *
 * [37] Sudoku Solver
 */

// @lc code=start
/**
 * @param {character[][]} board
 * @return {void} Do not return anything, modify board in-place instead.
 */
/**
 * 思路：横向、竖向、9 宫格校验是基本方法，每次单一值变更都需要执行一次
 * 可复用上一题代码
 * 每个空位置根据 横、纵、9 宫格可以存储候选值
 * 候选值从小到大依次选择
 * 当前 3 组校验通过则进入下一个值
 * 当前值最大都校验不通过或没有候选值则回溯
 * 回溯后上一值自增 1
 * 重复上述步骤
 * 最终 Solve 或 无解（此题已避免）
 */

const GROUP = [
  [0, 0, 0],
  [0, 1, 1],
  [0, 2, 2],
  [1, 0, 3],
  [1, 1, 4],
  [1, 2, 5],
  [2, 0, 6],
  [2, 1, 7],
  [2, 2, 8],
];

function findSquare(board, position) {
  const [x, y] = position;
  const [xRange, yRange] = GROUP.find(
    (v) => x < (v[0] + 1) * 3 && y < (v[1] + 1) * 3
  );
  let arr = [];

  for (let i = xRange * 3; i < xRange * 3 + 3; i++) {
    for (let j = yRange * 3; j < yRange * 3 + 3; j++) {
      const ele = board[i][j];
      if (ele !== '.') arr.push(ele);
    }
  }

  return arr;
}

const helpFn = (m, position) => {
  const [x, y] = position;
  let tmpArr = [],
    result = [];

  for (let i = 0; i < 9; i++) {
    if (m[x][i] !== '.') tmpArr.push(m[x][i]);
    if (m[i][y] !== '.') tmpArr.push(m[i][y]);
  }
  tmpArr = tmpArr.concat(findSquare(m, position));

  let count = 1;
  while (count < 10) {
    const num = count + '';
    if (!tmpArr.includes(num)) result.push(num);
    count++;
  }

  return result.length > 0 ? result : null;
};

var solveSudoku = function (board) {
  let allPoint = [],
    pointer = 0;

  for (let i = 0; i < 9; i++) {
    for (let j = 0; j < 9; j++) {
      const ele = board[i][j];
      if (ele === '.') {
        allPoint.push([[i, j], 'fresh']);
      }
    }
  }

  while (pointer >= 0) {
    let [position, rest] = allPoint[pointer];
    const [x, y] = position;

    if (position === 0) debugger;
    if (rest === 'fresh') {
      rest = helpFn(board, position);
      if (!rest) {
        board[x][y] = '.';
        pointer--;
      } else {
        allPoint[pointer][1] = rest;
      }
    } else if (rest.length === 0) {
      allPoint[pointer][1] = 'fresh';
      board[x][y] = '.';
      pointer--;
    } else {
      board[x][y] = rest.splice(0, 1)[0];
      pointer++;
      if (pointer === allPoint.length) return board;
    }
  }

  return false;
};
// @lc code=end

// const testSudo = [
//   ['5', '3', '.', '.', '7', '.', '.', '.', '.'],
//   ['6', '.', '.', '1', '9', '5', '.', '.', '.'],
//   ['.', '9', '8', '.', '.', '.', '.', '6', '.'],
//   ['8', '.', '.', '.', '6', '.', '.', '.', '3'],
//   ['4', '.', '.', '8', '.', '3', '.', '.', '1'],
//   ['7', '.', '.', '.', '2', '.', '.', '.', '6'],
//   ['.', '6', '.', '.', '.', '.', '2', '8', '.'],
//   ['.', '.', '.', '4', '1', '9', '.', '.', '5'],
//   ['.', '.', '.', '.', '8', '.', '.', '7', '9'],
// ];

// console.log(solveSudoku(testSudo));
