/*
 * @lc app=leetcode id=792 lang=javascript
 *
 * [792] Number of Matching Subsequences
 */

// @lc code=start
/**
 * @param {string} s
 * @param {string[]} words
 * @return {number}
 */
var numMatchingSubseq = function (s, words) {
    const len = s.length;
    
    let res = 0;
    for (let i = 0; i < words.length; i++) {
        const ele = words[i];
        if (ele.length <= len && new RegExp(".*" + ele.split("").join(".*") + ".*").test(s)) res++;
    }
    return res;
};
// @lc code=end

// console.log(numMatchingSubseq("abcde", ["a", "bb", "acd", "ace"])); // 3
// console.log(
//     numMatchingSubseq("dsahjpjauf", [
//         "ahjpjau",
//         "ja",
//         "ahbwzgqnuk",
//         "tnmlanowax",
//     ])
// ); // 2
