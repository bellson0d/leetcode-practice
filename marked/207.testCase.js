const targetFun = require("./207.course-schedule");
const IS_ARRAY_RESULT = false; // 输出是否为数组
const MULTI_ARGS = true; // 输入参数是否为多个

// prettier-ignore
const testStr = [
  [[1, []], true],
  [[100, []], true],
  [[100, [[1,2],[2,3],[3,4],[4,5]]], true],
  [[2, [[1,0]]], true],
  [[2, [[1,0],[0,1]]], false],
  [[3, [[1,0]]], true],
  [[3, [[1,0], [0,2]]], true],
  [[3, [[1,0], [0,2]]], true],
  [[3, [[1,0], [2,1]]], true],
  [[3, [[1,2], [0,1], [2,0]]], false],
  [[3, [[0,2], [0,1]]], true],
  [[3, [[0,2],[1,2],[2,0]]], false],
];

const time1 = new Date().getTime();
console.log(time1);

let count = 0;
while (count < testStr.length) {
    const [input, output] = testStr[count];
    let result;
    if (MULTI_ARGS) {
        result = targetFun(...input);
    } else {
        result = targetFun(input);
    }

    if (
        IS_ARRAY_RESULT
            ? result.join("|") !== output.join("|")
            : result !== output
    ) {
        console.log("Number " + count + " Case Wrong !!!");
        console.log("Input: ", input);
        console.log("Expected output: ", output);
        console.log("Output", result);
        return;
    }
    count++;
}
console.log("All pass!");

const time2 = new Date().getTime();
console.log("Duration: ", time2 - time1);
