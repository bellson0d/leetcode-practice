const targetFun = require('./475.供暖器');
const IS_ARRAY_RESULT = false; // 输出是否为数组
const MULTI_ARGS = true; // 输入参数是否为多个
const RANDOM_IDX = false; // 结果数组是否可以乱序

const testStr = [
  [[[1, 5], [2]], 3],
  [
    [
      [1, 2, 3],
      [1, 2, 3],
    ],
    0,
  ],
  [
    [
      [1, 2, 3],
      [1, 20, 30],
    ],
    2,
  ],
  [[[1, 2, 3], [2]], 1],
  [
    [
      [1, 2, 3, 4],
      [1, 4],
    ],
    1,
  ],
  [[[1, 2, 3], [1]], 2],
  [[[10, 20, 30], [1]], 29],
  [
    [
      [10, 20, 30],
      [1, 2, 3, 4, 10, 20, 30, 40, 50],
    ],
    0,
  ],
  [
    [
      [10, 20, 30],
      [1, 2, 3, 4, 30, 40, 50],
    ],
    10,
  ],
  [
    [
      [10, 20, 30],
      [30, 40, 50],
    ],
    20,
  ],
  [
    [
      [
        282475249, 622650073, 984943658, 144108930, 470211272, 101027544,
        457850878, 458777923,
      ],
      [
        823564440, 115438165, 784484492, 74243042, 114807987, 137522503,
        441282327, 16531729, 823378840, 143542612,
      ],
    ],
    161834419,
  ],
];

const time1 = new Date().getTime();
console.log(time1);

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  let result;
  if (MULTI_ARGS) {
    result = targetFun(...input);
  } else {
    result = targetFun(input);
  }

  let res = false;
  if (!IS_ARRAY_RESULT) {
    res = result === output;
  } else if (!RANDOM_IDX) {
    res = result.join('|') === output.join('|');
  } else {
    if (result.length === output.length) {
      res = !result.find((v) => !output.includes(v));
    }
  }

  if (!res) {
    console.log('Number ' + count + ' Case Wrong !!!');
    console.log('Input: ', input);
    console.log('Expected output: ', output);
    console.log('Output', result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
