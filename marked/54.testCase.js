const targetFun = require('./54.spiral-matrix');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  [
    [
      [1, 2, 3],
      [4, 5, 6],
      [7, 8, 9],
    ],
    [1, 2, 3, 6, 9, 8, 7, 4, 5],
  ],
  [
    [
      [1, 2, 3, 4],
      [5, 6, 7, 8],
      [9, 10, 11, 12],
    ],
    [1, 2, 3, 4, 8, 12, 11, 10, 9, 5, 6, 7],
  ],
  [
    [
      [1, 2, 3, 4],
      [5, 6, 7, 8],
      [9, 10, 11, 12],
    ],
    [1, 2, 3, 4, 8, 12, 11, 10, 9, 5, 6, 7],
  ],
  [[[]], []],
  [[], []],
  [[[1, 2, 3, 4]], [1, 2, 3, 4]],
  [[[1]], [1]],
  [
    [
      [1, 2],
      [3, 4],
    ],
    [1, 2, 4, 3],
  ],
];

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input);
  if (result.join('') !== output.join('')) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
