/*
 * @lc app=leetcode id=424 lang=javascript
 *
 * [424] Longest Repeating Character Replacement
 */

// @lc code=start
/**
 * @param {string} s
 * @param {number} k
 * @return {number}
 */

var characterReplacement = function (s, k) {
  const len = s.length;
  if (len === 1 || len - 2 < k) return len;

  let leftP = 0,
    rightP = 0,
    maxCharCount = 0,
    countArr = Array(26).fill(0);

  while (true) {
    let tmpCount = Math.max(...countArr) + k;
    const tmpLen = rightP - leftP;

    if (tmpCount >= tmpLen) {
      tmpCount = tmpLen;
      if (tmpCount > maxCharCount) maxCharCount = tmpCount;
      const ele = s[rightP];
      if (!ele) break;
      const idx = ele.charCodeAt() - 65;
      countArr[idx]++;
      rightP++;
    } else {
      countArr[s[leftP].charCodeAt() - 65]--;
      leftP++;
    }
  }

  return maxCharCount;
};
// @lc code=end

// console.log(characterReplacement('ABAB', 2)); // 4
// console.log(characterReplacement('AABABBA', 1)); // 4
// console.log(characterReplacement('AABABBBA', 3)); // 7
// console.log(characterReplacement('AABABCCBA', 3)); // 6
// console.log(characterReplacement('ABBB', 2)); // 4
// console.log(
//   characterReplacement(
//     'IMNJJTRMJEGMSOLSCCQICIHLQIOGBJAEHQOCRAJQMBIBATGLJDTBNCPIFRDLRIJHRABBJGQAOLIKRLHDRIGERENNMJSDSSMESSTR',
//     2
//   )
// ); // 6
// console.log(characterReplacement('NMJSDSSMESS', 2)); // 6

// @after-stub-for-debug-begin
module.exports = characterReplacement;
// @after-stub-for-debug-end

// 双指针一开始就想到，但是在处理统计字符最大值时用对象重复循环太多
// 开辟数组方法高效
