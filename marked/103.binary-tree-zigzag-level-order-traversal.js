/*
 * @lc app=leetcode id=103 lang=javascript
 *
 * [103] Binary Tree Zigzag Level Order Traversal
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[][]}
 */
var zigzagLevelOrder = function (root) {
  if (!root) return [];

  let tmpArr = [],
    nextArr = [],
    res = [],
    result = [[root.val]];
  if (root.left) tmpArr.unshift(root.left);
  if (root.right) tmpArr.unshift(root.right);
  let flag = false;

  while (tmpArr.length > 0) {
    const currentNode = tmpArr[flag ? 'pop' : 'shift']();

    if (currentNode) {
      res.push(currentNode.val);

      if (flag) {
        if (currentNode.left) nextArr.unshift(currentNode.left);
        if (currentNode.right) nextArr.unshift(currentNode.right);
      } else {
        if (currentNode.right) nextArr.push(currentNode.right);
        if (currentNode.left) nextArr.push(currentNode.left);
      }
    }

    if (tmpArr.length === 0) {
      result.push(res);
      res = [];
      if (nextArr.length > 0) {
        tmpArr = nextArr.slice();
        flag = !flag;
        nextArr = [];
      }
    }
  }

  return result;
};
// @lc code=end

// @lc code=end

// const { getTree } = require('./utils/treeNode');
// console.log(zigzagLevelOrder(getTree([])));
// console.log(zigzagLevelOrder(getTree([1])));
// console.log(zigzagLevelOrder(getTree([1, 2, 3])));
// console.log(zigzagLevelOrder(getTree([3, 9, 20, null, null, 15, 7])));
// console.log(zigzagLevelOrder(getTree([1, 2, 3, 4, 5, 6, 7])));
// console.log(zigzagLevelOrder(getTree([1, 2, 3, 4, 5, 6, 7, 8, 9])));
// console.log(
//   zigzagLevelOrder(getTree([0, 2, 4, 1, null, 3, -1, 5, 1, null, null, 6, 8]))
// );
// console.log(zigzagLevelOrder(getTree([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])));
