/*
 * @lc app=leetcode id=166 lang=javascript
 *
 * [166] Fraction to Recurring Decimal
 */

// @lc code=start
/**
 * @param {number} numerator
 * @param {number} denominator
 * @return {string}
 */
var fractionToDecimal = function (n, d) {
  if (!n) return '0';
  const isPositive = (n >= 0 && d > 0) || (n < 0 && d < 0);
  n = Math.abs(n);
  d = Math.abs(d);
  const res = n / d;
  let extra = 10 * (n % d),
    preNum = 0,
    arr = [],
    num = (isPositive ? '' : '-') + Math.abs(~~res),
    obj = {},
    count = 0;
  if (extra === 0) return num;

  while (true) {
    if (obj.hasOwnProperty(extra)) {
      const idx = obj[extra];
      const pre = arr.slice(0, idx).join('');
      if (arr[count - 1] === 0) return num + '.' + pre;
      const repeat = '(' + arr.slice(idx).join('') + ')';
      return num + '.' + pre + repeat;
    }
    if (extra < d) {
      obj[extra] = count;
      preNum = 0;
      arr.push(preNum);
      extra *= 10;
    } else {
      preNum = ~~(extra / d);
      obj[extra] = count;
      arr.push(preNum);
      extra = 10 * (extra % d);
    }

    count++;
  }
};
// @lc code=end

// module.exports = fractionToDecimal;
