const targetFun = require('./310.minimum-height-trees');
const IS_ARRAY_RESULT = true; // 输出是否为数组
const MULTI_ARGS = true; // 输入参数是否为多个
const RANDOM_IDX = true; // 结果数组是否可以乱序

const testStr = [
  [[1, []], [0]],
  [
    [2, [[0, 1]]],
    [1, 0],
  ],
  [
    [
      3,
      [
        [0, 1],
        [0, 2],
      ],
    ],
    [0],
  ],
  [
    [
      3,
      [
        [0, 1],
        [1, 2],
      ],
    ],
    [1],
  ],

  [
    [
      7,
      [
        [3, 0],
        [3, 1],
        [3, 2],
        [3, 4],
        [5, 4],
        [2, 6],
      ],
    ],
    [3],
  ],

  [
    [
      7,
      [
        [3, 0],
        [3, 1],
        [3, 2],
        [3, 4],
        [5, 4],
        [5, 6],
      ],
    ],
    [4],
  ],

  [
    [
      6,
      [
        [3, 0],
        [3, 1],
        [3, 2],
        [3, 4],
        [5, 4],
      ],
    ],
    [3, 4],
  ],

  [
    [
      9,
      [
        [0, 1],
        [1, 3],
        [3, 4],
        [3, 2],
        [4, 5],
        [5, 6],
        [5, 7],
        [7, 8],
      ],
    ],
    [4],
  ],

  [
    [
      7,
      [
        [0, 1],
        [1, 2],
        [1, 3],
        [2, 4],
        [3, 5],
        [4, 6],
      ],
    ],
    [1, 2],
  ],
];

const time1 = new Date().getTime();
console.log(time1);

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  let result;
  if (MULTI_ARGS) {
    result = targetFun(...input);
  } else {
    result = targetFun(input);
  }

  let res = false;
  if (!IS_ARRAY_RESULT) {
    res = result === output;
  } else if (!RANDOM_IDX) {
    res = result.join('|') === output.join('|');
  } else {
    if (result.length === output.length) {
      res = !result.find((v) => !output.includes(v));
    }
  }

  if (!res) {
    console.log('Number ' + count + ' Case Wrong !!!');
    console.log('Input: ', input);
    console.log('Expected output: ', output);
    console.log('Output', result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
