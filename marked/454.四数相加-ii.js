/*
 * @lc app=leetcode.cn id=454 lang=javascript
 *
 * [454] 四数相加 II
 */

// @lc code=start
/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @param {number[]} nums3
 * @param {number[]} nums4
 * @return {number}
 */
var fourSumCount = function (nums1, nums2, nums3, nums4) {
  const len = nums1.length;
  if (len === 1) return nums1[0] + nums2[0] + nums3[0] + nums4[0] === 0 ? 1 : 0;

  let obj1 = {},
    obj2 = {};
  for (const n1 of nums1) {
    for (const n2 of nums2) {
      const ele = n1 + n2;
      if (obj1[ele]) {
        obj1[ele][1]++;
      } else {
        obj1[ele] = [ele, 1];
      }
    }
  }
  for (const n3 of nums3) {
    for (const n4 of nums4) {
      const ele = n3 + n4;
      if (obj2[ele]) {
        obj2[ele][1]++;
      } else {
        obj2[ele] = [ele, 1];
      }
    }
  }

  let res = 0;
  const values1 = Object.values(obj1);

  for (const arr1 of values1) {
    if (obj2[-arr1[0]]) res += arr1[1] * obj2[-arr1[0]][1];
  }
  return res;
};
// @lc code=end
// 两两分组再计算，学习了
// console.log(fourSumCount([1, 2], [-2, -1], [-1, 2], [0, 2]));
// console.log(fourSumCount([0, 0], [0, 0], [0, 0], [0, 0]));
