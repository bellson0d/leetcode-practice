/*
 * @lc app=leetcode id=132 lang=javascript
 *
 * [132] Palindrome Partitioning II
 */

// @lc code=start
/**
 * @param {string} s
 * @return {number}
 */
var isPalindrome = (s) => {
  let count = 0,
    len = s.length;
  while (count < len / 2) {
    if (s[count] !== s[len - 1 - count]) return false;
    count++;
  }
  return true;
};

var minCut = function (s) {
  const len = s.length;
  if (len === 1) return 0;

  let tmp = [0];
  for (let i = 1; i < len; i++) {
    let count = tmp[i - 1] + 1;
    for (let j = 0; j < i; j++) {
      if (isPalindrome(s.slice(j, i + 1))) {
        count = Math.min(count, j > 0 ? tmp[j - 1] + 1 : 0);
      }
    }
    tmp.push(count);
  }

  return tmp[tmp.length - 1];
};
// @lc code=end

// console.log(minCut('aab')); // 1
// console.log(minCut('a')); // 0
// console.log(minCut('ab')); // 1
// console.log(minCut('aa')); // 0
// console.log(minCut('aabaa')); // 0
// console.log(minCut('aabab')); // 1
// console.log(minCut('abcdedcaba')); // 3
// console.log(minCut('cbbbcc')); // 1
