/*
 * @lc app=leetcode.cn id=450 lang=javascript
 *
 * [450] 删除二叉搜索树中的节点
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} key
 * @return {TreeNode}
 */

var getPeak = (root, key = 'left') => {
  let arr = [root];
  const key2 = key === 'left' ? 'right' : 'left';

  while (arr.length) {
    const node = arr.pop();
    if (node[key]) {
      node[key].parent = node;
      arr.push(node[key]);
    } else {
      if (node.parent) {
        if (node.parent.left && node.parent.left.val === node.val) {
          if (node[key2]) {
            node.parent.left = node[key2];
          } else {
            node.parent.left = null;
          }
        } else {
          if (node[key2]) {
            node.parent.right = node[key2];
          } else {
            node.parent.right = null;
          }
        }
      }
      return node.val;
    }
  }
};

var deleteNode = function (root, key) {
  if (!root) return root;

  let arr = [root],
    preNode = null,
    arrL = null,
    arrR = null;

  while (arr.length) {
    const node = arr.shift();
    if (node.val !== key) {
      if (node.left) {
        node.left.parent = node;
        arr.push(node.left);
      }
      if (node.right) {
        node.right.parent = node;
        arr.push(node.right);
      }
    } else {
      preNode = node;
      if (!node.left && !node.right) {
        if (preNode.parent) {
          if (preNode.parent.left && preNode.parent.left.val === key) {
            preNode.parent.left = null;
          } else {
            preNode.parent.right = null;
          }
          return root;
        }
        return null;
      }
      if (node.left) {
        node.left.parent = node;
        arrL = node.left;
      }
      if (node.right) {
        node.right.parent = node;
        arrR = node.right;
      }
      break;
    }
  }

  if (!preNode) return root; // No match value, return origin

  if (arrL) {
    preNode.val = getPeak(arrL, 'right');
  } else {
    preNode.val = getPeak(arrR);
  }

  return root;
};
// @lc code=end

const { getTree, logTreeWithFn, logTree } = require('../utils/treeNode');

// logTreeWithFn(getTree([1, 2, 3]), deleteNode, 4);
// logTreeWithFn(getTree([]), deleteNode, 4);
// logTreeWithFn(getTree([1]), deleteNode, 1);
logTreeWithFn(getTree([5, 3, 6, 2, 4, null, 7]), deleteNode, 3);
// logTreeWithFn(getTree([5, 3, 6, 2, 4, null, 7]), deleteNode, 5);
// logTreeWithFn(getTree([5, 3, 7, null, 4, 6, 8]), deleteNode, 5);
// logTreeWithFn(getTree([5, 3, 7, null, 4, 6, 8]), deleteNode, 7);
// logTreeWithFn(getTree([4, null, 7, 6, 8, 5, null, null, 9]), deleteNode, 7);
// logTreeWithFn(getTree([4, null, 7, 6, 8, 5, null, null, 9]), deleteNode, 4);
