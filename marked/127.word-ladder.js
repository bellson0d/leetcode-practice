/*
 * @lc app=leetcode id=127 lang=javascript
 *
 * [127] Word Ladder
 */

// @lc code=start
/**
 * @param {string} beginWord
 * @param {string} endWord
 * @param {string[]} wordList
 * @return {number}
 */
var diff1 = (s1, s2) => {
  let count = 0,
    diffCount = 0;
  while (count < s1.length) {
    if (s1[count] !== s2[count]) {
      diffCount++;
      if (diffCount > 1) return false;
    }
    count++;
  }
  return diffCount === 1;
};

function Node(val) {
  this.val = val;
  this.neighbor = [];
  this.addNeighbor = (v) => {
    if (!this.neighbor.includes(v)) {
      this.neighbor.push(v);
    }
  };
  return this;
}

var generatorGraph = (arr) => {
  let obj = {};
  const len = arr.length;
  for (let i = 0; i < len; i++) {
    const val1 = arr[i];
    if (!obj[val1]) obj[val1] = new Node(arr[i]);

    for (let j = i + 1; j < len; j++) {
      const val2 = arr[j];
      if (diff1(val1, val2)) {
        obj[val1].addNeighbor(val2);
        if (!obj[val2]) obj[val2] = new Node(val2);
        obj[val2].addNeighbor(val1);
      }
    }
  }

  return obj;
};

var ladderLength = function (begin, e, list) {
  if (!list.includes(e)) return 0;
  const obj = generatorGraph([begin, ...list]);

  let maxLen = Infinity;
  obj[begin].cost = 0;
  let arr = [[begin, [begin]]];
  var helper2 = function (b, pre) {
    const neighbor = obj[b].neighbor;
    for (const n of neighbor) {
      if (n === e) {
        if (pre.length + 1 < maxLen) maxLen = pre.length + 1;
        return pre.length + 1;
      }
      if (!pre.includes(n)) {
        if (obj[n].cost < obj[b].cost + 1) continue; // 这里有 2 个点
        // undefined < Number 恒等于 false
        // 因为是 BFS 所以其实 obj[n].cost 只可能 === obj[b].cost + 1 或者小于，不可能大于
        // 但其他情况有可能
        obj[n].cost = obj[b].cost + 1;
        arr.push([n, pre.concat(n)]);
      }
    }
    return 0;
  };

  while (arr.length) {
    const [v, p] = arr.shift();
    if (p.length < maxLen) {
      const res = helper2(v, p);
      if (res) return res;
    }
  }

  return 0;
};
// @lc code=end

// console.log(
//   ladderLength('hit', 'cog', ['hot', 'dot', 'dog', 'lot', 'log', 'cog'])
// );
