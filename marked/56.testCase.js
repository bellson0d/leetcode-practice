const targetFun = require('./56.merge-intervals');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  [
    [
      [1, 3],
      [2, 6],
      [8, 10],
      [15, 18],
    ],
    [
      [1, 6],
      [8, 10],
      [15, 18],
    ],
  ],
  [
    [
      [1, 4],
      [4, 5],
    ],
    [[1, 5]],
  ],
  [
    [
      [1, 4],
      [0, 4],
    ],
    [[0, 4]],
  ],
  [
    [
      [1, 7],
      [0, 3],
    ],
    [[0, 7]],
  ],
  [
    [
      [1, 4],
      [0, 0],
    ],
    [
      [0, 0],
      [1, 4],
    ],
  ],
  [
    [
      [2, 3],
      [4, 5],
      [6, 7],
      [8, 9],
      [1, 10],
    ],
    [[1, 10]],
  ],
];

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input);
  if (result.join('') !== output.join('')) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
