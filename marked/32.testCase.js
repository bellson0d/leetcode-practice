const targetFun = require('./32.longest-valid-parentheses');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  ['(()', 2],
  [')()())', 4],
  ['(())))((()))', 6],
  ['(())))((()))()()(((())))', 18],
  ['()(()', 2],
  ['()(())', 6],
  [')(((((()())()()))()(()))(', 22],
  ['(())((', 4],
  ['((((((((', 0],
  [')()())()()(', 4],
  ['))))((()((', 2],
];
let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input);
  if (result !== output) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
