/*
 * @lc app=leetcode id=30 lang=javascript
 *
 * [30] Substring with Concatenation of All Words
 */

// @lc code=start
/**
 * @param {string} s
 * @param {string[]} words
 * @return {number[]}
 */

var findSubstring = function (s, words) {
  const len = words.length;
  const len2 = s.length;
  let result = [];
  if (len < 1 || len2 < 1) return result;

  const step = words[0].length;

  for (let i = 0; i < len2 - len * step + 1; i++) {
    let j = i;
    let newWords = words;

    while (j < len2 - newWords.length * step + 1) {
      const ele = s.slice(j, j + step);
      idx = newWords.indexOf(ele);
      if (idx !== -1) {
        j = j + step;
        newWords = newWords.slice(0, idx).concat(newWords.slice(idx + 1));
        if (newWords.length === 0) {
          result.push(i);
          break;
        }
      } else {
        break;
      }
    }
  }

  return result;
};

// console.log(findSubstring('barfoothefoobarman', ['foo', 'bar']));
// console.log(
//   findSubstring('wordgoodgoodgoodbestword', ['word', 'good', 'best', 'word'])
// );
// console.log(findSubstring('a', ['a']));
// @lc code=end
