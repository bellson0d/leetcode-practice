/*
 * @lc app=leetcode id=79 lang=javascript
 *
 * [79] Word Search
 */

// @lc code=start
/**
 * @param {character[][]} board
 * @param {string} word
 * @return {boolean}
 */
var exist = function (board, word) {
  const xLen = board[0].length;
  const yLen = board.length;
  const wLen = word.length;

  let beginArr = [],
    begin = word[0];

  for (let i = 0; i < yLen; i++) {
    for (let j = 0; j < xLen; j++) {
      const ele = board[i][j];
      if (ele === begin) beginArr.push([i, j]);
    }
  }

  if (beginArr.length === 0) return false;
  if (wLen === 1 && beginArr.length > 0) return true;

  let count = 0;
  while (count < beginArr.length) {
    let strIdx = 1,
      preIdx = [],
      currentIdx = beginArr[count],
      record = [],
      record2 = [];

    while (strIdx < wLen) {
      let tmpArr = [];
      const [i, j] = currentIdx;
      const ele = word[strIdx];

      if (
        i > 0 &&
        board[i - 1][j] === ele &&
        !record2.find((v) => v[0] === i - 1 && v[1] === j)
      )
        tmpArr.push([i - 1, j]);

      if (
        j < xLen - 1 &&
        board[i][j + 1] === ele &&
        !record2.find((v) => v[0] === i && v[1] === j + 1)
      )
        tmpArr.push([i, j + 1]);

      if (
        i < yLen - 1 &&
        board[i + 1][j] === ele &&
        !record2.find((v) => v[0] === i + 1 && v[1] === j)
      )
        tmpArr.push([i + 1, j]);

      if (
        j > 0 &&
        board[i][j - 1] === ele &&
        !record2.find((v) => v[0] === i && v[1] === j - 1)
      )
        tmpArr.push([i, j - 1]);

      if (tmpArr.length > 0) {
        if (strIdx === wLen - 1) return true;
        preIdx = currentIdx;
        currentIdx = tmpArr.pop();
        record.unshift(tmpArr);
        record2.unshift(preIdx);
        strIdx++;
      } else {
        while (record[0] && record[0].length === 0) {
          record.shift();
          record2.shift();
          strIdx--;
        }
        if (!record[0]) break;
        currentIdx = record[0].shift();
        preIdx = record2[0];
      }
    }

    count++;
  }

  return false;
};
// @lc code=end

// const board = [
//   ['F', 'Y', 'C', 'E', 'N', 'R', 'D'],
//   ['K', 'L', 'N', 'F', 'I', 'N', 'U'],
//   ['A', 'A', 'A', 'R', 'A', 'H', 'R'],
//   ['N', 'D', 'K', 'L', 'P', 'N', 'E'],
//   ['A', 'L', 'A', 'N', 'S', 'A', 'P'],
//   ['O', 'O', 'G', 'O', 'T', 'P', 'N'],
//   ['H', 'P', 'O', 'L', 'A', 'N', 'O'],
// ];
// const b = [
//   ['A', 'B', 'C', 'E'],
//   ['S', 'F', 'C', 'S'],
//   ['A', 'D', 'E', 'E'],
// ];
// console.log(exist(b, 'ABCCED'));
// console.log(exist(b, 'SEE'));
// console.log(exist(b, 'ABCB'));
// console.log(exist(board, 'POLAND'));
// console.log(exist(Array(3).fill('a'.repeat(4).split('')), 'a'.repeat(15)));
