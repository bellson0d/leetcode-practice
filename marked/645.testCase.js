const targetFun = require('./645.错误的集合');
const IS_ARRAY_RESULT = true; // 输出是否为数组
const MULTI_ARGS = false; // 输入参数是否为多个
const RANDOM_IDX = false; // 结果数组是否可以乱序

// prettier-ignore
const testStr = [
  [[1, 2, 2, 4], [2,3]],
  [[1, 1], [1,2]],
  [[2, 2], [2,1]],
  [[1, 2, 2], [2,3]],
  [[1, 2, 3, 3], [3,4]],
  [[1, 2, 3, 4, 5, 5, 6, 7, 8, 9, 10], [5,11]],
  [[3, 2, 2], [2,1]],
  [[1, 3, 3], [3,2]],
  [[1, 2, 3, 5, 5], [5,4]],
  [[3, 2, 3, 4, 6, 5], [3,1]],
  [[5,3,6,1,5,4,7,8], [5,2]],
];

const time1 = new Date().getTime();
console.log(time1);

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  let result;
  if (MULTI_ARGS) {
    result = targetFun(...input);
  } else {
    result = targetFun(input);
  }

  let res = false;
  if (!IS_ARRAY_RESULT) {
    res = result === output;
  } else if (!RANDOM_IDX) {
    res = result.join('|') === output.join('|');
  } else {
    if (result.length === output.length) {
      res = !result.find((v) => !output.includes(v));
    }
  }

  if (!res) {
    console.log('Number ' + count + ' Case Wrong !!!');
    console.log('Input: ', input);
    console.log('Expected output: ', output);
    console.log('Output', result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
