/*
 * @lc app=leetcode.cn id=470 lang=javascript
 *
 * [470] 用 Rand7() 实现 Rand10()
 */

// @lc code=start
/**
 * The rand7() API is already defined for you.
 * var rand7 = function() {}
 * @return {number} a random integer in the range 1 to 7
 */
var obj = {
  11: 1,
  12: 2,
  13: 3,
  14: 4,
  15: 5,
  21: 6,
  22: 7,
  23: 8,
  24: 9,
  25: 10,
};
var rand10 = function () {
  let pre1 = 0;
  while (!pre1) {
    const n1 = rand7();
    if (n1 < 4) {
      pre1 = 1;
    } else if (n1 < 7) pre1 = 2;
  }

  let pre2 = 0;
  while (!pre2) {
    const n2 = rand7();
    if (n2 < 6) pre2 = n2;
  }

  return obj[String(pre1) + String(pre2)];
};
// @lc code=end
