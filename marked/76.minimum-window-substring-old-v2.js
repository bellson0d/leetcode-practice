/*
 * @lc app=leetcode id=76 lang=javascript
 *
 * [76] Minimum Window Substring
 */

// @lc code=start
/**
 * @param {string} s
 * @param {string} t
 * @return {string}
 */
var insertArr = (arr, n) => {
  let i = 0,
    tmp = [];
  while (i < arr.length) {
    const ele = arr[i];
    if (n < ele) {
      tmp.push(n);
      tmp = tmp.concat(arr.slice(i));
      return tmp;
    }
    tmp.push(ele);
    i++;
  }
  tmp.push(n);
  return tmp;
};

var minWindow = function (s, t) {
  if (!s || !t || s.length < t.length) return '';
  if (s.includes(t)) return t;
  const sLen = s.length;
  const tLen = t.length;
  let max = '',
    arr = [],
    idx = 0,
    tEle = t.split('');

  while (idx < sLen) {
    const ele = s[idx];
    const tIdx = tEle.findIndex((v) => v === ele);
    if (tIdx !== -1 && !arr.includes(idx)) {
      arr = insertArr(arr, idx);
      if (arr.length === tLen) {
        const str = s.slice(arr[0], arr[arr.length - 1] + 1);
        max = !max || max.length > str.length ? str : max;
        idx = arr.shift();
        tEle = [s[idx]];
      } else {
        tEle.splice(tIdx, 1);
      }
    }
    idx++;
  }

  return max;
};
// @lc code=end

module.exports = minWindow;
console.log(minWindow('ABABABCCC', 'ABBC'));
// console.log(minWindow('ADOBECODEBANC', 'ABC'));
