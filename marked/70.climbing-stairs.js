/*
 * @lc app=leetcode id=70 lang=javascript
 *
 * [70] Climbing Stairs
 */

// @lc code=start
/**
 * @param {number} n
 * @return {number}
 */
// 每一步都可以拆解为之前 走到剩 2 步或 1 步，故 Fib
var climbStairs = function (n) {
  if (n < 4) return n;

  let pre1 = 2,
    pre2 = 3,
    current,
    count = 3;

  while (count < n) {
    current = pre1 + pre2;
    pre1 = pre2;
    pre2 = current;
    count++;
  }

  return current;
};
// @lc code=end
