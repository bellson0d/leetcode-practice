/*
 * @lc app=leetcode.cn id=462 lang=javascript
 *
 * [462] 最少移动次数使数组元素相等 II
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var minMoves2 = function (nums) {
  nums.sort((a, b) => a - b);
  const mid1 = nums[~~(nums.length / 2)];

  let res1 = 0;
  for (const n of nums) {
    res1 += Math.abs(n - mid1);
  }
  return res1;
};
// @lc code=end
// 中位数只能靠猜，证明不出来
// 坐标系画线可以
// console.log(minMoves2([1, 2, 3])); // 2
// console.log(minMoves2([1, 0, 0, 8, 6])); // 14
// console.log(minMoves2([1, 1, 1, 1, 1, 1, 100])); // 99
