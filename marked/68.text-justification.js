/*
 * @lc app=leetcode id=68 lang=javascript
 *
 * [68] Text Justification
 */

// @lc code=start
/**
 * @param {string[]} words
 * @param {number} maxWidth
 * @return {string[]}
 */
const groupWord = (arr, maxLen) => {
  const len = arr.length;
  let count = 0,
    tmpArr = [],
    tmpCount = 0,
    result = [];

  while (count < len) {
    const str = arr[count];
    if (maxLen - tmpCount >= str.length) {
      tmpArr.push(str);
      tmpCount += str.length + 1;
    } else {
      result.push(tmpArr);
      tmpArr = [str];
      tmpCount = str.length + 1;
    }

    count++;
  }
  result.push(tmpArr);

  return result;
};

const arrToStr = (arr, maxLen, isLast) => {
  const lenCount = arr.length;

  if (lenCount === 1) return arr[0] + ' '.repeat(maxLen - arr[0].length);

  const lenStr = arr.reduce((a, b) => a + b, '').length;

  const diff = maxLen - lenStr;
  const spaces = ~~(diff / (lenCount - 1));
  let more = diff % (lenCount - 1);
  let str = '';

  for (let i = 0; i < lenCount; i++) {
    const tmpStr = arr[i];
    str += tmpStr;

    if (isLast) {
      if (i !== lenCount - 1) {
        str += ' ';
      } else {
        str += ' '.repeat(maxLen - str.length);
      }
      continue;
    }

    if (i !== lenCount - 1) str += ' '.repeat(spaces);
    if (more) {
      str += ' ';
      more--;
    }
  }

  return str;
};

var fullJustify = function (words, maxWidth) {
  const len = words.length;
  if (len === 1) return [words[0] + ' '.repeat(maxWidth - words[0].length)];

  const arr = groupWord(words, maxWidth);

  return arr.map((v, i) => arrToStr(v, maxWidth, i === arr.length - 1));
};

// @lc code=end

// console.log(
//   fullJustify(
//     ['This', 'is', 'an', 'example', 'of', 'text', 'justification.'],
//     16
//   )
// );
// console.log(
//   fullJustify(['What', 'must', 'be', 'acknowledgment', 'shall', 'be'], 16)
// );
// console.log(
//   fullJustify(
//     [
//       'Science',
//       'is',
//       'what',
//       'we',
//       'understand',
//       'well',
//       'enough',
//       'to',
//       'explain',
//       'to',
//       'a',
//       'computer.',
//       'Art',
//       'is',
//       'everything',
//       'else',
//       'we',
//       'do',
//     ],
//     20
//   )
// );

// [
//    "This    is    an",
//    "example  of text",
//    "justification.  "
// ]

// [
//   "What   must   be",
//   "acknowledgment  ",
//   "shall be        "
// ]

// [
//   "Science  is  what we",
//   "understand      well",
//   "enough to explain to",
//   "a  computer.  Art is",
//   "everything  else  we",
//   "do                  "
// ]
