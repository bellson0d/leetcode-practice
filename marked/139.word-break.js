/*
 * @lc app=leetcode id=139 lang=javascript
 *
 * [139] Word Break
 */

// @lc code=start
/**
 * @param {string} s
 * @param {string[]} wordDict
 * @return {boolean}
 */

var wordBreak = function (s, arr) {
  let map = new Set();
  for (const a of arr.join('')) {
    map.add(a);
  }
  for (const b of s) {
    if (!map.has(b)) return false;
  }

  let obj = {};
  var helper = (s) => {
    for (let i = 1; i <= s.length; i++) {
      const word = s.slice(0, i);
      if (arr.indexOf(word) !== -1) {
        const str = s.slice(word.length);
        if (str === '') {
          return true;
        } else {
          if (!obj.hasOwnProperty(str)) {
            obj[str] = helper(str);
          }
          if (obj[str]) return true;
        }
      }
    }
    return false;
  };

  return helper(s);
};
// @lc code=end

// console.log(wordBreak('leetcode', ['leet', 'code'])); // true
// console.log(wordBreak('applepenapple', ['apple', 'pen'])); // true
// console.log(wordBreak('catsandog', ['cats', 'dog', 'sand', 'and', 'cat'])); // false
// console.log(wordBreak('catsandog', ['cats', 'and'])); // false
// console.log(wordBreak('catsandog', ['cats', 'and', 'og'])); // true
// console.log(wordBreak('bb', ['a', 'b', 'bbb', 'bbbb'])); // true
// console.log(
//   wordBreak(
//     'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab',
//     [
//       'a',
//       'aa',
//       'aaa',
//       'aaaa',
//       'aaaaa',
//       'aaaaaa',
//       'aaaaaaa',
//       'aaaaaaaa',
//       'aaaaaaaaa',
//       'aaaaaaaaaa',
//     ]
//   )
// ); // false
// console.log(
//   wordBreak(
//     'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabaabaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
//     [
//       'aa',
//       'aaa',
//       'aaaa',
//       'aaaaa',
//       'aaaaaa',
//       'aaaaaaa',
//       'aaaaaaaa',
//       'aaaaaaaaa',
//       'aaaaaaaaaa',
//       'ba',
//     ]
//   )
// ); // false
