/*
 * @lc app=leetcode id=322 lang=javascript
 *
 * [322] Coin Change
 */

// @lc code=start
/**
 * @param {number[]} coins
 * @param {number} amount
 * @return {number}
 */
var coinChange = function (coins, amount) {
  if (!amount) return 0;

  coins = Array.from(new Set(coins));
  const len = coins.length;
  if (len === 1) return amount % coins[0] === 0 ? amount / coins[0] : -1;

  let count = 1,
    res = [0];
  while (count <= amount) {
    let tmp = [Infinity];
    for (const c of coins) {
      if (count === c) {
        tmp.push(1);
      } else if (count - c > 0) {
        tmp.push(res[count - c] + 1);
      }
    }
    res.push(Math.min(...tmp));
    count++;
  }

  return res[count - 1] === Infinity ? -1 : res[count - 1];
};
// @lc code=end

// console.log(coinChange([1, 2, 5], 11)); // 3
// console.log(coinChange([1, 2, 5], 19)); // 5
// console.log(coinChange([2], 3)); // -1
// console.log(coinChange([1], 0)); // 0
// console.log(coinChange([1], 1)); // 1
// console.log(coinChange([1], 2)); // 2
// console.log(coinChange([2, 17], 88)); // 14
// console.log(coinChange([2], 11)); // -1
// console.log(coinChange([474, 83, 404, 3], 264)); // 8
// console.log(coinChange([281, 20, 251, 251], 7323)); // 66
// console.log(coinChange([1, 2], 3)); // 2
