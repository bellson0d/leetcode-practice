/*
 * @lc app=leetcode id=316 lang=javascript
 *
 * [316] Remove Duplicate Letters
 */

// @lc code=start
/**
 * @param {string} s
 * @return {string}
 */
var removeDuplicateLetters = function (str) {
  const len = str.length;
  let stack = [str[0]],
    alphaMap = { [str[0]]: 0 },
    count = 1;

  while (count < len) {
    const pre = stack[stack.length - 1];
    const s = str[count];
    if (!pre) {
      stack.push(s);
      continue;
    }
    if (!stack.includes(s)) {
      if (s > pre) {
        stack.push(s);
      } else if (s < pre) {
        let idx = alphaMap[pre] + 1,
          hasNext = false;
        while (idx < len) {
          if (str[idx] === pre) {
            stack.pop();
            hasNext = true;
            break;
          }
          idx++;
        }

        if (hasNext) continue;
        stack.push(s);
      }
    }
    alphaMap[s] = count;
    count++;
  }
  return stack.join('');
};
// @lc code=end
// Inspired by https://leetcode-cn.com/problems/remove-duplicate-letters/solution/qu-chu-zhong-fu-zi-mu-by-leetcode-soluti-vuso/
// console.log(removeDuplicateLetters('bcabc')); // abc
// console.log(removeDuplicateLetters('cbacdcbc')); // acdb
// console.log(removeDuplicateLetters('abcad')); //abcd
// console.log(removeDuplicateLetters('eabcade')); //abcde
