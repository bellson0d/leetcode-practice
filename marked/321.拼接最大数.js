/*
 * @lc app=leetcode.cn id=321 lang=javascript
 *
 * [321] 拼接最大数
 */

// @lc code=start
/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @param {number} k
 * @return {number[]}
 */
var getMax = (arr, k) => {
  if (k === 0) return [];
  if (k === 1) return [Math.max(...arr)];
  const len = arr.length;
  if (len === k) return arr;
  const head = arr.slice(0, len - k + 1);

  let max = head[0],
    idx = 0;
  for (let i = 1; i < head.length; i++) {
    if (head[i] > max) {
      max = head[i];
      idx = i;
    }
  }

  return [max, ...getMax(arr.slice(idx + 1), k - 1)];
};

// const compare = (subsequence1, index1, subsequence2, index2) => {
//   const x = subsequence1.length,
//     y = subsequence2.length;
//   while (index1 < x && index2 < y) {
//     const difference = subsequence1[index1] - subsequence2[index2];
//     if (difference !== 0) {
//       return difference;
//     }
//     index1++;
//     index2++;
//   }
//   return x - index1 - (y - index2);
// };

// const mergeStack = (subsequence1, subsequence2) => {
//   const x = subsequence1.length,
//     y = subsequence2.length;
//   if (x === 0) {
//     return subsequence2;
//   }
//   if (y === 0) {
//     return subsequence1;
//   }
//   const mergeLength = x + y;
//   const merged = new Array(mergeLength).fill(0);
//   let index1 = 0,
//     index2 = 0;
//   for (let i = 0; i < mergeLength; i++) {
//     if (compare(subsequence1, index1, subsequence2, index2) > 0) {
//       merged[i] = subsequence1[index1++];
//     } else {
//       merged[i] = subsequence2[index2++];
//     }
//   }
//   const res = merged.join('');
//   if (res.slice(0, 19) === '9999977235510803044') {
//     console.log(subsequence1.length, subsequence2.length);
//     console.log('' + subsequence1, subsequence2);
//   }
//   return res;
// };

var mergeStack = (arr1, arr2) => {
  const len1 = arr1.length,
    len2 = arr2.length;
  let p1 = 0,
    p2 = 0,
    res = '';
  if (!len1) return arr2.join('');
  if (!len2) return arr1.join('');

  while (p1 < len1 || p2 < len2) {
    if (arr1[p1] === arr2[p2]) {
      const preP1 = p1;
      const preP2 = p2;
      let getArr1 = 0;

      while (
        getArr1 === 0 &&
        arr1[p1] === arr2[p2] &&
        (p1 < len1 - 1 || p2 < len2 - 1)
      ) {
        p1++;
        p2++;

        if (p1 >= len1) {
          const range = p1 - preP1;
          while (p2 < len2) {
            if (arr2[p2] > arr2[p2 - range]) {
              getArr1 = -1;
              break;
            }
            if (arr2[p2] < arr2[p2 - range]) {
              getArr1 = 1;
              break;
            }

            p2++;
          }
        }

        if (getArr1 !== 0) break;

        if (p2 >= len2) {
          const range = p2 - preP2;
          while (p1 < len1) {
            if (arr1[p1] > arr1[p1 - range]) {
              getArr1 = 1;
              break;
            }
            if (arr1[p1] < arr1[p1 - range]) {
              getArr1 = -1;
              break;
            }

            p1++;
          }
        }
      }

      if (getArr1 === 1 || arr1[p1] > arr2[p2]) {
        res += arr1[preP1];
        p1 = preP1 + 1;
        p2 = preP2;
      } else if (getArr1 === -1 || arr1[p1] <= arr2[p2]) {
        res += arr2[preP2];
        p2 = preP2 + 1;
        p1 = preP1;
      }
    } else {
      if (arr1[p1] > arr2[p2]) {
        res += arr1[p1];
        p1++;
      } else {
        res += arr2[p2];
        p2++;
      }
    }

    if (p1 >= len1) return res + arr2.slice(p2).join('');
    if (p2 >= len2) return res + arr1.slice(p1).join('');
  }
  return res;
};

var maxNumber = function (nums1, nums2, k) {
  if (k === 0) return [];
  if (k === 1) return [Math.max(...nums1, ...nums2)];
  const len1 = nums1.length;
  const len2 = nums2.length;
  let max = '-1',
    k1 = 0;

  while (k1 <= k && k1 <= len1) {
    const k2 = k - k1;
    if (k2 <= len2) {
      const tmp1 = getMax(nums1, k1);
      const tmp2 = getMax(nums2, k2);
      const res = mergeStack(tmp1, tmp2);
      if (res > max) max = res;
    }

    k1++;
  }

  return max.split('').map(Number);
};

// @lc code=end
module.exports = maxNumber;
// 思路：
// 先实现每个数组取 k1,k2 k1+k2 = k
// 子问题变成 数组取 k1|k2 个最大 (Greedy + DFS)
// 子问题 2 ，单调栈合并数组 (Merge)
// Over
// 实现太长懒得写
// 细节搞死人，不过最终还是自己的 mergeStack !
// console.log(maxNumber([4, 3, 9, 4], [5, 3, 9], 7));
// console.log(
//   '' +
//     maxNumber(
//       [9, 9, 9, 9, 9, 7, 6, 1, 7, 2, 7, 5, 5, 1],
//       [
//         9, 9, 7, 8, 1, 5, 2, 5, 7, 0, 4, 3, 8, 7, 3, 8, 5, 3, 8, 3, 4, 0, 2, 3,
//         8, 2, 7, 1, 2, 3, 8, 7, 6, 7, 1, 1, 3, 9, 0, 5, 2, 8, 2, 8, 7, 5, 0, 8,
//         0, 7, 2, 8, 5, 6, 5, 9, 5, 1, 5, 2, 6, 2, 4, 9, 9, 7, 6, 5, 7, 9, 2, 8,
//         8, 3, 5, 9, 5, 1, 8, 8, 4, 6, 6, 3, 8, 4, 6, 6, 1, 3, 4, 1, 6, 7, 0, 8,
//         0, 3, 3, 1, 8, 2, 2, 4, 5, 7, 3, 7, 7, 4, 3, 7, 3, 0, 7, 3, 0, 9, 7, 6,
//         0, 3, 0, 3, 1, 5, 1, 4, 5, 2, 7, 6, 2, 4, 2, 9, 5, 5, 9, 8, 4, 2, 3, 6,
//         1, 9,
//       ],
//       160
//     )
// );
// 14 146
// '99999997876172755152'  √
// '99999997876172755115'  ×

// console.log(
//   '' +
//     maxNumber(
//       [
//         9, 7, 0, 3, 0, 4, 4, 1, 0, 7, 3, 7, 5, 4, 5, 1, 3, 3, 8, 3, 3, 7, 3, 0,
//         2, 1, 8, 4, 8, 1, 0, 3, 1, 3, 1, 5, 3, 0, 8, 0, 2, 5, 0, 5, 6, 4, 7, 2,
//         6, 2, 4, 0, 4, 4, 1, 8, 1, 4, 1, 8, 2, 6, 7, 3, 4, 1, 1, 6, 0, 4, 7, 1,
//         8, 3, 0, 2, 9, 1, 6, 3, 8, 5, 5, 5, 0, 2, 2, 1, 1, 2, 1, 0, 9, 6, 8, 1,
//         0, 6, 4, 3, 8, 0, 0, 4, 8, 3, 9, 1, 9, 8, 7, 2, 0, 7, 6, 9, 1, 7, 1, 0,
//         0, 2, 0, 7, 1, 6, 2, 9, 0, 3, 1, 5, 6, 2, 4, 8, 8, 4, 5, 0, 9, 7, 0, 1,
//         0, 5, 8, 1, 2, 3, 5, 3, 7, 6, 4, 4, 4, 5, 6, 7, 7, 0, 0, 1, 3, 1, 0, 5,
//       ],
//       [9, 9, 9, 9, 7, 2, 3, 5, 5, 1, 0, 8],
//       180
//     )
// );
// 168 12
// '99999772355108' √
// '99999772355103' ×
