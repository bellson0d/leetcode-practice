const targetFun = require('./53.maximum-subarray');

const time1 = new Date().getTime();
console.log(time1);

const testStr = [
  [[-2, 1, -3, 4, -1, 2, 1, -5, 4], 6],
  [[-1, -1, -1, -1, -1, 100, -1, -1, -1, -1], 100],
  [[-1, -1, -1, -1, -1, 100, -1, -1, -1, -1, 5], 101],
  [[1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 10],
  [[0], 0],
  [[-1, 1, -1, 1, -1], 1],
  [[-1, -2, -1], -1],
  [[-1, -2, -3, -1], -1],
  [[-1, -2], -1],
  [[1, 2, 3, 4, 5, -100, 5, 4, 3, 2, 1], 15],
  [[-100, -200, -300, -40], -40],
  [[-1, 0, -2], 0],
  [[1, -1, -2], 1],
  [[3, -2, -3, -3, 1, 3, 0], 4],
];

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  const result = targetFun(input);
  if (result !== output) {
    console.log('Wrong !');
    console.log(input, output, result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
