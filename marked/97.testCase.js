const targetFun = require('./97.interleaving-string');
const IS_ARRAY_RESULT = false; // 输出是否为数组
const MULTI_ARGS = true; // 输入参数是否为多个

const testStr = [
  [['abc', '', 'abc'], true],
  [['', 'bca', 'bca'], true],
  [['', '', ''], true],
  [['', '', 'a'], false],
  [['aabcc', 'dbbca', 'aadbbcbcac'], true],
  [['aabcc', 'dbbca', 'aadbbbaccc'], false],
  [
    [
      'cabbacccabacbcaabaccacacc',
      'bccacbacabbabaaacbbbbcbbcacc',
      'cbccacabbacabbbaacbcacaaacbbacbcaabbbbacbcbcacccacacc',
    ],
    true,
  ],
  [
    [
      'abbbbbbcabbacaacccababaabcccabcacbcaabbbacccaaaaaababbbacbb',
      'ccaacabbacaccacababbbbabbcacccacccccaabaababacbbacabbbbabc',
      'cacbabbacbbbabcbaacbbaccacaacaacccabababbbababcccbabcabbaccabcccacccaabbcbcaccccaaaaabaaaaababbbbacbbabacbbacabbbbabc',
    ],
    true,
  ],
  [['a'.repeat(100), 'b'.repeat(100), 'ab'.repeat(100)], true],
  [
    [
      'bbbbbabbbbabaababaaaabbababbaaabbabbaaabaaaaababbbababbbbbabbbbababbabaabababbbaabababababbbaaababaa',
      'babaaaabbababbbabbbbaabaabbaabbbbaabaaabaababaaaabaaabbaaabaaaabaabaabbbbbbbbbbbabaaabbababbabbabaab',
      'babbbabbbaaabbababbbbababaabbabaabaaabbbbabbbaaabbbaaaaabbbbaabbaaabababbaaaaaabababbababaababbababbbababbbbaaaabaabbabbaaaaabbabbaaaabbbaabaaabaababaababbaaabbbbbabbbbaabbabaabbbbabaaabbababbabbabbab',
    ],
    false,
  ],
  [['a', 'b', 'aba'], false],
];

const time1 = new Date().getTime();
console.log(time1);

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  let result;
  if (MULTI_ARGS) {
    result = targetFun(...input);
  } else {
    result = targetFun(input);
  }

  if (
    IS_ARRAY_RESULT ? result.join('|') !== output.join('|') : result !== output
  ) {
    console.log('Number ' + count + ' Case Wrong !!!');
    console.log('Input: ', input);
    console.log('Expected output: ', output);
    console.log('Output', result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
