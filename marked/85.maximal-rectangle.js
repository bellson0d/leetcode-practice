/*
 * @lc app=leetcode id=85 lang=javascript
 *
 * [85] Maximal Rectangle
 */

// @lc code=start
/**
 * @param {character[][]} matrix
 * @return {number}
 */
var maximalRectangle = function (matrix) {
  let arr = [],
    max1 = 0;
  for (const row of matrix) {
    let tmp = [],
      count = 0;
    for (const val of row) {
      if (~~val) {
        count++;
      } else {
        count = 0;
      }
      tmp.push(count);
      if (count > max1) max1 = count;
    }
    arr.push(tmp);
  }

  let max = 0;
  const m = matrix.length;
  if (m === 0) return 0;
  const n = matrix[0].length;

  for (let k = 0; k < m; k++) {
    for (let j = 0; j < n; j++) {
      let x = 0,
        y = null;
      for (let i = k; i < m; i++) {
        if (arr[i][j] > 0) {
          x++;
          if (y === null || arr[i][j] < y) y = arr[i][j];

          const area = x * y;
          if (area > max) max = area;
        } else {
          x = 0;
          y = null;
        }
      }
    }
  }

  return Math.max(max, max1);
};
// @lc code=end
// Inspired by https://leetcode-cn.com/problems/maximal-rectangle/solution/zui-da-ju-xing-by-leetcode-solution-bjlu/
// module.exports = maximalRectangle;
// console.log(
//   maximalRectangle([
//     ['1', '0', '1', '0', '0'],
//     ['1', '0', '1', '1', '1'],
//     ['1', '1', '1', '1', '1'],
//     ['1', '0', '0', '1', '0'],
//   ])
// );

// console.log(maximalRectangle([]));
// console.log(maximalRectangle([['1', '0', '1', '1', '0']]));

// console.log(
//   maximalRectangle([
//     ['1', '0', '1', '1', '0'],
//     ['1', '0', '1', '1', '0'],
//   ])
// );

// console.log(
//   maximalRectangle([
//     ['0', '0', '1'],
//     ['1', '1', '1'],
//   ])
// );

// console.log(
//   maximalRectangle([
//     ['0', '0', '0', '0', '0', '0', '1'],
//     ['0', '0', '0', '0', '1', '1', '1'],
//     ['1', '1', '1', '1', '1', '1', '1'],
//     ['0', '0', '0', '1', '1', '1', '1'],
//   ])
// );
