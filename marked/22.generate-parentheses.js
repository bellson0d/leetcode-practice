/**
 * @param {number} n
 * @return {string[]}
 */
// WSF
var generateParenthesis = function (n) {
  if (n === 0) return [];
  if (n === 1) return ['()'];
  if (n === 2) return ['()()', '(())'];

  const fullArr = '('.repeat(n) + ')'.repeat(n);
  let tmpArr = [],
    count = 0;

  function combination(leftCount, leftStr, str) {
    let arr = [];
    for (let i = 0; i < leftStr.length; i++) {
      const ele = leftStr[i];
      const newLeftStr = leftStr.slice(0, i) + leftStr.slice(i + 1);

      if (ele === '(') {
        if (!arr.find((v) => v[1] === newLeftStr)) {
          arr.push([leftCount + 1, newLeftStr, str + ele]);
        }
      }

      if (ele === ')' && leftCount > 0) {
        if (!arr.find((v) => v[1] === newLeftStr)) {
          arr.push([leftCount - 1, newLeftStr, str + ele]);
        }
      }
    }
    return arr;
  }
  tmpArr = combination(0, fullArr, []);

  while (count < n * 2 - 1) {
    let subArr = [];
    for (let i = 0; i < tmpArr.length; i++) {
      const ele = tmpArr[i];
      subArr = subArr.concat(combination(...ele));
    }
    tmpArr = subArr;
    count++;
  }

  return tmpArr.map((v) => v[2]);
};

// console.log(generateParenthesis(5));
// console.log(generateParenthesis(3));
