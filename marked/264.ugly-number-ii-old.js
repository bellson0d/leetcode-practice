/*
 * @lc app=leetcode id=264 lang=javascript
 *
 * [264] Ugly Number II
 */

// @lc code=start
/**
 * @param {number} n
 * @return {number}
 */

var nthUglyNumber = function (n) {
    let count = 1,
        obj = { 1: 1 },
        tmpArr = [1];

    while (count < n * 3.3) {
        let newArr = [];
        for (const num of tmpArr) {
            if (!obj[num * 2]) {
                obj[num * 2] = num * 2;
                newArr.push(num * 2);
                count++;
            }
            if (!obj[num * 3]) {
                obj[num * 3] = num * 3;
                newArr.push(num * 3);
                count++;
            }
            if (!obj[num * 5]) {
                obj[num * 5] = num * 5;
                newArr.push(num * 5);
                count++;
            }
        }
        tmpArr = newArr.sort((a, b) => a-b).slice();
        newArr = [];
    }

    return Object.values(obj).sort((a, b) => a-b)[n - 1];
};
// @lc code=end

// console.log(nthUglyNumber(27)); // 64
// console.log(nthUglyNumber(1407)); // 536870912
// console.log(nthUglyNumber(1690));
// console.log(nthUglyNumber(1000));
// console.log(nthUglyNumber(354)); // 174960
// console.log(nthUglyNumber(100));
// console.log(nthUglyNumber(10));
// console.log(nthUglyNumber(11));
// console.log(nthUglyNumber(12));
// console.log(nthUglyNumber(1))
// console.log(nthUglyNumber(2))
// console.log(nthUglyNumber(3))
// console.log(nthUglyNumber(4))
// console.log(nthUglyNumber(5))
// console.log(nthUglyNumber(6))
