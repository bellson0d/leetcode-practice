/*
 * @lc app=leetcode id=304 lang=javascript
 *
 * [304] Range Sum Query 2D - Immutable
 */

// @lc code=start
/**
 * @param {number[][]} matrix
 */
var NumMatrix = function (matrix) {
  const m = matrix.length;
  const n = matrix[0].length;
  this.mArr = Array(m + 1)
    .fill(1)
    .map((v, i) => (i === 0 ? Array(n + 1).fill(0) : [0]));

  for (let i = 1; i <= m; i++) {
    for (let j = 1; j <= n; j++) {
      res =
        matrix[i - 1][j - 1] +
        this.mArr[i - 1][j] +
        this.mArr[i][j - 1] -
        this.mArr[i - 1][j - 1];
      this.mArr[i][j] = res;
    }
  }

  return this;
};

/**
 * @param {number} row1
 * @param {number} col1
 * @param {number} row2
 * @param {number} col2
 * @return {number}
 */
NumMatrix.prototype.sumRegion = function (row1, col1, row2, col2) {
  const square1 = this.mArr[row1][col1];
  const square2 = this.mArr[row2 + 1][col1];
  const square3 = this.mArr[row1][col2 + 1];
  const square4 = this.mArr[row2 + 1][col2 + 1];

  return square4 - square3 - square2 + square1;
};

/**
 * Your NumMatrix object will be instantiated and called as such:
 * var obj = new NumMatrix(matrix)
 * var param_1 = obj.sumRegion(row1,col1,row2,col2)
 */
// @lc code=end

// const numMatrix = new NumMatrix([
//   [3, 0, 1, 4, 2],
//   [5, 6, 3, 2, 1],
//   [1, 2, 0, 1, 5],
//   [4, 1, 0, 1, 7],
//   [1, 0, 3, 0, 5],
// ]);
// console.log(numMatrix.sumRegion(2, 1, 4, 3)); // return 8 (i.e sum of the red rectangle)
// console.log(numMatrix.sumRegion(1, 1, 2, 2)); // return 11 (i.e sum of the green rectangle)
// console.log(numMatrix.sumRegion(1, 2, 2, 4)); // return 12 (i.e sum of the blue rectangle)
