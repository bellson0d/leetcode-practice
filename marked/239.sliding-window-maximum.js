/*
 * @lc app=leetcode id=239 lang=javascript
 *
 * [239] Sliding Window Maximum
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */
var maxSlidingWindow = function (nums, k) {
    const len = nums.length;
    let max = nums.slice(0, k).sort((a, b) => b - a)[0],
        arr = [max],
        idx = 0;

    while (idx < len - k) {
        const newN = nums[idx + k];
        const oldN = nums[idx];
        if (newN >= max) {
            max = newN;
        } else if (oldN === max) {
            if (nums[idx + 1] !== max) {
                const newArr = nums.slice(idx + 1, idx + k + 1);
                let tmpMax = -Infinity;
                for (let i = 0; i < newArr.length; i++) {
                    const ele = newArr[i];
                    if (ele > tmpMax) tmpMax = ele;
                }
                max = tmpMax;
            }
        }

        arr.push(max);
        idx++;
    }

    return arr;
};
// @lc code=end

// console.log(maxSlidingWindow([1, 3, -1, -3, 5, 3, 6, 7], 3)); // [3,3,5,5,6,7]
// console.log(maxSlidingWindow([1], 1)); // [1]
// console.log(maxSlidingWindow([1, -1], 1)); // [1，-1]
// console.log(maxSlidingWindow([9, 11], 2)); // [11]
// console.log(maxSlidingWindow([4, -2], 2)); // [4]
// console.log(maxSlidingWindow([7, 2, 4], 2)); // [7, 4]
// console.log(maxSlidingWindow([1,3,1,2,0,5], 3)); // [3,3,2,5]
