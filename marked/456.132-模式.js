/*
 * @lc app=leetcode.cn id=456 lang=javascript
 *
 * [456] 132 模式
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {boolean}
 */
var find132pattern = function (nums) {
  const len = nums.length;
  if (len < 3) return false;

  let med = -Infinity,
    stack = [];
  for (let i = len - 1; i >= 0; i--) {
    if (nums[i] < med) return true;

    while (stack.length > 0 && stack[stack.length - 1] < nums[i]) {
      med = stack.pop();
    }

    stack.push(nums[i]);
  }

  return false;
};
// @lc code=end
module.exports = find132pattern;
// console.log(find132pattern([1, 2, 3, 4]));
// console.log(find132pattern([3, 1, 4, 2]));
// console.log(find132pattern([-1, 3, 2, 0]));
