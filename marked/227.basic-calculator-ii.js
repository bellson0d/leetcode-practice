/*
 * @lc app=leetcode id=227 lang=javascript
 *
 * [227] Basic Calculator II
 */

// @lc code=start
/**
 * @param {string} s
 * @return {number}
 */
var helper2 = (left, right, operator) => {
    switch (operator) {
        case "+":
            return left + right;
        case "-":
            return left - right;
        case "*":
            return left * right;
        default:
            return ~~(left / right);
    }
};

const reg = /[\*\/]/;

var helper = (str) => {
    let s = str.split(/([\*\/\+\-])/);
    while (s.length > 1) {
        let idx = 1;
        // 速度这么慢的原因在于，如果是常长的 *、/ 的时候，循环次数太多，
        // 如果可以合并处理就能优化
        if (reg.test(s)) {
            while (true) {
                if (s[idx] === "*" || s[idx] === "/") break;
                idx += 2;
            }
            const res = helper2(Number(s[idx - 1]), Number(s[idx + 1]), s[idx]);
            s = s
                .slice(0, idx - 1)
                .concat([res])
                .concat(s.slice(idx + 2));
        } else {
            let res = Number(s[idx-1]);
            while (idx < s.length) {
                res = helper2(res, Number(s[idx + 1]), s[idx]);
                idx += 2;
            }
            return res;
        }
    }
    return s[0];
};

var calculate = function (str) {
    let s = str
        .replace(/ /g, "")
        .split(/([\(\)])/)
        .filter(Boolean);
    while (s.includes("(")) {
        const idx = s.indexOf(")");
        let res = helper(s[idx - 1]),
            i = idx - 3;
        while (s[i] !== "(" && i >= 0) {
            res = s[i] + res;
            i--;
        }

        s = s
            .slice(0, i + 1)
            .concat([res])
            .concat(s.slice(idx + 1));
    }
    return helper(s.join(""));
};
// @lc code=end

// console.log(calculate("3+2*2")); // 7
// console.log(calculate(" 3/2 ")); // 1
// console.log(calculate(" 3+5 / 2 ")); // 5
// console.log(calculate("6*6/8+(1+(2+(3+(4+5))))")); // 19
// console.log(calculate("2*3/(7+4) + 12/5 + (1+2+2)/2")); // 4
// console.log(calculate("14/3*2")); // 8
