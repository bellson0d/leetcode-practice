/*
 * @lc app=leetcode id=10 lang=javascript
 *
 * [10] Regular Expression Matching
 */

// @lc code=start
/**
 * @param {string} s
 * @param {string} p
 * @return {boolean}
 */

function findLastIdx(arr) {
  for (let i = arr.length - 1; i >= 0; i--) {
    const ele = arr[i];
    if (!ele[1]) return i;
  }
  return -1;
}

function sliceBack(arr) {
  let idx = -1;

  // for (let i = arr.length - 1; i >= 0; i--) {
  for (let i = 0; i < arr.length; i++) {
    const ele = arr[i];
    if (ele[1] === '?') {
      idx = i;
      break;
    }
  }

  const canSlice = idx !== -1;
  return [
    canSlice,
    canSlice ? arr.slice(0, idx).concat(arr.slice(idx + 1)) : arr,
  ];
}

function isSame(arr, str) {
  for (let i = 0; i < arr.length; i++) {
    const ele = arr[i][0];
    const ele2 = str[i];

    if (ele !== ele2 && ele !== '.') return false;
  }
  return true;
}

var isMatch = function (s, p) {
  // console.log(s);
  const sLen = s.length;
  const pLen = p.length;

  let tmpArr = [];
  for (let i = 0; i < pLen; i++) {
    const ele = p[i];
    if (p[i + 1] === '*') {
      tmpArr.push(ele + '*');
      i++;
    } else {
      tmpArr.push(ele);
    }
  }
  // tmpArr Sample: ["a*", "b", "c", "d*"]
  const tmpLen = tmpArr.filter((v) => v[1] !== '*').length; // arr length without * char

  if (tmpLen > sLen) return false; // Test case: aa bbca*
  if (tmpLen === pLen && sLen !== pLen) return false; // Test case: aa a

  let extraCount = sLen - tmpLen;
  let fillNum = extraCount;
  let j = 0;
  let jVal, hasMore;
  let matchStr = [];

  for (let i = 0; i < sLen; i++) {
    const ele = s[i];
    jVal = tmpArr[j];
    if (!jVal) {
      return false; // Test case: aaab a*
    }
    hasMore = jVal[1];

    if (
      (ele !== jVal[0] && jVal[0] !== '.') ||
      !isSame(matchStr, s.slice(0, i))
    ) {
      if (!hasMore) {
        const [canSlice, newArr] = sliceBack(matchStr);
        if (canSlice && extraCount < fillNum) {
          matchStr = newArr;
          i = i - 2;
          extraCount++;
        } else {
          return false;
        }
      } else {
        if (extraCount < 0) return false;
        i--;
        j++;
      }
    } else {
      if (!hasMore) {
        matchStr.push(jVal[0] === '.' ? '.' : ele);
        if (i === sLen - 1) break;
        if (extraCount < 0) return false;
        j++;
      } else if (extraCount > 0) {
        matchStr.push((jVal[0] === '.' ? '.' : ele) + '?');
        extraCount--;
      } else {
        i--;
        j++;
      }
    }
  }

  const lastSolidNum = findLastIdx(tmpArr);
  if (lastSolidNum === -1) return true;

  return j >= lastSolidNum ? true : false;
};

console.log(isMatch('aabccbcbacabaab', '.*c*a*b.*a*ba*bb*'));

module.exports = isMatch;
// @lc code=end
