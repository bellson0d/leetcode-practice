/*
 * @lc app=leetcode id=188 lang=javascript
 *
 * [188] Best Time to Buy and Sell Stock IV
 */

// @lc code=start
/**
 * @param {number} k
 * @param {number[]} prices
 * @return {number}
 */
var maxProfit = function (k, p) {
  if (!k) return 0;
  let pre = p[0],
    prices = [pre];
  for (let i = 1; i < p.length; i++) {
    const ele = p[i];
    if (ele !== pre) {
      prices.push(ele);
      pre = ele;
    }
  }
  const len = prices.length;
  if (len < 2) return 0;

  let buySell = [];
  for (let i = 0; i < k; i++) {
    buySell[i] = [-p[0], 0];
  }

  for (let i = 1; i < len; i++) {
    if (k === 1) {
      buySell[0][0] = Math.max(buySell[0][0], -prices[i]);
      buySell[0][1] = Math.max(buySell[0][1], buySell[0][0] + prices[i]);
    } else {
      for (let j = 1; j < k; j++) {
        buySell[j - 1][0] = Math.max(buySell[j - 1][0], -prices[i]);
        buySell[j - 1][1] = Math.max(
          buySell[j - 1][1],
          buySell[j - 1][0] + prices[i]
        );
        buySell[j][0] = Math.max(buySell[j][0], buySell[j - 1][1] - prices[i]);
        buySell[j][1] = Math.max(buySell[j][1], buySell[j][0] + prices[i]);
      }
    }
  }

  return Math.max(...buySell.map((v) => v[1]));
};
// @lc code=end

// console.log(maxProfit(0, [2, 4, 1])); // 0
// console.log(maxProfit(2, [])); // 0
// console.log(maxProfit(2, [2, 4, 1])); // 2
// console.log(maxProfit(2, [3, 2, 6, 5, 0, 3])); // 7
// console.log(maxProfit(1, [1, 8, 5, 11])); // 10
// console.log(maxProfit(2, [1, 8, 5, 11])); // 13
// console.log(maxProfit(2, [1, 8, 8, 100])); // 99
// console.log(maxProfit(1, [1, 8, 8, 100])); // 99
// console.log(maxProfit(1, [1, 8, 8, 8, 8])); // 7
// console.log(maxProfit(1, [1, 8, 7, 8])); // 7
// console.log(maxProfit(2, [1, 8, 7, 8])); // 8
// console.log(maxProfit(20, [9, 8, 7, 5, 4])); // 0
// console.log(maxProfit(20, [1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3])); // 8
