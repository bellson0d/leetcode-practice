create table demo (
    Id INTEGER PRIMARY key,
    Client_Id INTEGER,
    Driver_Id INTEGER,
    City_Id INTEGER,
    Status VARCHAR(30),
    Request_at DATE
)
  