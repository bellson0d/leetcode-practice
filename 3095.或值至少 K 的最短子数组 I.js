
/**
 * @param {number[]} nums
 * @param {number} k 
 * @return {number}
 */
var minimumSubarrayLength = function (nums, k) {
    let current = nums[0], idx1 = 0, idx2 = 1, res = Infinity;
    if (current >= k) return 1;
    while (idx1 < nums.length && idx2 < nums.length) {
        current = current | nums[idx2]
        // current = nums.slice(idx1, idx2 + 1).reduce((a, b) => a | b);
        if (current >= k) {
            res = Math.min(res, (idx2 - idx1) + 1);
            if (res === 1) return res;
            idx1++;
            current = current ^ nums[idx1]
        } else {
            idx2++;
            
        }
    }
    if (res !== Infinity) return res;
    return -1;
};
const log = console.log;

log(minimumSubarrayLength([2, 1, 8], 10)); //3
log(minimumSubarrayLength([1, 2, 3], 2)); //1
log(minimumSubarrayLength([16, 1, 2, 20, 32], 45)); //2
log(minimumSubarrayLength([1, 2], 0)); //1
// @lc code=end
