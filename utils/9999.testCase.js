const targetFun = require('./9999.file-name');
const IS_ARRAY_RESULT = false; // 输出是否为数组
const MULTI_ARGS = false; // 输入参数是否为多个
const RANDOM_IDX = true; // 结果数组是否可以乱序

const testStr = [
  [[3, 2], 3],
  [[7, 3], 28],
  [[8, 3], 36],
  [[9, 4], 165],
  [[11, 6], 3003],
];

const time1 = new Date().getTime();
console.log(time1);

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];
  let result;
  if (MULTI_ARGS) {
    result = targetFun(...input);
  } else {
    result = targetFun(input);
  }

  let res = false;
  if (!IS_ARRAY_RESULT) {
    res = result === output;
  } else if (!RANDOM_IDX) {
    res = result.join('|') === output.join('|');
  } else {
    if (result.length === output.length) {
      res = !result.find((v) => !output.includes(v));
    }
  }

  if (!res) {
    console.log('Number ' + count + ' Case Wrong !!!');
    console.log('Input: ', input);
    console.log('Expected output: ', output);
    console.log('Output', result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
