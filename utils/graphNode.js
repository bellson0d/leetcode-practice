function GraphNode(val, arr) {
  if (val === undefined) throw Error('No initial value!');
  this.val = val;
  this.neighbor = arr || [];
}

module.exports = {
  GraphNode,
};
