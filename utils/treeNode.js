// 使用数组生成树数据结构方便测试
const getTree = (arr) => {
  if (!arr || !arr.length) {
    return null;
  }
  let index = 0;
  const queue = [];
  const len = arr.length;
  const head = new TreeNode(arr[index]);
  queue.push(head);

  while (index < len) {
    index++;
    const parent = queue.shift();
    if (arr[index] !== null && arr[index] !== undefined) {
      const node = new TreeNode(arr[index]);
      parent.left = node;
      queue.push(node);
    }

    index++;
    if (arr[index] !== null && arr[index] !== undefined) {
      const node = new TreeNode(arr[index]);
      parent.right = node;
      queue.push(node);
    }
  }
  return head;
};

// 树变为数组，省略 null
const tree2Arr = (root) => {
  let res = [];
  if (!root) return res;

  let arr = [root];
  while (arr.length) {
    const node = arr.shift();
    if (!node) {
      res.push(null);
      continue;
    }
    res.push(node.val);

    if (!node.left && !node.right) continue;
    if (node.left) {
      arr.push(node.left);
    } else {
      arr.push(null);
    }

    if (node.right) {
      arr.push(node.right);
    } else {
      arr.push(null);
    }
  }

  return res;
};

function TreeNode(val, left, right) {
  this.val = val === undefined ? 0 : val;
  this.left = left === undefined ? null : left;
  this.right = right === undefined ? null : right;
}

// 树变为数组，传统形式不省略 null，包含树的高度
const tree2ArrLegacyWithLevel = (tree) => {
  if (!tree) return [];
  let res = [],
    maxLevel = 1,
    count = 0;
  const arr = [[tree, 1]];

  while (arr.length) {
    let [node, level] = arr.pop();
    maxLevel = Math.max(maxLevel, level);
    level++;
    if (node !== '*') {
      res.push(node.val);
      if (node.left) {
        arr.unshift([node.left, level]);
      } else {
        arr.unshift(['*', level]);
      }
      if (node.right) {
        arr.unshift([node.right, level]);
      } else {
        arr.unshift(['*', level]);
      }
    } else {
      res.push('*');
      arr.unshift(['*', level]);
      arr.unshift(['*', level]);
    }

    count++;

    if (arr.find((v) => v[0] !== '*')) {
      continue;
    } else {
      break;
    }
  }

  return [res, maxLevel];
};

// 树变为数组，传统形式不省略 null
const tree2ArrLegacy = (tree) => {
  if (!tree) return [];
  let res = [],
    count = 0;
  const arr = [tree];

  while (arr.length) {
    let node = arr.pop();
    if (node) {
      res.push(node.val);
      if (node.left) {
        arr.unshift(node.left);
      } else {
        arr.unshift(null);
      }
      if (node.right) {
        arr.unshift(node.right);
      } else {
        arr.unshift(null);
      }
    } else {
      res.push(null);
      arr.unshift(null);
      arr.unshift(null);
    }

    count++;

    if (arr.find((v) => v)) {
      continue;
    } else {
      break;
    }
  }

  return res;
};

// 打印树结构
const logTree = (tree) => {
  let [treeArr, maxLevel] = tree2ArrLegacyWithLevel(tree);

  const splitLine = '='.repeat(Math.min(2 ** (maxLevel - 1), 64));
  console.log(splitLine + ' Start ' + splitLine);
  let level = 1,
    idx = 0,
    gap = 2 ** (maxLevel - 2) + 1,
    gapCount = maxLevel - 3;

  while (level <= maxLevel) {
    const levelDiff = maxLevel - level;
    const edgeGap = 2 ** (levelDiff - 1);
    const edgeGapStr = ' '.repeat(edgeGap);
    const underlineLen = levelDiff > 1 ? 2 ** (levelDiff - 1) - 1 : 0;
    const underline = '_'.repeat(underlineLen);
    const noUnderline = ' '.repeat(underlineLen);
    const centerOffset = ' '.repeat(4);

    let count = 2 ** (level - 1),
      str = centerOffset + edgeGapStr;

    while (count > 0) {
      const val = treeArr[idx];
      if ((!val || val === '*') && val !== 0) {
        str += noUnderline + ' ' + noUnderline;
      } else {
        const leftChild = treeArr[idx * 2 + 1];
        if ((leftChild || leftChild === 0) && leftChild !== '*') {
          str += underline;
        } else {
          str += noUnderline;
        }

        str += String(val);

        const rightChild = treeArr[idx * 2 + 2];
        if ((rightChild || rightChild === 0) && rightChild !== '*') {
          str += underline;
        } else {
          str += noUnderline;
        }
      }

      if (idx > 0 && count > 1) {
        str += ' '.repeat(gap > 2 ? gap : 1);
      }

      idx++;
      count--;
    }

    if (level > 1) {
      gap = gapCount < 0 ? 1 : gap - 2 ** gapCount;
      gapCount--;
    }
    str += edgeGapStr;
    console.log(str);
    level++;
  }

  return console.log(splitLine + '  End  ' + splitLine);
};

// 把树结构在控制台打印出来，同时将传入函数应用后的结果也打印，作比较
const logTreeWithFn = (tree, fn, ...args) => {
  logTree(tree);

  console.log('Args: ', args);

  logTree(fn(tree, ...args));

  console.log('*'.repeat(80));
};

module.exports = {
  getTree,
  tree2Arr,
  logTree,
  logTreeWithFn,
  tree2ArrLegacyWithLevel,
  tree2ArrLegacy,
};
