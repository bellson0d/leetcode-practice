// 使用数组生成链表数据方便测试

const getList = (arr, { isCycle, cycleIdx } = {}) => {
    const len = arr.length;
    if (len === 0) return null;
    if (len === 1) return new ListNode(arr[0]);

    let head = new ListNode(arr[0]),
        current = head,
        count = 1,
        tmpNode = null;

    while (count < len) {
        const next = new ListNode(arr[count]);
        if (isCycle && Number.isInteger(cycleIdx)) {
            if (count === cycleIdx) {
                tmpNode = next;
            }
        }
        current.next = next;
        current = next;
        count++;
    }
    if (isCycle) current.next = tmpNode;

    return head;
};

const listToArr = (head) => {
    let res = [],
        currentNode = head;

    while (currentNode) {
        res.push(currentNode.val);
        currentNode = currentNode.next;
    }

    return res;
};

const isSameList = (head1, head2, showLog = false) => {
    let node1 = head1,
        node2 = head2;

    while (node1 || node2) {
        if (showLog)
            console.log(
                "List-1: ",
                node1 ? node1.val : node1,
                "   List-2: ",
                node2 ? node2.val : node2
            );
        if (!node1 || !node2 || node1.val !== node2.val) return false;
        node1 = node1.next;
        node2 = node2.next;
    }
    console.log("--------- SAME! ---------");
    return true;
};

const logList = (head) => {
    let node = head;
    console.log("--------- START ---------");
    while (node) {
        console.log(node.val);
        node = node.next;
    }
    console.log("--------- END ---------");
};

function ListNode(val, next) {
    this.val = val === undefined ? 0 : val;
    this.next = next === undefined ? null : next;
}

module.exports = {
    getList,
    isSameList,
    logList,
    listToArr,
};
