const targetFun = require('./archive/8888.file-name-list');
const { getList, isSameList, logList } = require('./listNode');
const MULTI_ARGS = false; // 输入参数是否为多个

const testStr = [
  [[getList([1, 4, 3, 2, 5, 2]), 3], getList([1, 2, 2, 4, 3, 5])],
];

const time1 = new Date().getTime();
console.log(time1);

let count = 0;
while (count < testStr.length) {
  const [input, output] = testStr[count];

  let result;
  if (MULTI_ARGS) {
    result = targetFun(...input);
  } else {
    result = targetFun(input);
  }

  if (!isSameList(result, output, true)) {
    console.log('Number ' + count + ' Case Wrong !!!');
    console.log('Input: ', input);
    console.log('Expected output: ', output);
    console.log('Output', result);
    return;
  }
  count++;
}
console.log('All pass!');

const time2 = new Date().getTime();
console.log('Duration: ', time2 - time1);
