function ListNode(val, next) {
  this.val = val === undefined ? 0 : val;
  this.next = next === undefined ? null : next;
}

function DoubleListNode(val, pre, next) {
  this.val = val === undefined ? 0 : val;
  this.pre = pre === undefined ? null : pre;
  this.next = next === undefined ? null : next;
}

module.exports = {
  ListNode,
  DoubleListNode,
};
