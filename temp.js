var removeDuplicates = function (nums) {
    let idx = 0, count = 0, preCount = 0;
    while (count < nums.length) {
        if (nums[count] !== nums[count + 1]) {
            nums[idx] = nums[count];
            idx++;
            preCount = 0;
        } else {
            if (preCount < 1) {
                nums[idx] = nums[count];
                preCount++;
                idx++;
            } 
        }
        count++;
    }
    // console.log(nums)
    return idx;
};

console.log(removeDuplicates([0,0,1,1,1,1,2,3,3]))
console.log(removeDuplicates([0,0,1,1,1,1,2,3,3]))
console.log(removeDuplicates([1, 1, 1, 1, 1, 1, 1, 1, 1]))