/*
 * @lc app=leetcode id=315 lang=javascript
 *
 * [315] Count of Smaller Numbers After Self
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number[]}
 */
var countSmaller = function (nums) {
  nums = nums.map((v, i) => [v, i]).sort((a, b) => b[0] - a[0]);
  const len = nums.length,
    res = [];
  for (let i = 0; i < len - 1; i++) {
    const ele = nums[i];
    let count = 0;
    for (let j = i + 1; j < len; j++) {
      const ele2 = nums[j];
      if (ele2 < ele) count++;
    }
    res.push(count);
  }
  res.push(0);
  return res;
};
// @lc code=end

// console.log(countSmaller([5, 2, 6, 1]));
// console.log(countSmaller([-1]));
// console.log(countSmaller([-1, -1]));
