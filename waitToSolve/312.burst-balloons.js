/*
 * @lc app=leetcode id=312 lang=javascript
 *
 * [312] Burst Balloons
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var maxCoins = function (nums) {
  let res = 0;
  while (nums.length) {
    let max = 0,
      idx = -1;
    for (let i = 0; i < nums.length; i++) {
      const tmp = (nums[i - 1] || 1) * nums[i] * (nums[i + 1] || 1);
      if (tmp > max) {
        max = tmp;
        idx = i;
      }
    }
    res += max;
    nums = nums.slice(0, idx).concat(nums.slice(idx + 1));
  }
  return res;
};
// @lc code=end

console.log(maxCoins([3, 1, 5, 8])); // 167
// console.log(maxCoins([1, 5])); // 10
