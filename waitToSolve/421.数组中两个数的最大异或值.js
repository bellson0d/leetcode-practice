/*
 * @lc app=leetcode.cn id=421 lang=javascript
 *
 * [421] 数组中两个数的最大异或值
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var findMaximumXOR = function (nums) {
  const len = nums.length;
  let obj = {};
  let max = 0;
  for (let i = 0; i < len - 1; i++) {
    for (let j = i + 1; j < len; j++) {
      let res = 0;
      if (obj[nums[i] + '-' + nums[j]] || obj[nums[j] + '-' + nums[i]]) {
        res = obj[nums[i] + '-' + nums[j]];
      } else {
        res = nums[i] ^ nums[j];
        obj[nums[i] + '-' + nums[j]] = res;
        obj[nums[j] + '-' + nums[i]] = res;
      }
      max = Math.max(max, res);
    }
  }
  return max;
};
// @lc code=end

// 好奇官方的 O(n) 结果是 O(n logC) C < 2**31
// 所以简单暴力一下

// console.log(findMaximumXOR([3, 10, 5, 25, 2, 8]));
// console.log(findMaximumXOR([0]));
// console.log(findMaximumXOR([1]));
// console.log(findMaximumXOR([2, 4]));
// console.log(findMaximumXOR([8, 10, 2]));
// console.log(findMaximumXOR([14, 70, 53, 83, 49, 91, 36, 80, 92, 51, 66, 70]));
