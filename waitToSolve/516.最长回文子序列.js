/*
 * @lc app=leetcode.cn id=516 lang=javascript
 *
 * [516] 最长回文子序列
 */

// @lc code=start
/**
 * @param {string} s
 * @return {number}
 */
var longestPalindromeSubseq = function (s) {};
// @lc code=end

// Hint:
// const arr = [
//      c b b b
//   c [1,1,2,3]
//   b [0,1,2,3]
//   b [0,0,1,2]
//   b [0,0,0,1]
// ]
