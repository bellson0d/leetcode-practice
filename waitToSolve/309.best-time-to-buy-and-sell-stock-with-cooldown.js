/*
 * @lc app=leetcode id=309 lang=javascript
 *
 * [309] Best Time to Buy and Sell Stock with Cooldown
 */

// @lc code=start
/**
 * @param {number[]} prices
 * @return {number}
 */
var maxProfit = function (prices) {};
// @lc code=end

console.log(maxProfit([1, 2, 3, 10, 2, 15, 1, 2, 20])); // 33
console.log(maxProfit([1, 2, 3, 10, 7, 15, 1, 8, 20])); // 28
console.log(maxProfit([1, 2, 3, 0, 2])); // 3
console.log(maxProfit([1])); // 0
console.log(maxProfit([3, 2, 1])); // 0
console.log(maxProfit([3, 2, 1, 2, 3])); // 2
