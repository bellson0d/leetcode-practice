/*
 * @lc app=leetcode.cn id=494 lang=javascript
 *
 * [494] 目标和
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var findTargetSumWays = function (nums, target) {
  let count = [],
    zero = 0;
  const len = nums.length;

  nums = nums.filter((v) => !!v);
  zero = len - nums.length;

  let sum = nums.reduce((a, b) => Math.abs(a) + Math.abs(b));

  const helper = (pre, arr, sum) => {
    if (!arr.length) {
      if (pre === target) count++;
      return;
    }
    const current = arr[0];

    const pre1 = pre + current;
    const pre2 = pre - current;
    const newArr = arr.slice(1);

    if (pre1 <= sum - current) {
      helper(pre1, key1, newArr);
    }
    if (pre2 <= sum - current) {
      helper(pre1, key1, newArr);
    }
    helper(pre2, key2, newArr);
  };
  helper(0, '', nums);

  return count * 2 ** zero;
};
// @lc code=end

// console.log(findTargetSumWays([1, 1, 1, 1, 1], 3));
// console.log(findTargetSumWays([1], 1));
// console.log(findTargetSumWays([1, 0], 1));
console.log(
  findTargetSumWays(
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    1
  )
); // 524288
findTargetSumWays(
  [29, 6, 7, 36, 30, 28, 35, 48, 20, 44, 40, 2, 31, 25, 6, 41, 33, 4, 35, 38],
  35
); // 0
