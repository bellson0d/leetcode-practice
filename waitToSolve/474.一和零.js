/*
 * @lc app=leetcode.cn id=474 lang=javascript
 *
 * [474] 一和零
 */

// @lc code=start
/**
 * @param {string[]} strs
 * @param {number} m
 * @param {number} n
 * @return {number}
 */
var count = (str) => {
  let arr = [0, 0];
  for (const s of str) {
    if (s === '1') {
      arr[1]++;
    } else {
      arr[0]++;
    }
  }
  return arr;
};

var findMaxForm = function (strs, zero, one) {
  let max = 0,
    obj = {};
  const originArr = strs
    .map(count)
    .filter((v) => v[0] <= zero && v[1] <= one)
    .sort((a, b) => a[0] - b[0]);
  const [allZ, arrO] = originArr.reduce(
    (a, b) => [a[0] + b[0], a[1] + b[1]],
    [0, 0]
  );
  if (allZ <= zero && arrO <= one) return strs.length;
  if (allZ <= zero) {
    let restOne = one;
    const arr = originArr.slice().sort((a, b) => a[1] - b[1]);
    for (let i = 0; i < arr.length; i++) {
      const [z, o] = arr[i];
      if (restOne >= o) {
        restOne -= o;
      } else {
        return i;
      }
    }
    return strs.length;
  }
  if (arrO <= one) {
    let restZero = zero;
    const arr = originArr.slice().sort((a, b) => a[0] - b[0]);
    for (let i = 0; i < arr.length; i++) {
      const [z, o] = arr[i];
      if (restZero >= z) {
        restZero -= z;
      } else {
        return i;
      }
    }
    return strs.length;
  }

  let tmp = [[originArr, zero, one, 0]];

  while (tmp.length) {
    const [arr, z, o, len] = tmp.pop();
    const key = arr.join('-');
    if (obj[key]) {
      continue;
    } else {
      obj[key] = true;
    }
    if (len > max) max = len;

    for (let i = 0; i < arr.length; i++) {
      if (arr[i][0] <= z && arr[i][1] <= o) {
        tmp.push([
          arr.slice(0, i).concat(arr.slice(i + 1)),
          z - arr[i][0],
          o - arr[i][1],
          len + 1,
        ]);
      }
    }
  }
  return max;
};
// @lc code=end

// console.log(findMaxForm(['10', '0001', '111001', '1', '0'], 5, 3));
// console.log(findMaxForm(['10', '0', '1'], 1, 1));
// console.log(findMaxForm(['10', '0', '1'], 2, 1));
// console.log(findMaxForm(['10', '0', '1'], 2, 2));
// console.log(findMaxForm(['1100', '110', '11'], 1, 10));
// console.log(findMaxForm([], 10, 10));
// console.log(findMaxForm(['1100', '1100', '1100', '1100'], 1, 1));
// console.log(findMaxForm(['11111', '100', '1101', '1101', '11000'], 5, 7));
console.log(
  findMaxForm(
    [
      '0',
      '11',
      '1000',
      '01',
      '0',
      '101',
      '1',
      '1',
      '1',
      '0',
      '0',
      '0',
      '0',
      '1',
      '0',
      '0110101',
      '0',
      '11',
      '01',
      '00',
      '01111',
      '0011',
      '1',
      '1000',
      '0',
      '11101',
      '1',
      '0',
      '10',
      '0111',
    ],
    29,
    33
  )
); // 17
// console.log(
//   findMaxForm(
//     [
//       '0',
//       '11',
//       '1000',
//       '01',
//       '0',
//       '101',
//       '1',
//       '1',
//       '1',
//       '0',
//       '0',
//       '0',
//       '0',
//       '1',
//       '0',
//       '0110101',
//       '0',
//       '11',
//       '01',
//       '00',
//       '01111',
//       '0011',
//       '1',
//       '1000',
//       '0',
//       '11101',
//       '1',
//       '0',
//       '10',
//       '0111',
//     ],
//     30,
//     80
//   )
// ); // 17
