/*
 * @lc app=leetcode id=416 lang=javascript
 *
 * [416] Partition Equal Subset Sum
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {boolean}
 */
var canPartition = function (nums) {
  const len = nums.length;
  if (len === 1) return false;
  if (len === 2) return nums[0] === nums[1];

  nums.sort((a, b) => a - b);
  let sum1 = nums.reduce((a, b) => a + b, 0),
    sum2 = 0;
};
// @lc code=end

console.log(canPartition([1, 5, 11, 5]));
console.log(canPartition([1, 2, 3, 5]));
