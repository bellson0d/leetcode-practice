/*
 * @lc app=leetcode id=390 lang=javascript
 *
 * [390] Elimination Game
 */

// @lc code=start
/**
 * @param {number} n
 * @return {number}
 */
var lastRemaining = function (n) {
  let arr = Array(n)
    .fill(0)
    .map((v, i) => i + 1);
  let flag = true,
    idx = 0,
    min = 0,
    max = n - 1,
    turn = 1;
  while (arr[idx + turn ** 2] || arr[idx - turn ** 2]) {
    arr[idx] = null;
    if (flag) {
      if (idx + turn ** 2 > max) {
        flag = !flag;
        if (idx + 1 === max) {
          max = idx;
          idx += 1;
        } else {
          max = idx - 1;
        }
      } else {
        idx += 2;
      }
    } else {
      if (idx - turn ** 2 < min) {
        flag = !flag;
        if (idx + 1 === max) idx += 1;
      } else {
        idx -= turn ** 2;
      }
    }
  }
  return arr[idx];
};
// @lc code=end

console.log(
  Array(1000)
    .fill(0)
    .map((v, i) => lastRemaining(i + 1))
);
// console.log(lastRemaining(10 ** 5));
// console.log(lastRemaining(10 ** 6));
// console.log(lastRemaining(10 ** 7));
// console.log(lastRemaining(10 ** 8));
