/*
 * @lc app=leetcode id=421 lang=javascript
 *
 * [421] Maximum XOR of Two Numbers in an Array
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {number}
 */
var findMaximumXOR = function (nums) {
  const len = nums.length;
  if (len === 1) return 0;
  if (len === 2) return Math.max(nums[0], nums[0] ^ nums[1]);

  let max2 = -Infinity;
  for (let i = 0; i < len; i++) {
    const ele = nums[i];
    if (ele > max2) max2 = ele;
  }
  const maxLen = max2.toString(2).length;
  const maxNum = 2 ** maxLen - 1;
  const nums2 = nums
    .map((n, i) => [n.toString(2), i])
    .filter((v) => v[0].length === maxLen);

  let max = -Infinity;
  for (let i = 0; i < nums2.length; i++) {
    const idx = nums2[i][1];
    const ele = nums[idx];
    for (let j = 0; j < len; j++) {
      if (i !== j) {
        const res = ele ^ nums[j];
        if (res === maxNum) return res;
        if (res > max) max = res;
      }
    }
  }
  return Math.max(max2, max);
};
// @lc code=end

// console.log(findMaximumXOR([3, 10, 5, 25, 2, 8]));
// console.log(findMaximumXOR([0]));
// console.log(findMaximumXOR([2, 4]));
// console.log(findMaximumXOR([8, 10, 2]));
// console.log(findMaximumXOR([14, 70, 53, 83, 49, 91, 36, 80, 92, 51, 66, 70]));
