/*
 * @lc app=leetcode.cn id=1018 lang=javascript
 *
 * [1018] 可被 5 整除的二进制前缀
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @return {boolean[]}
 */
var prefixesDivBy5 = function (nums) {
  const res = [];
  for (let i = 1; i <= nums.length; i++) {
    let arr = nums.slice(0, i);
    const idx = arr.indexOf(1);
    if (idx === -1) {
      res.push(true);
    } else {
      if (arr.length < 10) {
        res.push(parseInt(arr.join(''), 2) % 5 === 0);
      } else {
        const tail = arr.slice(arr.length - 3);
        res.push(tail[2] === 0 || tail.join('') === '101');
      }
    }
  }
  return res;
};
// @lc code=end
// console.log(prefixesDivBy5([0, 1, 1, 1, 1, 1]));
// console.log(
//   prefixesDivBy5([
//     1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0,
//     0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1,
//     0, 0, 1, 1, 0, 0, 1, 1, 1,
//   ])
// );

// [false,false,true,true,true,true,true,true,true,true,true,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,true,true,false,false,false,true,true,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,true,true,true,false,false,false]
